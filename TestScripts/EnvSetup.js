//Setup Env Properties
BatBallBetsLib.EnvConfigs.ENV_DEFAULT_CONFIG_GROUP = BatBallBetsLib.EnvConfigConstants.ENV_GROUP_AUCHEM;
BatBallBetsLib.EnvConfigs.ENV_NAME = BatBallBetsLib.EnvConfigConstants.ENV_NAME_TEST;
BatBallBetsLib.EnvConfigs.ENV_ADDITIONAL_ADMIN_EMAIL_LIST = [];
BatBallBetsLib.EnvConfigs.ENV_SEND_EMAILS_FROM_NON_PROD = false;

BatBallBetsLib.EnvConfigs.ENV_CURRENT_LOG_LEVEL = BatBallBetsLib.EnvConfigConstants.ENV_LOG_LEVEL_INFO;
BatBallBetsLib.EnvConfigs.ENV_GET_PROD_FORM_RESPONSES = true;
BatBallBetsLib.EnvConfigs.ENV_TEMP_HALT_TRIGGER_EXECUTIONS = false;
BatBallBetsLib.EnvConfigs.ENV_UPDATE_MATCH_RESULTS_MAX_PER_MINUTE = 2;
BatBallBetsLib.EnvConfigs.MATCH_START_TIME.AddMinutesToCloseDailyBets = -31;

/**
 * 
 */
function testFunction(e) {
  BatBallBetsLib.sendTestEmail()
}

/**
 * OnOpen
 */
function onOpen() {
  BatBallBetsLib.setupEnv();
}

/**
 * (Re)Creates all triggers 
 */
function createTriggers() {
  BatBallBetsLib.deleteAllTriggers();

  BatBallBetsLib.createDailyMainTrigger(0, 30);
  BatBallBetsLib.createDailyUpdatesTrigger(12, 00);//Every day at 12:00 pm
  BatBallBetsLib.createOnFormSubmitTrigger();
}

/**
 * Deletes all triggers
 */
function deleteAllTriggers() {
  BatBallBetsLib.deleteAllTriggers();
}

/**
 * 
 */
function promoteSheetsAll() {
  BatBallBetsLib.promoteSheetsAll();
}

/**
 * 
 */
function promoteAllForms() {
  BatBallBetsLib.promoteAllForms();
}
