/**
 * showOverallPredictionsSheetColumnsUI is a UI-facing function that is responsible for triggering the 
 * showOverallPredictionsSheetColumns_ function to show all columns in the overall predictions sheet.
 * 
 * @param {Event} e - the event object that triggers this function.
 */
function showSeriesBetsSheetColumnsUI(e) {
  showSeriesBetsSheetColumns_();
}

/**
 * hideOverallPredictionsSheetColumnsUI is a UI-facing function that is responsible for triggering the 
 * hideOverallPredictionsSheetColumns_ function to hide all columns in the overall predictions sheet.
 * 
 * @param {Event} e - the event object that triggers this function.
 */
function hideSeriesBetsSheetColumnsUI(e) {
  hideSeriesBetsSheetColumns_();
}

/**
 * resetOverallPredictionsSheetUI is a UI-facing function that is responsible for triggering the 
 * resetOverallPredictionsSheet_ function to reset the overall predictions sheet.
 * 
 * @param {Event} e - the event object that triggers this function.
 */
function resetSeriesBetsSheetUI(e) {
  resetSeriesBetsSheets_();
}

/**
 * showOverallPredictionsSheetColumns_ is the main function that is responsible for showing all columns 
 * in the overall predictions sheet.
 * 
 * 1. It gets the overall predictions sheet using the getOverallPredictionsSheet_ function.
 * 2. The activateSheet_ function is called with the overall predictions sheet as an argument to activate it.
 * 3. The showColumns method of the sheet object is called to show all columns (first argument is 1 and the second argument 
 *    is the result of calling the getLastColumn method on the sheet object).
 * 4. There is a commented-out line of code (//enableCopy_();) that may or may not be relevant.
 * 5. The autoResizeColumns_ function is called with the sheet object as an argument.
 * 
 * @param {Event} e - the event object that triggers this function.
 */
function showSeriesBetsSheetColumns_(e) {
  let sheet = getSeriesBetsSheet_();
  activateSheet_(sheet);

  sheet.showColumns(1, sheet.getLastColumn());   // Show all columns in the sheet 
  //enableCopy_();
  autoResizeColumns_(sheet);
}

/**
 * Hides columns in the overall predictions sheet based on the range specified in `overallPredictionsSheetObj.hideColRange`.
 * @param {Event} e - The event that triggers the function.
 */
function hideSeriesBetsSheetColumns_(e) {
  const sheet = getSeriesBetsSheet_();
  activateSheet_(sheet);

  const startColNum = overallPredictionsSheetObj.hideColRange[0];
  hideColumnsAfter_(sheet, startColNum);

  const backupSheet = getSeriesBetsBkpSheet_();
  hideColumnsAfter_(backupSheet, startColNum);

  //disableSpreadsheetCopy_(e);
  flushAndWait_();
}

/**
 * Resets the overall predictions sheet by deleting excess empty rows and hiding certain columns.
 * @param {Event} e - The event that triggers the function.
 */
function resetSeriesBetsSheets_(e) {
  console.time(resetSeriesBetsSheets_.name);

  const sheet = getSeriesBetsSheet_();
  const backupSheet = getSeriesBetsBkpSheet_();

  const isProtected = isSheetProtected_(sheet);
  if (isProtected) {
    removeSheetProtection_(sheet);
  }

  activateSheet_(sheet);
  deleteSeriesBetsExcessEmptyRows_();
  hideSeriesBetsSheetColumns_(e);

  syncSheetColumns_(sheet, backupSheet);

  autoResizeColumns_(sheet);
  autoResizeColumns_(backupSheet);

  if (isProtected) {
    addSheetProtection_(sheet);
  }

  console.timeEnd(resetSeriesBetsSheets_.name);
}


/**
 * Deletes the excess empty rows in the overall predictions and backup sheets.
 * The number of empty rows to keep is determined by the `emptyRowsToKeep` property of the `overallPredictionsSheetObj` object.
 */
function deleteSeriesBetsExcessEmptyRows_() {
  deleteExcessEmptyRows_(getSeriesBetsSheet_(), overallPredictionsSheetObj.emptyRowsToKeep);
  deleteExcessEmptyRows_(getSeriesBetsBkpSheet_(), overallPredictionsSheetObj.emptyRowsToKeep);
}

