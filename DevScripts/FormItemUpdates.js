//UPDATE FORM ITEMS//
/**
 * Updates a form item based on its type.
 * 
 * @param {string} item - The name of the form item to update.
 * @param {string} itemType - The type of the form item to update.
 * @param {string} helpText - The help text for the form item.
 * @param {Array} values - The values for the form item (if applicable).
 * @param {boolean} required - Whether the form item is required or not.
 * @returns {void}
 */
function updateFormItem_(item, itemType, helpText, values, required) {
  switch (itemType) {
    case FormItemTypes.LIST:
      updateListItem_(item, helpText, values, required);
      break;
    case FormItemTypes.SECTION_HEADER:
      updateSectionHeaderItem_(item, helpText);
      break;
    case FormItemTypes.CHECKBOX:
      updateCheckboxItem_(item, helpText, values, required);
      break;
    case FormItemTypes.MULTIPLE_CHOICE:
      updateMultipleChoiceItem_(item, helpText, values, required);
      break;
    case FormItemTypes.TEXT:
      updateTextItem_(item, helpText, required);
      break;
    case FormItemTypes.TEXT_WHOLE_NUMBER:
      updateTextItem_(item, helpText, required);
      break;
    case FormItemTypes.SCALE:
      updateScaleItem_(item, helpText, required);
      break;
    default:
      throw new Error(`Unknown item type = "${itemType}", helpText = "${helpText}", values = "${values}", required = "${required}"`);
  }
}

/**
 * Updates a section header form item.
 * 
 * @param {Item} item - The section header form item to update.
 * @param {string} helpText - The help text for the form item.
 * @returns {void}
 */
function updateSectionHeaderItem_(item, helpText) {
  item.setHelpText(helpText);
}

/**
 * Updates a scale form item.
 * 
 * @param {Item} item - The scale form item to update.
 * @param {string} helpText - The help text for the form item.
 * @param {boolean} required - Whether the form item is required or not.
 * @returns {void}
 */
function updateScaleItem_(item, helpText, required) {
  item.asScaleItem().setRequired(required);
  item.setHelpText(helpText);
}

/**
 * Updates a text form item.
 * 
 * @param {Item} item - The text form item to update.
 * @param {string} helpText - The help text for the form item.
 * @param {boolean} required - Whether the form item is required or not.
 * @returns {void}
 */
function updateTextItem_(item, helpText, required) {
  item = item.asTextItem();
  item.setRequired(required);
  item.setHelpText(helpText);
}

/**
 * Update checkboc in the form
 */
function updateCheckboxItem_(item, helpText, values, required) {
  item.asCheckboxItem().setChoiceValues(values).setRequired(required);
  item.setHelpText(helpText);
}

/**
 * Update multiple choice items in the form
 */
function updateMultipleChoiceItem_(item, helpText, values, required) {
  item.asMultipleChoiceItem().setChoiceValues(values).setRequired(required);
  item.setHelpText(helpText);
}

/**
 * Update dropdown in the form
 */
function updateListItem_(item, helpText, values, required) {
  item.asListItem().setChoiceValues(values).setRequired(required);
  item.setHelpText(helpText);
}

/**
 * Update dropdown in the form
 */
function updateMultipleListItems_(form, questionaArr, values, required) {
  if (Array.isArray(questionaArr) && isNotEmpty_(values)) {
    for (let i = 0; i < questionaArr.length; i++) {
      let question = questionaArr[i];
      let item = getFormItem_(form, question);
      if (item) {
        item.asListItem().setChoiceValues(values).setRequired(required);
      }
    }
  }
}
