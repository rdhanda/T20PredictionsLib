//Reminders.gs//
function sendReminders(e) {
  sendReminders_(e);
}

/**
 * Sends reminders to active punters with unsubmitted predictions for the first open match in the schedule.
 * 
 * @param {Object} e - The event object.
 */
function sendReminders_(e) {
  if (!ConfigManager.getConfigClass().REMINDERS_ENABLED) {
    return;
  }
  console.time(sendReminders_.name);

  const firstMatchID = ScheduleUtils.getFirstOpenMatchID();
  const matchDate = ScheduleUtils.getFirstOpenMatchShortDate();
  const startTime = ScheduleUtils.getFirstOpenMatchStartTimeMST();

  if (isEmpty_(firstMatchID) || !ScheduleUtils.isReminderNotSent(matchDate) || !isItTimeToSendReminders_(startTime)) { return; }

  const puntersNeedingReminders = PuntersUtils.getPuntersRequiringReminder_();
  if (isEmpty_(puntersNeedingReminders)) {
    doAfterReminders_(matchDate);
    return;
  }

  const puntersWhoSubmittedEntries = getPuntersWithSubmittedEntries_(firstMatchID);
  const puntersToSendReminders = getPuntersToRemind_(puntersWhoSubmittedEntries, puntersNeedingReminders);
  if (isEmpty_(puntersToSendReminders)) {
    doAfterReminders_(matchDate);
    return;
  }

  EmailReminders.sendEmails(puntersToSendReminders, matchDate);
  doAfterReminders_(matchDate);
}

/**
 * Perform actions after sending reminders
 */
function doAfterReminders_(matchDate) {
  updateReminderTimestamp_(matchDate);
  clearMatchSheets_();

  console.timeEnd(sendReminders_.name);
}

/**
 * Returns an array of punters who have submitted their entries for the first open match
 * 
 * @param {Object} e - The event object
 * @returns {string[]} - An array of punters who submitted
 */
function getPuntersWithSubmittedDailyPicks_(e) {
  const firstOpenMatchID = ScheduleUtils.getFirstOpenMatchID();

  if (!firstOpenMatchID) { return []; }

  const puntersWhoSubmittedEntries = getPuntersWithSubmittedEntries_(firstOpenMatchID);
  return puntersWhoSubmittedEntries.filter(row => isNotEmpty_(row));
}

/**
 * Returns an array of punters who have not yet submitted their entries for the first open match
 * 
 * @param {Object} e - The event object
 * @returns {string[]} - An array of punters to remind
 */
function getMissingDailyPredictions_(e) {
  if (ScheduleUtils.isAnyMatchOpen()) {
    const puntersWhoSubmittedEntries = getPuntersWithSubmittedDailyPicks_(e);
    const activePunters = PuntersUtils.getActivePunterNames_();
    const puntersToRemind = getPuntersToRemind_(puntersWhoSubmittedEntries, activePunters);

    const missingEntries = puntersToRemind.filter(row => row.length > 0).sort();

    return missingEntries;
  }
}

/**
 * Returns a boolean indicating if it's time to send reminders to punters based on the start time of a match.
 * @param {Date} startTime The start time of the match.
 * @return {boolean} True if it's time to send reminders, false otherwise.
 */
function isItTimeToSendReminders_(startTime) {
  let remindersTime = addMinutes_(startTime, EnvConfigClass.getAddMinutesToSendReminders());
  let closingTime = addMinutes_(startTime, EnvConfigClass.getAddMinutesToCloseDailyBets());
  let isItTimeToSendReminders = isBetweenTimes_(remindersTime, closingTime);

  return isItTimeToSendReminders;
}

/**
 * Get punters with submitted entries for a specific match.
 * 
 * @param {number} firstMatchID - The match ID for which to retrieve punter submissions.
 * 
 * @return {Array} An array of punter names who have submitted entries for the specified match.
 */
function getPuntersWithSubmittedEntries_(firstMatchID) {
  let data = getMatchDataFromAnySourceSheet_(firstMatchID, false);

  const submittedEntries = data.map(row => {
    const emailIndex = colNameToIndex_(dailyPredictionsSheetObj.emailColName);
    const punterColumnIndex = colNameToIndex_(dailyPredictionsSheetObj.punterColName);
    const punter = row[punterColumnIndex].trim();

    return isNotEmpty_(punter) ? punter : PuntersUtils.getPunterNameByEmail_(row[emailIndex].trim());
  });

  return submittedEntries;
}

/**
 * Filters punters who need reminders
 * 
 * @param {Array} submitted - Array of punters who submitted entries
 * @param {Array} needing - Array of punters needing reminders
 * @return {Array} - Array of punters who need reminders, filtered by excluding those who already submitted entries
 */
function getPuntersToRemind_(submitted, needing) {
  return submitted && needing && Array.isArray(submitted) && Array.isArray(needing)
    ? needing.filter(x => !submitted.includes(x))
    : needing;
}

