//HTML Content Utils//

/**
 * Cheerio Library ID: 1ReeQ6WO8kKNxoaA_O0XEQ589cIrRvEBA9qcWpNqdOP17i47u6N9M5Xh0
 * https://github.com/tani/cheeriogs 
 */

/**
 * Gets a Cheerio instance or an array of Cheerio instances from the given URL(s).
 * @param {string|string[]} urls - The URL(s) to fetch and parse.
 * @returns {object|object[]} A Cheerio instance or an array of Cheerio instances.
 */
function getCheerioInstances_(urls) {
  const isArray = Array.isArray(urls);
  const urlsArray = isArray ? urls : [urls];

  const timer = `${getCheerioInstances_.name}. URLs: ${urlsArray.map(url => extractUrlPart_(url)).join(', ')}`;
  console.time(timer);

  const htmlContents = getURLContents_(urlsArray);
  const cheerioInstances = htmlContents.map(content => isNotEmpty_(content) ? Cheerio.load(content) : null);

  console.timeEnd(timer);
  return isArray ? cheerioInstances : cheerioInstances[0];
}

/**
 * Fetches the content of one or more URLs and returns the text content(s).
 * @param {string[]} urls - The URL(s) to fetch.
 * @returns {string[]} The text content of the URL(s), or an empty string if the URL is empty or fetch fails.
 */
function getURLContents_(urls) {
  const options = {
    muteHttpExceptions: true // Prevents fetchAll from throwing exceptions on HTTP errors
  };

  // Create an array of the same length as urls, initially filled with empty strings
  const contents = Array(urls.length).fill('');

  // Filter out invalid URLs and keep track of their original indices
  const validUrls = urls.map((url, index) => {
    const strippedUrl = stripQueryParams(url);
    return isValidUrl_(strippedUrl) ? { url: strippedUrl, index } : null;
  }).filter(item => item);

  if (validUrls.length === 0) {
    return contents; // Return an array of empty strings if no valid URLs
  }

  try {
    const responses = UrlFetchApp.fetchAll(validUrls.map(item => ({ url: item.url, ...options })));

    responses.forEach((response, idx) => {
      const originalIndex = validUrls[idx].index;
      try {
        if (response.getResponseCode() === 200) {
          contents[originalIndex] = response.getContentText();
        } else {
          log(`URL = '${validUrls[idx].url}'. Response Code received = '${response.getResponseCode()}'`, LogLevel.ERROR);
        }
      } catch (error) {
        log(`URL = '${validUrls[idx].url}'. Error = ${error}`, LogLevel.ERROR);
      }
    });
  } catch (error) {
    log(`Error occurred while fetching URLs: ${error}`, LogLevel.ERROR);
  }

  return contents;
}

/**
 * Strips query parameters from the URL.
 * @param {string} url - The URL to strip query parameters from.
 * @returns {string} The URL without query parameters.
 */
function stripQueryParams(url) {
  try {
    const strippedUrl = url.split('?')[0];
    return strippedUrl;
  } catch (error) {
    log(error, LogLevel.ERROR);
    return url; // Return the original URL if an error occurs
  }
}

/**
 * Checks if a URL is valid.
 * @param {string} url - The URL to check.
 * @returns {boolean} True if the URL is valid, false otherwise.
 */
function isValidUrl_(url) {
  const urlPattern = /^(https?:\/\/).*?\.(com|ca|net|org|edu|gov|mil|us|uk|co)(\/|$)/i;
  const isValid = urlPattern.test(url);

  // Log an error if the URL is invalid
  if (!isValid) {
    log(`Invalid URL detected: ${url}`, LogLevel.ERROR);
  }

  return isValid;
}

/**
 * Extracts a specific part from a URL.
 * @param {string} url - The URL from which to extract the part.
 * @returns {string|null} - The extracted part or null if not found.
 */
function extractUrlPart_(url) {
  if (isEmpty_(url)) { return ''; }

  const parts = url.split('/');

  if (parts && parts.length >= 2) {
    const extracted = parts.slice(-2).join('/');
    return extracted;
  } else {
    return url;
  }
}

/////////////// Table Utils /////////////

/**
 * Extracts data from all tables in the provided HTML content.
 * @param {string} $ - The HTML content containing the tables.
 * @returns {Object} An object containing table data, where the keys are table index names (e.g., table1, table2) and the values are arrays representing table rows.
 */
function extractAllTableData_($) {
  // Extract the table data
  var tablesData = {};

  $('table').each(function (index) {
    var $table = $(this);
    var rowData = extractTableData_($, $table);
    tablesData['table' + (index + 1)] = rowData;
  });

  return tablesData;
}

/**
 * Extract standing points table data
 */
function extractPointsTableData_($) {
  // Extract the table data
  var tablesData = [];
  $('table').each(function (index) {
    var $table = $(this);

    // Check if the first <th> in the <thead><tr> is equal to "TEAMS"
    if ($table.find('thead tr th:first-child').text().trim() === "TEAMS") {
      var rowData = extractTableData_($, $table);
      tablesData.push({ index: index + 1, data: rowData });
    }
  });

  return tablesData;
}

/**
 * Extracts data from all available tables in the content and categorizes them into BATTING tables, BOWLING tables, MATCH DETAILS table, and other tables.
 * @param {string} $ - The HTML content containing the tables.
 * @returns {Object} An object containing separate arrays for BATTING tables, BOWLING tables, MATCH DETAILS table, and other tables.
 */
function extractCategorizedTablesData_($) {
  var $tables = $('table');
  var battingTables = [];
  var bowlingTables = [];
  var matchDetailsTable = null;
  var otherTables = [];

  $tables.each(function () {
    var $table = $(this);
    var tableData = extractTableData_($, $table);

    // Check if any row contains match details in the first column
    var hasMatchDetails = tableData.some(rowData =>
      Object.values(MATCH_DETAILS_TABLE_KEYS).some(key => new RegExp(`^${key}$`, 'i').test(rowData[0].trim()))
    );

    // Categorize the table based on the presence of match details
    if (hasMatchDetails) {
      matchDetailsTable = matchDetailsTable || tableData;
    } else {
      var headerRow = tableData[0];
      if (headerRow && headerRow.length > 0) {
        var firstColumnValue = headerRow[0];
        if (firstColumnValue === 'BATTING') { battingTables.push(tableData); }
        else if (firstColumnValue === 'BOWLING') { bowlingTables.push(tableData); }
        else { otherTables.push(tableData); }
      }
    }
  });

  const hasMatchStarted = battingTables.length > 0 && bowlingTables.length > 0;

  return { battingTables, bowlingTables, matchDetailsTable, otherTables, hasMatchStarted, };
}

/**
 * Extracts table data from the HTML content based on the specified table index.
 *
 * @param {string} $ - The HTML content.
 * @param {number} tableIndex - The index of the table to extract data from.
 * @returns {Array[]} The extracted table data as a 2D array.
 */
function extractTableDataByIndex_($, tableIndex) {
  // Extract the table data
  var tableData = [];
  var $table = $('table').eq(tableIndex);

  var tableData = extractTableData_($, $table);

  return tableData;
}

/**
 * Extracts the table data from a given Cheerio table element.
 * @param {Cheerio} $table - The Cheerio table element.
 * @returns {Array[]} An array of rows, where each row is an array of cells.
 */
function extractTableData_($, $table) {
  var tableData = [];
  var $rows = $table.find('tr');
  $rows.each(function () {
    var rowData = [];
    $(this)
      .find('td, th')
      .each(function () {
        var cellText = $(this).text().trim();
        rowData.push(cellText);
      });
    if (rowData.length && !rowData.every((cell) => cell === '')) {
      tableData.push(rowData);
    }
  });

  return tableData;
}

/**
 * Filters the HTML table data based on the specified header conditions.
 *
 * @param {Array[]} tableData - The HTML table data as a 2D array.
 * @param {Object[]} headerConditions - The header conditions to filter the data.
 * @param {boolean} [includeHeaderRow=true] - Whether to include the header row in the filtered data.
 * @returns {Array[]} The filtered HTML table data as a 2D array.
 */
function filterHtmlTableDataByConditions_(tableData, headerConditions, includeHeaderRow = true) {
  const includedColumns = headerConditions.map(condition => condition.index);
  const filteredData = [];

  if (includeHeaderRow) {
    const filteredHeaderRow = getFilteredHtmlTableHeaderRow_(tableData[0], includedColumns, headerConditions);
    filteredData.push(filteredHeaderRow);
  }

  for (let i = 1; i < tableData.length; i++) {
    const row = tableData[i];
    const filteredRow = getFilteredHtmlTableRow_(row, includedColumns);

    if (shouldIncludeHtmlTableRow_(row, headerConditions)) {
      filteredData.push(filteredRow);
    }
  }

  return filteredData;
}

/**
 * Retrieves the filtered header row of an HTML table based on the included columns and header conditions.
 *
 * @param {string[]} headerRow - The header row of the HTML table.
 * @param {number[]} includedColumns - The indices of the included columns.
 * @param {Object[]} headerConditions - The header conditions used for filtering.
 * @returns {string[]} The filtered header row.
 */
function getFilteredHtmlTableHeaderRow_(headerRow, includedColumns, headerConditions) {
  return includedColumns.map(index => {
    const condition = headerConditions.find(condition => condition.index === index);
    return headerRow[condition.index];
  });
}

/**
 * Retrieves the filtered row of an HTML table based on the included columns.
 *
 * @param {string[]} row - The row of the HTML table.
 * @param {number[]} includedColumns - The indices of the included columns.
 * @returns {string[]} The filtered row.
 */
function getFilteredHtmlTableRow_(row, includedColumns) {
  return includedColumns.map(index => row[index]);
}

/**
 * Determines whether a HTML table row should be included based on the header conditions.
 *
 * @param {string[]} row - The row of the HTML table.
 * @param {Object[]} headerConditions - The header conditions used for filtering.
 * @returns {boolean} True if the row should be included, false otherwise.
 */
function shouldIncludeHtmlTableRow_(row, headerConditions) {
  return headerConditions.every(headerCondition => {
    const { index, conditions } = headerCondition;
    let value = row[index];
    if (isEmpty_(value)) { value = ""; }
    //log(`value = ${value}, headerConditions = ${JSON.stringify(headerConditions)}`, LogLevel.DEBUG);

    if (Array.isArray(conditions.exclude) && conditions.exclude.some(excludeValue => value.includes(excludeValue))) { return false; }
    if (Array.isArray(conditions.include) && !conditions.include.some(includeValue => value.includes(includeValue))) { return false; }

    const isValidNumber = /^\d+(\.\d+)?$/.test(value.trim());
    const numericValue = isValidNumber ? parseFloat(value) : NaN;

    if (conditions.min !== undefined && (isNaN(numericValue) || numericValue < (conditions.min))) { return false; }
    if (conditions.gte !== undefined && (isNaN(numericValue) || numericValue < (conditions.gte))) { return false; }
    if (conditions.max !== undefined && (isNaN(numericValue) || numericValue > (conditions.max))) { return false; }
    if (conditions.lte !== undefined && (isNaN(numericValue) || numericValue > (conditions.lte))) { return false; }
    if (conditions.gt !== undefined && (isNaN(numericValue) || numericValue <= (conditions.gt))) { return false; }
    if (conditions.lt !== undefined && (isNaN(numericValue) || numericValue >= (conditions.lt))) { return false; }
    if (conditions.ne !== undefined && (isNaN(numericValue) || numericValue == (conditions.ne))) { return false; }

    return true;
  });
}

/**
 * Extracts batting data from the batting tables.
 *
 * @param {string[][]} battingTables - The batting tables.
 * @returns {string[][]} The extracted batting data.
 */
function extractBattingData_(battingTables) {
  battingTables = transformArrayToLevel_(battingTables, 3);//This should always be a 3 level array

  const headerConditions = [
    { index: BattingTableIndex.Player, conditions: { exclude: BATTING_TABLE_NON_PLAYERS_LIST } },
    { index: BattingTableIndex.HowOut, conditions: {} },//How out
    { index: BattingTableIndex.Runs, conditions: {} },//Runs
    { index: BattingTableIndex.BallsFaced, conditions: {} },//Balls faced
    { index: BattingTableIndex.Minutes, conditions: {} },//Minutes
    { index: BattingTableIndex.Fours, conditions: {} },//4s
    { index: BattingTableIndex.Sixes, conditions: {} },//6s
    { index: BattingTableIndex.StrikeRate, conditions: {} },//Strike Rate
  ];

  var battingData = [];

  for (table of battingTables) {
    const data = filterHtmlTableDataByConditions_(table, headerConditions, false);
    battingData = battingData.concat(data);
  }

  return sanitizePlayerData_(battingData, 0);
}

/**
 * Extracts innings score data from the batting tables.
 *
 * @param {string[][]} battingTables - The batting tables.
 * @returns {string[][]} The extracted innings score data.
 */
function extractInningsScoreData_(battingTables) {
  const headerConditions = [
    { index: BattingTableIndex.Total, conditions: { include: ["TOTAL"] } },
    { index: BattingTableIndex.InningsOvers, conditions: {} },//Overs and RR
    { index: BattingTableIndex.InningsScore, conditions: {} },//Total innings score, e.g. 240, 198/5
  ];
  var battingData = [];

  for (table of battingTables) {
    const data = filterHtmlTableDataByConditions_(table, headerConditions, false);
    battingData = battingData.concat(data);
  }

  return battingData;
}

/**
 * Extracts fall of wickets data from the batting tables.
 *
 * @param {string[][]} battingTables - The batting tables.
 * @returns {string[][]} The extracted fall of wickets data.
 */
function extractFallOfWicketsData_(battingTables) {
  const headerConditions = [
    { index: BattingTableIndex.FallOfWickets, conditions: { include: ["Fall of wickets:"] } },
  ];
  var battingData = [];

  for (const table of battingTables) {
    const data = filterHtmlTableDataByConditions_(table, headerConditions, false);
    battingData.push(...data);
  }

  // Add empty arrays for tables that didn't match the conditions
  while (battingData.length < battingTables.length) {
    battingData.push([]);
  }

  return battingData;
}

/**
 * Extracts bowling data from the bowling tables.
 *
 * @param {string[][]} bowlingTables - The bowling tables.
 * @returns {string[][]} The extracted bowling data.
 */
function extractBowlingData_(bowlingTables) {
  bowlingTables = transformArrayToLevel_(bowlingTables, 3);//This should always be a 3 level array

  const headerConditions = [
    { index: BowlingTableIndex.Player, conditions: {} },
    { index: BowlingTableIndex.Overs, conditions: { gte: 0 } },
    { index: BowlingTableIndex.Maiden, conditions: {} },
    { index: BowlingTableIndex.Runs, conditions: {} },
    { index: BowlingTableIndex.Wickets, conditions: {} },
    { index: BowlingTableIndex.Economy, conditions: {} },
  ];

  var bowlingData = [];

  for (table of bowlingTables) {
    const data = filterHtmlTableDataByConditions_(table, headerConditions, false);
    bowlingData = bowlingData.concat(data);
  }

  return sanitizePlayerData_(bowlingData, 0);
}

/**
 * Extracts the values of specific keys from the second column of the given match details table.
 * @param {Array[]} matchDetailsTable - The match details table data.
 * @returns {Object} An object with the extracted values.
 */
function extractMatchDetailsTableValues_(matchDetailsTable) {
  if (!Array.isArray(matchDetailsTable) || matchDetailsTable.length === 0) { return {}; }

  var values = {};

  // Iterate over the rows of the match details table
  matchDetailsTable.forEach(function (rowData) {
    if (rowData.length >= 2) {
      var firstColumnValue = rowData[0].trim();
      var secondColumnValue = rowData[1].trim();

      Object.entries(MATCH_DETAILS_TABLE_KEYS).forEach(function ([key, value]) {
        var regex = new RegExp('^' + value.replace(/\s+/g, '\\s*') + '$', 'i');
        if (regex.test(firstColumnValue)) {
          values[key] = secondColumnValue;
        }
      });
    }
  });

  return Object.keys(values).length > 0 ? values : {};
}

/////////////////// CSS Utils /////////////////////

/**
 * Extracts the root element with the specified CSS selector from the content.
 *
 * @param {string} $ - The content to extract the root element from.
 * @param {string} cssSelector - The CSS selector of the root element.
 * @returns {Array<string>} The extracted root element.
 */
function extractRootElementWithCSSSelector_($, cssSelector) {
  // Extract root element with the specified CSS Selector
  const elements = [];

  $(cssSelector).each(function () {
    const text = $(this)
      .clone() // Clone the element to remove child elements
      .children()
      .remove() // Remove all child elements from the clone
      .end() // Go back to the original element
      .text()
      .trim();

    if (text.length > 0) {
      elements.push(text);
    }
  });

  return elements;
}

/**
 * Extracts the text content of elements based on the provided CSS selector.
 *
 * @param {string} $ - The HTML content to search within.
 * @param {string} cssSelector - The CSS selector to target elements.
 * @returns {string} The text content of the selected elements.
 */
function extractElementsText_($, cssSelector) {
  const elements = $(cssSelector);
  return elements.text().trim() || '';
}

/**
 * Extracts elements with the specified CSS selector from the content.
 *
 * @param {string} $ - The content to extract the elements from.
 * @param {string} cssSelector - The CSS selector of the elements.
 * @returns {Array<string|Array<string>>} The extracted elements.
 */
function extractElementsWithCSSSelector_($, cssSelector) {
  // Extract elements with the specified CSS Selector
  const elements = [];

  $(cssSelector).children().each(function () {
    const childElements = extractChildElements_($(this), $);
    elements.push(childElements);
  });

  if (elements.length === 0) {
    $(cssSelector).each(function () {
      const text = $(this).text().trim();
      if (text.length > 0) {
        elements.push(text);
      }
    });
  }

  return elements;
}

/**
 * Extracts child elements recursively from the given element.
 *
 * @param {Cheerio} $element - The Cheerio element to extract child elements from.
 * @param {CheerioStatic} $ - The Cheerio instance.
 * @returns {Array<string>} The extracted child elements.
 */
function extractChildElements_($element, $) {
  var elements = [];

  $element.contents().each(function () {
    if (this.nodeType === 3) { // Text node
      var text = $(this).text().trim();
      if (text.length > 0) {
        elements.push(text);
      }
    } else if (this.nodeType === 1) { // Element node
      var childElements = extractChildElements_($(this), $);
      elements = elements.concat(childElements);
    }
  });

  return elements;
}

//Generic Utils

/**
 * Sanitizes the provided player name by removing special characters and specified roles.
 * @param {string} playerName - The player name to be cleaned.
 * @returns {string} The cleaned player name.
 */
function sanitizePlayerName_(playerName) {
  if (isEmpty_(playerName) || "-" === playerName.trim()) { return; }

  let cleanedPlayer = playerName;

  for (const role of PLAYER_ROLES_TO_STRIP) {
    const rolePattern = new RegExp(`\\b${role}\\b`, 'gi');
    cleanedPlayer = cleanedPlayer.replace(rolePattern, '');
  }
  for (const char of PLAYER_SPECIAL_CHARS_TO_REMOVE) {
    cleanedPlayer = cleanedPlayer.replace(new RegExp(`${char}`, 'g'), '');
  }

  return removeTextInParentheses_(cleanedPlayer);
}

/**
 * Removes text within parentheses (including the parentheses themselves) from a given input text.
 *
 * @param {string} inputText - The input text from which to remove text within parentheses.
 * @returns {string} The modified text with content within parentheses removed.
 *
 * @example
 * const inputText = "Virat Kohli* (rhb)";
 * const result = removeTextInParentheses_(inputText);
 * // result will be "Virat Kohli*"
 */
function removeTextInParentheses_(inputText) {
  var regex = /\([^)]*\)/g; // Regular expression to match text within parentheses
  var result = inputText.replace(regex, '').trim(); // Replace matched text with an empty string
  return result;
}
