//Series.Class.gs//

/**
 * Represents a Series.
 * @class
 */
class Series {
  /**
   * Creates an instance of Series.
   * @param {number} id - The ID of the series.
   * @param {string} name - The name of the series.
   * @param {boolean} isActive - Whether the series is active.
   * @param {string} homeURL - The home URL of the series.
   * @param {string} hostCountry - The host country of the series.
   * @param {string} category - The category of the series.
   * @param {boolean} isImpactPlayerAllowed - Whether impact players are allowed.
   * @param {string} timezone - The timezone of the series.
   */
  constructor(id, name, isActive, homeURL, hostCountry, category, isImpactPlayerAllowed, timezone) {
    var id_ = id;
    var name_ = name;
    var isActive_ = isActive;
    var homeURL_ = homeURL;
    var hostCountry_ = hostCountry;
    var category_ = category;
    var isImpactPlayerAllowed_ = isImpactPlayerAllowed;
    var timezone_ = timezone;

    // Getters
    this.getId = function () { return id_; };
    this.getName = function () { return name_; };
    this.getIsActive = function () { return isActive_; };
    this.getHomeURL = function () { return homeURL_; };
    this.getHostCountry = function () { return hostCountry_; };
    this.getCategory = function () { return category_; };
    this.getIsImpactPlayerAllowed = function () { return isImpactPlayerAllowed_; };
    this.getTimezone = function () { return timezone_; };

    // Setters
    this.setId = function (id) { id_ = id; };
    this.setName = function (name) { name_ = name; };
    this.setIsActive = function (isActive) { isActive_ = isActive; };
    this.setHomeURL = function (homeURL) { homeURL_ = homeURL; };
    this.setHostCountry = function (hostCountry) { hostCountry_ = hostCountry; };
    this.setCategory = function (category) { category_ = category; };
    this.setIsImpactPlayerAllowed = function (isImpactPlayerAllowed) { isImpactPlayerAllowed_ = isImpactPlayerAllowed; };
    this.setTimezone = function (timezone) { timezone_ = timezone; };
  }

  /**
   * Serializes the Series instance to a plain object.
   * @returns {Object} The plain object representation of the Series.
   */
  toObject() {
    return {
      id: this.getId(),
      name: this.getName(),
      isActive: this.getIsActive(),
      homeURL: this.getHomeURL(),
      hostCountry: this.getHostCountry(),
      category: this.getCategory(),
      isImpactPlayerAllowed: this.getIsImpactPlayerAllowed(),
      timezone: this.getTimezone()
    };
  }

  /**
   * Static method to create a Series instance from a plain object.
   * @param {Object} obj - The plain object representation of a Series.
   * @returns {Series} The Series instance.
   */
  static fromObject(obj) {
    return new Series(
      obj.id,
      obj.name,
      obj.isActive,
      obj.homeURL,
      obj.hostCountry,
      obj.category,
      obj.isImpactPlayerAllowed,
      obj.timezone
    );
  }
}
