// Playing XI //

function testPlayingXI(e) {
  const sheet = getFirstMatchSheet_();

  log(getPlayingXIData_(sheet));
  log(getPlayingXIDataFromScorecard_(getMatchID_(sheet)));
}

/**
 * Retrieves the playing XI data from the match sheet.
 *
 * @param {Object} matchSheet - The match sheet object.
 * @returns {Object} The playing XI data including team 1 and team 2 playing XI.
 */
function getPlayingXIData_($playingXI, matchSheet, firstBattingTeam) {
  const playersData = getPlayingXIDataFromScorecard_($playingXI, firstBattingTeam);
  updateMatchPlayingXIData_(matchSheet, playersData);

  return playersData;
}

/**
 * Retrieves playing XI data from a scorecard.
 * @param {?} $ - The cheerio instance
 * @param {string} firstBattingTeam - The team that bats first.
 * @returns {Object} An object containing teams and players data.
 */
function getPlayingXIDataFromScorecard_($, firstBattingTeam) {
  if (isEmpty_($)) {
    return {};
  }

  const tableElement = $('table').eq(0);
  const teams = extractTeamsFromTable_($, tableElement);
  const players = extractPlayersFromTable_($, tableElement, teams.team1, teams.team2, firstBattingTeam);

  const result = { ...teams, ...players };

  return result;
}

/**
 * Extracts team IDs from the given table element.
 * @param {function} $ - The Cheerio function.
 * @param {CheerioElement} tableElement - The table element containing team data.
 * @returns {Object} An object containing team IDs.
 */
function extractTeamsFromTable_($, tableElement) {
  const $tableData = $(tableElement).find('th');

  const team1Text = $($tableData[1]).find('span').text().trim();
  const team2Text = $($tableData[2]).find('span').text().trim();

  const team1 = isNotEmpty_(team1Text) ? getTeamIdByName_(team1Text) : "";
  const team2 = isNotEmpty_(team2Text) ? getTeamIdByName_(team2Text) : "";

  return { team1, team2 };
}

/**
 * Extracts players' data from the given table element.
 * @param {function} $ - The Cheerio function.
 * @param {CheerioElement} tableElement - The table element containing players' data.
 * @param {string} team1 - The ID of team 1.
 * @param {string} team2 - The ID of team 2.
 * @param {string} firstBattingTeam - The ID of the team batting first.
 * @returns {Object} An object containing players' data.
 */
function extractPlayersFromTable2_($, tableElement, team1, team2, firstBattingTeam) {
  const players = { team1PlayingXI: new Set(), team2PlayingXI: new Set(), team1ImpactPlayers: new Set(), team2ImpactPlayers: new Set(), team1NotInPlayingXI: new Set(), team2NotInPlayingXI: new Set(), };
  let isBeforeBenchRow = true;
  const impactPlayersAllowed = isImpactPlayerAllowed_();

  $(tableElement).find('tr').each((index, element) => {
    const $tableData = $(element).find('td');
    const col0Value = $($tableData[0]).text().trim();

    if (isEmpty_(col0Value)) { return; }
    if (col0Value === 'Bench') {
      isBeforeBenchRow = false;
      return;
    }

    const $col1 = $($tableData[1]);
    const $col2 = $($tableData[2]);

    const team1Player = sanitizePlayerName_($col1.find('div a span').text().trim());
    const team2Player = sanitizePlayerName_($col2.find('div a span').text().trim());

    if (isBeforeBenchRow) {
      if (isNotEmpty_(team1Player)) { players.team1PlayingXI.add(team1Player); }
      if (isNotEmpty_(team2Player)) { players.team2PlayingXI.add(team2Player); }
    } else if (impactPlayersAllowed) {
      if ($col1.find('img[alt="sub"]').length > 0) { players.team1ImpactPlayers.add(team1Player); }
      else { players.team1NotInPlayingXI.add(team1Player); }
      if ($col2.find('img[alt="sub"]').length > 0) { players.team2ImpactPlayers.add(team2Player); }
      else { players.team2NotInPlayingXI.add(team2Player); }
    }
  });

  if (impactPlayersAllowed && players.team1ImpactPlayers.size == 0) { players.team1NotInPlayingXI.clear(); }
  if (impactPlayersAllowed && players.team2ImpactPlayers.size == 0) { players.team2NotInPlayingXI.clear(); }

  const isPlayingXIAvailable = isFullPlayingXIAvailable_(players.team1PlayingXI.size, players.team2PlayingXI.size);
  const isFullImpactPlayersAvailable = isFullImpactPlayersAvailable_(players.team1ImpactPlayers.size, players.team2ImpactPlayers.size);
  const playersCount = players.team1PlayingXI.size + players.team2PlayingXI.size;

  if (firstBattingTeam != team1 && firstBattingTeam == team2) {
    swapTeamPlayersSets_(players.team1PlayingXI, players.team2PlayingXI);
    swapTeamPlayersSets_(players.team1ImpactPlayers, players.team2ImpactPlayers);
  }

  Object.keys(players).forEach(key => { players[key] = Array.from(players[key]); });

  return {
    ...players,
    isPlayingXIAvailable,
    isFullImpactPlayersAvailable,
    playersCount,
  };
}

/**
 * Extracts players from a table element.
 *
 * @param {object} $ - jQuery object.
 * @param {object} tableElement - Table element containing the players.
 * @param {string} team1 - Name of the first team.
 * @param {string} team2 - Name of the second team.
 * @param {string} firstBattingTeam - Name of the team batting first.
 * @returns {object} An object containing the players' information.
 */
function extractPlayersFromTable_($, tableElement, team1, team2, firstBattingTeam) {
  const players = initializePlayers_();
  let isBeforeBenchRow = true;
  const impactPlayersAllowed = isImpactPlayerAllowed_();

  $(tableElement).find('tr').each((index, element) => {
    const $tableData = $(element).find('td');
    const col0Value = $($tableData[0]).text().trim();

    if (isEmpty_(col0Value)) { return; }
    if (col0Value === 'Bench') {
      isBeforeBenchRow = false;
      return;
    }

    const team1Player = extractPlayerName_($, $tableData, 1);
    const team2Player = extractPlayerName_($, $tableData, 2);

    if (isBeforeBenchRow) {
      addToPlayingXI_(players.team1PlayingXI, team1Player);
      addToPlayingXI_(players.team2PlayingXI, team2Player);
    } else {
      handleImpactPlayers_($, players, $tableData, team1Player, team2Player, impactPlayersAllowed);
    }
  });

  finalizePlayers_(players, impactPlayersAllowed);

  const isPlayingXIAvailable = isFullPlayingXIAvailable_(players.team1PlayingXI.size, players.team2PlayingXI.size);
  const isFullImpactPlayersAvailable = isFullImpactPlayersAvailable_(players.team1ImpactPlayers.size, players.team2ImpactPlayers.size);
  const playersCount = players.team1PlayingXI.size + players.team2PlayingXI.size;

  if (firstBattingTeam !== team1 && firstBattingTeam === team2) {
    swapTeamPlayersSets_(players.team1PlayingXI, players.team2PlayingXI);
    swapTeamPlayersSets_(players.team1ImpactPlayers, players.team2ImpactPlayers);
  }

  convertSetsToArrays_(players);

  return {
    ...players,
    isPlayingXIAvailable,
    isFullImpactPlayersAvailable,
    playersCount,
  };
}

/**
 * Initializes the players object.
 *
 * @returns {object} An object with empty sets for team players.
 */
function initializePlayers_() {
  return {
    team1PlayingXI: new Set(),
    team2PlayingXI: new Set(),
    team1ImpactPlayers: new Set(),
    team2ImpactPlayers: new Set(),
    team1NotInPlayingXI: new Set(),
    team2NotInPlayingXI: new Set(),
  };
}

/**
 * Extracts the player name from the specified column in the table row.
 *
 * @param {object} $ - jQuery object.
 * @param {object} $tableData - Table data cells.
 * @param {number} colIndex - Column index to extract player name from.
 * @returns {string} Sanitized player name.
 */
function extractPlayerName_($, $tableData, colIndex) {
  return sanitizePlayerName_($($tableData[colIndex]).find('div a span').text().trim());
}

/**
 * Adds a player to the playing XI if the conditions are met.
 *
 * @param {Set} playingXI - Set of players in the playing XI.
 * @param {string} player - Player name to add.
 */
function addToPlayingXI_(playingXI, player) {
  if (isNotEmpty_(player) && playingXI.size < getPlayingXIAllowedCount_()) {
    playingXI.add(player);
  }
}

/**
 * Handles the addition of impact players and players not in the playing XI.
 *
 * @param {object} $ - jQuery object.
 * @param {object} players - Players object containing sets of players.
 * @param {object} $tableData - Table data cells.
 * @param {string} team1Player - Player name from team 1.
 * @param {string} team2Player - Player name from team 2.
 * @param {boolean} impactPlayersAllowed - Flag indicating if impact players are allowed.
 */
function handleImpactPlayers_($, players, $tableData, team1Player, team2Player, impactPlayersAllowed) {
  if (impactPlayersAllowed) {
    if ($($tableData[1]).find('img[alt="sub"]').length > 0) {
      players.team1ImpactPlayers.add(team1Player);
    } else {
      players.team1NotInPlayingXI.add(team1Player);
    }
    if ($($tableData[2]).find('img[alt="sub"]').length > 0) {
      players.team2ImpactPlayers.add(team2Player);
    } else {
      players.team2NotInPlayingXI.add(team2Player);
    }
  }
}

/**
 * Finalizes the sets of players based on whether impact players are allowed.
 *
 * @param {object} players - Players object containing sets of players.
 * @param {boolean} impactPlayersAllowed - Flag indicating if impact players are allowed.
 */
function finalizePlayers_(players, impactPlayersAllowed) {
  if (impactPlayersAllowed) {
    if (players.team1ImpactPlayers.size === 0) {
      players.team1NotInPlayingXI.clear();
    }
    if (players.team2ImpactPlayers.size === 0) {
      players.team2NotInPlayingXI.clear();
    }
  }
}

/**
 * Converts sets of players to arrays.
 *
 * @param {object} players - Players object containing sets of players.
 */
function convertSetsToArrays_(players) {
  Object.keys(players).forEach(key => {
    players[key] = Array.from(players[key]);
  });
}

/**
 * Swaps elements between two Set objects.
 *
 * @param {Set} set1 - The first Set to swap elements with the second Set.
 * @param {Set} set2 - The second Set to swap elements with the first Set.
 */
function swapTeamPlayersSets_(set1, set2) {
  const tempArray1 = [...set1];
  const tempArray2 = [...set2];

  set1.clear();
  set2.clear();

  tempArray2.forEach(element => set1.add(element));
  tempArray1.forEach(element => set2.add(element));
}

/**
 * Checks if the full Playing XI is available for both teams.
 * @private
 * @param {number} team1Count - The count of Playing XI players for Team 1.
 * @param {number} team2Count - The count of Playing XI players for Team 2.
 * @returns {boolean} `true` if the full Playing XI is available, `false` otherwise.
 */
function isFullPlayingXIAvailable_(team1Count, team2Count) {
  const playersNeeded = getPlayingXIAllowedCount_();
  return team1Count >= playersNeeded && team2Count >= playersNeeded;
}

/**
 * Checks if the full impact players list is available based on the counts of players in two teams.
 * 
 * @param {number|null|undefined} team1Count - The count of players in team 1.
 * @param {number|null|undefined} team2Count - The count of players in team 2.
 * @returns {boolean} - True if the full impact players list is available, false otherwise.
 */
function isFullImpactPlayersAvailable_(team1Count, team2Count) {
  const playersNeeded = getPlayingXIBenchMinCount_();
  const countTeam1 = team1Count ?? 0;
  const countTeam2 = team2Count ?? 0;
  return countTeam1 >= playersNeeded && countTeam2 >= playersNeeded;
}

/**
 * Updates the match sheet with provided Playing XI data.
 *
 * @param {Object} sheet - The match sheet object to update.
 * @param {Object} playingXIObj - The playing XI object containing playing XI data.
 */
function updateMatchPlayingXIData_(sheet, playingXIObj) {
  if (isEmpty_(playingXIObj)) { return; }

  const team1PlayingXI = playingXIObj.team1PlayingXI.join('\n');
  const team2PlayingXI = playingXIObj.team2PlayingXI.join('\n');
  const combinedPlayingXIList = [...playingXIObj.team1PlayingXI, ...playingXIObj.team2PlayingXI].join(', ');

  const team1ImpactPlayersList = playingXIObj.team1ImpactPlayers.join('\n');
  const team2ImpactPlayersList = playingXIObj.team2ImpactPlayers.join('\n');
  const combinedImpactPlayersList = [...playingXIObj.team1ImpactPlayers, ...playingXIObj.team2ImpactPlayers].join(', ');

  const team1NotInPlayingXIList = playingXIObj.team1NotInPlayingXI.join('\n');
  const team2NotInPlayingXIList = playingXIObj.team2NotInPlayingXI.join('\n');
  const combinedNotInPlayingXIList = [...playingXIObj.team1NotInPlayingXI, ...playingXIObj.team2NotInPlayingXI].join(', ');

  const rangesToUpdate = [
    { range: matchSheetObj.playingXIRange, values: [[playingXIObj.isPlayingXIAvailable, team1PlayingXI, team2PlayingXI, combinedPlayingXIList]] },
    { range: matchSheetObj.impactPlayersRange, values: [[playingXIObj.isFullImpactPlayersAvailable, team1ImpactPlayersList, team2ImpactPlayersList, combinedImpactPlayersList]] },
    { range: matchSheetObj.notInPlayingXIRange, values: [[playingXIObj.isFullImpactPlayersAvailable, team1NotInPlayingXIList, team2NotInPlayingXIList, combinedNotInPlayingXIList]] },
  ];

  updateSheetRanges_(sheet, rangesToUpdate);
  updateRefreshCounter_(sheet, matchSheetObj.playingXIRefreshCounterRange);
}

/**
 * Retrieves the impact player configuration object based on the active series.
 *
 * @typedef {Object} ImpactPlayerConfig
 * @property {number} playingXICount - Number of players in the playing XI.
 * @property {number} [benchPlayersMinCount] - Minimum number of bench players (only applicable if impact players are allowed).
 *
 * @returns {ImpactPlayerConfig} The impact player configuration object.
 */
function getImpactPlayerObject_() {
  const activeSeries = getActiveSeries_();

  const impactPlayerAllowed = activeSeries ? activeSeries.getIsImpactPlayerAllowed() : false;
  return impactPlayerAllowed ? IMPACT_PLAYERS.ALLOWED : IMPACT_PLAYERS.NOT_ALLOWED;
}

/**
 * Determines if impact players are allowed in the active series.
 *
 * @returns {boolean|null} True if impact players are allowed, false if not allowed, and null if there is no active series.
 */
function isImpactPlayerAllowed_() {
  const activeSeries = getActiveSeries_();
  return activeSeries ? activeSeries.getIsImpactPlayerAllowed() : null;
}

/**
 * Retrieves the number of players allowed in the playing XI based on the impact player configuration.
 *
 * @returns {number} The number of players allowed in the playing XI.
 */
function getPlayingXIAllowedCount_() {
  return getImpactPlayerObject_().playingXICount;
}

/**
 * Retrieves the minimum number of bench players allowed based on the impact player configuration.
 *
 * @returns {number|undefined} The minimum number of bench players allowed, or undefined if impact players are not allowed.
 */
function getPlayingXIBenchMinCount_() {
  return getImpactPlayerObject_().benchPlayersMinCount;
}

/**
 * Retrieves the playing XI URL based on the given match ID.
 * @param {string} matchID - The match ID.
 * @returns {string} The playing XI URL.
 */
function getPlayingXIURL_(matchID) {
  if (isEmpty_(matchID)) { return; }

  const url = getFullScorecardURL_(matchID).replace(URLs.FULL_SCORECARD_URL_PART, URLs.MATCH_PLAYINGXI_URL_PART);
  //log(`The URL for PlayingXI for Match ID "${matchID}" = '${url}'`, LogLevel.INFO);

  return url;
}
