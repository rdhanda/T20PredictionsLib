//EmailUtils.gs//

/**
 * Represents a utility class for sending emails when the daily bets form is opened.
 */
class EmailBetsOpened {

  /**
   * Sends an email to a list of recipients when the specified Google Form is opened.
   * 
   * @param {Object} e - The event object passed to the form open trigger function.
   * @param {string} date - The date for which the form is open.
   */
  static sendEmail(e, date) {
    let subject = `🟢 ${date} Bets are OPEN`;
    let body = `Greetings 📢,<p/>The bets for ${date} are now open! 🟢 Don't miss out on the excitement! Submit your bets at ${getDailyBetsPublishedURLLink_()} and join the fun! 🎊🏆`;

    // Send the email to the list of recipients.
    EmailUtils.sendEmail(EmailBetsOpened.getEmailListOnFormOpen_(), subject, body);
  }

  /**
   * Gets the list of email recipients when the form is opened.
   * 
   * @returns {string[]} The list of email recipients.
   */
  static getEmailListOnFormOpen_() {
    return EmailUtils.getAllAdminsEmailList();
  }
}

/**
 * Represents a utility class for sending emails when the daily bets form is closed.
 */
class EmailBetsClosed {

  /**
   * Sends an email to a list of recipients when the specified Google Form is closed.
   * 
   * @param {Object} e - The event object passed to the form close trigger function.
   * @param {boolean} success - A boolean indicating whether the form submission was successful or not.
   * @param {string} date - The date for which the form is closed.
   */
  static sendEmail(e, success, date) {
    let subject = `🛑 ${date} Bets are CLOSED`;
    let body = `Greetings 📢,<p/>The bets for ${date} are now closed. 🛑 Thank you to all participants for submitting the bets! Here are the bets:<p/>`;

    let htmlTable = "";
    if (success) {
      // If the form submission was successful, construct an HTML table with match data.
      let matchesNum = ScheduleUtils.getNumberOfMatchesClosed(date);
      let matchSheetNames = getMatchSheetNames_().slice(0, matchesNum);

      htmlTable = matchSheetNames.map(sheetName => {
        let matchSheet = getSheetByName_(sheetName);
        return getMatchDataTable_(matchSheet) + "<p/>";
      }).join('');
    }
    // Send the email to the list of recipients.
    EmailUtils.sendEmail(EmailBetsClosed.getEmailListOnFormClose_(), subject, body + htmlTable);
  }

  /**
   * Gets the list of email recipients when the form is closed.
   * 
   * @returns {string[]} The list of email recipients.
   */
  static getEmailListOnFormClose_() {
    return EmailUtils.getAllAdminsEmailList();
  }
}

/**
 * Represents a utility class for sending emails when a punter submits a bet on a form.
 */
class EmailFormSubmission {

  /**
   * Sends an email when a punter submits bets on a form.
   *
   * @param {string} sheetName - The name of the sheet where the bets were submitted.
   * @param {string} punterName - The name of the punter who submitted the bets.
   * @param {string} latestFormData - The latest data received from the form.
   * @param {string} eventValues - The event data submitted by the punter.
   * @returns {void}
   */
  static sendEmail(sheetName, punterName, latestFormData, eventValues) {
    let formName = addSpacesToCamelCase_(sheetName);  // Assumes this function is defined elsewhere
    if (dailyBetsSheetName === sheetName) {  // Assumes dailyBetsSheetName is defined elsewhere
      formName = `${ScheduleUtils.getFirstOpenMatchShortDate() || ''} ${formName}`.trim();  // Assumes this function is defined elsewhere
    }

    let subject = `📥 ${formName} Received from ${punterName}`;
    let body = `📩 Greetings,<p/>${punterName} submitted an entry for ${formName}, containing the following bets entries:<p/>${EmailUtils.formatBets(latestFormData)}<p/>${eventValues}`;

    EmailUtils.sendEmail(EmailUtils.getActiveUserEmail(), subject, body, false);  // Assumes EmailUtils is defined elsewhere
  }
}

/**
 * Represents a utility class for sending emails regarding invalid entries.
 */
class EmailInvalidEntries {

  /**
   * Sends an email notification when a wrong name is selected.
   * @param {string} registeredPunterName - The registered punter's name.
   * @param {string} submittedEmail - The email submitted in the form.
   * @param {string} selectedPunterName - The name selected in the form.
   * @param {string} formTitle - The title of the form.
   */
  static sendEmailWrongPunter(registeredPunterName, submittedEmail, selectedPunterName, formTitle) {
    const placeholders = [
      { placeholder: TemplateUtils.REGISTERED_PUNTER_NAME, value: registeredPunterName },
      { placeholder: TemplateUtils.SUBMITTED_EMAIL, value: submittedEmail },
      { placeholder: TemplateUtils.SELECTED_PUNTER_NAME, value: selectedPunterName },
      { placeholder: TemplateUtils.FORM_TITLE, value: formTitle },
    ];

    let subject = TemplateUtils.getRandomValueFromTemplate(EmailInvalidEntries.getWrongPunterSubjects());
    subject = TemplateUtils.replacePlaceholders(subject, placeholders);

    let body = TemplateUtils.getRandomValueFromTemplate(EmailInvalidEntries.getWrongPunterBodies());
    body = TemplateUtils.replacePlaceholders(body, placeholders);

    const recipients = [submittedEmail, PuntersUtils.getPunterEmail_(selectedPunterName)];
    EmailUtils.sendEmail(recipients, subject, body);
  }

  /**
   * Sends an email notification when an unregistered person is selected with a punter.
   * @param {string} submittedEmail - The email submitted in the form.
   * @param {string} selectedPunterName - The name selected in the form.
   * @param {string} formTitle - The title of the form.
   */
  static sendEmailUnknownEmailWithPunter(submittedEmail, selectedPunterName, formTitle) {
    const placeholders = [
      { placeholder: TemplateUtils.SUBMITTED_EMAIL, value: submittedEmail },
      { placeholder: TemplateUtils.SELECTED_PUNTER_NAME, value: selectedPunterName },
      { placeholder: TemplateUtils.FORM_TITLE, value: formTitle },
    ];

    let subject = TemplateUtils.getRandomValueFromTemplate(EmailInvalidEntries.getUnknownEmailWithPunterSubjects())
    subject = TemplateUtils.replacePlaceholders(subject, placeholders);

    let body = TemplateUtils.getRandomValueFromTemplate(EmailInvalidEntries.getUnknownEmailWithPunterBodies())
    body = TemplateUtils.replacePlaceholders(body, placeholders);

    const recipients = [submittedEmail, PuntersUtils.getPunterEmail_(selectedPunterName)];
    EmailUtils.sendEmail(recipients, subject, body);
  }

  /**
   * Sends an email notification when an unregistered person is selected with no punter.
   * @param {string} submittedEmail - The email submitted in the form.
   * @param {string} formTitle - The title of the form.
   */
  static sendEmailUnknownEmailWithoutPunter(submittedEmail, formTitle) {
    const subject = `❓ Unknown Punter: ${submittedEmail}`;
    const body = `Hello,<br/><br/>An unknown punter with the email address '${submittedEmail}' has attempted to submit entries for the form linked to '${formTitle}' sheet. Their entry will be ignored. If this email belongs to a registered punter, please update their registered email list.<br/><br/>Regards,<br/>Your Team`;

    const recipients = [submittedEmail];
    EmailUtils.sendEmail(recipients, subject, body);
  }

  /**
   * Gets the subject templates for wrong name selected emails.
   * @returns {Array} - The array of subject templates.
   */
  static getWrongPunterSubjects() {
    return [
      `🤔 Oops, ${TemplateUtils.REGISTERED_PUNTER_NAME}! Did You Pick the Wrong Name '${TemplateUtils.SELECTED_PUNTER_NAME}'?`,
      `🤔 Uh-oh, ${TemplateUtils.REGISTERED_PUNTER_NAME}! Name Selection Mix-up '${TemplateUtils.SELECTED_PUNTER_NAME}'?`,
      `🤔 Heads Up, ${TemplateUtils.REGISTERED_PUNTER_NAME}! Name Selection Error '${TemplateUtils.SELECTED_PUNTER_NAME}'!`,
      `🤔 Hey ${TemplateUtils.REGISTERED_PUNTER_NAME}, Did You Mean to Choose '${TemplateUtils.SELECTED_PUNTER_NAME}'?`,
      `🤔 Notice: ${TemplateUtils.REGISTERED_PUNTER_NAME}, Possible Name Selection Error '${TemplateUtils.SELECTED_PUNTER_NAME}'!`,
    ];
  }

  /**
   * Gets the body templates for wrong name selected emails.
   * @returns {Array} - The array of body templates.
   */
  static getWrongPunterBodies() {
    return [
      `Hey ${TemplateUtils.REGISTERED_PUNTER_NAME}! 👀 <p/>It looks like you might have selected the wrong punter name '${TemplateUtils.SELECTED_PUNTER_NAME}' for our ${TemplateUtils.FORM_TITLE} form. If this was accidental, please let the admins know. If you indeed meant to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}', we have cc'd him to let him know about it! 🔍`,
      `Whoa, ${TemplateUtils.REGISTERED_PUNTER_NAME}! 🕵️‍♂️ <p/>It seems you selected the wrong punter name '${TemplateUtils.SELECTED_PUNTER_NAME}' for our ${TemplateUtils.FORM_TITLE} form. If this was a mistake, please inform the admins. If you meant to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}', we've cc'd him to notify him about the submission! 🚫`,
      `Attention ${TemplateUtils.REGISTERED_PUNTER_NAME}! 🚨 <p/>There seems to be a mix-up with the punter name '${TemplateUtils.SELECTED_PUNTER_NAME}' you selected for our ${TemplateUtils.FORM_TITLE} form. If this was unintentional, please let the admins know. If you meant to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}', we've cc'd him to keep him in the loop! 🕵️‍♀️`,
      `Hey ${TemplateUtils.REGISTERED_PUNTER_NAME}! 📧 <p/>It looks like there might be an error with the punter name '${TemplateUtils.SELECTED_PUNTER_NAME}' you chose for our ${TemplateUtils.FORM_TITLE} form. If this was an accident, please contact the admins. If you meant to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}', we've cc'd him to inform him! ❓`,
      `Hold onto your hats, ${TemplateUtils.REGISTERED_PUNTER_NAME}! 🎩 <p/>It appears you may have picked the wrong punter name '${TemplateUtils.SELECTED_PUNTER_NAME}' for our ${TemplateUtils.FORM_TITLE} form. If this was by mistake, please notify the admins. If you intended to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}', we've cc'd him to let him know! 🤔`,
    ];
  }

  /**
   * Gets the subject templates for unregistered person with punter selected emails.
   * @returns {Array} - The array of subject templates.
   */
  static getUnknownEmailWithPunterSubjects() {
    return [
      `🕵️‍♂️ Alert! Unregistered Email '${TemplateUtils.SUBMITTED_EMAIL}'\"' Used for ${TemplateUtils.SELECTED_PUNTER_NAME}!`,
      `🕵️‍♂️ Suspicious Activity Detected: Unregistered Email '${TemplateUtils.SUBMITTED_EMAIL}' for ${TemplateUtils.SELECTED_PUNTER_NAME}`,
      `🕵️‍♂️ Warning! Unknown Email '${TemplateUtils.SUBMITTED_EMAIL}' Submitted for ${TemplateUtils.SELECTED_PUNTER_NAME}`,
      `🕵️‍♂️ Heads Up! Unrecognized Email '${TemplateUtils.SUBMITTED_EMAIL}' Attempt for ${TemplateUtils.SELECTED_PUNTER_NAME}`,
      `🕵️‍♂️ Notice: Unregistered Email '${TemplateUtils.SUBMITTED_EMAIL}' Submission for ${TemplateUtils.SELECTED_PUNTER_NAME}!`,
    ];
  }

  /**
   * Gets the body templates for unregistered person with punter selected emails.
   * @returns {Array} - The array of body templates.
   */
  static getUnknownEmailWithPunterBodies() {
    return [
      `Alert! 🚨 <p/>The email '${TemplateUtils.SUBMITTED_EMAIL}' you used to submit our ${TemplateUtils.FORM_TITLE} form is not registered in our app. Are you using another email to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}'? If so, please inform the admins and confirm if you will be using this email in the future so we can update your registered emails. 🔍`,
      `Heads up! 🚨 <p/>The email '${TemplateUtils.SUBMITTED_EMAIL}' used to submit our ${TemplateUtils.FORM_TITLE} form isn't registered in our app. Are you submitting for '${TemplateUtils.SELECTED_PUNTER_NAME}' with a different email? Please let the admins know and confirm if you'll use this email in the future so we can update your records. 🚫`,
      `Warning! 🚨 <p/>You submitted our ${TemplateUtils.FORM_TITLE} form using the unregistered email '${TemplateUtils.SUBMITTED_EMAIL}'. Are you using this email to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}'? If so, please inform the admins and confirm if this will be your future email so we can update your registered emails. 🕵️‍♀️`,
      `Caution! 🚨 <p/>The email '${TemplateUtils.SUBMITTED_EMAIL}' you used for our ${TemplateUtils.FORM_TITLE} form isn't registered in our app. Are you using another email to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}'? Please notify the admins and confirm if this email will be used in the future so we can update your registration. ❓`,
      `Notice! 🚨 <p/>The email '${TemplateUtils.SUBMITTED_EMAIL}' you submitted with for our ${TemplateUtils.FORM_TITLE} form is not in our app's registry. Are you using a different email to submit for '${TemplateUtils.SELECTED_PUNTER_NAME}'? If so, please inform the admins and confirm if this will be your email going forward so we can update our records. 🤔`
    ];
  }
}

/**
 * Represents a utility class for sending reminder emails to punters.
 */
class EmailReminders {
  /**
   * Sends reminder emails to a list of punters.
   * 
   * @param {Array} puntersNameList - The list of punter names.
   * @param {string} matchDate - The match date.
   */
  static sendEmails(puntersNameList, matchDate) {
    const reminderSubjectTemplates = EmailReminders.getReminderSubjectTemplates();
    const reminderEmailTemplates = EmailReminders.getReminderEmailTemplates();

    let selectedSubjectTemplate = TemplateUtils.getRandomValueFromTemplate(reminderSubjectTemplates);
    let selectedEmailTemplate = TemplateUtils.getRandomValueFromTemplate(reminderEmailTemplates);
    let formUrl = getDailyBetsPublishedURLLink_();

    for (let i = 0; i < puntersNameList.length; i++) {
      let punterName = puntersNameList[i];

      const placeholders = [
        { placeholder: TemplateUtils.PUNTER_NAME, value: punterName },
        { placeholder: TemplateUtils.MATCH_DATE, value: matchDate },
        { placeholder: TemplateUtils.FORM_URL, value: formUrl }
      ];

      let subject = TemplateUtils.replacePlaceholders(selectedSubjectTemplate, placeholders);
      let body = TemplateUtils.replacePlaceholders(selectedEmailTemplate, placeholders);

      EmailUtils.sendEmail(PuntersUtils.getPunterEmail_(punterName), subject, body);
    }
  }

  /**
   * Gets the subject templates for reminder emails.
   * 
   * @returns {Array} - The array of subject templates.
   */
  static getReminderSubjectTemplates() {
    return [
      `⏰ Time's up, ${TemplateUtils.PUNTER_NAME}! Submit your bets now or face the consequences!`,
      `🔔 Get it together, ${TemplateUtils.PUNTER_NAME}! Don't fall behind in the bets game!`,
      `⚠️ Bets deadline alert: ${TemplateUtils.PUNTER_NAME}! Your bets are due!`,
      `📢 Last chance, ${TemplateUtils.PUNTER_NAME}! Don't miss out on the bets fun!`,
      `🎯 Are you ready for bets action, ${TemplateUtils.PUNTER_NAME}?`,
      `⏳ Bets time is running out! Submit your bets, ${TemplateUtils.PUNTER_NAME}!`,
      `😤 Don't be a slacker, ${TemplateUtils.PUNTER_NAME}! Submit your bets now!`,
      `⏱️ The clock is ticking, ${TemplateUtils.PUNTER_NAME}! Submit your bets before it's too late!`,
      `🏃 Get your act together, ${TemplateUtils.PUNTER_NAME}! Bets deadline approaching!`
    ];
  }

  /**
   * Gets the email templates for reminder emails.
   * 
   * @returns {Array} - The array of email templates.
   */
  static getReminderEmailTemplates() {
    return [
      `Hey ${TemplateUtils.PUNTER_NAME}! 👋<p/>Submit your bets for the upcoming matches on ${TemplateUtils.MATCH_DATE}! ⚽ Time's ticking! <p/>Here's the link: ${TemplateUtils.FORM_URL}. <p/>Good luck! 🍀`,
      `Hey ${TemplateUtils.PUNTER_NAME}! 📢<p/>You haven't submitted bets for the matches on ${TemplateUtils.MATCH_DATE} yet. ⏰ Act fast! <p/>Link: ${TemplateUtils.FORM_URL}. <p/>The bets throne awaits! ⌛️`,
      `Greetings ${TemplateUtils.PUNTER_NAME}! 🎩<p/>Matches on ${TemplateUtils.MATCH_DATE} are waiting for your bets! 🏀 You've got less than an hour! <p/>Link: ${TemplateUtils.FORM_URL}. <p/>Happy punting! 🎉`,
      `Hey ${TemplateUtils.PUNTER_NAME}! ⏳<p/>Still no bets for matches on ${TemplateUtils.MATCH_DATE}? ⚠️ Get moving! <p/>Link: ${TemplateUtils.FORM_URL}. <p/>Tick-tock! ⌛️`,
      `Listen up ${TemplateUtils.PUNTER_NAME}! 📣<p/>Deadline for bets on ${TemplateUtils.MATCH_DATE} is near! ⏱️ Don't miss out! <p/>Link: ${TemplateUtils.FORM_URL}. <p/>Last chance! ⏰`
    ];
  }
}

/**
 * Represents a utility class for sending email notifications to punters after their bets have been auto-submitted.
 */
class EmailAutoSubmission {

  /**
   * Sends email notifications to punters after their bets have been auto-submitted.
   * 
   * @param {Map} betsMap - Map containing the bets of all punters.
   * @param {string} matchDate - The date of the matches for which bets were submitted.
   * @return {void}
   */
  static sendEmails(betsMap, matchDate) {
    if (betsMap) {
      for (const [punterName, bets] of betsMap.entries()) {
        const subject = `🤖 Bets Auto-Submitted for ${matchDate}`;
        const body = `Hello ${punterName}! 👋,<p/>Your bets for the matches on ${matchDate} have been automatically submitted by the bot. Thank you for participating in the bets game! <p/>Here are your entries: <p/>${EmailUtils.formatBets(bets)}.<p/>Best regards, Your Bets Team`;

        // Replace the placeholder with the actual punter's email address
        const punterEmail = PuntersUtils.getPunterEmail_(punterName);
        if (isNotEmpty_(punterEmail)) {
          // Send the email
          EmailUtils.sendEmail(punterEmail, subject, body);
        } else {
          log(`Email not found for Punter '${punterName}'`, LogLevel.WARN);
        }
      }
    }
  }
}

/**
 * Represents a utility class for sending emails related to match result completions.
 */
class EmailResultCompletion {
  /**
   * Sends an email to a list of recipients when the results for a match are completed.
   * 
   * @param {Object} e - The event object passed to the result completion trigger function.
   * @param {Object} sheet - The sheet containing the completed match data.
   * @param {string} matchID - The ID of the completed match.
   */
  static sendEmail(e, sheet, matchID) {
    const subject = `✅ Match [#${matchID}] Results Completed`;

    const leaderboardStandings = getDashboardLeaderboardTable_();
    const lastMatchPoints = getDashboardLastMatchPointsTable_(sheet);
    const matchResults = getMatchDataTable_(sheet);

    const body = `Greetings 📢,<p/>The results for match ${ScheduleUtils.getMatchTitle(matchID)} have been completed and are now available on the ${getDashboardSheetLink_()} spreadsheet. Below are the latest leaderboard standings and details of the completed match:<p/><b>Latest Leaderboard Standings:</b> 📊<br/>${leaderboardStandings}<b>Match Points Summary:</b>${lastMatchPoints}<br/><b>Match Results:</b> 📝<br/>${matchResults} 🎉`;

    EmailUtils.sendEmail(EmailResultCompletion.getEmailListOnResultCompletion_(), subject, body);
  }

  /**
   * Retrieves the list of email recipients for the result completion email.
   * 
   * @returns {string[]} - The list of email recipients.
   */
  static getEmailListOnResultCompletion_() {
    return EmailUtils.getAllAdminsEmailList();
  }
}

/**
 * Represents a utility class for sending emails related to Trump Card usage.
 */
class EmailTrumpCardUsage {
  /**
   * Sends emails to punters with failed attempts using the provided email template.
   *
   * @param {Object} failedAttempts - Object containing failed attempts data.
   */
  static sendEmailsOnFailedAttempt(e, failedAttempts) {
    for (const punterName in failedAttempts) {
      const punterEmail = PuntersUtils.getPunterEmail_(punterName);

      if (isNotEmpty_(punterEmail)) {
        const subject = `🚨 Code Red Alert! ${punterName}'s Trump Card Antics Unveiled!`;
        const body = EmailTrumpCardUsage.getBodyTemplate(
          punterName,
          failedAttempts[punterName].failedAttemptMatchIDs,
          failedAttempts[punterName].usedMatchIDs
        );

        EmailUtils.sendEmail(punterEmail, subject, body);
      }
    }
  }

  /**
   * Generate a funny email template for punters with placeholders for name, failed match ids, and used match ids. 🏏
   *
   * @param {string} name - The punter's name.
   * @param {Array} failedMatchIDs - The array of failed match IDs.
   * @param {Array} usedMatchIDs - The array of used match IDs.
   * @returns {string} - The generated email template.
   */
  static getBodyTemplate(name, failedMatchIDs, usedMatchIDs) {
    return `Dear ${name},<p/>
    Oops! It seems you tried to play your trump card in matches with IDs ${failedMatchIDs.join(', ') || 'None'}, but the cricket gods had other plans! Fear not, though. You've already unleashed the trump card magic in matches with IDs ${usedMatchIDs.join(', ') || 'None'}.<p/>
    Just a heads up, any failed attempts with the mentioned IDs are being ignored. Remember, cricket is a game of strategy, not just a numbers game.<p/>
    Note: You can only use the trump card in a maximum of ${ConfigManager.getConfigClass().TrumpCardConfig.MAX_LIMIT} matches.`;
  }
}

/**
 * Represents a utility class for managing emails.
 */
class EmailUtils {
  /**
   * Sends an email.
   * @param {string|string[]} recipients - The recipient(s) of the email.
   * @param {string} subject - The subject of the email.
   * @param {string} body - The body of the email.
   * @param {boolean} [ccAdmins=true] - Whether to CC admins or not. Default is true.
   */
  static sendEmail(recipients, subject, body, ccAdmins = true) {
    if (isNotProdEnv_()) {
      console.log(`NON PROD ENV: Replacing '${recipients}' with '${EmailUtils.getActiveUserEmail()}', Subject: '${subject}'`);
      recipients = EmailUtils.getActiveUserEmail();

      if (!EnvConfigClass.isSendingEmailsFromNonProd()) {
        console.log(`NON PROD ENV: Not sending emails to '${recipients}', Subject: '${subject}'`);
        console.log(body)
        return;
      }
    }

    subject = EmailUtils.toSubject(subject);
    const textBody = EmailUtils.toTextBody(body);
    const htmlBody = EmailUtils.toHtmlBody(body);

    let ccList = ccAdmins ? EmailUtils.getAllAdminsEmailList() : EmailUtils.getActiveUserEmail();

    console.log(`Sending email - Recipients: ${recipients}, CC: ${ccList} Subject: ${subject}`);

    const emailData = {
      to: recipients,
      cc: ccList,
      emailFrom: EmailUtils.getActiveUserEmail(),
      nameFrom: EmailUtils.getSenderDisplayName(),
      subject: subject,
      textBody: textBody,
      htmlBody: htmlBody,
    };

    try {
      const rawEmail = EmailUtils.convertEmailData(emailData);
      Gmail.Users.Messages.send({ raw: rawEmail }, "me");
    } catch (error) {
      log(`Gmail.Users.Messages.send: error occurred: ${error}`, LogLevel.ERROR);
    }
  }

  /**
   * Converts email data into a MIME message suitable for sending via Gmail API.
   * Ref: https://gist.github.com/tanaikech/187863d97d2b5e60938d8316574a2850
   * 
   * @param {Object} data - Email data.
   * @param {string} data.to - Email recipient.
   * @param {string} data.cc - CC recipient.
   * @param {string} [data.emailFrom] - Sender's email address.
   * @param {string} [data.nameFrom] - Sender's name.
   * @param {string} data.subject - Email subject.
   * @param {string} data.textBody - Plain text body.
   * @param {string} data.htmlBody - HTML body.
   * @returns {string} Base64 encoded MIME message.
   */
  static convertEmailData({ to, cc, emailFrom, nameFrom, subject, textBody, htmlBody }) {
    const boundary = "boundaryboundary";

    const mailData = [];
    mailData.push(`MIME-Version: 1.0`);
    mailData.push(`To: ${to}`);
    if (isNotEmpty_(cc)) {
      mailData.push(`Cc: ${cc}`);
    }
    if (nameFrom && emailFrom) {
      mailData.push(`From: "${nameFrom}" <${emailFrom}>`);
    }
    mailData.push(`Subject: =?UTF-8?B?${Utilities.base64Encode(
      subject,
      Utilities.Charset.UTF_8
    )}?=`);
    mailData.push(`Content-Type: multipart/alternative; boundary=${boundary}`);
    mailData.push(``);
    mailData.push(`--${boundary}`);
    mailData.push(`Content-Type: text/plain; charset=UTF-8`);
    mailData.push(``);
    mailData.push(textBody);
    mailData.push(``);
    mailData.push(`--${boundary}`);
    mailData.push(`Content-Type: text/html; charset=UTF-8`);
    mailData.push(`Content-Transfer-Encoding: base64`);
    mailData.push(``);
    mailData.push(Utilities.base64Encode(htmlBody, Utilities.Charset.UTF_8));
    mailData.push(``);
    mailData.push(`--${boundary}--`);

    const mailString = mailData.join("\r\n");

    return Utilities.base64EncodeWebSafe(mailString);
  }

  /**
   * Appends the environment and tournament name to the given subject.
   *
   * @param {string} subject - The subject to be appended with the environment and tournament name.
   * @returns {string} The subject appended with the environment and tournament name.
   */
  static toSubject(subject) {
    return `${appendEnvAndTournamentName_(" " + subject)}`;
  }

  /**
   * Wraps the given body in HTML and adds a footer with the active tournament name and the timestamp at which
   * the email is sent.
   *
   * @param {string} body - The body of the email.
   * @returns {string} The body of the email wrapped in HTML and appended with a footer.
   */
  static toHtmlBody(body) {
    return `<div style='font-family: ${FontConstants.FONT_TREBUCHET_MS};'>
              ${body}
              <p/><br/>
              This email was automatically generated by the ${EmailUtils.getSenderDisplayName()} app.
              <p/>
              Thank you!<br/>
              Sent at: ${getCurrentLongTimesptamp_()} MST
            </div>`;
  }

  static toTextBody(body) {
    return `${body}
              \n\n
              This email was automatically generated by the ${EmailUtils.getSenderDisplayName()} app.
              \n\n
              Thank you!\n
              Sent at: ${getCurrentLongTimesptamp_()} MST`;
  }

  /**
   * Returns the sender display name for the email.
   * 
   * @returns {string} The sender display name.
   */
  static getSenderDisplayName() {
    return `${EnvConfigClass.getEnvDefaultConfigGroup()} ${getActiveSeriesName_()} ${spreadsheetFileNameSuffix}`;
  }

  /**
   * Get active user email
   */
  static getActiveUserEmail() {
    // Get the email address of the currently authenticated user
    return Session.getActiveUser().getEmail();
  }

  /**
   * Gets Email recipients name from env properties
   */
  static getAllAdminsEmailList() {
    const adminEmailsSet = EmailUtils.getAdminEmails();

    const additionalAdmins = EnvConfigClass.getEnvAdditionalAdminEmailList();
    if (isNotEmpty_(additionalAdmins)) {
      additionalAdmins.forEach(email => adminEmailsSet.add(email));
    }

    return Array.from(adminEmailsSet);
  }

  /**
   * Gets the email list of all admins (owner and editors) of the current Google Spreadsheet.
   *
   * @return {string[]} An array of email addresses of all admins.
   */
  static getAdminEmails() {
    const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
    const adminEmailsSet = new Set([spreadsheet.getOwner().getEmail()]);
    spreadsheet.getEditors().forEach(editor => adminEmailsSet.add(editor.getEmail()));

    return adminEmailsSet;
  }

  /**
   * Formats bets into a string with each non-null entry separated by '<br/>'.
   * 
   * @param {Array<any>} bets - The bets to format.
   * @returns {string} The formatted bets string.
   */
  static formatBets(bets) {
    if (!Array.isArray(bets)) {
      return bets;
    }

    const betsLog = bets
      .filter(entry => isNotEmpty_(entry)) // Remove empty entries
      .map(entry => JSON.stringify(entry)) // Convert each entry to JSON string
      .join("<br/>"); // Join entries with '<br/>'

    return betsLog;
  }

  /**
   * Sends a test email to the active user.
   */
  static sendTestEmail() {
    var recipient = EmailUtils.getActiveUserEmail();
    var subject = 'Test Email from Google Apps Script';
    var body = 'Hello,\n\nThis is a test email sent using Google Apps Script.\n\nBest regards,\nYour Name';

    EmailUtils.sendEmail(recipient, subject, body)
  }
}

/**
 * Utility class for handling placeholders in strings.
 */
class TemplateUtils {

  static get SUBMITTED_EMAIL() { return '%%submittedEmail%%'; }
  static get FORM_TITLE() { return '%%formTitle%%'; }
  static get FORM_URL() { return '%%formUrl%%'; }
  static get PUNTER_NAME() { return '%%punterName%%'; }
  static get REGISTERED_PUNTER_NAME() { return '%%registeredPunterName%%'; }
  static get SELECTED_PUNTER_NAME() { return '%%selectedPunterName%%'; }
  static get MATCH_DATE() { return '%%matchDate%%'; }

  /**
   * Replaces placeholders in a string with provided values.
   * @param {string} string - The string containing placeholders.
   * @param {Array} placeHoldersList - List of placeholder-value pairs.
   * @returns {string} The string with replaced placeholders.
   */
  static replacePlaceholders(string, placeHoldersList) {
    let result = string;
    placeHoldersList.forEach(({ placeholder, value }) => {
      const regex = new RegExp(placeholder, 'g');
      result = result.replace(regex, value);
    });
    return result;
  }

  /**
   * Retrieves a random value from an array.
   * 
   * @param {Array} templateArr - The array from which to retrieve a random value.
   * @returns {*} A random value from the input array, or null if the input is not a valid non-empty array.
   */
  static getRandomValueFromTemplate(templateArr) {
    const isArrayAndNotNull = Array.isArray(templateArr) && templateArr.length > 0;

    // Check if input is an array and not null
    if (!isArrayAndNotNull) {
      log("Input is not a valid non-empty array.", LogLevel.WARN);
      return '';
    }

    // Get a random index within the array length
    const randomIndex = Math.floor(Math.random() * templateArr.length);

    // Return the value at the random index
    return templateArr[randomIndex];
  }
}
