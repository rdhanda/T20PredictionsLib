// ScorecardUtils.js //

/**
 * Retrieves the full scorecard URL based on the given match ID.
 * @param {string} matchID - The match ID.
 * @returns {string} The full scorecard URL.
 */
function getFullScorecardURL_(matchID) {
  if (isEmpty_(matchID)) { return; }

  let url = ScheduleUtils.getScorecardURL(matchID).replace(URLs.MATCH_PREVIEW_URL_PART, URLs.FULL_SCORECARD_URL_PART).replace(URLs.LIVE_SCORECARD_URL_PART, URLs.FULL_SCORECARD_URL_PART);
  return appendBaseURL_(url);
}

/**
 * 
 */
function getLiveScorecardURL_(matchID) {
  if (isEmpty_(matchID)) { return; }

  let url = ScheduleUtils.getScorecardURL(matchID).replace(URLs.MATCH_PREVIEW_URL_PART, URLs.LIVE_SCORECARD_URL_PART);
  return appendBaseURL_(url);
}

/**
 * Sanitizes the player names in an array of player data.
 * @param {Array<Array<any>>} playerData - The array of player data.
 * @param {number} nameIndex - The index of the player name in each row.
 * @returns {Array<Array<any>>} The sanitized array of player data.
 */
function sanitizePlayerData_(playerData, nameIndex = 0) {
  if (isEmpty_(playerData)) { return; }

  return playerData.map((row) => {
    const sanitizedRow = [...row];
    if (sanitizedRow[nameIndex]) {
      sanitizedRow[nameIndex] = sanitizePlayerName_(sanitizedRow[nameIndex]);
    }
    return sanitizedRow;
  });
}

/**
 * Finds the players who achieved the maximum value from a specific index in an array of arrays.
 * @param {Array<Array<any>>} data - The array of arrays containing the data.
 * @param {number} index - The index to find the maximum value.
 * @param {number} valueGreaterThan - The max value should be greater than.
 * @returns {Object} An object with the maximum value and the players who achieved it.
 *                  The object has the following structure:
 *                  {
 *                    max: number,
 *                    players: Array<string>
 *                  }
 */
function findPlayersWithMaxValue_(data, index, valueGreaterThan) {
  if (!Array.isArray(data) || data.length === 0) { return {}; }

  let maxValue = -Infinity;
  const playersSet = new Set();
  data.forEach(inningData => {
    const value = Number(inningData[index]);
    if (!isNaN(value) && value >= maxValue && value > valueGreaterThan) {
      if (value > maxValue) {
        maxValue = value;
        playersSet.clear();
      }
      playersSet.add(inningData[0]);
    }
  });

  const players = [...playersSet].sort();
  const display = getDisplayList_([...players, maxValue]);

  return players.length === 0 ? {} : { players, value: maxValue, display };
}

/**
 * Finds players with the minimum value at the specified index in the given data array.
 * @param {Array} data - The data array.
 * @param {number} index - The index to compare values.
 * @returns {Object} - An object containing the players, the minimum value, display string, and the row.
 */
function findPlayersWithMinValue_(data, index) {
  if (!Array.isArray(data) || data.length === 0) { return {}; }

  let minValue = Infinity;
  const playersSet = new Set();
  data.forEach((inningData) => {
    const value = Number(inningData[index]);
    if (!isNaN(value) && value <= minValue) {
      if (value < minValue) {
        minValue = value;
        playersSet.clear();
      }
      playersSet.add(inningData[0]);
    }
  });

  const players = [...playersSet].sort();
  const display = getDisplayList_([...players, minValue]);

  return players.length === 0 ? {} : { players, value: minValue, display };
}

/**
 * Extracts the winning team from an input string containing toss information.
 *
 * @param {string} inputString - The input string containing toss information.
 * @returns {string} The winning team name.
 *
 * @example
 * const inputString1 = "Delhi Capitals, elected to field first";
 * const teamName1 = extractTossWinningTeam_(inputString1);
 * console.log(teamName1); // Output: "Delhi Capitals"
 *
 * const inputString2 = "Delhi Capitals chose to field first";
 * const teamName2 = extractTossWinningTeam_(inputString2);
 * console.log(teamName2); // Output: "Delhi Capitals"
 */
function extractTossWinningTeam_(inputString) {
  if (isEmpty_(inputString)) { return; }

  const regex = /^(?:.*?,\s*)?([^,]+)(?:, elected| chose)\s+/i;
  const match = inputString.match(regex);
  const teamName = match ? match[1].trim() : '';

  return teamName;
}

/**
 * Extracts winning team details from a match result string.
 *
 * @param {string} input - The input string containing match result information.
 *   Examples:
 *   - 'S Africa (W) won by 8 wickets (with 28 balls remaining)'
 *   - 'S Africa won by 1 run (with 5 balls remaining)'
 *   - 'Match tied'
 *   - 'Match tied (DLS method)'
 *   - 'Match tied (Oman won the Super Over)'
 *   - 'Match tied (India (W) won the one-over eliminator)'
 *   - 'Match tied (India won the one-over eliminator)'
 *
 * @returns {Object} - An object containing the parsed match result information.
 *   Example:
 *   {
 *     winningTeamName: 'S Africa (W)',
 *     winningMarginRuns: null,
 *     winningMarginWickets: 8,
 *   }
 *   If the match is tied and there is a winning team,
 *   the object will be 
 *   { 
 *     winningTeamName: 'Oman',
 *     winningMarginRuns: 0,
 *     winningMarginWickets: null,
 *   }.
 *   If the match is tied with no winner, the object will be { winningTeamName: 'TIED' }.
 */
function extractWinningTeamDetails_(input) {
  var result = {};

  // Extract relevant information using regular expressions for tied matches
  var matchInfo = input.match(/Match tied(?: \(([^()]+(?:\([^()]+\)[^()]*)*) won[^)]*\))?/);

  if (matchInfo) {
    result.winningTeamName = matchInfo[1] ? matchInfo[1].trim() : MATCH_RESULT_TIED;
    result.winningMarginRuns = 0;
    result.winningMarginWickets = null;
    return result;
  }

  // Extract relevant information using regular expressions for regular match results
  matchInfo = input.match(/(.+?)\swon\sby\s(\d+)\s(run|wicket)/);

  if (matchInfo) {
    result.winningTeamName = matchInfo[1].trim();
    result.winningMarginRuns = matchInfo[3] === "run" ? parseInt(matchInfo[2], 10) : null;
    result.winningMarginWickets = matchInfo[3] === "wicket" ? parseInt(matchInfo[2], 10) : null;
  }

  return result;
}

/**
 * Extracts the number of wickets from the given fall of wickets string.
 * @param {string} fallOfWickets - The fall of wickets string.
 * @returns {number} The number of wickets fell.
 */
function extractNumberOfWickets_(fallOfWickets) {
  if (isEmpty_(fallOfWickets)) { return 0; }

  var regex = /\d+-\d+/g;
  var matches = fallOfWickets.match(regex);
  var numberOfWicketsFell = matches ? matches.length : 0;

  return numberOfWicketsFell;
}

/**
 * Converts the input of cricket overs to the total number of overs with decimal value.
 *
 * @param {string|number} cricketOvers - The input of cricket overs (e.g., "9.3", 9.1).
 * @returns {number} The total number of overs with decimal value (e.g. 9.5, 9.166666667).
 */
function convertToDecimalOvers_(cricketOvers) {
  if (isEmpty_(cricketOvers)) { return 0; }

  const overs = String(cricketOvers).trim();

  let [completedOvers, ballsInNextOver] = overs.split('.').map(parseFloat);

  if (!isValidNumber_(completedOvers)) {
    completedOvers = 0;
  }
  if (!isValidNumber_(ballsInNextOver)) {
    ballsInNextOver = 0;
  } else if (ballsInNextOver > 6) {
    const additionalOvers = Math.floor(ballsInNextOver / 6);
    completedOvers += additionalOvers;
    ballsInNextOver %= 6;
  }

  return (completedOvers || 0) + (ballsInNextOver || 0) / 6;
}

/**
 * Extracts the overs target value from a given target string.
 * 
 * @param {string} target - The target string containing the overs value.
 * @returns {number | undefined} The extracted overs target value, or undefined if the target is empty or no match is found.
 */
function extractOversTarget_(target) {
  if (isEmpty_(target)) { return; }

  const regex = /\/([\d.]+)/;
  //const regex = /.*\/(-*\d*\.?\d+)/;//Equivalent of used in S1 sheet
  const match = target.match(regex);
  return match && parseFloat(match[1]);
}

/**
 * Extracts the team name and the number of runs needed to win from the given input string.
 * @param {string} matchRunningStatus - The input string containing the information about runs needed to win.
 * @returns {Object|null} An object containing the team name and runs needed to win, or null if the input does not contain the required information.
 * @example
 * var matchRunningStatus = "Australia need 34 runs from 19 overs.";
 * var result = extractTeamAndRunsNeededToWin_(matchRunningStatus);
 * Logger.log("Team: " + result.teamName + ", Runs needed to win: " + result.runsNeeded);
 */
function extractTeamAndRunsNeededToWin_(matchRunningStatus) {
  var match = /(.+?) need (\d+) run/i.exec(matchRunningStatus);
  if (match) {
    var teamName = getTeamIdByName_(match[1].trim()); // Trim to remove any leading/trailing whitespace
    var runsNeeded = parseInt(match[2]);
    return { teamName: teamName, runsNeeded: runsNeeded };
  } else {
    return {};
  }
}

/**
 * Extracts the number of overs done and the total number of overs from the input string.
 * @param {string} inputString - The input string containing the overs information.
 * @returns {Object} An object with the properties 'oversDone' and 'totalOvers'.
 * @example
 * var inputString = "(42.5/43 ov, T:246)";
 * var oversInfo = extractOversInfo(inputString);
 * Logger.log("Overs done: " + oversInfo.oversDone);
 * Logger.log("Total overs: " + oversInfo.totalOvers);
 */
function extractOversInfo_(inputString) {
  var regex = /\(([\d.]+)\/([\d.]+) ov/;
  var match = regex.exec(inputString);
  if (match) {
    var oversDone = parseFloat(match[1]);
    var totalOvers = parseFloat(match[2]);
    return {
      oversDone: oversDone,
      totalOvers: totalOvers
    };
  } else {
    return {
      oversDone: undefined,
      totalOvers: undefined
    };
  }
}

/**
 * Retrieves the scorecard status from the given content.
 *
 * @param {string} $ - The content to extract the scorecard status from.
 * @returns {string} The scorecard status.
 */
function getScorecardStatus_($) {
  const cssSelector = "strong.ds-text-tight-m";//strong
  return extractElementsWithCSSSelector_($, cssSelector).toString();
}

/**
 * Sample Content:
 * 
<div class="ds-flex ds-flex-col ds-mt-3 md:ds-mt-0 ds-mt-0 ds-mb-1">
    <div class="ci-team-score ds-flex ds-justify-between ds-items-center ds-text-typo ds-opacity-50 ds-mb-2">
        <div class="ds-flex ds-items-center ds-min-w-0 ds-mr-1" title="India">
            <img width="32" height="32" alt="India Flag" class="ds-mr-2" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_80/lsci/db/PICTURES/CMS/313100/313128.logo.png" style="width: 32px; height: 32px;">
            <a href="/team/india-6" title="India" class="ds-inline-flex ds-items-start ds-leading-none">
                <span class="ds-text-tight-l ds-font-bold ds-text-typo hover:ds-text-typo-primary ds-block ds-truncate">India</span>
            </a>
        </div>
        <div class="ds-text-compact-m ds-text-typo ds-text-right ds-whitespace-nowrap">
            <span class="ds-text-compact-s ds-mr-0.5"></span>
            <strong class="">211</strong>
        </div>
    </div>
    <div class="ci-team-score ds-flex ds-justify-between ds-items-center ds-text-typo ds-mb-2">
        <div class="ds-flex ds-items-center ds-min-w-0 ds-mr-1" title="South Africa">
            <img width="32" height="32" alt="South Africa Flag" class="ds-mr-2" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_80/lsci/db/PICTURES/CMS/313100/313125.logo.png" style="width: 32px; height: 32px;">
            <a href="/team/south-africa-3" title="South Africa" class="ds-inline-flex ds-items-start ds-leading-none">
                <span class="ds-text-tight-l ds-font-bold ds-text-typo hover:ds-text-typo-primary ds-block ds-truncate">South Africa</span>
            </a>
        </div>
        <div class="ds-text-compact-m ds-text-typo ds-text-right ds-whitespace-nowrap fadeIn-exit-done">
            <span class="ds-text-compact-s ds-mr-0.5">(42.3/50 ov, T:212) </span>
            <strong class="">215/2</strong>
        </div>
    </div>
</div>
 *
 * Extracts team scores, target, and maximum overs information from the provided content.
 *
 * @param {string} $ - The content containing team scores and information.
 * India 211
 * South Africa (42.3/50 ov, T:212) 215/2
 * 
 * @returns {{
 *   team1Info: string,
 *   team2Info: string,
 *   display: string,
 *   firstBattingTeam: string,
 *   innings1MaxOvers: number,
 *   secondBattingTeam: string,
 *   innings2MaxOvers: number
 * }} Object containing extracted information.
 */
function getTeamsScoresTargetMaxOvers_($) {
  const rootCssSelector = "div.ds-flex.ds-flex-col.ds-mt-3.md\\:ds-mt-0.ds-mt-0.ds-mb-1 > "

  const cssSelector1 = rootCssSelector + "div:first-child";//First Team Score
  const cssSelector2 = rootCssSelector + "div:nth-child(2)";//Second Team Score and Target
  const cssSelector3 = rootCssSelector + "div:first-child > div > a > span";//First Team Name
  const cssSelector4 = rootCssSelector + "div:nth-child(2) > div > a > span";//Second Team Name

  const team1Info = extractElementsWithCSSSelector_($, cssSelector1).join(' ');
  const team2Info = extractElementsWithCSSSelector_($, cssSelector2).join(' ');
  const firstBattingTeamName = extractElementsWithCSSSelector_($, cssSelector3);
  const firstBattingTeam = getTeamIdByName_(firstBattingTeamName.toString());
  const secondBattingTeamName = extractElementsWithCSSSelector_($, cssSelector4);
  const secondBattingTeam = getTeamIdByName_(secondBattingTeamName.toString());

  //log(`firstBattingTeamName=${firstBattingTeamName}, secondBattingTeamName=${secondBattingTeamName}`)

  const display = `${team1Info}\n${team2Info}`;
  let innings1MaxOvers = extractOversInfo_(team1Info).totalOvers;
  if (isEmpty_(innings1MaxOvers)) { innings1MaxOvers = getMaxOversPerInnings_(); }
  let innings2MaxOvers = extractOversInfo_(team2Info).totalOvers;
  if (isEmpty_(innings2MaxOvers)) { innings2MaxOvers = getMaxOversPerInnings_(); }

  return {
    team1Info,
    team2Info,
    display,
    firstBattingTeam,
    innings1MaxOvers,
    secondBattingTeam,
    innings2MaxOvers,
  };
}

/**
 * Retrieves the match running status from the given content.
 * India need 154 runs from 23.1 overs.
 * 
 * @param {string} $ - The content to extract the match running status from.
 * @returns {string} The match running status.
 */
function getMatchRunningStatus_($) {
  const cssSelector = "p.ds-text-tight-s.ds-font-medium.ds-truncate.ds-text-typo > span";
  return extractElementsWithCSSSelector_($, cssSelector).toString();
}

/**
 * Extracts Player Of The Match (POTM) information from the provided HTML content.
 * @param {CheerioStatic} $ - The Cheerio instance representing the HTML content.
 * @returns {Object} An object containing POTM information.
 */
function extractPOTM_($) {
  const potmString = "Player Of The Match";

  const potmElement = $(`.ds-text-eyebrow-xs:contains('${potmString}')`).parent();
  const potmName = sanitizePlayerName_(potmElement.find('.ds-text-tight-m.ds-font-medium').first().text().trim());
  const potmInfo = potmElement.find('.ds-text-tight-s.ds-font-regular.ds-text-typo-mid1').first().text().trim();

  const display = isNotEmpty_(potmName) ? `${potmName}\n${potmInfo}`.trim() : '';

  return { potm: potmName, display };
}

/**
 * Extracts and formats the current run rate (CRR) and required run rate (RRR) status from the provided content.
 *
 * @param {string} $ - The content containing run rate information.
 * Current RR:4.16 Required RR:6.64 Last 5 ov(RR):19/1(3.80) 
 * 
 * @returns {string} Formatted run rate status (CRR and RRR).
 */
function getCurrentRequiredRunRateStatus_($) {
  //const cssSelector = 'div.ds-text-tight-s.ds-font-regular.ds-overflow-x-auto.ds-scrollbar-hide.ds-whitespace-nowrap';
  const rootCssSelector = 'div.ds-text-tight-s.ds-font-regular.ds-overflow-x-auto.ds-scrollbar-hide.ds-whitespace-nowrap > div.ds-flex > ';

  let runRateStatus = ''

  const cssSelectorDiv1Span1 = rootCssSelector + 'div:nth-child(1) > span:nth-child(1)';
  const div1Span1 = extractElementsWithCSSSelector_($, cssSelectorDiv1Span1).toString().replace(/,/g, "").replace(/•/g, " ");
  if (div1Span1.indexOf("Current RR") != -1) {
    const cssSelectorDiv1Span2 = rootCssSelector + 'div:nth-child(1) > span:nth-child(2)';
    runRateStatus += "\nCRR: " + extractElementsWithCSSSelector_($, cssSelectorDiv1Span2).toString().replace(/,/g, "").replace(/•/g, " ");
  }

  //Now getting Required RR only
  const cssSelector2Div2Span2 = rootCssSelector + 'div:nth-child(2) > span:nth-child(2)';
  const div2Span2 = extractElementsWithCSSSelector_($, cssSelector2Div2Span2).toString().replace(/,/g, "").replace(/•/g, " ");
  if (div2Span2.indexOf("Required RR") != -1) {
    const cssSelectorDiv2Span3 = rootCssSelector + 'div:nth-child(2) > span:nth-child(3)';
    runRateStatus += "\nRRR: " + extractElementsWithCSSSelector_($, cssSelectorDiv2Span3).toString().replace(/,/g, "").replace(/•/g, " ");
  }

  return runRateStatus.trim();
}

/**
 * Extracts first innings score forecast or win probability text from the provided HTML content.
 * Live Forecast: LSG 167
 * Win Probability: SRH 80% LSG 20%
 *
 * @param {string} $ - The HTML content containing live forecast win probability information.
 * @returns {string} The extracted live forecast win probability text.
 */
function getMatchForecast_($) {
  const cssSelector = 'div.ds-flex.ds-items-center.ds-bg-fill-content-alternate.lg\\:ds-p-0';
  const text = extractElementsText_($, cssSelector);

  return text;
}

/**
 * Removes numeric lines and empty lines from an array of strings.
 *
 * @param {string[]} inputArray - The array of strings to process.
 * @returns {string[]} The filtered array with numeric and empty lines removed.
 */
function removeNumericAndEmptyLinesFromArray_(inputArray) {
  if (isEmpty_(inputArray)) {
    return [];
  }
  /**
   * Remove numeric lines and empty lines from a string.
   *
   * @param {string} item - The string item to process.
   * @returns {string} The string with numeric and empty lines removed.
   */
  function filterLines(item) {
    if (isEmpty_(item)) {
      return '';
    }

    if (typeof item === 'string') {
      var lines = item.split('\n');
      var nonNumericLines = lines.filter(function (line) {
        return isNaN(line.trim()) && line.trim() !== '';
      });
      return nonNumericLines.join('\n');
    }
    return item;
  }

  var filteredArray = inputArray.map(filterLines).filter(function (item) {
    return typeof item === 'string' && item.trim() !== '';
  });

  return filteredArray;
}


/**
 * Returns a display list by joining the elements of the array with newline separators.
 *
 * @param {Array} arr - The array of elements to join.
 * @returns {string} The display list string.
 */
function getDisplayList_(arr) {
  if (isEmpty_(arr)) { return; }

  return arr.join("\n").trim();
}

/**
 * Calculates the current level of the array.
 *
 * @param {Array} arr - The array to calculate the level of.
 * @returns {number} The current level of the array.
 */
function getCurrentLevel_(arr) {
  let level = 0;
  while (Array.isArray(arr)) {
    level++;
    arr = arr[0];
  }
  return level;
}

/**
 * Transforms an array to the target level by adding nested layers.
 * If the array is already at or below the target level, it is returned as is.
 *
 * @param {Array} array - The array to transform.
 * @param {number} targetLevel - The target level to transform the array to.
 * @returns {Array} The transformed array at the target level.
 */
function transformArrayToLevel_(array, targetLevel) {
  const currentLevel = getCurrentLevel_(array);

  if (targetLevel <= currentLevel) { return array; }

  let result = array;
  for (let i = currentLevel; i < targetLevel; i++) {
    result = [result];
  }

  return result;
}

/**
 * Calculates the number of runs conceded by a bowler based on the given overs and economy rate.
 *
 * @param {number} overs - The number of overs bowled.
 * @param {number} economy - The economy rate of the bowler.
 * @returns {number} The calculated number of runs conceded by the bowler.
 */
function calculateBowlerRunsConcededInMaxOvers_(economy) {
  if (isNaN(economy) || economy <= 0) { return; }

  // Calculate the total runs conceded
  const runs = Math.round(getMaxOversPerBowler_() * economy);
  return runs;
}

/**
 * Safely concatenates arrays, handling null or undefined arrays.
 *
 * @param {...Array} arrays - Arrays to be concatenated.
 * @returns {Array} - Concatenated array.
 */
function safeConcatArrays_(...arrays) {
  const mergedArray = [];
  for (const arr of arrays) {
    if (Array.isArray(arr)) {
      mergedArray.push(...arr);
    }
  }
  return mergedArray;
}

/**
 * Safely concatenates the 'players' arrays from multiple objects, handling null objects or missing 'players' property.
 *
 * @param {...Object} playerObjects - Objects containing 'players' arrays.
 * @returns {Array} - Concatenated 'players' array.
 */
function safeConcatPlayers_(...playerObjects) {
  const players = [];
  for (const obj of playerObjects) {
    if (obj && obj.players) {
      players.push(...obj.players);
    }
  }
  return players;
}

/**
 * Retrieves the minimum number of balls required for a batter strike rate.
 * @returns {number} The minimum balls for batter strike rate.
 */
function getMinBallsForBatterSR_() {
  return ConfigManager.getConfigClass().MIN_BALLS_FOR_BATTER_STRIKERATE_VALUES[getActiveSeriesMatchCategory_()];
}

/**
 * Retrieves the minimum number of overs required for a bowler economy.
 * @returns {number} The minimum overs for bowler economy.
 */
function getMinOversForBowlerEconomy_() {
  return ConfigManager.getConfigClass().MIN_OVERS_FOR_BOWLER_ECONOMY_VALUES[getActiveSeriesMatchCategory_()];
}

/**
 * Retrieves the maximum number of overs allowed per bowler.
 * @returns {number} The maximum overs per bowler.
 */
function getMaxOversPerBowler_() {
  return ConfigManager.getConfigClass().MAX_OVERS_PER_BOWLER_VALUES[getActiveSeriesMatchCategory_()];
}

/**
 * Retrieves the maximum number of overs allowed per innings.
 * @returns {number} The maximum overs per innings.
 */
function getMaxOversPerInnings_() {
  return ConfigManager.getConfigClass().MAX_OVERS_PER_INNINGS_VALUES[getActiveSeriesMatchCategory_()];
}
