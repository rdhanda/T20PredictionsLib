//Players.gs

/**
 * Updates the players 
 *
 * @param {Object} e - The event object.
 */
function updatePlayersMenu(e) {
  updatePlayers_(e, null);
}

/**
 * Updates the players sheet with data from squads URL.
 *
 * @param {Object} e - The event object.
 * @param {Array} teams - The array of teams for updating the players, if null/empty, all teams players will be updated.
 */
function updatePlayers_(e, teams) {
  if (!EnvConfigClass.isAutoModeEnabled()) { return; }
  
  console.time(updatePlayers_.name);

  const sheet = getPlayersSheet_();
  activateSheet_(sheet);

  const url = getSquadsHomeURL_();

  if (isNotEmpty_(url)) {
    const $ = getCheerioInstances_(url);

    if (isNotEmpty_($)) {
      const teamAndPlayers = extractTeamPlayers_($, teams);
      doUpdatePlayers_(sheet, teamAndPlayers);

      flushAndWait_();
    }
  }
  console.timeEnd(updatePlayers_.name);
}

/**
 * Extracts the team and player information from the content.
 *
 * @param {object} $ - The Cheerio instance containing the HTML content.
 * @param {Array} teams - The array of teams for updating the players, if null/empty, all teams players will be updated.
 * @returns {Array} - The team and player information.
 */
function extractTeamPlayers_($, teams = []) {
  const teamAndPlayers = [];
  const urls = [];

  const teamDivs = $('div.ds-flex.ds-flex-row.ds-space-x-2:not(.ds-justify-center.ds-items-start)');
  teamDivs.each((index, element) => {
    let teamName = $(element).find('span.ds-text-comfortable-m.ds-text-typo.ds-underline.ds-decoration-ui-stroke').text();
    const teamID = getTeamIdByName_(teamName);
    if (isNotEmpty_(teamID) && (isEmpty_(teams) || teams.includes(teamID))) {
      let url = $(element).find('a').attr('href');
      urls.push(appendBaseURL_(url));
      teamAndPlayers.push({ teamID, url: appendBaseURL_(url), players: [] });
    }
  });

  const cheerioInstances = getCheerioInstances_(urls);

  teamAndPlayers.forEach((team, index) => {
    team.players = extractPlayerNamesFromCheerio_(cheerioInstances[index]);
  });

  teamAndPlayers.sort((a, b) => a.teamID.localeCompare(b.teamID));
  return teamAndPlayers;
}

/**
 * Extracts player names from the given Cheerio instance and excludes players with the "Withdrawn" status.
 *
 * @param {object} $ - The Cheerio instance.
 * @returns {string[]} - The array of player names excluding players with the "Withdrawn" status.
 */
function extractPlayerNamesFromCheerio_($) {
  const players = [];
  if (isEmpty_($)) {
    return players;
  }

  $('.ds-p-0 div div').each((index, element) => {
    const playerType = $(element).prev('div.ds-bg-fill-content-alternate').find('span.ds-text-eyebrow-s.ds-font-bold.ds-uppercase').text().trim();
    const playerNames = $(element).find('div.ds-grid.lg\\:ds-grid-cols-2 span.ds-text-compact-s.ds-font-bold.ds-text-typo.ds-underline.ds-decoration-ui-stroke');

    playerNames.each((index, nameElement) => {
      const playerName = $(nameElement).text().trim();
      const playerDiv = $(nameElement).closest('div.ds-flex.ds-flex-row.ds-justify-between');
      const withdrawnSpan = playerDiv.find('span.ds-text-tight-xs:contains("Withdrawn")');
      if (withdrawnSpan.length === 0) {
        players.push(`${sanitizePlayerName_(playerName)}\n${isNotEmpty_(playerType) ? playerType : PlayerTypes.UNKNOWN_PLAYERS}`);
      }
    });
  });

  return players;
}

/**
 * Updates the players in the sheet based on the provided data.
 *
 * @param {Array} teamAndPlayers - The data containing team information and player names.
 */
function doUpdatePlayers_(sheet, teamAndPlayers) {
  if (isEmpty_(teamAndPlayers)) { return; }

  const range = sheet.getRange(playersSheetObj.sourcePlayersRange);
  const sourceData = range.getValues();
  const updatedTeamIDs = [];

  teamAndPlayers.forEach(function (teamData) {
    const teamId = teamData.teamID;
    const players = teamData.players;

    const column = getColumnNumByTeamID_(teamId);
    if (column !== -1 && players.length >= playersSheetObj.minPlayersCountRequired) {
      const rangeData = players.map(function (player) {
        return [player];
      });

      const sourceDataColumn = column - 2;//IMPORTANT to match the column in array and sheet
      sourceData.forEach(row => {
        row[sourceDataColumn] = undefined;
      });

      rangeData.forEach((value, index) => {
        sourceData[index][sourceDataColumn] = value[0];
      });

      updatedTeamIDs.push(teamId);
    } else {
      log(`Players data will NOT be updated for Team '${teamId}', Column Num '${column}', Players Length '${players.length}'`, LogLevel.WARN);
    }
  });

  console.log(`Updating Players for TeamIDs [${updatedTeamIDs}]`);

  if (isNotEmpty_(updatedTeamIDs)) {
    range.setValues(sourceData);
    updateRefreshCounter_(sheet, playersSheetObj.randomNumCell);

    removePlayersCachedData_();
  }
}

/**
 * Returns the column number based on the team ID.
 *
 * @param {string} teamId - The team ID to search for in the header row.
 * @returns {number} - The column number if team ID is found, -1 otherwise.
 */
function getColumnNumByTeamID_(teamId) {
  const columnIndex = getPlayingTeamIDsCachedData_().indexOf(teamId);
  return columnIndex !== -1 ? columnIndex + 2 : -1;
}

////Data Usage////

/**
 * Get player list in the specified format for a list form.
 *
 * @function
 * @param {string|string[]} teamNames - A single team name or an array of team names.
 * @param {string|string[]} playerTypeCategories - A single player type category or an array of player type categories.
 * @param {boolean} [prefixTeamName=false] - Whether to prefix team names with "--------" separators.
 * @returns {string[]} Output in the specified format.
 */
function getPlayersList_(teamNames, playerTypeCategories, prefixTeamName = false) {
  if (isEmpty_(teamNames)) {
    return NOT_APPLICABLE_ARR;
  }

  const playerData = getPlayerDetails_(teamNames, playerTypeCategories);
  const output = [];

  for (const [team, players] of Object.entries(playerData)) {
    if (prefixTeamName) { output.push(`-------- ${team} --------`); }

    const sortedPlayers = players.map(player => player.playerName).sort();
    output.push(...sortedPlayers);
  }

  return output;
}

/**
 * Get the team name of a player based on the provided player data object.
 *
 * @function
 * @param {Object} playerData - Input player data object.
 * @param {string} playerName - Player name for which to find the team name.
 * @returns {string|null} Team name or null if player not found.
 */
function getTeamNameByPlayer_(playerData, playerName) {
  if (isEmpty_(playerData) || isEmpty_(playerName)) {
    return;
  }

  for (const [team, categories] of Object.entries(playerData)) {
    for (const players of Object.values(categories)) {
      const playerFound = players.some(player => player.playerName === playerName);

      if (playerFound) {
        return team;
      }
    }
  }

  // Return null if player not found in any team
  return null;
}

/**
 * Get player details for specific teams and player types.
 * @function
 * @param {string|string[]} teamNames - A single team name or an array of team names.
 * @param {string|string[]} [playerTypeCategories] - A single player type or an array of player types (optional, if empty, consider all player types).
 * @returns {Object} An object with team names as keys, each containing an array of player details.
 * @example
 * // Call the function with a single teamName and a single playerType
 * var teamPlayerDetailsSingle = getPlayerDetails("AFG", "BATTERS");
 * Logger.log(teamPlayerDetailsSingle);
 * 
 * // Call the function with an array of teamNames and an array of playerTypes
 * var teamPlayerDetailsMultiple = getPlayerDetails(["AFG", "IND"], ["BOWLERS", "BATTERS"]);
 * Logger.log(teamPlayerDetailsMultiple);
 */
function getPlayerDetails_(teamNames, playerTypeCategories = PlayerTypeCategories.ALL_PLAYER_CATEGORIES) {
  const allPlayerData = getPlayersCachedData_();
  const dailyBetsFormExcludedPlayers = getDailyBetsFormExcludedPlayers_(); // Get the list of excluded players from Daily Bets Form
  const result = {};

  // Ensure teamNames is an array
  if (!Array.isArray(teamNames)) { teamNames = [teamNames]; }
  if (!Array.isArray(playerTypeCategories) && isNotEmpty_(playerTypeCategories)) { playerTypeCategories = [playerTypeCategories]; }

  teamNames.forEach(function (teamName) {
    const teamDetails = allPlayerData[teamName];

    if (teamDetails) {
      // Step 1: Get all "source" players
      let teamPlayers = teamDetails.source;

      // Step 2: Exclude players from "excluded"
      teamPlayers = teamPlayers.filter(player => 
        !teamDetails.excluded.find(excludedPlayer => excludedPlayer.playerName === player.playerName)
      );

      // Step 3: Add all "manual" players
      teamPlayers = teamPlayers.concat(teamDetails.manual);

      // Step 4: Filter based on playerTypeCategories if provided
      if (isNotEmpty_(playerTypeCategories)) {
        teamPlayers = teamPlayers.filter(player => playerTypeCategories.includes(player.playerType));
      }

      // Step 5: Exclude players from dailyBetsFormExcludedPlayers
      teamPlayers = teamPlayers.filter(player => !dailyBetsFormExcludedPlayers.includes(player.playerName));

      // Step 6: Map to the desired output format
      teamPlayers = teamPlayers.map(player => ({
        playerName: player.playerName,
        playerType: player.playerType
      }));

      result[teamName] = teamPlayers;
    }
  });

  return result;
}

/**
 * Get player details for specific teams and player types.
 * @function
 * @param {string|string[]} teamNames - A single team name or an array of team names.
 * @param {string|string[]} [playerTypeCategories] - A single player type or an array of player types (optional, if empty, consider all player types).
 * @returns {Object} An object with team names as keys, each containing an array of player details.
 * @example
 * // Call the function with a single teamName and a single playerType
 * var teamPlayerDetailsSingle = getPlayerDetails("AFG", "BATTERS");
 * Logger.log(teamPlayerDetailsSingle);
 * 
 * // Call the function with an array of teamNames and an array of playerTypes
 * var teamPlayerDetailsMultiple = getPlayerDetails(["AFG", "IND"], ["BOWLERS", "BATTERS"]);
 * Logger.log(teamPlayerDetailsMultiple);
 */
function getPlayerDetails2_(teamNames, playerTypeCategories = PlayerTypeCategories.ALL_PLAYER_CATEGORIES) {
  const allPlayerData = getPlayersCachedData_();
  const dailyBetsFormExcludedPlayers = getDailyBetsFormExcludedPlayers_(); // Get the list of excluded players from Daily Bets Form
  const result = {};

  // Ensure teamNames is an array
  if (!Array.isArray(teamNames)) { teamNames = [teamNames]; }
  if (!Array.isArray(playerTypeCategories) && isNotEmpty_(playerTypeCategories)) { playerTypeCategories = [playerTypeCategories] }

  teamNames.forEach(function (teamName) {
    const teamDetails = allPlayerData[teamName];

    if (teamDetails) {
      let teamPlayers = [];

      if (isNotEmpty_(playerTypeCategories)) {
        var playersOfTypes = [];

        // Include players from "source"
        playersOfTypes = playersOfTypes.concat(teamDetails.source.filter(player => playerTypeCategories.includes(player.playerType)));

        // Include players from "manual"
        playersOfTypes = playersOfTypes.concat(teamDetails.manual.filter(player => playerTypeCategories.includes(player.playerType)));

        // Exclude players from "excluded"
        playersOfTypes = playersOfTypes.filter(player => !teamDetails.excluded.find(excludedPlayer => excludedPlayer.playerName === player.playerName));

        // Exclude players from getDailyBetsFormExcludedPlayers
        playersOfTypes = playersOfTypes.filter(player => !dailyBetsFormExcludedPlayers.includes(player.playerName));

        teamPlayers = playersOfTypes.map(player => ({
          playerName: player.playerName,
          playerType: player.playerType
        }));
      } else {
        // If no playerTypes are specified, include all players from "source" and "manual", excluding those from "excluded"
        teamPlayers = teamDetails.source.concat(teamDetails.manual)
          .filter(player => !teamDetails.excluded.find(excludedPlayer => excludedPlayer.playerName === player.playerName))
          .filter(player => !dailyBetsFormExcludedPlayers.includes(player.playerName))
          .map(player => ({
            playerName: player.playerName,
            playerType: player.playerType
          }));
      }
      
      result[teamName] = teamPlayers;
    }
  });

  return result;
}

/**
 * Updates the team IDs in the players sheet.
 * @param {string[]} teamIDs An array containing the team IDs to update in the sheet.
 */
function updatePlayingTeamIDs_(teamIDs) {
  const sheet = getPlayersSheet_();
  const range = sheet.getRange(playersSheetObj.teamIDsRange);
  const numColumns = range.getNumColumns();

  if (teamIDs.length > numColumns) {
    log(new Error(`Number of teamIDs '${teamIDs.length}' exceeds the number of columns '${numColumns}' in the range.`), LogLevel.ERROR);
    return;
  }

  // Ensure that teamIDs has enough elements to fill the range
  const data = [teamIDs.slice(0, numColumns).concat(Array(numColumns - teamIDs.length).fill(""))];

  // Set the values in the range
  range.setValues(data);
  console.log(`Updating Playing TeamIDs: [${teamIDs}]`);
  removePlayingTeamIDsCachedData_();

  flushAndWait_();
}

/**
 * Returns the squad home URL based on the active tournament URL.
 *
 * @returns {string} - The squad home URL.
 */
function getSquadsHomeURL_() {
  let url = getActiveSeriesURL_();
  if (isNotEmpty_(url)) {
    url = url + "/squads";
    return url;
  }
  log('No Active Tournament URL Found.', LogLevel.ERROR);
}
