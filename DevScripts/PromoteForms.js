/**
 * Promotes all prediction forms from non-PROD environments to PROD environment.
 * If running in non-PROD environment, it promotes the forms in the following order:
 * Overall Predictions Form, Daily Predictions Form, and resets all sheet settings.
 * 
 * @return {void}
 */
function promoteAllForms_(e) {
  logSTART_(arguments.callee.name);
  if (isNotProdEnv_()) {
    // Start in reverse order
    promoteSeriesBetsForm_(e);
    flushAndWait_();
    promoteDailyBetsForm_(e);
    flushAndWait_();

    resetAllSheetsSettings_();
  } else {
    log("Form promotion is allowed in non PROD envs only. Current env is " + EnvConfigClass.getEnvName(), LogLevel.ERROR);
  }
  logEND_(arguments.callee.name);
}

/**
 * Promotes Daily Bets Form from non-PROD environment to PROD environment.
 * 
 * @return {void}
 */
function promoteDailyBetsForm_(e) {
  logSTART_(arguments.callee.name);

  let clonedForm = promoteForm_(dailyBetsSheetName);
  //resetMatchIDFormulasInDailyPredictions_();
  resetDailyBetsForm_(e, clonedForm);

  flushAndWait_();

  logEND_(arguments.callee.name);
}

/**
 * Promotes Overall Predictions Form from non-PROD environment to PROD environment.
 * 
 * @return {void}
 */
function promoteSeriesBetsForm_() {
  logSTART_(arguments.callee.name);

  let clonedForm = promoteForm_(seriesBetsSheetName);
  resetOverallPredictionsForm_(clonedForm);

  flushAndWait_();

  logEND_(arguments.callee.name);
}

/**
 * Promotes the form with the given sheet name from non-PROD environment to PROD environment.
 * 
 * @param {string} sheetName - The name of the sheet to promote.
 * @return {Form} - The cloned form.
 */
function promoteForm_(sheetName) {
  const sourceSpreadsheet = SpreadsheetApp.openById(ConfigManager.getConfigClass().PROD_ENV_SPREADSHEET_ID);

  let sourceSheet = sourceSpreadsheet.getSheetByName(sheetName);
  let sourceFormUrl = sourceSheet.getFormUrl();

  let clonedForm = cloneSourceForm_(sourceFormUrl, sheetName);

  replaceTargetForm_(clonedForm, sheetName);
  return clonedForm;
}

/**
 * Clones the form with the given source form URL and sheet name.
 * 
 * @param {string} sourceFormUrl - The URL of the form to clone.
 * @param {string} sheetName - The name of the sheet associated with the form.
 * @return {Form} - The cloned form.
 */
function cloneSourceForm_(sourceFormUrl, sheetName) {
  let sourceForm = FormApp.openByUrl(sourceFormUrl);
  let sourceFormId = sourceForm.getId();

  let clonedFormName = appendEnvAndTournamentName_(sheetName);// + ":RD:" + getCurrentFormattedTimesptamp_();
  let clonedFormFile = DriveApp.getFileById(sourceFormId).makeCopy(clonedFormName);
  clonedFormFile.setStarred(true);
  clonedFormFile.moveTo(getCurrentSpreadsheetFolder_());

  let clonedForm = FormApp.openById(clonedFormFile.getId());
  clonedForm.deleteAllResponses();
  clonedForm.removeDestination();

  return clonedForm;
}

/**
 * Replace the form associated with a sheet with a cloned form
 * @param {Form} clonedForm - The cloned form to be associated with the sheet
 * @param {string} sheetName - The name of the sheet
 */
function replaceTargetForm_(clonedForm, sheetName) {
  let targetSheet = getActiveSpreadsheet_().getSheetByName(sheetName);
  if (targetSheet) {
    let targetFormUrl = targetSheet.getFormUrl();

    if (targetFormUrl) {
      let targetForm = FormApp.openByUrl(targetFormUrl);
      targetForm.removeDestination();
      let targetFormFile = DriveApp.getFileById(targetForm.getId());
      targetFormFile.setStarred(false);
      targetFormFile.setTrashed(true);
    }
    getActiveSpreadsheet_().deleteSheet(targetSheet);
    flushAndWait_();
    connectCurrentSpreadsheet_(clonedForm, sheetName);
  }
}

/**
 * Connect the form to active spreadsheet
 * @param {Form} form - The form to be connected
 * @param {string} sheetName - The name of the sheet
 */
function connectCurrentSpreadsheet_(form, sheetName) {
  const spreadsheet = getActiveSpreadsheet_();
  form.setDestination(FormApp.DestinationType.SPREADSHEET, spreadsheet.getId());
  let formEditURL = form.getEditUrl();
  let publishedUrl = form.getPublishedUrl();
  log(sheetName + "'s form Edit URL is " + formEditURL, LogLevel.DEBUG);
  log(sheetName + "'s form Published URL is " + publishedUrl, LogLevel.DEBUG);

  // rename spreadsheet
  const formUrl = formEditURL.replace('/edit', '');
  flushAndWait_();
  spreadsheet.getSheets().forEach(sheet => {
    let destFormUrl = sheet.getFormUrl();
    if (destFormUrl) {
      destFormUrl = destFormUrl.replace('/viewform', '');
      if (destFormUrl === formUrl) {
        log("Renaming the sheet.....", LogLevel.DEBUG);
        sheet.setName(sheetName);
      }
    }
  });
  flushAndWait_();
}
