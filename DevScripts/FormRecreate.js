//FormsRecreate.gs//

/**
 * Recreates the form questions for the daily predictions form.
 * 
 * Deletes all existing form data and columns, and creates new form questions based on the number of matches.
 * @param {Object} e The event parameter for the form submission trigger.
 * @param {Google Forms} form The daily predictions form.
 * @param {number} numOfMatches The number of matches for the current round of predictions.
 */
function recreateDailyBetsFormQuestions_(e, form, numOfMatches) {
  console.time(recreateDailyBetsFormQuestions_.name);

  deleteDailyBetsFormData_(e, form);

  createDailyBetsFormQuestions_(e, form, numOfMatches);
  resetBetsSheets_(e);

  flushAndWait_();

  console.timeEnd(recreateDailyBetsFormQuestions_.name);
}

/**
 * Creates questions for the Daily Bets form based on the number of open matches.
 * @param {GoogleAppsScript.Events.FormsOnSubmit} e - The form submission event object.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which questions are added.
 */
function createDailyBetsFormQuestions_(e, form, numOfMatches) {
  console.time(createDailyBetsFormQuestions_.name);

  for (let i = 0; i < numOfMatches; i++) {
    addFormItem_(form, FormApp.ItemType.SECTION_HEADER, sectionHeaderTitlesArr[i], null, null);
    let matchNum = (i + 1);
    for (const question of dailyBetsFormMatchQuestions()) {
      const itemType = question.itemType;
      const title = `${matchNum}${question.title}`;
      const values = NOT_APPLICABLE_ARR;
      const helpText = getHelpTextValue_(question);
      addFormItem_(form, itemType, title, values, helpText);
    }
  }

  console.timeEnd(createDailyBetsFormQuestions_.name);
  flushAndWait_();
}

/**
 * Deletes the data from the Daily Bets form by retrying the deletion
 * operation if it fails initially.
 */
function deleteDailyBetsFormData_(e, form) {
  moveDailyBetsToClosedSheet_(e);
  removeCachedFormTitles_(form);

  executeWithRetry_(function () {
    doDeleteDailyBetsFormData_(form);
  });
}

/**
 * Deletes all form data and columns, clears the daily predictions sheet, and deletes orphaned columns.
 * 
 * @param {Google Forms} form The daily predictions form.
 */
function doDeleteDailyBetsFormData_(form) {
  console.time(doDeleteDailyBetsFormData_.name);

  for (let i = 0; i < 1; i++) {//Trying deleting twice, otherwise it leaves orphaned columns, sometimes.
    deleteFormResponses_(form);
    deleteFormQuestions_(form);
    flushAndWait_();
  }

  clearDailyBetsSheet_();
  deleteOrphanedColumns_();

  console.timeEnd(doDeleteDailyBetsFormData_.name);
}

/**
 * Deletes any orphaned columns from the Daily Picks sheet.
 *
 * @returns {void}
 */
function deleteOrphanedColumns_() {
  flushAndWait_();
  var sheet = getDailyBetsSheet_();
  var startColumn = 4; // column index to start deleting from
  var lastColumn = sheet.getMaxColumns(); // last column index
  var numColumnsToDelete = lastColumn - startColumn + 1; // number of columns to delete
  if (numColumnsToDelete > 0) {
    sheet.deleteColumns(startColumn, numColumnsToDelete);
    flushAndWait_();
  }
}

/**
 * Deletes all responses from the specified form.
 * @param {GoogleAppsScript.Forms.Form} form - The form from which responses are to be deleted.
 */
function deleteFormResponses_(form) {
  form.deleteAllResponses();
}

/**
 * Deletes all questions from the specified form.
 * @param {GoogleAppsScript.Forms.Form} form - The form from which questions are to be deleted.
 */
function deleteFormQuestions_(form) {
  let items = form.getItems();
  for (let i = 1; i < (items.length); i++) {
    form.deleteItem(items[i]);
  }
}

/**
 * Reattaches the form to the specified sheet after deleting responses and questions.
 * @param {GoogleAppsScript.Forms.Form} form - The form to be reattached.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to which the form is reattached.
 */
function reattachForm_(form, sheet) {
  let sheetName = sheet.getName();
  removeSheetProtection_(sheet);
  form.removeDestination();
  getActiveSpreadsheet_().deleteSheet(sheet);
  connectCurrentSpreadsheet_(form, sheetName);
}

/**
 * Adds a form item of the specified type to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the item is to be added.
 * @param {FormItemTypes} itemType - The type of form item to add.
 * @param {string} title - The title of the form item.
 * @param {string[]} values - The values for the form item (if applicable).
 * @param {string} description - The description or help text for the form item.
 */
function addFormItem_(form, itemType, title, values, description) {
  switch (itemType) {
    case FormItemTypes.LIST:
      addListItem_(form, title, values, description)
      break;
    case FormItemTypes.SECTION_HEADER:
      addSectionHeaderItem_(form, title, description);
      break;
    case FormItemTypes.CHECKBOX:
      addCheckboxItem_(form, title, values, description);
      break;
    case FormItemTypes.MULTIPLE_CHOICE:
      addMultipleChoiceItem_(form, title, values, description);
      break;
    case FormItemTypes.TEXT:
      addTextItem_(form, title, description);
      break;
    case FormItemTypes.TEXT_WHOLE_NUMBER:
      addWholeNumberTextItem_(form, title, values, description);
      break;
    case FormItemTypes.SCALE:
      addScaleItem_(form, title, description);
      break;
    default:
      log("Unknown item type = " + itemType + ", title = " + title + ", values = " + values, LogLevel.ERROR);
  }
}

/**
 * Adds a list item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the list item is to be added.
 * @param {string} title - The title of the list item.
 * @param {string[]} values - The values for the list item.
 * @param {string} description - The description or help text for the list item.
 */
function addListItem_(form, title, values, description) {
  var item = form.addListItem();
  item.setTitle(title);
  item.setChoiceValues(values);
  item.setHelpText(description);
  item.setRequired(true);
}

/**
 * Adds a section header item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the section header item is to be added.
 * @param {string} title - The title of the section header item.
 * @param {string} description - The description or help text for the section header item.
 */
function addSectionHeaderItem_(form, title, description) {
  var item = form.addSectionHeaderItem();
  item.setTitle(title);
  item.setHelpText(description);
}

/**
 * Adds a checkbox item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the checkbox item is to be added.
 * @param {string} title - The title of the checkbox item.
 * @param {string[]} values - The values for the checkbox item.
 * @param {string} description - The description or help text for the checkbox item.
 */
function addCheckboxItem_(form, title, values, description) {
  var item = form.addCheckboxItem();
  item.setTitle(title);
  item.setChoiceValues(values);
  item.setRequired(true);
  item.setHelpText(description);
}

/**
 * Adds a multiple choice item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the multiple choice item is to be added.
 * @param {string} title - The title of the multiple choice item.
 * @param {string[]} values - The values for the multiple choice item.
 * @param {string} description - The description or help text for the multiple choice item.
 */
function addMultipleChoiceItem_(form, title, values, description) {
  var item = form.addMultipleChoiceItem();
  item.setTitle(title);
  item.setChoiceValues(values);
  item.setRequired(true);
  item.setHelpText(description);
}

/**
 * Adds a text item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the text item is to be added.
 * @param {string} title - The title of the text item.
 * @param {string} description - The description or help text for the text item.
 */
function addTextItem_(form, title, description) {
  var item = form.addTextItem();
  item.setTitle(title);
  item.setRequired(true);
  item.setHelpText(description);
}

/**
 * Adds a whole number text item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the whole number text item is to be added.
 * @param {string} title - The title of the whole number text item.
 * @param {string[]} values - The values for the whole number text item.
 * @param {string} description - The description or help text for the whole number text item.
 */
function addWholeNumberTextItem_(form, title, values, description) {
  var item = form.addTextItem();
  item.setTitle(title);
  item.setRequired(true);
  item.setHelpText(description);

  var textValidation = FormApp.createTextValidation()
    .setHelpText(`Must be a whole number.`)
    .requireWholeNumber()
    .build();
  item.setValidation(textValidation);
}

/**
 * Adds a scale item to the form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to which the scale item is to be added.
 * @param {string} title - The title of the scale item.
 * @param {string} description - The description or help text for the scale item.
 */
function addScaleItem_(form, title, description) {
  var item = form.addScaleItem();
  item.setTitle(title);
  item.setBounds(1, 10);
  item.setRequired(true);
  item.setHelpText(description);
}
