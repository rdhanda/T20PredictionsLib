//EnvConfigClass.gs//

/**
 * EnvConfigClass provides static methods to access environment configuration settings.
 */
class EnvConfigClass {
  /**
   * Get the default configuration group for the environment.
   * @returns {string} The default configuration group.
   */
  static getEnvDefaultConfigGroup() {
    return EnvConfigs.ENV_DEFAULT_CONFIG_GROUP;
  }

  /**
   * Get the name of the environment.
   * @returns {string} The name of the environment.
   */
  static getEnvName() {
    return EnvConfigs.ENV_NAME;
  }

  /**
   * Get the additional admin email list for the environment.
   * @returns {string[]} The additional admin email list.
   */
  static getEnvAdditionalAdminEmailList() {
    return EnvConfigs.ENV_ADDITIONAL_ADMIN_EMAIL_LIST;
  }

  /**
   * Check if sending emails from non-production environments is allowed.
   * @returns {boolean} True if sending emails from non-production environments is allowed, false otherwise.
   */
  static isSendingEmailsFromNonProd() {
    return EnvConfigs.ENV_SEND_EMAILS_FROM_NON_PROD;
  }

  /**
   * Get the current log level for the environment.
   * @returns {string} The current log level.
   */
  static getEnvCurrentLogLevel() {
    return EnvConfigs.ENV_CURRENT_LOG_LEVEL;
  }

  /**
   * Check if form responses are being fetched from the production environment.
   * @returns {boolean} True if form responses are fetched from production, false otherwise.
   */
  static isGettingProdFormResponses() {
    return EnvConfigs.ENV_GET_PROD_FORM_RESPONSES;
  }

  /**
   * Check if trigger execution is temporarily halted.
   * @returns {boolean} True if trigger execution is halted, false otherwise.
   */
  static isTriggerExecutionHalted() {
    return EnvConfigs.ENV_TEMP_HALT_TRIGGER_EXECUTIONS;
  }

  /**
   * Get the interval in minutes for updating match results.
   * @returns {number} The interval in minutes.
   */
  static getUpdateMatchResultsTriggerEveryMinutes() {
    return EnvConfigs.ENV_UPDATE_MATCH_RESULTS_TRIGGER_EVERY_MINUTES;
  }

  /**
   * Get the maximum number of match results updates allowed per minute.
   * @returns {number} The maximum updates per minute.
   */
  static getUpdateMatchResultsMaxPerMinute() {
    return EnvConfigs.ENV_UPDATE_MATCH_RESULTS_MAX_PER_MINUTE;
  }

  /**
   * Get the number of minutes added to the match start time to close daily bets.
   * @returns {number} The number of minutes.
   */
  static getAddMinutesToCloseDailyBets() {
    return EnvConfigs.MATCH_START_TIME.AddMinutesToCloseDailyBets;
  }

  /**
   * Get the number of minutes added to the match start time to send reminders.
   * @returns {number} The number of minutes.
   */
  static getAddMinutesToSendReminders() {
    return EnvConfigs.MATCH_START_TIME.AddMinutesToSendReminders;
  }

  /**
   * Check if auto mode is enabled.
   * @returns {boolean} True if auto mode is enabled, false otherwise.
   */
  static isAutoModeEnabled() {
    return EnvConfigs.ENV_AUTO_MODE;
  }
}
