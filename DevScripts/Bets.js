//** Predictions Setup */

/**
 * Number of seconds the script will sleep for.
 * @type {number}
 */
const SECONDS_TO_SLEEP = 5;

/**
 * 
 */
function toggleBetsMenu(e) {
  Bets.toggleBets(e);
}

/**
 * Represents a utility class for managing bets.
 */
class Bets {
  /**
   * User by the trigger
   * @param {Object} e The event object.
   */
  static toggleBets(e) {
    console.time(Bets.toggleBets.name);

    Bets.closeBets(e);
    Bets.openBets(e);

    console.timeEnd(Bets.toggleBets.name);
  }

  /**
   * Performs the necessary actions for closing bets based on the provided event.
   * @param {GoogleAppsScript.Events.FormsOnFormSubmit} e - The event object.
   * @returns {boolean} True if the bets are successfully closed, false otherwise.
   */
  static closeBets(e) {
    let matches = Bets.getMatchesToClose(e);

    if (isNotEmpty_(matches)) {
      Bets.doCloseBets(e, matches);
      return true;
    } else {
      activateSheet_(getScheduleViewSheet_());
      return false;
    }
  }

  /**
   * Get the matches to close
   * @param {Object} e The event object.
   * @returns {[ScheduleMatch]|null} The scheduled matches to close, currently open.
   */
  static getMatchesToClose(e) {
    const openMatches = ScheduleUtils.getOpenMatches();
    if (isEmpty_(openMatches)) { return; }

    const startTime = ScheduleUtils.getFirstMatchStartTimeMST(openMatches);

    if (startTime) {
      const closingTime = addMinutes_(startTime, EnvConfigClass.getAddMinutesToCloseDailyBets());
      const isItTimeToClose = isBeforeCurrentTime_(closingTime);
      if (isItTimeToClose) {
        const date = ScheduleUtils.getFirstMatchShortDate(openMatches);
        console.info(`NOW is the time to Close for ${date}`);
        return openMatches;
      }
    }
  }

  /**
   * Performs the necessary actions for closing bets on the specified date.
   * @param {GoogleAppsScript.Events.FormsOnFormSubmit} e - The event object.
   * @param {[ScheduleMatch]} matches - The matches for closing bets.
   */
  static doCloseBets(e, matches) {
    console.time(Bets.doCloseBets.name);

    const date = ScheduleUtils.getFirstMatchShortDate(matches);

    Bets.doBeforeClosingBets(e);
    Bets.closeBetsForm(e, date);

    let success = importByMatchID_(e, matches[0].getId());
    Bets.doAfterClosingBets(e, success, matches, date);

    console.timeEnd(Bets.doCloseBets.name);
  }

  /**
   * This function is called before closing the prediction form.
   *
   * @param {Event} e - The form submit trigger event object.
   */
  static doBeforeClosingBets(e) {
    autoSubmitBets_(e);
  }

  /**
   * Closes the prediction form and imports the predictions to the prediction sheet for the given match date.
   *
   * @param {Event} e - The form submit trigger event object.
   * @param {Date} date - The match date.
   * @return {boolean} Whether the import was successful.
   */
  static closeBetsForm(e, date) {
    stopAcceptingDailyBets_();
    stopAcceptingSeriesBets_();

    console.info(`Closed the Bets form at "${getCurrentLongTimesptamp_()}" for date ${date}`);
  }

  /**
   * This function is called after the prediction form is closed.
   *
   * @param {Event} e - The form submit trigger event object.
   * @param {boolean} success - Whether the import was successful.
   * @param {Date} date - The match date.
   */
  static doAfterClosingBets(e, success, matches, date) {
    updateMatchesStatusClosed_(matches);
    moveDailyBetsToClosedSheet_(e);

    updateDashboardBetsLists_(e);

    hideAllBetsSheetsColumns_(e);
    EmailBetsClosed.sendEmail(e, success, date);
    PlayerStatsUtils.clearSheet();

    Triggers.createResetAllSheetSettingsScheduledTrigger();
  }

  /**
   * Opens the predictions form for a match if the current time is appropriate and no other predictions form is open.
   * 
   * @param {Event} e - The event object.
   * @returns {TriggerExecutionResult} - The result of the trigger execution, indicating success or the need for a retry.
   */
  static openBets(e) {
    console.time(Bets.openBets.name);
    let result = new TriggerExecutionResult(true);

    Bets.doBeforeOpeningBets(e);

    if (ScheduleUtils.isItTimeToOpen(e)) {
      let matches = ScheduleUtils.getMatchesToOpen(e);

      if (isNotEmpty_(matches)) {
        const date = matches[0].getShortDate();
        console.info(`Match Date To Open '${date}'`);
        Bets.openBetsForm(e, matches);
        Bets.doAfterOpeningBets(e, date);
      } else {
        result = new TriggerExecutionResult(false, true, 120); // Retry 
      }
    }

    console.timeEnd(Bets.openBets.name);
    return result;
  }

  /**
   * Resets all sheets' settings.
   *
   * @param {Event} e - The event parameter from the onOpen() trigger.
   */
  static doBeforeOpeningBets(e) {
    hideAllBetsSheetsColumns_();
    updateScheduleSheet_(e);

    flushAndWait_();
  }

  /**
   * Opens the daily predictions form for a given date.
   *
   * @param {Event} e - The event parameter from the onOpen() trigger.
   * @param {[ScheduleMatch]} matches - The matches to open the form for
   */
  static openBetsForm(e, matches) {
    //Update the form
    updateDailyBetsForm_(e, matches);
    startAcceptingDailyBets_();

    updateMatchesStatusOpen_(matches);
    console.info(`Opened the form at "${getCurrentLongTimesptamp_()}"`);
  }

  /**
   * Performs actions after opening the daily bets form.
   *
   * @param {Event} e - The event parameter from the onOpen() trigger.
   * @param {string} date - The date the form was opened for, in short match date format (e.g. "MM/dd").
   */
  static doAfterOpeningBets(e, date) {
    EmailBetsOpened.sendEmail(e, date);
    ScheduleUtils.hideScheduleViewRows();

    updateDashboardBetsLists_(e);

    //Predicto Updates
    Triggers.createUpdatePredictoSheetScheduledTrigger();

    //Update player stats
    Triggers.createUpdatePlayerStatsScheduledTrigger();

    //Reset all sheet settings
    Triggers.createResetAllSheetSettingsScheduledTrigger();
  }
}
