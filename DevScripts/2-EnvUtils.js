//Env Utils

/**
 * Constants object for environment configurations.
 * @typedef {Object} EnvConfigConstants
 * @property {string} ENV_GROUP_AUCHEM - The AUChem environment group.
 * @property {string} ENV_GROUP_YEG - The YEG environment group.
 * @property {string} ENV_NAME_DEV - The development environment name.
 * @property {string} ENV_NAME_TEST - The testing environment name.
 * @property {string} ENV_NAME_PROD - The production environment name.
 * @property {Object} ENV_LOG_LEVEL_DEBUG - Log level debug configuration.
 * @property {string} ENV_LOG_LEVEL_DEBUG.level - Log level debug level.
 * @property {string} ENV_LOG_LEVEL_DEBUG.text - Log level debug text.
 * @property {number} ENV_LOG_LEVEL_DEBUG.value - Log level debug value.
 * @property {Object} ENV_LOG_LEVEL_INFO - Log level info configuration.
 * @property {string} ENV_LOG_LEVEL_INFO.level - Log level info level.
 * @property {string} ENV_LOG_LEVEL_INFO.text - Log level info text.
 * @property {number} ENV_LOG_LEVEL_INFO.value - Log level info value.
 * @property {Object} ENV_LOG_LEVEL_WARN - Log level warn configuration.
 * @property {string} ENV_LOG_LEVEL_WARN.level - Log level warn level.
 * @property {string} ENV_LOG_LEVEL_WARN.text - Log level warn text.
 * @property {number} ENV_LOG_LEVEL_WARN.value - Log level warn value.
 * @property {Object} ENV_LOG_LEVEL_ERROR - Log level error configuration.
 * @property {string} ENV_LOG_LEVEL_ERROR.level - Log level error level.
 * @property {string} ENV_LOG_LEVEL_ERROR.text - Log level error text.
 * @property {number} ENV_LOG_LEVEL_ERROR.value - Log level error value.
 */
var EnvConfigConstants = {
  ENV_GROUP_AUCHEM: 'AUChem',
  ENV_GROUP_YEG: 'YEG',
  ENV_NAME_DEV: "DEV",
  ENV_NAME_TEST: "TEST",
  ENV_NAME_TRIAL: "TRIAL",
  ENV_NAME_PROD: "PROD",
  ENV_LOG_LEVEL_DEBUG: { level: 'debug', text: "DEBUG", value: 0 },
  ENV_LOG_LEVEL_INFO: { level: 'info', text: "INFO", value: 1 },
  ENV_LOG_LEVEL_WARN: { level: 'warn', text: "WARN", value: 2 },
  ENV_LOG_LEVEL_ERROR: { level: 'error', text: "ERROR", value: 3 },
};

const LibraryID = "BatBallBetsLib.";

/**
 * Calls `addAllMenus_` function for each spreadsheet using the script.
 * Displays start and end times in the logs.
 */
function setupEnv() {
  addAllMenus_();
}

/**
 * Displays the environment information user interface for the current script.
 * @example
 * // Display the environment information user interface
 * displayEnvInfoUI();
 */
function displayEnvInfoMenu() {
  displayEnvInfo_();
}

/**
 * Display Enf Info from MENU
 */
function displayEnvInfo_() {
  const message = getEnvInfo_();

  showCustomInfoAlert_("Environment Info", message);
}

function getEnvInfo_() {
  var envInfo = {
    allEnvProperties: getAllEnvProperties_(),
    env: EnvConfigClass.getEnvName(),
    tournament: getActiveSeriesName_(),
    libraryId: getLibraryID_(),
    formResponsesFromProd: EnvConfigClass.isGettingProdFormResponses()
  };

  var dailyPredictions = {
    title: getDailyBetsFormTitle_(),
    url: getDailyBetsFormPublishedURL_()
  };

  var overallPredictions = {
    title: getSeriesBetsFormTitle_(),
    url: getSeriesBetsFormPublishedURL_()
  };

  var message = `ENV PROPS:
    ${envInfo.allEnvProperties}
    ENV=${envInfo.env}
    TOURNAMENT=${envInfo.tournament}
    LIBRARY_ID=${envInfo.libraryId}
    ENV_GET_FORM_RESPONSES_FROM_PROD=${envInfo.formResponsesFromProd}

    Daily Predictions Form: ${dailyPredictions.title}
    ${dailyPredictions.url}

    Overall Predictions Form: ${overallPredictions.title}
    ${overallPredictions.url}`;

  return message;
}

/**
 * Gets the library ID from the environment properties.
 * @return {string} The library ID, or an empty string if the current environment is the development environment.
 */
function getLibraryID_() {
  return isDevEnv_() ? "" : LibraryID;
}

/**
 * Returns `true` if the current environment is the production environment, `false` otherwise.
 * @return {boolean} `true` if the current environment is the production environment, `false` otherwise.
 */
function isProdEnv_() {
  return EnvConfigConstants.ENV_NAME_PROD == EnvConfigClass.getEnvName();
}

/**
 * Returns `true` if the current environment is not the production environment, `false` otherwise.
 * @return {boolean} `true` if the current environment is not the production environment, `false` otherwise.
 */
function isNotProdEnv_() {
  return !isProdEnv_();
}

/**
 * Returns `true` if the current environment is the test environment, `false` otherwise.
 * @return {boolean} `true` if the current environment is the test environment, `false` otherwise.
 */
function isTestEnv_() {
  return EnvConfigConstants.ENV_NAME_TEST == EnvConfigClass.getEnvName();
}

/**
 * Returns `true` if the current environment is not the test environment, `false` otherwise.
 * @return {boolean} `true` if the current environment is not the test environment, `false` otherwise.
 */
function isNotTestEnv_() {
  return !isTestEnv_();
}

/**
 * Returns `true` if the current environment is the development environment, `false` otherwise.
 * @return {boolean} `true` if the current environment is the development environment, `false` otherwise.
 */
function isDevEnv_() {
  return EnvConfigConstants.ENV_NAME_DEV == EnvConfigClass.getEnvName();
}

/**
 * Returns `true` if the current environment is not the development environment, `false` otherwise.
 * @return {boolean} `true` if the current environment is not the development environment, `false` otherwise.
 */
function isNotDevEnv_() {
  return !isDevEnv_();
}
