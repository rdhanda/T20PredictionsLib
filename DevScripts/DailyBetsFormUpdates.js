//DailyBetsFormUpdates.gs//

function updateDailyBetsFormMenu(e) {
  const matches = ScheduleUtils.getOpenMatches();
  updateDailyBetsForm_(e, matches);
}

/**
 * Updates the punters list for the Daily Bets form via a menu option.
 * @param {GoogleAppsScript.Events.AppsScriptEvent} e - The event object.
 */
function updateDailyBetsFormPuntersMenu(e) {
  let form = getDailyBetsForm_();
  updatePuntersList_(form);
}

/**
 * Updates the title and description of the Daily Bets form via a menu option.
 * @param {GoogleAppsScript.Events.AppsScriptEvent} e - The event object.
 */
function updateDailyBetsFormTitleDescMenu(e) {
  let form = getDailyBetsForm_();
  const matches = ScheduleUtils.getOpenMatches();
  updateDailyBetsFormTitle_(form, matches);
  updateDailyBetsFormDescription_(form, matches);
}

/**
 * Updates the match questions of the Daily Bets form via a menu option.
 * @param {GoogleAppsScript.Events.AppsScriptEvent} e - The event object.
 */
function updateDailyBetsFormMatchQuestionsMenu(e) {
  let form = getDailyBetsForm_();
  const matches = ScheduleUtils.getOpenMatches();
  updateDailyBetsFormQuestions_(form, matches);
}

/**
 * Updates the entire Daily Bets form, including description, team names,
 * player names dropdowns, and match questions (mandatory/optional).
 * @param {GoogleAppsScript.Events.AppsScriptEvent} e - The event object.
 * @param {[ScheduleMatch]} matches - The matches to open.
 */
function updateDailyBetsForm_(e, matches) {
  if (isEmpty_(matches)) {
    log(`No matches to update the form.`, LogLevel.WARN);
    return;
  }

  console.time(updateDailyBetsForm_.name);

  let form = getDailyBetsForm_();
  removeCachedFormTitles_(form);
  recreateDailyBetsFormQuestions_(e, form, matches.length);

  const teams = ScheduleUtils.getMatchesAllTeams(matches);
  updatePlayers_(e, teams);
  updateDailyBetsFormTitle_(form, matches);
  updateDailyBetsFormDescription_(form, matches);
  updatePuntersList_(form);
  updateDailyBetsFormQuestions_(form, matches);

  console.timeEnd(updateDailyBetsForm_.name);
}

/**
 * Updates the title of the Daily Bets form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to update.
 */
function updateDailyBetsFormTitle_(form, matches) {
  const shortDate = ScheduleUtils.getFirstMatchShortDate(matches);
  form.setTitle(appendEnvAndTournamentName_(shortDate + " Bets"));
}

/**
 * Updates the description of the Daily Bets form with all match names
 * scheduled for the date.
 * @param {GoogleAppsScript.Forms.Form} form - The form to update.
 */
function updateDailyBetsFormDescription_(form, matches) {
  const descList = ScheduleUtils.getMatchesDescList(matches);
  const desc = `${descList.join('\n')}\n\n${getPoweredByStr_()}`;
  form.setDescription(desc);
}

/**
 * Updates the questions for each match in the Daily Bets form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to update.
 * @param {[ScheduleMatch]} matches - The matches to update.
 */
function updateDailyBetsFormQuestions_(form, matches) {
  for (const [index, match] of matches.entries()) {
    let matchNum = (index + 1);
    let matchID = match.getId();
    const matchTitle = match.getTitle();
    const homeTeam = match.getHomeTeam();
    const awayTeam = match.getAwayTeam();

    updateMatchQuestions_(form, matchNum, matchID, matchTitle, homeTeam, awayTeam);
  }
}

/**
 * Updates the questions for a specific match in the Daily Bets form.
 * @param {GoogleAppsScript.Forms.Form} form - The form to update.
 * @param {number} matchNum - The match number.
 * @param {string} matchID - The match ID.
 * @param {string} matchTitle - The match title.
 * @param {string} homeTeam - The home team name.
 * @param {string} awayTeam - The away team name.
 */
function updateMatchQuestions_(form, matchNum, matchID, matchTitle, homeTeam, awayTeam) {
  let matchTitleValues = [matchTitle]; // Must be in 2D array
  if (isTrumpCardUsageEnabledByMatchID_(matchID)) {
    matchTitleValues.push(`${matchTitle} ${TrumpCardKeys.INDICATOR_EMOJI}`);
  }

  let required = isNotEmpty_(matchID);

  for (const question of dailyBetsFormMatchQuestions()) {
    const title = `${matchNum}${question.title}`;
    const item = getFormItem_(form, title);
    const itemType = question.itemType;
    const helpText = getHelpTextValue_(question);
    const valueType = question.valueType;
    let values = NOT_APPLICABLE_ARR;
    switch (valueType) {
      case ItemValueTypes.MATCH_TITLE:
        values = matchTitleValues;
        break;
      case ItemValueTypes.TEAMS:
        values = [homeTeam, awayTeam];
        break;
      case ItemValueTypes.PLAYERS_ALL:
        values = getPlayersList_([homeTeam, awayTeam], PlayerTypeCategories.ALL_PLAYER_CATEGORIES, true);
        break;
      case ItemValueTypes.PLAYERS_BATTERS:
        values = getPlayersList_([homeTeam, awayTeam], PlayerTypeCategories.BATTER_PLAYER_CATEGORIES, true);
        break;
      case ItemValueTypes.PLAYERS_BOWLERS:
        values = getPlayersList_([homeTeam, awayTeam], PlayerTypeCategories.BOWLER_PLAYER_CATEGORIES, true);
        break;
    }
    updateFormItem_(item, itemType, helpText, values, required);
  }
}
