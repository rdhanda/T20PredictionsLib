//SheetConfigs.Class.gs//

/**
 * Represents a sheet configuration.
 * @class
 */
class SheetConfigs {
  /**
   * Creates an instance of SheetConfigs.
   * @param {string} name - The name of the sheet.
   * @param {SheetVisibility} visibility - The visibility of the sheet.
   * @param {Protection} protection - The protection settings of the sheet.
   * @param {Promotion} promotion - The promotion settings of the sheet.
   * @param {boolean} [replaceHeader] - Indicates whether to replace headers.
   */
  constructor(name, visibility, protection, promotion, replaceHeader = false) {
    var name_ = name;
    var visibility_ = visibility;
    var protection_ = protection;
    var promotion_ = promotion;
    var replaceHeader_ = replaceHeader;

    // Getters
    this.getName = function () { return name_; };
    this.getVisibility = function () { return visibility_; };
    this.getProtection = function () { return protection_; };
    this.getPromotion = function () { return promotion_; };
    this.getReplaceHeader = function () { return replaceHeader_; };

    // Setters
    this.setName = function (name) { name_ = name; };
    this.setVisibility = function (visibility) { visibility_ = visibility; };
    this.setProtection = function (protection) { protection_ = protection; };
    this.setPromotion = function (promotion) { promotion_ = promotion; };
    this.setReplaceHeader = function (replaceHeader) { replaceHeader_ = replaceHeader; };
  }

  /**
   * Serializes the SheetConfigs instance to a plain object.
   * @returns {Object} The plain object representation of the SheetConfigs.
   */
  toObject() {
    return {
      name: this.getName(),
      visibility: this.getVisibility(),
      protection: this.getProtection(),
      promotion: this.getPromotion(),
      replaceHeader: this.getReplaceHeader(),
    };
  }

  /**
   * Static method to create a SheetConfigs instance from a plain object.
   * @param {Object} obj - The plain object representation of a SheetConfigs.
   * @returns {SheetConfigs} The SheetConfigs instance.
   */
  static fromObject(obj) {
    return new SheetConfigs(
      obj.name,
      obj.visibility,
      obj.protection,
      obj.promotion,
      obj.replaceHeader,
    );
  }
}

/**
 * 
 */
const BaseSheetConfigsList = [
  new SheetConfigs(homeSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(tableSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(predictoSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(match1SheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH2),
  new SheetConfigs(match2SheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH2),
  new SheetConfigs(match3SheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH2),
  new SheetConfigs(matchTSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH2),
  new SheetConfigs(dailyTeamResultsSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(dailyBetsSheetName, SheetVisibility.SHOW, Protection.ON, Promotion.EXCLUDED),
  new SheetConfigs(dailyBetsClosedSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(dailyBetsBackupSheetName, SheetVisibility.ALWAYS_HIDDEN, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(dailyBetsPointsSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(dailyBetsResultsSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(dailyBetsResultsJournalSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(dailyTopBetsSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(dailyAutoBetsLogSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(seriesBetsSheetName, SheetVisibility.SHOW, Protection.ON, Promotion.EXCLUDED),
  new SheetConfigs(seriesBetsBackupSheetName, SheetVisibility.ALWAYS_HIDDEN, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(seriesBetsPointsSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(seriesBetsModSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(seriesBetsFormSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH3),
  new SheetConfigs(firstInningsScorePointsJSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(firstInningsScoreSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(playersSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(playersExcludedSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(scheduleViewSheetName, SheetVisibility.SHOW, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(scheduleTableSheetName, SheetVisibility.HIDE, Protection.ON, Promotion.EXCLUDED),
  new SheetConfigs(systemLogsSheetName, SheetVisibility.AS_IS, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(puntersSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.EXCLUDED, true),
  new SheetConfigs(teamsSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(seriesViewSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(seriesTableSheetName, SheetVisibility.HIDE, Protection.ON, Promotion.EXCLUDED, true),
  new SheetConfigs(countriesSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
  new SheetConfigs(winnersSheetName, SheetVisibility.HIDE, Protection.OFF, Promotion.BATCH1),
];
