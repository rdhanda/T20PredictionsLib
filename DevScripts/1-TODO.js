//TEST FUNCTIONS//
function testTODO(e) {
  console.time(testTODO.name);

  console.timeEnd(testTODO.name);
}

/**
 * Implement
 *  - Punter class
 *  - DailyBetsResults class
 *
 *   //log(getClosedMatchIdsByDate_("JUN-07"))
 *   //Now get the emails of punters who submitted these matches.... Send email on Close. Config for each group and each Punter
 *
 * IMP: series backup not working properly.... saving the forms at wrong location
 *
 * For Individual Groups:
 * 1. Manage Sheets Config to include different sheets
 * 2. Manage number of punters
 *
 * 1. Give Bot closest margin if between punter closest and actual margin.
 * 2. Schedule extract - use ScheduleMatch class
 *
 */

/**
 * Table vs View - sheets:
 * Table: NO Formulas, No Conditional Formatting, No last updated cell. Basic formatting e.g. alterate line is OK
 * View: Any conditional formatting, Last Updated Cell
 *
 * Series (T, V) - DONE
 * Punters (T, V)
 * Schedule (T, V) - DONE
 * Players (T, V)
 * Results (T, V)
 * Teams (only T, no V)
 *
 */

/**
 * Classes to implement
 * Schedule - matches[], teams[]
 * Team - players[]
 * Player
 * Series - DONE
 * ActiveSeries?
 * MatchResult - punter entries, result
 *
 *
 * Players - Exclusion - better management - in one sheet, manage for both auto bets and form dropdown
 */

/**
 * Schedule - make ScheduleV(only formatting data) and ScheduleT (all data, all updates here)
 * Players - exclusion list, manual additions - new sheet
 * Write UNIT Test - call all key functions, probably with test data (e.g. schedule start times etc)
 *
 * T20 WC
 * 1. Number of trump cards?
 * 2. Bot submissions, max?
 * 3. S/R, min 10 balls, OR min 20 runs
 * 4. Runs/Wickets margin - within range instead of closest?
 *
 * TODO:
 *
 * Reminder emails - add if Bot will submit for them or NOT going forward
 *
 * Punters.gs - use new strctured object for punters data retrival, for all functions in auto submission etc
 * Cache (Use script cache to share among al ENVs) the espncricinfo data. Scorecard (key=Tournament ID + Match ID + Schedule URL - for 1 minutes)
 *
 * Introduce concenpt of punters group, e.g. AUChem, ECCB etc
 *
 * Provide: Manual mechanism, in case espncricinfo doesnt work. A toggle to control. When enabled. 1. Individual match scorecards (including live). 2. Players updates 3. Schedule updates. 4. Standing table, i.e. anything that calls the getHtmlContent()
 */

/**
 * Google Forms Replacement:
 * 1. List of all matches (excluding closed)
 * 2. User can submit any match - provided it has all the required info (opening time, both team names)
 * 3. User can edit until closing time
 * 4. Can view own submissions - all data
 * 5. Can view who all have submitted for upcoming matches (maybe next day?) - limited data
 * 6. Bot to submit at closing time
 *
 * SETUP Needed:
 * 1. Schedule
 * 2. Players List
 * 3. Reminders to be sent before closing
 *
 */

/**
 * NEXT Season:
 *
 * Overall (Toutnament): (rename it to TournamentPicks, T-Picks)
 * 1. Overall to be independent
 * 2. Overall: Bonus for all 4 qualifying teams (not for top 4 positions)
 *
 * Daily (Match): (D-Picks, or M-Picks)
 * 2. DONE: Using current impl for now. Punters missed submitted entried by bot? OR, can a punter ask for bot's submitted entries?. MAX 2 days( 2 to 4 matches). Choice (before the tournament) designate the punter/bot/new entry by bot
 *    OPtins: Submnitted by the BOT, 2. Use another punter's
 * 4. Second Innings Score - account for any innings, remove runs margin?
 */

/**
 *
 * TODO
 * 1. Fix AutoPicksLogs to have Date, not 1st Match Title
 * 2. Fix the auto submissions to look for source file, and only submit if not submitted
 * 3. AutoPredictions - move GUID to the last column to indicate it was auto submitted
 * 4. AutoPredictions - use email instead of guid in the DailyPredictions/Backup sheets
 *
 */

/**
 * When adding/removing a new question in the Daily Predictions Form:
 *
 * Form Changes:
 * 1. Update the form (if or as needed), the questions list in the code will probably be enough
 *
 * Sheets Changes:
 * 2. Modify Match-1 (all INDIRECT referecnes). D-M, Q-Z columns
 * 3. Modify DailyResults. D-M, Q-Z columns
 * 4. Modify Dashboard
 * 5. Expand Temp
 * 6. Expand DailyPredictionsClosed, Backup
 *
 * Code Changes:
 * 1. Modify DailyPredictionForm.gs questions array, and any correspoding questions
 * 2. Modify SheetDefinitions for dailyPredictionsOpenSheetObj, matchSheetObj, scoreSheetObj
 * 3. Modify ImportMatches
 *
 */

/**
 * When adding more punters in the mix
 *
 * 1. SheetsDefinitions maxPredictionsNum = <new val>
 * 2. Update both forms punter list
 * 3. Update Match 1 sheet, copy row formulas to new rows
 * 4. Update Mathc 2, 3 sheets
 * 4. Update Points 3 sections
 * 5. Update Dashboard Top Section and Bottom section
 */


// function testHTMLData_(maxRuns, battersToSelect) {
//   const sampleTable = [
//     ["BATTING", "How Out", "R", "B", "M", "4s", "6s", "SR"],
//     ["Prabhsimran Singh", "c Chahal b Boult", "20", "4", "3", "1", "0", "500.00"],
//     ["Jonny Bairstow †", "c Parag b Chahal", "14", "22", "41", "1", "0", "63.63"],
//     ["Rilee Rossouw", "c Jaiswal b Avesh Khan", "22", "13", "18", "5", "0", "169.23"],
//     ["Shashank Singh", "lbw b Avesh Khan", "0", "2", "2", "0", "0", "0.00"],
//     ["Sam Curran (c)", "not out", "63", "41", "62", "5", "3", "153.65"],
//     ["Jitesh Sharma", "c Parag b Chahal", "22", "20", "31", "0", "2", "110.00"],
//     ["Ashutosh Sharma", "not out", "17", "11", "16", "1", "1", "154.54"]
//   ];


/**
 *
  const sampleTable = [
    ["BATTING", "How Out", "R", "B", "M", "4s", "6s", "SR"],
    ["Prabhsimran Singh", "c Chahal b Boult", "20", "4", "3", "1", "0", "500.00"],
    ["Jonny Bairstow †", "c Parag b Chahal", "14", "22", "41", "1", "0", "63.63"],
    ["Rilee Rossouw", "c Jaiswal b Avesh Khan", "22", "13", "18", "5", "0", "169.23"],
    ["Shashank Singh", "lbw b Avesh Khan", "0", "2", "2", "0", "0", "0.00"],
    ["Sam Curran (c)", "not out", "63", "41", "62", "5", "3", "153.65"],
    ["Jitesh Sharma", "c Parag b Chahal", "22", "20", "31", "0", "2", "110.00"],
    ["Ashutosh Sharma", "not out", "17", "11", "16", "1", "1", "154.54"]
  ];

  // 1. Who has the max runs (out or not out)
  console.log("Player(s) with max runs (out or not out):", getPlayerWithMaxRuns(sampleTable));

  // 2. Who has the max runs (out)
  console.log("Player(s) with max runs (out):", getPlayerWithMaxRunsOut(sampleTable));

  // 3. Who has the highest SR (a. min balls played 10)
  console.log("Player(s) with highest SR (min balls played 10):", getPlayerWithHighestSR(sampleTable, 10));

  // 4. Who has the highest SR (a. min balls played 10 OR min run scored 20)
  console.log("Player(s) with highest SR (min balls played 10 OR min runs scored 20):", getPlayerWithHighestSRWithCondition(sampleTable, 10, 20));

  // Repeat 3 and 4, for both (out or not out), and (out)
  // 3. Who has the highest SR (with min balls played), excluding not out
  console.log("Player(s) with highest SR (min balls played 10) excluding not out:", getPlayerWithHighestSR(sampleTable, 10, false));

  // 4. Who has the highest SR (with min balls played or min runs scored), excluding not out
  console.log("Player(s) with highest SR (min balls played 10 OR min runs scored 20) excluding not out:", getPlayerWithHighestSRWithCondition(sampleTable, 10, 20, false));

 */

//   //console.log(getBattersBelowMaxRuns_(sampleTable, maxRuns, battersToSelect));
//   console.log(getPlayersMaxStrikeRateData2_(sampleTable, false, true))
// }

// // Function to find player with maximum runs
// function getPlayerWithMaxRuns(battingData, includeNotOut = true) {
//   return findPlayersWithMaxValue(battingData, BattingTableIndex.Runs, 0, includeNotOut);
// }

// // Function to find player with maximum runs (out)
// function getPlayerWithMaxRunsOut(battingData) {
//   return findPlayersWithMaxValue(battingData.filter(player => player[BattingTableIndex.HowOut].trim().toLowerCase() !== BATTER_NOT_OUT.toLowerCase()), BattingTableIndex.Runs, 0, false);
// }

// // Function to find player with highest strike rate (with minimum balls played)
// function getPlayerWithHighestSR(battingData, minBallsPlayed = 10, includeNotOut = true) {
//   return findPlayersWithMaxValue(battingData.filter(player => Number(player[BattingTableIndex.BallsFaced]) >= minBallsPlayed), BattingTableIndex.StrikeRate, 0, includeNotOut);
// }

// // Function to find player with highest strike rate (with minimum balls played or minimum runs scored)
// function getPlayerWithHighestSRWithCondition(battingData, minBallsPlayed = 10, minRunsScored = 20, includeNotOut = true) {
//   return findPlayersWithMaxValue(battingData.filter(player =>
//     Number(player[BattingTableIndex.BallsFaced]) >= minBallsPlayed || Number(player[BattingTableIndex.Runs]) >= minRunsScored
//   ), BattingTableIndex.StrikeRate, 0, includeNotOut);
// }

// // Function to find players who can't score the current max runs (i.e., who are already out)
// function getPlayersCantScoreMaxRuns(battingData) {
//     const maxRunsPlayers = getPlayerWithMaxRuns(battingData);
//     return battingData
//         .filter(player => !maxRunsPlayers.includes(player[BattingTableIndex.Player]) && player[BattingTableIndex.HowOut].trim().toLowerCase() !== BATTER_NOT_OUT.toLowerCase())
//         .map(player => player[BattingTableIndex.Player]);
// }

// // Function to find players who can't score the highest SR (i.e., who are already out)
// function getPlayersCantScoreHighestSR(battingData, minBallsPlayed = 10) {
//     const highestSRPlayers = getPlayerWithHighestSR(battingData, minBallsPlayed);
//     return battingData
//         .filter(player => !highestSRPlayers.includes(player[BattingTableIndex.Player]) && player[BattingTableIndex.HowOut].trim().toLowerCase() !== BATTER_NOT_OUT.toLowerCase())
//         .map(player => player[BattingTableIndex.Player]);
// }

// // Generic function to find players with maximum value for a given index
// function findPlayersWithMaxValue(battingData, index, defaultValue, includeNotOut) {
//   if (!Array.isArray(battingData) || battingData.length === 0) {
//     return [];
//   }

//   let maxPlayers = [];
//   let maxValue = defaultValue;

//   for (const player of battingData) {
//     const value = Number(player[index]);
//     if (value > maxValue || (includeNotOut && value === maxValue && player[BattingTableIndex.HowOut].trim().toLowerCase() !== BATTER_NOT_OUT.toLowerCase())) {
//       maxPlayers = [player[BattingTableIndex.Player]];
//       maxValue = value;
//     } else if (value === maxValue) {
//       maxPlayers.push(player[BattingTableIndex.Player]);
//     }
//   }

//   return maxPlayers;
// }


// function getPlayersMaxStrikeRateData2_(battingData, excludeNotOut = false, checkMinRuns = false) {
//   const minRuns = 20; // Minimum number of runs
//   if (!Array.isArray(battingData) || battingData.length === 0) {
//     return {};
//   }

//   // If battingData is a single innings, convert it into an array
//   if (!Array.isArray(battingData[0])) {
//     battingData = [battingData];
//   }

//   let filteredData = battingData.filter((playerRow) => {
//     const ballsFaced = Number(playerRow[BattingTableIndex.BallsFaced]);
//     const runs = Number(playerRow[BattingTableIndex.Runs]);

//     return (ballsFaced >= getMinBallsForBatterSR_() || (checkMinRuns && runs >= minRuns)) &&
//       (!excludeNotOut || playerRow[BattingTableIndex.HowOut].trim().toLowerCase() !== BATTER_NOT_OUT.toLowerCase());
//   });

//   return findPlayersWithMaxValue_(filteredData, BattingTableIndex.StrikeRate, 0);
// }
