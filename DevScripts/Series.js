//Series//

/**
 * Shows a UI prompt to backup and reset tournament sheets.
 * If the user enters the correct tournament name, then the tournament sheets will be backed up and reset.
 * If the user cancels the prompt or enters the incorrect tournament name, then no sheets will be reset.
 */
function backupAndResetSeriesSheetsMenu(e) {
  activateSheet_(getScheduleViewSheet_());
  flushAndWait_();

  const ui = SpreadsheetApp.getUi();
  const promptMessage = `⚠️ Backup & Reset Current Tournament
  NOTE: Spreadsheet and Forms will be first backed up for the Current Active Tournament.

  If you are certain to reset all tournament setup specific sheets:
  ${scheduleTableSheetName}
  ${playersSheetName}
  ${seriesBetsPointsSheetName}
  ${dailyBetsResultsSheetName}
  ${dailyBetsSheetName}
  ${seriesBetsSheetName} 

  Enter the tournament name EXACTLY as below:

  ${getActiveSeriesName_()}
  `;
  const result = ui.prompt("Backup & Reset Current Tournament", promptMessage, ui.ButtonSet.OK_CANCEL);

  const button = result.getSelectedButton();
  if (button === ui.Button.OK && result.getResponseText().trim() === getActiveSeriesName_().trim()) {
    // Reset sheets
    backupAndResetSeriesSheets_(e);
    showInfoAlert_("All tournament specific sheets were reset. You can now setup sheets for the new tournament.");
  } else if (button !== ui.Button.CANCEL && button !== ui.Button.CLOSE) {
    activateSheet_(getSeriesViewSheet_());
    flushAndWait_();
    showWarningAlert_(`You entered incorrect tournament name ${result.getResponseText()}. No sheets were reset. Try again.`);
  }
}

/**
 * Performs a backup of the active spreadsheet and resets the tournament sheets.
 * 
 */
function backupAndResetSeriesSheets_(e) {
  logSTART_(arguments.callee.name);

  let clonedSheetName = copyAndArchiveActiveSpreadsheet_();
  log(`Before resetting, this Spreadsheet and all attached FORMs were cloned.\n\n[${clonedSheetName}]`, LogLevel.DEBUG);

  clearSeries_(e);

  flushAndWait_();
  logEND_(arguments.callee.name);
}

/**
 * Sets up the user interface for creating a new tournament.
 * @param {Object} e - The event object.
 */
function setupNewSeriesMenu(e) {
  logSTART_(arguments.callee.name);
  const isClear = tryClearSeries_(e);

  if (isClear) {
    activateSheet_(getSeriesViewSheet_());
    flushAndWait_();

    const ui = SpreadsheetApp.getUi();
    const result = ui.prompt(
      "New Tournament Setup",
      `Enter the ID from ${getSeriesViewSheet_().getName()} sheet\n\n`,
      ui.ButtonSet.OK_CANCEL
    );

    if (result.getSelectedButton() === ui.Button.OK) {
      const tournamentID = result.getResponseText();

      if (isValidNumber_(tournamentID) && isSeriesIdValid_(tournamentID)) {
        setupNewSeries_(e, tournamentID);
      }
    }
  }

  logEND_(arguments.callee.name);
}

/**
 * Checks if the provided series ID is valid and exists.
 * @param {string} seriesID - The series ID to check.
 * @returns {boolean} True if the series ID is valid and exists, false otherwise.
 */
function isSeriesIdValid_(seriesID) {
  if (isEmpty_(seriesID)) { return false; }

  const seriesData = getDataFromSheet_(seriesTableSheetName, true);
  return seriesData.some(tournamentRow => tournamentRow[colNameToIndex_(seriesSheetObj.idColName)] == seriesID);
}

/**
 * Sets up sheets for a new Series.
 * 
 * @param {number} seriesID - The ID of the new tournament.
 * @param {string} url - The URL of the tournament.
 */
function setupNewSeries_(e, seriesID) {
  toast_("Setup new Tournament", "Starting");

  setActiveSeriesStatus_(seriesID);
  resetActiveSeriesInfo_();

  updateScheduleSheet_(e);
  excludeMatchesByID_(ConfigManager.getConfigClass().EXCLUDED_MATCH_IDS_IN_SCHEDULE);

  activateSheet_(getSeriesViewSheet_());
  toast_("Setup new Tournament", "Done");
}

/**
 * Resets the user interface (UI) for the active tournament information.
 * This function delegates the actual reset to the private function `resetActiveTournamentInfo_`.
 */
function resetActiveSeriesInfoMenu() {
  resetActiveSeriesInfo_();
}

/**
 * Resets the active tournament information, including removing it, updating the spreadsheet name, and updating the attached form names.
 * This function is meant to be used internally and is not intended to be called directly by external code.
 * @private
 */
function resetActiveSeriesInfo_() {
  removeActiveSeriesFromCache_();

  //Reset Sheet and Form names
  updateActiveSpreadsheetName_();
  updateAttachedFormNames_();
}

/**
 * Retrieves the active series from cache or fetches it if not available.
 * @returns {Series|null} The active series object, or null if not found.
 */
function getActiveSeries_() {
  let activeSeries = getActiveSeriesFromCache_();

  if (isEmpty_(activeSeries)) {
    activeSeries = getActiveSeriesObj_();
    if (activeSeries) {
      putActiveSeriesInCache_(activeSeries);
    }
  }

  return activeSeries;
}

/**
 * Fetches the active series object from the series data.
 * @returns {Series} The active series object.
 */
function getActiveSeriesObj_() {
  const seriesData = getDataFromSheet_(seriesTableSheetName, true);
  const countriesData = getDataFromSheet_(countriesSheetName, true);
  const row = seriesData.find(row => row[colNameToIndex_(seriesSheetObj.activeStatusColName)] === STATUS_Y);

  if (!row) {
    console.log(`No Active Series found in '${seriesTableSheetName}' sheet.`);
    return null;
  }

  const hostCountry = row[colNameToIndex_(seriesSheetObj.hostCountryColName)].trim() || SeriesConstants.DEFAULT_HOST_COUNTRY;
  const timezoneRow = countriesData.find(countryRow => countryRow[colNameToIndex_(countriesMasterSheetObj.countryColName)] === hostCountry);
  const timezone = timezoneRow ? timezoneRow[colNameToIndex_(countriesMasterSheetObj.timezoneColName)].trim() : IST_TZ;
  const matchCategory = row[colNameToIndex_(seriesSheetObj.matchCategoryColName)].trim() || SeriesConstants.MATCH_TYPES_T20;

  const activeSeries = new Series(
    row[colNameToIndex_(seriesSheetObj.idColName)],
    row[colNameToIndex_(seriesSheetObj.nameColName)].trim(),
    toBoolean_(row[colNameToIndex_(seriesSheetObj.activeStatusColName)].trim()),
    row[colNameToIndex_(seriesSheetObj.urlColName)].trim(),
    hostCountry,
    matchCategory,
    toBoolean_(row[colNameToIndex_(seriesSheetObj.impactPlayerAllowedColName)]),
    timezone,
  );

  return activeSeries;
}

/**
 * Checks if there is an active series.
 * @returns {boolean} True if there is an active series, otherwise false.
 */
function isSeriesActive_() {
  const colIdx = colNameToIndex_(seriesSheetObj.activeStatusColName);
  const seriesData = getDataFromSheet_(seriesTableSheetName, true);
  return seriesData.some(row => row[colIdx] === STATUS_Y);
}

/**
 * Checks if there is no active series.
 * @returns {boolean} True if there is no active series, otherwise false.
 */
function isSeriesNotActive_() {
  return !isSeriesActive_();
}

/**
 * Gets the tournament name (Column B) where Active (Column C) = Y
 * @return {String} - The name of the active tournament
 */
function getActiveSeriesName_() {
  const activeSeries = getActiveSeries_();
  return activeSeries ? activeSeries.getName() : SeriesConstants.NEW_SERIES;
}

/**
 * Gets the ID of the active series.
 * @returns {number|null} The ID of the active series, or null if not found.
 */
function getActiveSeriesID_() {
  const activeSeries = getActiveSeries_();
  return activeSeries ? activeSeries.getId() : null;
}

/**
 * Retrieves the URL of the active series.
 * @returns {string} The URL of the active series.
 */
function getActiveSeriesURL_() {
  const activeSeries = getActiveSeries_();
  return activeSeries ? activeSeries.getHomeURL() : null;
}

/**
 * Retrieves the match category of the active series.
 * @returns {string} The match category of the active series.
 */
function getActiveSeriesMatchCategory_() {
  const activeSeries = getActiveSeries_();
  return activeSeries ? activeSeries.getCategory() : null;
}

/**
 * Retrieves the timezone of the active series.
 *
 * @returns {string} The timezone of the active series, or the default timezone (IST_TZ) if there is no active series.
 */
function getActiveSeriesTimezone_() {
  const activeSeries = getActiveSeries_();
  return activeSeries ? activeSeries.getTimezone() : IST_TZ;
}

/**
 * Set the given tournament as the active tournament.
 * @param {number} seriesID - The ID of the tournament to set as active.
 */
function setActiveSeriesStatus_(seriesID) {
  const sheet = getSeriesTableSheet_();
  activateSheet_(sheet);
  clearActiveSeriesStatus_();

  // Find the row num of the tournament with the given ID.
  let rowNum = getSheetRowNum_(getSeriesTableSheet_(), colNameToIndex_(seriesSheetObj.idColName), seriesID);

  // Set the active status of the tournament with the given ID to 'Y'.
  sheet.getRange(rowNum, colNameToNum_(seriesSheetObj.activeStatusColName)).setValue(STATUS_Y);

  flushAndWait_();
}


