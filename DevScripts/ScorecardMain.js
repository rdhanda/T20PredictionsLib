//Scorecard.gs//

function testScorecard(e) {
  const sheet = getSheetByName_(match1SheetName);
  if (isNotEmpty_(getMatchID_(sheet))) {
    updateSheetWithScorecardData_(e, sheet);
  }
}

const innings1str = "**1st: ";
const innings2str = "**2nd: ";
const empty2CellsArray = [[null, null]];

/**
 * Performs pre-scorecard update tasks, including flushing, showing the sheet, and activating it.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet on which to perform pre-scorecard update tasks.
 */
function doBeforeScorecardUpdates_(sheet) {
  flushAndWait_();

  if (sheet.isSheetHidden()) {
    sheet.showSheet();
  }
  activateSheet_(sheet);
}

/**
 * Updates the sheet with scorecard data.
 * @param {Object} e - The event object.
 * @param {Object} sheet - The sheet object to update.
 * @returns {Object|null} The match result, or null if no data is processed.
 */
function updateSheetWithScorecardData_(e, sheet) {
  console.time(`${updateSheetWithScorecardData_.name}${sheet.getName()}`);

  doBeforeScorecardUpdates_(sheet);

  const matchID = getMatchID_(sheet);
  const urls = [getFullScorecardURL_(matchID)];
  const urlIndices = { main: 0 };

  const urlLive = getLiveScorecardURL_(matchID);
  urlIndices.live = urls.push(urlLive) - 1;

  const urlPlayingXI = getPlayingXIURL_(matchID);
  urlIndices.playingXI = urls.push(urlPlayingXI) - 1;

  const cheerioInstances = getCheerioInstances_(urls);
  const $main = cheerioInstances[urlIndices.main];
  const $live = cheerioInstances[urlIndices.live];
  const $playingXI = cheerioInstances[urlIndices.playingXI];

  const matchResult = isNotEmpty_($main) ? processScorecardData_(sheet, $main, $live, $playingXI) : null;

  console.timeEnd(`${updateSheetWithScorecardData_.name}${sheet.getName()}`);
  return matchResult;
}

/**
 * Processes scorecard data based on the provided HTML content and updates the specified sheet accordingly.
 *
 * @param {Sheet} sheet - The Google Sheets spreadsheet to be updated.
 * @param {string} $main - The HTML content containing scorecard information.
 * @returns {string} - The match status after the scorecard data processing (e.g., "ABANDONED", "LIVE", "READY").
 *
 * @throws {Error} If an error occurs during the scorecard data processing.
 *
 * @example
 * const matchStatus = processScorecardData_(spreadsheet, htmlContent, false);
 * console.log(`Match Status: ${matchStatus}`);
 */
function processScorecardData_(sheet, $main, $live, $playingXI) {
  let matchRunningStatus = processMatchRunningStatus_($main);
  if (isEmpty_(matchRunningStatus)) {
    log(`Unable to find the Match Running status. Please verify if there have been any changes in the CSS selector.`, LogLevel.INFO);
    matchRunningStatus = "N/A";
  }

  let scorecardStatus = getScorecardStatus_($main);
  if (isEmpty_(scorecardStatus)) {
    log(`Unable to find the Match Scorecard status. Please verify if there have been any changes in the CSS selector.`, LogLevel.INFO);
    scorecardStatus = "N/A";
  }

  if (isMatchNoResult_(scorecardStatus) || isMatchNoResult_(matchRunningStatus)) {
    return processNoResultMatch_(sheet, matchRunningStatus, scorecardStatus);
  } else {
    return processActiveMatch_(sheet, $main, $live, $playingXI, matchRunningStatus, scorecardStatus);
  }
}

/**
 * Processes and formats the match running status based on the provided HTML content.
 *
 * @param {string} $ - The HTML content containing match information.
 * @returns {string} - The processed match running status, including any additional run rate information.
 *
 * @example
 * const runningStatus = processMatchRunningStatus_(htmlContent);
 * console.log(`Formatted Running Status: ${runningStatus}`);
 */
function processMatchRunningStatus_($) {
  const matchRunningStatus = getMatchRunningStatus_($) || '';
  const runRateStatus = getCurrentRequiredRunRateStatus_($) || '';

  return `${matchRunningStatus}\n${runRateStatus}`.trim();
}

/**
 * Processes an abandoned match, updates the specified sheet, and returns the match result.
 *
 * @param {Sheet} sheet - The Google Sheets spreadsheet to be updated.
 * @param {string} matchRunningStatus - The match running status indicating abandonment.
 * @param {string} scorecardStatus - The scorecard status to set in the sheet.
 * @returns {string} - The match result after processing the abandonment (e.g., "ABANDONED").
 *
 * @example
 * const matchResult = processAbandonedMatch_(spreadsheet, "Match Abandoned", "Abandoned");
 * console.log(`Match Result: ${matchResult}`);
 * 
 * https://www.espncricinfo.com/series/india-in-south-africa-2023-24-1387592/south-africa-vs-india-1st-t20i-1387597/full-scorecard
 * 
 */
function processNoResultMatch_(sheet, matchRunningStatus, scorecardStatus) {
  sheet.getRange(matchSheetObj.scoreResultCopyRange).clearContent();
  sheet.getRange(matchSheetObj.scoreDisplayDataRange).clearContent();

  updateMatchPlayingXIData_(sheet, {});
  updateScorecardData_(sheet, null, scorecardStatus, matchRunningStatus, null, null, null, {}, {}, empty2CellsArray, empty2CellsArray, '');

  return MatchResultStatus.NO_RESULT;
}

/**
 * Processes an active (non-abandoned) match, updates the specified sheet, and returns the match status.
 *
 * @param {Sheet} sheet - The Google Sheets spreadsheet to be updated.
 * @param {string} $main - The HTML content containing match information.
 * @param {string} scorecardStatus - The scorecard status to set in the sheet.
 * @returns {string} - The match status after processing the active match (e.g., "LIVE", "READY").
 *
 * @throws {Error} If an error occurs during the processing of the active match.
 *
 * @example
 * const matchStatus = processActiveMatch_(spreadsheet, htmlContent, scorecardStatus, false);
 * console.log(`Match Status: ${matchStatus}`);
 */
function processActiveMatch_(sheet, $main, $live, $playingXI, matchRunningStatus, scorecardStatus) {
  const allTablesObj = extractCategorizedTablesData_($main);
  const teamsScoresTargetMaxOversObj = getTeamsScoresTargetMaxOvers_($main);
  const forecast = getMatchForecast_($main);

  const firstBattingTeam = teamsScoresTargetMaxOversObj.firstBattingTeam;
  const innings1MaxOvers = teamsScoresTargetMaxOversObj.innings1MaxOvers;
  const secondBattingTeam = teamsScoresTargetMaxOversObj.secondBattingTeam;

  const allInningsScoreData = extractInningsScoreData_(allTablesObj.battingTables);
  const inningsScoreOversObj = getInningsScore_(allInningsScoreData);
  const hasMatchStarted = allTablesObj.hasMatchStarted;

  const matchDetailsObj = extractMatchDetailsTableValues_(allTablesObj.matchDetailsTable);
  const teamsScoreTargetForecast = `${teamsScoresTargetMaxOversObj.display}\n${forecast}`;

  const runs2Win = extractTeamAndRunsNeededToWin_(matchRunningStatus).runsNeeded;
  const wickets2TakeObj = getRemainingWicketsToTake_(allTablesObj);
  const potm = extractPOTM_($main).display;

  const resultsObj = getResultsDetails_(allTablesObj, inningsScoreOversObj, matchDetailsObj, matchRunningStatus, runs2Win, wickets2TakeObj.innings2WicketsToTake, potm);

  const firstInningsCompleted = isFirstInningsCompleted_(allTablesObj, inningsScoreOversObj.innings1Overs, scorecardStatus, innings1MaxOvers);

  const firstSecondBattingTeams = hasMatchStarted ? [[firstBattingTeam, secondBattingTeam]] : empty2CellsArray;
  const closestMargins = getCurrentClosestMargins_(sheet, resultsObj.runsMarginResult, resultsObj.wicketsMarginResult, firstBattingTeam, secondBattingTeam, resultsObj.matchWin);
  const closestMarginsValues = [[closestMargins.runsMargin, closestMargins.wicketsMargin]];
  const matchStatus = resultsObj.isResultReady ? MatchResultStatus.READY : MatchResultStatus.LIVE;

  //Update non results data, e.g. playing XI, smudging lists, live scorecard
  const playingXIObj = getPlayingXIData_($playingXI, sheet, firstBattingTeam) || {};
  const smudgingListsObj = getAllSmudgingLists_(allTablesObj, playingXIObj.team1PlayingXI, playingXIObj.team2PlayingXI, playingXIObj.team1ImpactPlayers, playingXIObj.team2ImpactPlayers, firstInningsCompleted, resultsObj.maxRunsObj, resultsObj.maxWicketsObj.value, runs2Win, wickets2TakeObj) || {};

  const liveScorecardInfo = getLiveScorecardStats_($live) || '';
  const liveScorecardInfo1 = liveScorecardInfo?.part1 || '';
  const liveScorecardInfo2 = liveScorecardInfo?.part2 || '';
  const liveScorecardInfo3 = liveScorecardInfo?.part3 || '';
  const playersCount = playingXIObj.playersCount || '';

  updateScorecardData_(sheet, firstInningsCompleted, scorecardStatus, teamsScoreTargetForecast, matchRunningStatus, liveScorecardInfo1, liveScorecardInfo2, liveScorecardInfo3, resultsObj, smudgingListsObj, firstSecondBattingTeams, closestMarginsValues, playersCount);

  return matchStatus;
}

/**
 * Builds and formats the match running status by combining the original status with run rate information.
 *
 * @param {string} runRateStatus - The run rate status information to be added.
 * @param {string} originalMatchRunningStatus - The original match running status.
 * @returns {string} - The formatted match running status with run rate information.
 *
 * @example
 * const formattedStatus = buildMatchRunningStatus_("Run rate: 6.2", "In Progress");
 * console.log(`Formatted Status: ${formattedStatus}`);
 */
function buildMatchRunningStatus_(runRateStatus, originalMatchRunningStatus) {
  if (isNotEmpty_(originalMatchRunningStatus) && isNotEmpty_(runRateStatus)) {
    return originalMatchRunningStatus.trim() + ' ' + runRateStatus;
  }
  return originalMatchRunningStatus;
}

/**
 * Updates the scorecard data in the specified Google Sheets.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The Google Sheets sheet object to update.
 * @param {boolean} firstInningsCompleted - Indicates if the first innings is completed.
 * @param {string} scorecardStatus - The status of the scorecard.
 * @param {string} teamsScoreTargetForecast - Forecast of teams' scores/targets.
 * @param {string} matchRunningStatus - The running status of the match.
 * @param {string} liveScorecardInfo1 - Information for the live scorecard (part 1).
 * @param {string} liveScorecardInfo2 - Information for the live scorecard (part 2).
 * @param {Object} resultsObj - Object containing match results.
 * @param {Object} smudgingListsObj - Object containing smudging lists.
 * @param {string[][]} firstSecondBattingTeams - Teams batting order for first and second innings.
 * @param {string[][]} closestMargins - Closest margins in the match.
 * @param {number} playersCount - Number of players.
 */
function updateScorecardData_(sheet, firstInningsCompleted, scorecardStatus, teamsScoreTargetForecast, matchRunningStatus, liveScorecardInfo1, liveScorecardInfo2, liveScorecardInfo3, resultsObj, smudgingListsObj, firstSecondBattingTeams, closestMargins, playersCount) {
  const newLines = scorecardStatus && scorecardStatus.length < 15 ? "\n\n\n" : "\n\n";
  scorecardStatus = `${scorecardStatus.toUpperCase()}${newLines}${getScoreUpdatedTimestamp_()}`;

  const rangesToUpdate = [
    { range: matchSheetObj.scoreResultCopyRange, values: resultsObj.values },
    { range: matchSheetObj.scorecardStatusRange, value: scorecardStatus },
    { range: matchSheetObj.teamsScoreTargetForecastRange, value: teamsScoreTargetForecast },
    { range: matchSheetObj.matchRunningStatusRange, value: matchRunningStatus },
    { range: matchSheetObj.liveScorecardRange1, value: liveScorecardInfo1 },
    { range: matchSheetObj.liveScorecardRange2, value: liveScorecardInfo2 },
    { range: matchSheetObj.liveScorecardRange3, value: liveScorecardInfo3 },
    { range: matchSheetObj.playersCountCell, value: playersCount },
    { range: matchSheetObj.flagsRange, values: [[firstInningsCompleted, resultsObj.isMatchDone, resultsObj.isResultReady]] },
    { range: matchSheetObj.potmProbableSmudgingListsRange, values: [[resultsObj.potmProbables, smudgingListsObj.maxRunsListDisplay, smudgingListsObj.highestSRListDisplay, smudgingListsObj.maxWicketsListDisplay, smudgingListsObj.bestEconomyListDisplay]] },
    { range: matchSheetObj.firstSecondBattingTeamsRange, values: firstSecondBattingTeams },
    { range: matchSheetObj.closestMarginsRange, values: closestMargins },
  ];

  // Update the specified ranges in the sheet
  updateSheetRanges_(sheet, rangesToUpdate);

  // Update the refresh counter in the sheet
  updateRefreshCounter_(sheet, matchSheetObj.resultRefreshCounterRange);
}

/**
 * Retrieves detailed results and statistics for a cricket match.
 *
 * @param {Object} allTablesObj - The object containing all relevant tables.
 * @param {Object} inningsScoreOversObj - The object containing innings scores and overs information.
 * @param {Object} matchDetailsObj - The object containing details about the match.
 * @param {string} matchRunningStatus - The running status of the match.
 * @param {number} runs2Win - The runs needed to win.
 * @param {number} wickets2Take - The wickets needed to win.
 * @returns {Object} An object containing detailed results and statistics.
 * @property {string} tossWin - The team that won the toss.
 * @property {string} innings1Score - The score of the first innings.
 * @property {string} innings2Score - The score of the second innings.
 * @property {string} matchWin - The team that won the match.
 * @property {number} runsMarginResult - The runs margin or runs needed to win.
 * @property {number} wicketsMarginResult - The wickets margin or wickets needed to win.
 * @property {string} potm - The Player of the Match.
 * @property {Object} maxRunsObj - Object containing information about the player with the maximum runs.
 * @property {Object} highestSRObj - Object containing information about the player with the highest strike rate.
 * @property {Object} maxWicketsObj - Object containing information about the player with the maximum wickets.
 * @property {Object} bestEconomyObj - Object containing information about the player with the best economy.
 * @property {Array} values - A 2D array for updating the sheet results row.
 * @property {Array} potmProbables - An array of potential Player of the Match candidates.
 * @property {boolean} isMatchDone - Indicates if the match is completed.
 * @property {boolean} isResultReady - Indicates if the detailed results are ready.
 */
function getResultsDetails_(allTablesObj, inningsScoreOversObj, matchDetailsObj, matchRunningStatus, runs2Win, wickets2Take, potmMvp) {
  const tossWin = getTossWinningTeam_(matchDetailsObj.toss, matchRunningStatus);
  const innings1Score = inningsScoreOversObj.innings1Score;
  const innings2Score = inningsScoreOversObj.innings2Score;
  const winnerTeamDetails = extractWinningTeamDetails_(matchRunningStatus);
  const matchWin = getMatchWinningTeam_(winnerTeamDetails.winningTeamName);
  const winMarginRuns = winnerTeamDetails.winningMarginRuns;
  const winMarginWickets = winnerTeamDetails.winningMarginWickets;
  const potm = isEmpty_(potmMvp) ? sanitizePlayerName_(matchDetailsObj.potm) : potmMvp;

  const battingData = extractBattingData_(allTablesObj.battingTables);
  const maxRunsObj = getPlayersMaxRunsData_(battingData);
  const highestSRObj = getPlayersMaxStrikeRateData_(battingData);

  const bowlingData = extractBowlingData_(allTablesObj.bowlingTables);
  const maxWicketsObj = getPlayersMaxWicketsData_(bowlingData);
  const bestEconomyObj = getPlayersBestEconomyData_(bowlingData);

  const winMarginAvailble = isNotEmpty_(winMarginRuns) || isNotEmpty_(winMarginWickets);
  const isMatchDone = isNotEmpty_(tossWin) && isNotEmpty_(innings1Score) && isNotEmpty_(innings2Score) && isNotEmpty_(matchWin) && winMarginAvailble;

  const areCategoriesComplete = isNotEmpty_(potm) && isNotEmpty_(maxRunsObj.display) && isNotEmpty_(highestSRObj.display) && isNotEmpty_(maxWicketsObj.display) && isNotEmpty_(bestEconomyObj.display);
  const isResultReady = (isMatchDone && areCategoriesComplete);

  const runsMarginResult = winMarginAvailble ? winMarginRuns : runs2Win;
  const wicketsMarginResult = winMarginAvailble ? winMarginWickets : wickets2Take;

  //@TODO add second innings score when ready. NOTE: 'values' is used for updating the sheet results row
  const values = [[tossWin, innings1Score, matchWin, runsMarginResult, wicketsMarginResult, potm, maxRunsObj.display, highestSRObj.display, maxWicketsObj.display, bestEconomyObj.display]];
  const potmProbables = getDisplayList_([...new Set(safeConcatPlayers_(maxRunsObj, highestSRObj, maxWicketsObj, bestEconomyObj))].sort());

  console.info(`RESULTs: \nTossWin = '${tossWin}', \nInnings1Score = '${innings1Score}', \nInnings2Score = '${innings2Score}', \nMatchWin = '${matchWin}', \nRunsMarginResult = '${runsMarginResult}', \nWicketsMarginResult = '${wicketsMarginResult}', \nPOTM = '${potm}', \nMaxRuns = '${(maxRunsObj.display)}', \nHighestSR = '${(highestSRObj.display)}', \nMaxWickets = '${(maxWicketsObj.display)}', \nBestEconomy = '${(bestEconomyObj.display)}', \nIsMatchDone = '${isMatchDone}', \nIsResultReady = '${isResultReady}'`);

  return { tossWin, innings1Score, innings2Score, matchWin, runsMarginResult, wicketsMarginResult, potm, maxRunsObj, highestSRObj, maxWicketsObj, bestEconomyObj, values, potmProbables, isMatchDone, isResultReady };
}

/**
 * 
 */
function getRemainingWicketsToTake_(allTablesObj) {
  const innings1Wickets = getSecondInningsWickets_(allTablesObj.battingTables).innings1Wickets;
  const innings2Wickets = getSecondInningsWickets_(allTablesObj.battingTables).innings2Wickets;

  const innings1WicketsToTake = isValidNumber_(innings1Wickets) ? 10 - innings1Wickets : null;
  const innings2WicketsToTake = isValidNumber_(innings2Wickets) ? 10 - innings2Wickets : null;

  return { innings1WicketsToTake, innings2WicketsToTake };
}

/**
 * Checks if the first innings is completed based on the available data.
 * @param {object} allTablesObj - An object containing all the tables data.
 * @param {number} innings1Overs - The number of overs played in the first innings.
 * @returns {boolean} A boolean value indicating if the first innings is completed.
 */
function isFirstInningsCompleted_(allTablesObj, innings1Overs, scorecardStatus, innings1MaxOvers) {
  const allInningsWicketsData = extractFallOfWicketsData_(allTablesObj.battingTables);

  const inningsWicketsObj = getInningsWickets_(allInningsWicketsData);
  const innings1Wickets = inningsWicketsObj.innings1Wickets;

  const inningsBreak = isInningsBreak_(allTablesObj.battingTables, scorecardStatus, innings1Overs, innings1Wickets, innings1MaxOvers);
  const secondInningsOn = isSecondInningsOn_(allTablesObj.battingTables);

  return inningsBreak || secondInningsOn;
}

/**
 * To implement usage of it
 */
function getSecondInningsWickets_(battingTables) {
  const allInningsWicketsData = extractFallOfWicketsData_(battingTables);

  const inningsWicketsObj = getInningsWickets_(allInningsWicketsData);
  const innings1Wickets = inningsWicketsObj.innings1Wickets;
  const innings2Wickets = inningsWicketsObj.innings2Wickets;

  return { innings1Wickets, innings2Wickets };
}

/**
 * Checks if the first innings of a T20 match is currently on.
 *
 * @param {string[][][]} battingTables - The batting tables for all innings.
 * @param {number} numOfOvers - The number of overs bowled in the first innings.
 * @param {number} numOfWickets - The number of wickets fallen in the first innings.
 * @returns {boolean} True if the first innings is currently on, false otherwise.
 */
function isFirstInningsOn_(battingTables, numOfOvers, numOfWickets) {
  if (isEmpty_(battingTables)) {
    return false;
  } else if (battingTables.length === 1 && numOfOvers < getMaxOversPerInnings_() && numOfWickets < MAX_WICKETS_PER_INNINGS) {
    return true;
  }

  return false;
}

/**
 * Checks if there is an innings break between the first and second innings of a T20 match.
 *
 * @param {string[][][]} battingTables - The batting tables for all innings.
 * @param {number} numOfOvers - The number of overs bowled in the first innings.
 * @param {number} numOfWickets - The number of wickets fallen in the first innings.
 * @param {number} maxOvers - The maximum number of overs allowed in an innings.
 * @returns {boolean} True if there is an innings break, false otherwise.
 */
function isInningsBreak_(battingTables, scorecardStatus, numOfOvers, numOfWickets, maxOvers = getMaxOversPerInnings_()) {
  if (isEmpty_(battingTables)) {
    return false;
  } else if (battingTables.length === 1 && (scorecardStatus.trim().toLowerCase() == "innings break" || convertToDecimalOvers_(numOfOvers) === maxOvers || numOfWickets === MAX_WICKETS_PER_INNINGS)) {
    return true;
  }

  return false;
}

/**
 * Checks if the second innings of a T20 match is currently on.
 *
 * @param {string[][][]} battingTables - The batting tables for all innings.
 * @returns {boolean} True if the second innings is currently on, false otherwise.
 */
function isSecondInningsOn_(battingTables) {
  if (isEmpty_(battingTables)) { return false; }

  return battingTables.length === 2;
}

/**
 * Checks if the input string contains any of the specified match strings.
 *
 * @param {string} input - The input string to be checked.
 * @returns {boolean} - Returns true if the input contains any of the match strings, false otherwise.
 */
function isMatchNoResult_(input) {
  if (isEmpty_(input)) { return false; }

  const lowercaseInput = input.toLowerCase();
  return NoResultMatchResultStatusList.some(matchString => lowercaseInput.includes(matchString.toLowerCase()));
}

/**
 * Extracts the innings score, overs from the given data.
 * @param {Array<Array<string>>} data - The data containing the score information.
 * @returns {Object} An object with innings numbers and corresponding scores, overs.
 */
function getInningsScore_(data) {
  if (!Array.isArray(data) || data.length === 0) { return {}; }

  return data.reduce(function (result, row, index) {
    var scoreString = row[2];
    var scoreParts = scoreString.split('/');
    var score = scoreParts.length > 1 ? parseInt(scoreParts[0]) : parseInt(scoreString);
    var oversString = row[1];
    var overs = parseFloat(oversString.split(' ')[0]);
    result['innings' + (index + 1) + 'Score'] = score;
    result['innings' + (index + 1) + 'Overs'] = overs;
    return result;
  }, {});
}

/**
 * Extracts the number of wickets for each innings from the given data.
 * @param {Array<Array<string>>} data - The data containing the wicket information.
 * @returns {Object} An object with innings numbers and corresponding number of wickets.
 */
function getInningsWickets_(data) {
  if (!Array.isArray(data) || data.length === 0) { return {}; }

  return data.reduce(function (inningsWickets, innings, index) {
    var wicketString = innings[0];
    var numberOfWickets = extractNumberOfWickets_(wicketString);
    inningsWickets['innings' + (index + 1) + 'Wickets'] = numberOfWickets;
    return inningsWickets;
  }, {});
}

/**
 * Gets the team code of the toss-winning team.
 *
 * @param {string} matchTossDetails - The match toss details.
 * @param {string} matchRunningStatus - The match running status.
 * @returns {string|null} The team code of the toss-winning team, or null if not found.
 */
function getTossWinningTeam_(matchTossDetails, matchRunningStatus) {
  let teamName = extractTossWinningTeam_(matchTossDetails);
  if (isEmpty_(teamName)) {
    teamName = extractTossWinningTeam_(matchRunningStatus);
  }
  const teamCode = getTeamIdByName_(teamName);
  return teamCode;
}

/**
 * Gets the team code of the match-winning team.
 *
 * @param {string} teamName - The name of the match-winning team.
 * @returns {string|null} The team code of the match-winning team, or null if not found.
 */
function getMatchWinningTeam_(teamName) {
  const teamCode = getTeamIdByName_(teamName);
  return teamCode;
}

/**
 * Calculates the maximum runs across all innings.
 * @param {Array<Array<any>>} battingData - The array of arrays containing the players' data for all innings.
 * @returns {Object} An object with the maximum runs and the players who achieved it.
 *                  The object has the following structure:
 *                  {
 *                    maxRuns: number,
 *                    players: Array<string>
 *                  }
 */
function getPlayersMaxRunsData_(battingData) {
  return findPlayersWithMaxValue_(battingData, BattingTableIndex.Runs, 0);
}

/**
 * Retrieves the data of players with the maximum strike rate from the batting data.
 *
 * @param {Array} battingData - The batting data containing the player information.
 * @param {boolean} [excludeNotOut=false] - Optional. Specifies whether to exclude not out players from consideration. Default is false.
 * @returns {Object} The data of players with the maximum strike rate.
 */
function getPlayersMaxStrikeRateData_(battingData, excludeNotOut = false) {
  if (!Array.isArray(battingData) || battingData.length === 0) {
    return {};
  }

  // If battingData is a single innings, convert it into an array
  if (!Array.isArray(battingData[0])) {
    battingData = [battingData];
  }

  let filteredData = battingData.filter((inningData) =>
    Number(inningData[BattingTableIndex.BallsFaced]) >= getMinBallsForBatterSR_()
  );

  if (excludeNotOut) {
    filteredData = filteredData.filter((inningData) => {
      return inningData[BattingTableIndex.HowOut].trim().toLowerCase() !== BATTER_NOT_OUT.toLowerCase();
    }
    );
  }
  return findPlayersWithMaxValue_(filteredData, BattingTableIndex.StrikeRate, 0);
}

/**
 * Gets the players with the maximum number of wickets.
 *
 * @param {Array} bowlingData - The data of the players.
 * @param {number} wicketsIndex - The index of the wickets column in the players data.
 * @returns {Array} The players with the maximum number of wickets.
 */
function getPlayersMaxWicketsData_(bowlingData) {
  return findPlayersWithMaxValue_(bowlingData, BowlingTableIndex.Wickets, 0);
}

/**
 * Gets the players with the minimum bowling economy.
 *
 * @param {Array} bowlingData - The data of the players.
 * @returns {Object} The player with the minimum bowling economy.
 */
function getPlayersBestEconomyData_(bowlingData, minimumOversBowled = getMinOversForBowlerEconomy_()) {
  if (!Array.isArray(bowlingData) || bowlingData.length === 0) {
    return {};
  }

  const filteredData = bowlingData.filter((playerData) =>
    Number(playerData[BowlingTableIndex.Overs]) >= minimumOversBowled
  );

  return findPlayersWithMinValue_(filteredData, BowlingTableIndex.Economy);
}
