//DailyBetsResult.gs//

/**
 * Represents a DailyBetsResult.
 * @class
 */
class DailyBetsResult {
  /**
   * Creates an instance of DailyBetsResult.
   * @param {string} matchID - The match ID.
   * @param {string} entryID - The entry ID.
   * @param {string} punterName - The punter's name.
   * @param {string} tossWinTeam - The team that won the toss.
   * @param {number} firstInningsScore - The first innings score.
   * @param {string} matchWinTeam - The team that won the match.
   * @param {number} runsMargin - The margin of runs.
   * @param {number} wicketsMargin - The margin of wickets.
   * @param {string} potm - Player of the match.
   * @param {number} maxRuns - Maximum runs scored.
   * @param {number} highestSR - Highest strike rate.
   * @param {number} maxWickets - Maximum wickets taken.
   * @param {number} bestEconomy - Best economy rate.
   * @param {number} points - Total points.
   * @param {number} rank - Rank achieved.
   * @param {number} tossWinPoints - Points for winning the toss.
   * @param {number} firstInningsScorePoints - Points for first innings score.
   * @param {number} matchWinPoints - Points for winning the match.
   * @param {number} runsMarginPoints - Points for runs margin.
   * @param {number} wicketsMarginPoints - Points for wickets margin.
   * @param {number} potmPoints - Points for player of the match.
   * @param {number} maxRunsPoints - Points for maximum runs.
   * @param {number} highestSRPoints - Points for highest strike rate.
   * @param {number} maxWicketsPoints - Points for maximum wickets.
   * @param {number} bestEconomyPoints - Points for best economy rate.
   */
  constructor(
    matchID,
    entryID,
    punterName,
    tossWinTeam,
    firstInningsScore,
    matchWinTeam,
    runsMargin,
    wicketsMargin,
    potm,
    maxRuns,
    highestSR,
    maxWickets,
    bestEconomy,
    points,
    rank,
    tossWinPoints,
    firstInningsScorePoints,
    matchWinPoints,
    runsMarginPoints,
    wicketsMarginPoints,
    potmPoints,
    maxRunsPoints,
    highestSRPoints,
    maxWicketsPoints,
    bestEconomyPoints
  ) {
    var matchID_ = matchID;
    var entryID_ = entryID;
    var punterName_ = punterName;
    var tossWinTeam_ = tossWinTeam;
    var firstInningsScore_ = firstInningsScore;
    var matchWinTeam_ = matchWinTeam;
    var runsMargin_ = runsMargin;
    var wicketsMargin_ = wicketsMargin;
    var potm_ = potm;
    var maxRuns_ = maxRuns;
    var highestSR_ = highestSR;
    var maxWickets_ = maxWickets;
    var bestEconomy_ = bestEconomy;
    var points_ = points;
    var rank_ = rank;
    var tossWinPoints_ = tossWinPoints;
    var firstInningsScorePoints_ = firstInningsScorePoints;
    var matchWinPoints_ = matchWinPoints;
    var runsMarginPoints_ = runsMarginPoints;
    var wicketsMarginPoints_ = wicketsMarginPoints;
    var potmPoints_ = potmPoints;
    var maxRunsPoints_ = maxRunsPoints;
    var highestSRPoints_ = highestSRPoints;
    var maxWicketsPoints_ = maxWicketsPoints;
    var bestEconomyPoints_ = bestEconomyPoints;

    // Getters
    this.getMatchID = () => matchID_;
    this.getEntryID = () => entryID_;
    this.getPunterName = () => punterName_;
    this.getTossWinTeam = () => tossWinTeam_;
    this.getFirstInningsScore = () => firstInningsScore_;
    this.getMatchWinTeam = () => matchWinTeam_;
    this.getRunsMargin = () => runsMargin_;
    this.getWicketsMargin = () => wicketsMargin_;
    this.getPotm = () => potm_;
    this.getMaxRuns = () => maxRuns_;
    this.getHighestSR = () => highestSR_;
    this.getMaxWickets = () => maxWickets_;
    this.getBestEconomy = () => bestEconomy_;
    this.getPoints = () => points_;
    this.getRank = () => rank_;
    this.getTossWinPoints = () => tossWinPoints_;
    this.getFirstInningsScorePoints = () => firstInningsScorePoints_;
    this.getMatchWinPoints = () => matchWinPoints_;
    this.getRunsMarginPoints = () => runsMarginPoints_;
    this.getWicketsMarginPoints = () => wicketsMarginPoints_;
    this.getPotmPoints = () => potmPoints_;
    this.getMaxRunsPoints = () => maxRunsPoints_;
    this.getHighestSRPoints = () => highestSRPoints_;
    this.getMaxWicketsPoints = () => maxWicketsPoints_;
    this.getBestEconomyPoints = () => bestEconomyPoints_;

    // Derived attributes
    this.getIsAutoSubmitted = () => entryID_ && entryID_.includes(BOT_SUBMITTED_EMOJI);
    this.getIsAutoSubmittedSameAsBot = () => entryID_ && entryID_.includes(BOT_SUBMITTED_EMOJI) && entryID_.includes(STAR_CHAR);
    this.getIsUsingTrumpCard = () => entryID_ && entryID_.includes(TrumpCardKeys.INDICATOR_EMOJI);
    this.getIsPunterRow = () => entryID_ && !entryID_.includes(RESULT);
    this.getIsResultRow = () => entryID_ && entryID_.includes(RESULT);
  }

  /**
   * Serializes the DailyBetsResult instance to a plain object.
   * @returns {Object} The plain object representation of DailyBetsResult.
   */
  toObject() {
    return {
      matchID: this.getMatchID(),
      entryID: this.getEntryID(),
      punterName: this.getPunterName(),
      tossWinTeam: this.getTossWinTeam(),
      firstInningsScore: this.getFirstInningsScore(),
      matchWinTeam: this.getMatchWinTeam(),
      runsMargin: this.getRunsMargin(),
      wicketsMargin: this.getWicketsMargin(),
      potm: this.getPotm(),
      maxRuns: this.getMaxRuns(),
      highestSR: this.getHighestSR(),
      maxWickets: this.getMaxWickets(),
      bestEconomy: this.getBestEconomy(),
      points: this.getPoints(),
      rank: this.getRank(),
      tossWinPoints: this.getTossWinPoints(),
      firstInningsScorePoints: this.getFirstInningsScorePoints(),
      matchWinPoints: this.getMatchWinPoints(),
      runsMarginPoints: this.getRunsMarginPoints(),
      wicketsMarginPoints: this.getWicketsMarginPoints(),
      potmPoints: this.getPotmPoints(),
      maxRunsPoints: this.getMaxRunsPoints(),
      highestSRPoints: this.getHighestSRPoints(),
      maxWicketsPoints: this.getMaxWicketsPoints(),
      bestEconomyPoints: this.getBestEconomyPoints(),
      isAutoSubmitted: this.getIsAutoSubmitted(),
      isAutoSubmittedSameAsBot: this.getIsAutoSubmittedSameAsBot(),
      isUsingTrumpCard: this.getIsUsingTrumpCard(),
      isPunterRow: this.getIsPunterRow(),
      isResultRow: this.getIsResultRow(),
    };
  }

  /**
   * Static method to create a DailyBetsResult instance from a plain object.
   * @param {Object} obj - The plain object representation of DailyBetsResult.
   * @returns {DailyBetsResult} The DailyBetsResult instance.
   */
  static fromObject(obj) {
    return new DailyBetsResult(
      obj.matchID,
      obj.entryID,
      obj.punterName,
      obj.tossWinTeam,
      obj.firstInningsScore,
      obj.matchWinTeam,
      obj.runsMargin,
      obj.wicketsMargin,
      obj.potm,
      obj.maxRuns,
      obj.highestSR,
      obj.maxWickets,
      obj.bestEconomy,
      obj.points,
      obj.rank,
      obj.tossWinPoints,
      obj.firstInningsScorePoints,
      obj.matchWinPoints,
      obj.runsMarginPoints,
      obj.wicketsMarginPoints,
      obj.potmPoints,
      obj.maxRunsPoints,
      obj.highestSRPoints,
      obj.maxWicketsPoints,
      obj.bestEconomyPoints
    );
  }
}

/**
 * Gets the column indices for the Results sheet.
 * Columns range from A to Z. A is match ID, Z is bestEconomyPoints.
 * All booleans after bestEconomyPoints are not in this sheet and are derived from other existing attributes.
 * Column P is skipped.
 * @returns {Object} The column indices for the Results sheet.
 */
function getResultsSheetIndices_() {
  return {
    matchID: colNameToIndex_("A"),
    entryID: colNameToIndex_("B"),
    punterName: colNameToIndex_("C"),
    tossWinTeam: colNameToIndex_("D"),
    firstInningsScore: colNameToIndex_("E"),
    matchWinTeam: colNameToIndex_("F"),
    runsMargin: colNameToIndex_("G"),
    wicketsMargin: colNameToIndex_("H"),
    potm: colNameToIndex_("I"),
    maxRuns: colNameToIndex_("J"),
    highestSR: colNameToIndex_("K"),
    maxWickets: colNameToIndex_("L"),
    bestEconomy: colNameToIndex_("M"),
    points: colNameToIndex_("N"),
    rank: colNameToIndex_("O"),
    tossWinPoints: colNameToIndex_("Q"),
    firstInningsScorePoints: colNameToIndex_("R"),
    matchWinPoints: colNameToIndex_("S"),
    runsMarginPoints: colNameToIndex_("T"),
    wicketsMarginPoints: colNameToIndex_("U"),
    potmPoints: colNameToIndex_("V"),
    maxRunsPoints: colNameToIndex_("W"),
    highestSRPoints: colNameToIndex_("X"),
    maxWicketsPoints: colNameToIndex_("Y"),
    bestEconomyPoints: colNameToIndex_("Z")
  };
}
