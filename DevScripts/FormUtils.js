//FORM UTILITIES//

/**
 * Gets a form item by question title with retry mechanism.
 *
 * @param {GoogleAppsScript.Forms.Form} form - The Google Form.
 * @param {string} question - The title of the question.
 * @param {number} [maxRetries=2] - The maximum number of retry attempts.
 * @param {number} [retryDelayMillis=1000] - The delay between retry attempts in milliseconds.
 * @returns {GoogleAppsScript.Forms.Item | undefined} The form item or undefined if not found.
 */
function getFormItem_(form, question, doRetry = true, maxRetries = 2, retryDelayMillis = 1000) {
  let formTitles, pos, retries = 0;

  do {
    formTitles = getFormTitles_(form);
    pos = formTitles.indexOf(question);

    if (doRetry && pos === -1) {
      console.log(`Question "${question}" not found in the form. Clearing the cache and retrying.`);
      removeCachedFormTitles_(form);
      Utilities.sleep(retryDelayMillis);
    }

    retries++;
  } while (doRetry && pos === -1 && retries < maxRetries);

  if (pos === -1) {
    console.log(`Question "${question}" still not found in the form after ${retries} retries.`);
    return undefined; // or handle the error in another way
  }

  return form.getItemById(form.getItems()[pos].getId());
}

/**
 * Updates the titles of form questions based on the provided array of titles.
 * 
 * @param {GoogleAppsScript.Forms.Form} form - The Google Forms object to update.
 * @param {string[]} titles - An array of question titles in the order they appear in the form.
 * @throws {Error} If the number of form items and question titles do not match.
 */
function updateFormQuestionTitles_(form, titles) {
  const items = form.getItems();
  if (items.length !== titles.length) {
    log(`Form Items = ${items}\nQuestion Titles = ${titles}`, LogLevel.DEBUG);
    throw new Error(`Form Items has ${items.length} questions, but Questions Titles has ${titles.length}. Both MUST have the same number of items/questions.`);
  }
  items.forEach((item, i) => {
    const itemById = form.getItemById(item.getId());
    const title = titles[i].trim();
    if (title) {
      itemById.setTitle(title);
    }
  });
}

/**
 * Updates the list of active punters in a Google Form dropdown question.
 * 
 * @param {Google Form} form - The Google Form to update.
 * @throws {Error} If there are more than 10 active punters.
 */
function updatePuntersList_(form) {
  const activePunters = PuntersUtils.getActivePunterNames_();

  if (activePunters.length > maxPuntersNum14) {
    log(`Active Punters Max Limit Reached. ${activePunters}`, LogLevel.WARN);
    throw new Error(`There are ${activePunters.length} active punters, only ${maxPuntersNum14} allowed.`);
  }
  const item = getFormItem_(form, FormConstants.PUNTER_QUESTION_TITLE);
  updateListItem_(item, FormConstants.PUNTER_QUESTION_DESCRIPTION, activePunters, false);
}
/**
 * Updates the names and titles of the attached forms.
 */
function updateAttachedFormNames_() {
  updateDailyBetsFormNameTitle_();
  updateSeriesBetsFormNameTitle_();
}

/**
 * Updates the name and title of the Daily Bets form.
 */
function updateDailyBetsFormNameTitle_() {
  updateFormFileName_(getDailyBetsSheet_().getFormUrl(), appendEnvAndTournamentName_(dailyBetsFormNameSuffix));
  getDailyBetsForm_().setTitle(appendEnvAndTournamentName_(dailyBetsFormNameSuffix));
}

/**
 * Updates the name and title of the Series Bets form.
 */
function updateSeriesBetsFormNameTitle_() {
  updateFormFileName_(getSeriesBetsSheet_().getFormUrl(), appendEnvAndTournamentName_(seriesBetsFormNameSuffix));
  getSeriesBetsForm_().setTitle(appendEnvAndTournamentName_(seriesBetsFormNameSuffix));
}

/**
 * Updates the file name of the form given its URL and the new name.
 * @param {string} formUrl - The URL of the form to be updated.
 * @param {string} newName - The new name for the form.
 */
function updateFormFileName_(formUrl, newName) {
  let form = FormApp.openByUrl(formUrl);
  let formId = form.getId();
  let formFile = DriveApp.getFileById(formId);
  formFile.setName(newName);
}

/**
 * Retrieves the Daily Bets form from the Daily Predictions sheet.
 * @returns {GoogleAppsScript.Forms.Form} The Daily Bets form.
 */
function getDailyBetsForm_() {
  let formURL = getDailyBetsSheet_().getFormUrl();
  let form = FormApp.openByUrl(formURL);
  return form;
}

/**
 * Retrieves the Series Bets form from the Overall Picks sheet.
 * @returns {GoogleAppsScript.Forms.Form} The Series Bets form.
 */
function getSeriesBetsForm_() {
  let formURL = getSeriesBetsSheet_().getFormUrl();
  let form = FormApp.openByUrl(formURL);
  return form;
}

/**
 * Retrieves the title of the Daily Bets form.
 * @returns {string} The title of the Daily Bets form.
 */
function getDailyBetsFormTitle_() {
  return getDailyBetsForm_().getTitle();
}

/**
 * Retrieves the published URL of the Daily Bets form.
 * @returns {string} The published URL of the Daily Bets form.
 */
function getDailyBetsFormPublishedURL_() {
  return getDailyBetsForm_().getPublishedUrl();
}

/**
 * Generates an HTML link to the Daily Bets form.
 * @returns {string} The HTML link to the Daily Bets form.
 */
function getDailyBetsPublishedURLLink_() {
  return '<a href="' + getDailyBetsFormPublishedURL_() + '">' + getDailyBetsFormTitle_() + '</a>';
}

/**
 * Retrieves the title of the Series Bets form.
 * @returns {string} The title of the Series Bets form.
 */
function getSeriesBetsFormTitle_() {
  return getSeriesBetsForm_().getTitle();
}

/**
 * Retrieves the published URL of the Series Bets form.
 * @returns {string} The published URL of the Series Bets form.
 */
function getSeriesBetsFormPublishedURL_() {
  return getSeriesBetsForm_().getPublishedUrl();
}

/**
 * Get the value of the helpText property for an item, handling both function and direct value cases.
 *
 * @param {Object} question - The item from the dailyPredictionsFormMatchQuestions array.
 * @returns {*} The value of the helpText property.
 */
function getHelpTextValue_(question) {
  return typeof question.helpText === 'function' ? question.helpText() : question.helpText;
}

/**
 * 
 */
function getPoweredByStr_(){
  return `Powered By: ${spreadsheetFileNameSuffix}©️`;
}
