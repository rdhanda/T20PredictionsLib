//Constants//
class SeriesConstants {
  static get NEW_SERIES() { return "NEW_SERIES"; }
  static get MATCH_TYPES_T20() { return "T20"; }
  static get MATCH_TYPES_ODI() { return "ODI"; }
  static get DEFAULT_HOST_COUNTRY() { return "IND"; }
}

const STATUS_Y = "Y";
const RESULT = "RESULT";

class FormConstants {
  static get PUNTER_QUESTION_TITLE() { return "Punter"; }
  static get PUNTER_QUESTION_DESCRIPTION() { return `● optional (select the punter's name when submitting on behalf of others, or leave it blank.)`; }
  static get EMAIL_ADDRESS_QUESTION_TITLE() { return "Email Address"; }
}

class FontConstants {
  static get FONT_LATO() { return 'Lato'; }
  static get FONT_ROBOTO() { return 'Roboto'; }
  static get FONT_OPEN_SANS() { return 'Open Sans'; }
  static get FONT_TREBUCHET_MS() { return 'Trebuchet MS'; }
  static get DEFAULT_FONT_FAMILY() { return FontConstants.FONT_LATO; }
  static get DEFAULT_FONT_SIZE() { return 10; }
}

class TrumpCardKeys {
  static get INDICATOR_EMOJI() { return `🚀`; }
  static get USED_MATCH_IDS_KEY() { return 'usedMatchIDs'; }
  static get FAILED_ATTEMP_MATCH_IDS_KEY() { return 'failedAttemptMatchIDs'; }
}

const BOT_SUBMITTED_EMOJI = '🤖';
const STAR_CHAR = '★';//Used when Bot submits for punters using a new set of bets, instead of its own.

const EMAIL_LIST_SEPARATOR = ",";

const NOT_APPLICABLE_ARR = ["#NA"];
const TEAM_TBA = "TBA";
const INVALID_TEAMS_ARR = ["Winner", "Loser", "TBD"];
const MATCH_RESULT_TIED = "TIED";

const DRIVE_ROOT_FOLDER = "@BatBallBets";//Move to Config or prefix/suffix with GroupID
const DRIVE_ARCHIVED_SUB_FOLDER = "@Archived";
const ARCHIVED_FILE_SUFFIX = "_Archived";

class FormItemTypes {
  static get LIST() { return FormApp.ItemType.LIST; }
  static get SECTION_HEADER() { return FormApp.ItemType.SECTION_HEADER; }
  static get CHECKBOX() { return FormApp.ItemType.CHECKBOX; }
  static get MULTIPLE_CHOICE() { return FormApp.ItemType.MULTIPLE_CHOICE; }
  static get TEXT() { return FormApp.ItemType.TEXT; }
  static get TEXT_WHOLE_NUMBER() { return "TEXT WHOLE NUMBER"; }
  static get SCALE() { return FormApp.ItemType.SCALE; }
}

/**
 * Enum for player categories.
 * @readonly
 * @enum {string}
 */
const PlayerQuestionCategories = {
  POTM: 'POTM',
  MAX_RUNS: 'MaxRuns',
  BATTING_SR: 'BattingSR',
  MAX_WICKETS: 'MaxWickets',
  ECONOMY: 'Economy'
};
/**
 * Enum for match result statuses.
 * @readonly
 * @enum {Object}
 */
const MatchResultStatus = Object.freeze({
  NA: { label: "NA", color: null },
  WAITING: { label: "WAITING", color: "black" },
  LIVE: { label: "LIVE", color: "#00ffff" },
  NO_RESULT: { label: "NO RESULT", color: "#FF0000" },
  READY: { label: "READY", color: "#b9ffb9" },
  DONE: { label: "DONE", color: "#00FF00" }
});

const ActiveMatchResultStatusList = [MatchResultStatus.WAITING, MatchResultStatus.LIVE, MatchResultStatus.READY];
const NoResultMatchResultStatusList = ["NO RESULT", "ABANDONED", "CANCELLED"];//status from cricinfo scorecard

const MatchScheduleStatus = {
  OPEN: "OPEN",
  CLOSED: "CLOSED",
  EXCLUDED: "EXCLUDED",
  EMPTY: "",
};

const SheetVisibility = {
  SHOW: "SHOW",
  HIDE: "HIDE",
  ALWAYS_HIDDEN: "ALWAYS_HIDDEN",
  AS_IS: "AS_IS",
};

const Protection = {
  OFF: "OFF",
  ON: "ON",
};

const Promotion = {
  EXCLUDED: "EXCLUDED",
  BATCH1: "BATCH1",
  BATCH2: "BATCH2",
  BATCH3: "BATCH3",
};

const LogLevel = {
  DEBUG: EnvConfigConstants.ENV_LOG_LEVEL_DEBUG,
  INFO: EnvConfigConstants.ENV_LOG_LEVEL_INFO,
  WARN: EnvConfigConstants.ENV_LOG_LEVEL_WARN,
  ERROR: EnvConfigConstants.ENV_LOG_LEVEL_ERROR,
};

//Scorecard
const MATCH_TYPES = {
  T20: SeriesConstants.MATCH_TYPES_T20,
  ODI: SeriesConstants.MATCH_TYPES_ODI,
};

/**
 * Constants representing the rules for impact players.
 *
 * @typedef {Object} ImpactPlayerConfig
 * @property {boolean} flag - Indicates whether impact players are allowed.
 * @property {number} playingXICount - Number of players in the playing XI.
 * @property {number} [benchPlayersMinCount] - Minimum number of bench players (only applicable if impact players are allowed).
 */

/**
 * Configuration for impact players.
 *
 * @constant
 * @type {Object<string, ImpactPlayerConfig>}
 */
const IMPACT_PLAYERS = {
  ALLOWED: {
    playingXICount: 12,
    benchPlayersMinCount: 1,
  },
  NOT_ALLOWED: {
    playingXICount: 11,
    benchPlayersMinCount: 0,
  },
};


const MAX_WICKETS_PER_INNINGS = 10;
const BATTER_NOT_OUT = 'not out';

const BattersToSelect = {
  All: "ALL",
  OutOnly: "Out",
  NotOutOnly: BATTER_NOT_OUT,
}

const BowlersToSelect = {
  All: "ALL",
  MaxOvers: "MAX_OVERS",
  BelowMaxOvers: "BELOW_MAX_OVERS",
}

//Scorecard Table Utils
const BattingTableIndex = {
  Player: 0,
  Total: 0,
  FallOfWickets: 0,
  HowOut: 1,
  InningsOvers: 1,
  Runs: 2,
  InningsScore: 2,
  BallsFaced: 3,
  Minutes: 4,
  Fours: 5,
  Sixes: 6,
  StrikeRate: 7,
};

const BowlingTableIndex = {
  Player: 0,
  Overs: 1,
  Maiden: 2,
  Runs: 3,
  Wickets: 4,
  Economy: 5,
  Dots: 6,
  Fours: 7,
  Sixes: 8,
  Wides: 9,
  NoBalls: 10,
};

const BATTING_TABLE_NON_PLAYERS_LIST = ["TOTAL", "Extras", "Did not bat", "Fall of wickets", "Yet to bat"];

const MATCH_DETAILS_TABLE_KEYS = {
  toss: 'Toss',
  potm: 'Player Of The Match',
};

//Scorecard Utils//
const URLs = {
  BASE_URL: "https://www.espncricinfo.com",
  MATCH_PREVIEW_URL_PART: '/match-preview',
  LIVE_SCORECARD_URL_PART: "/live-cricket-score",
  FULL_SCORECARD_URL_PART: "/full-scorecard",
  MATCH_PLAYINGXI_URL_PART: "/match-playing-xi",
  TOURNAMENT_SCHEDULE_URL_PART: "/match-schedule-fixtures-and-results"
};

const PLAYER_ROLES_TO_STRIP = ['top-order', 'opening', 'middle-order', 'bowler', 'batter', 'batting', 'bowling', 'wicketkeeper', 'allrounder'];
const PLAYER_SPECIAL_CHARS_TO_REMOVE = ['†', '\\(c\\)'];

const TEAM_NAME_CHARS_TO_REMOVE = ['.', 'ODI', 'Squad', 'T20I', 'Women', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];

const PLAYOFFS_LIST = ["Semi-Final", "Final", "Qualifier", "Eliminator"];

//Players
const PlayerTypes = {
  BATTERS: 'BATTERS',
  ALLROUNDERS: 'ALLROUNDERS',
  BOWLERS: 'BOWLERS',
  UNKNOWN_PLAYERS: 'UNKNOWN',
}

const PlayerTypeCategories = {
  ALL_PLAYER_CATEGORIES: [],
  BATTER_PLAYER_CATEGORIES: [PlayerTypes.BATTERS, PlayerTypes.ALLROUNDERS, PlayerTypes.UNKNOWN_PLAYERS],
  BOWLER_PLAYER_CATEGORIES: [PlayerTypes.ALLROUNDERS, PlayerTypes.BOWLERS, PlayerTypes.UNKNOWN_PLAYERS],
}

// "Enum" for stats order
const StatsOrder = {
  ASCENDING: 'ascending',
  DESCENDING: 'descending'
};

//Series Results
const KEY_MOST_RUNS = { key: 'mostRuns', value: 'Most runs', playerNameHeader: 'Player', playerStatHeader: 'Runs', statsOrder: StatsOrder.DESCENDING };
const KEY_HIGHEST_SR = { key: 'highestSR', value: 'Highest strike rates', playerNameHeader: 'Player', playerStatHeader: 'SR', statsOrder: StatsOrder.DESCENDING };
const KEY_MOST_WICKETS = { key: 'mostWickets', value: 'Most wickets', playerNameHeader: 'Player', playerStatHeader: 'Wkts', statsOrder: StatsOrder.DESCENDING };
const KEY_BEST_ECONOMY = { key: 'bestEconomy', value: 'Best economy rates', playerNameHeader: 'Player', playerStatHeader: 'Econ', statsOrder: StatsOrder.ASCENDING };

const KEY_VIEW_ALL_STATS = "View all stats";

const SERIES_STATS_CATEGORIES_LIST = [
  KEY_MOST_RUNS,
  KEY_HIGHEST_SR,
  KEY_MOST_WICKETS,
  KEY_BEST_ECONOMY,
];


//Predictions
// Define keys for predictions object
const PredictionKeys = {
  Title: 'title',
  Toss: 'toss',
  Score: 'score',
  Win: 'win',
  RunsMargin: 'runsMargin',
  WicketsMargin: 'wicketsMargin',
  POTM: 'potm',
  MaxRuns: 'maxRuns',
  StrikeRate: 'strikeRate',
  MaxWickets: 'maxWickets',
  Economy: 'economy'
};

//Generic Sheet Columns
const ColumnNums = {
  A: 1, B: 2, C: 3, D: 4, E: 5, F: 6, G: 7, H: 8, I: 9,
  J: 10, K: 11, L: 12, M: 13, N: 14, O: 15, P: 16, Q: 17, R: 18,
  S: 19, T: 20, U: 21, V: 22, W: 23, X: 24, Y: 25, Z: 26,
};

const ColumnIndices = {
  A: 0, B: 1, C: 2, D: 3, E: 4, F: 5, G: 6, H: 7, I: 8,
  J: 9, K: 10, L: 11, M: 12, N: 13, O: 14, P: 15, Q: 16, R: 17,
  S: 18, T: 19, U: 20, V: 21, W: 22, X: 23, Y: 24, Z: 25,
};

const ColumnNames = {
  A: "A", B: "B", C: "C", D: "D", E: "E", F: "F", G: "G", H: "H", I: "I",
  J: "J", K: "K", L: "L", M: "M", N: "N", O: "O", P: "P", Q: "Q", R: "R",
  S: "S", T: "T", U: "U", V: "V", W: "W", X: "X", Y: "Y", Z: "Z",
};
