//Daily Match Results Cache//

/**
 * A cache object to store match results.
 * @type {Object.<string, MatchResult>}
 */
const MatchResultsCache = {};

/**
 * 
 */
function isMatchResultsCacheEmpty_() {
  const isEmpty = Object.keys(MatchResultsCache).length === 0;
  return isEmpty;
}

/**
 * Updates the result status of a match result in the DailyMatchResultsCache_.
 * @param {string} sheetName - The name of the sheet.
 * @param {string} resultStatus - The new status of the match result.
 */
function updateMatchResultSheetStatusCache_(sheetName, resultStatus) {
  if (MatchResultsCache.hasOwnProperty(sheetName)) {
    var matchResult = MatchResultsCache[sheetName];
    matchResult.setResultStatus(resultStatus);
  }
}

/**
 * Updates the match result cache for a given sheet name.
 * 
 * @param {string} sheetName - The name of the sheet to update the cache for.
 */
function updateMatchResultCache_(sheetName) {
  if (MatchResultsCache.hasOwnProperty(sheetName)) { return; }
  const sheet = getSheetByName_(sheetName);
  const matchID = getMatchID_(sheet);
  const resultStatus = getMatchResultStatus_(sheet);

  if (isNotEmpty_(matchID) && ActiveMatchResultStatusList.includes(resultStatus)) {
    const matchStartTimeMST = ScheduleUtils.getMatchStartTimeMST(matchID);
    const matchScheduledClosedTime = ScheduleUtils.getMatchScheduleClosedTime(matchID);
    var matchResult = new MatchResult(sheetName, sheet, matchID, resultStatus, matchStartTimeMST, matchScheduledClosedTime);
    MatchResultsCache[sheetName] = matchResult;
  }
}

/**
 * Checks if any match in the DailyMatchResultsCache_ is in progress.
 * @return {boolean} True if any match has a status that indicates it is in progress, otherwise false.
 */
function isAnyMatchInProgress_() {
  for (const entry of Object.values(MatchResultsCache)) {
    const timeToContinue = isBeforeCurrentTime_(entry.getMatchScheduledClosedTime());
    if (ActiveMatchResultStatusList.includes(entry.getResultStatus()) && timeToContinue) {
      return true;
    }
  }
  return false;
}

/**
 * Checks if any match in the DailyMatchResultsCache_ is in progress.
 * @return {boolean} True if any match has a status that indicates it is in progress, otherwise false.
 */
function isMatchInProgress_(sheetName) {
  if (MatchResultsCache.hasOwnProperty(sheetName)) {
    var matchResult = MatchResultsCache[sheetName];
    const timeToContinue = isBeforeCurrentTime_(matchResult.getMatchScheduledClosedTime());
    if (ActiveMatchResultStatusList.includes(matchResult.getResultStatus()) && timeToContinue) {
      return true;
    }
  }

  return false;
}

/**
 * Class representing a match result.
 */
class MatchResult {
  /**
   * Create a MatchResult.
   * @param {string} sheetName - The name of the sheet.
   * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet object.
   * @param {string} matchID - The ID of the match.
   * @param {string} resultStatus - The status of the match result.
   * @param {string} matchStartTimeMST - The start time of the match in MST.
   * @param {string} matchScheduledClosedTime - The scheduled closed time of the match.
   */
  constructor(sheetName, sheet, matchID, resultStatus, matchStartTimeMST, matchScheduledClosedTime) {
    var sheetName_ = sheetName;
    var sheet_ = sheet;
    var matchID_ = matchID;
    var resultStatus_ = resultStatus;
    var matchStartTimeMST_ = matchStartTimeMST;
    var matchScheduledClosedTime_ = matchScheduledClosedTime;

    // Getters
    this.getSheetName = function () { return sheetName_; };
    this.getSheet = function () { return sheet_; };
    this.getMatchID = function () { return matchID_; };
    this.getResultStatus = function () { return resultStatus_; };
    this.getMatchStartTimeMST = function () { return matchStartTimeMST_; };
    this.getMatchScheduledClosedTime = function () { return matchScheduledClosedTime_; };

    // Setters
    this.setSheetName = function (newValue) { sheetName_ = newValue; };
    this.setSheet = function (newValue) { sheet_ = newValue; };
    this.setMatchID = function (newValue) { matchID_ = newValue; };
    this.setResultStatus = function (newValue) { resultStatus_ = newValue; };
    this.setMatchStartTimeMST = function (newValue) { matchStartTimeMST_ = newValue; };
    this.setMatchScheduledClosedTime = function (newValue) { matchScheduledClosedTime_ = newValue; };
  }

  /**
   * Serializes the MatchResult instance to a plain object.
   * @returns {Object} The plain object representation of the MatchResult.
   */
  toObject() {
    return {
      sheetName: this.getSheetName(),
      sheet: this.getSheet(),
      matchID: this.getMatchID(),
      resultStatus: this.getResultStatus(),
      matchStartTimeMST: this.getMatchStartTimeMST(),
      matchScheduledClosedTime: this.getMatchScheduledClosedTime()
    };
  }

  /**
   * Static method to create a MatchResult instance from a plain object.
   * @param {Object} obj - The plain object representation of a MatchResult.
   * @returns {MatchResult} The MatchResult instance.
   */
  static fromObject(obj) {
    return new MatchResult(
      obj.sheetName,
      obj.sheet,
      obj.matchID,
      obj.resultStatus,
      obj.matchStartTimeMST,
      obj.matchScheduledClosedTime
    );
  }
}
