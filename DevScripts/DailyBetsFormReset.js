//DailtBetsFormReset.gs//

/**
 * Resets the Daily Bets form via a menu option.
 * @param {GoogleAppsScript.Events.AppsScriptEvent} e - The event object.
 */
function resetDailyBetsFormMenu(e) {
  let form = getDailyBetsForm_();
  resetDailyBetsForm_(e, form);
}

/**
 * Resets the Daily Bets form, including moving it to a closed sheet,
 * removing cached titles, and updating form items.
 * @param {GoogleAppsScript.Events.AppsScriptEvent} e - The event object.
 * @param {GoogleAppsScript.Forms.Form} form - The form to reset.
 */
function resetDailyBetsForm_(e, form) {
  deleteDailyBetsFormData_(e, form);

  form.setTitle(appendEnvAndTournamentName_(dailyBetsFormNameSuffix));
  form.setDescription("");
  const item = getFormItem_(form, FormConstants.PUNTER_QUESTION_TITLE, false);
  updateListItem_(item, FormConstants.PUNTER_QUESTION_DESCRIPTION, NOT_APPLICABLE_ARR, false);

  let numOfMatches = MatchSheetConfigs.length; // Just using it for the number of matches
  for (let i = 0; i < 1; i++) { // Reset to 1 set of questions only
    let matchNum = (i + 1);
    for (const question of dailyBetsFormMatchQuestions()) {
      const title = `${matchNum}${question.title}`;
      const item = getFormItem_(form, title, false);
      if (item) {
        const itemType = question.itemType;
        const helpText = getHelpTextValue_(question);
        let values = NOT_APPLICABLE_ARR;
        updateFormItem_(item, itemType, helpText, values, false);
      }
    }
  }
}
