
/**
 * Update player stats
 */
function updatePlayerStatsMenu() {
  const sheet = getSheetByName_(playerStatsSheetName);
  activateSheet_(sheet);

  updatePlayerStats_();
}

function updatePlayerStats_() {
  PlayerStatsUtils.updateStats();
}

/**
 * Class representing series player stats utils
 */
class PlayerStatsUtils {

  /**
   * Update the series statistics on the specified Google Sheet.
   */
  static updateStats() {
    const sheet = getSheetByName_(playerStatsSheetName);
    PlayerStatsUtils.clearSheet();

    const teams = ScheduleUtils.getOpenMatchesAllTeams();
    if (isEmpty_(teams)) { return; }

    const stats = PlayerStatsUtils.getStats(teams);
    if (!stats) { return; }

    const categories = stats.getCategories();
    const categoryNames = categories.map(category => category.getName().toUpperCase());

    const data = PlayerStatsUtils.buildData(categories, teams, categoryNames);
    if (isEmpty_(data)) { return; }

    // Append all data to the sheet at once
    sheet.getRange(1, 1, data.length, data[0].length).setValues(data);

    //autoResizeColumns_(sheet);
  }

  /**
   * Clears all content from the given sheet.
   * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet The sheet to clear.
   */
  static clearSheet() {
    const sheet = getSheetByName_(playerStatsSheetName);
    sheet.getRange(2, 1, sheet.getMaxRows(), sheet.getMaxColumns()).clearContent();
  }

  /**
   * Builds the data array for the series statistics.
   * @param {Array} categories The array of categories.
   * @param {Array} teams The array of teams.
   * @param {Array} categoryNames The array of category names.
   * @returns {Array} The data array for the series statistics.
   */
  static buildData(categories, teams, categoryNames) {
    const data = [];
    data.push(categoryNames);

    teams.forEach(team => {
      const teamHeaderRow = categoryNames.map(() => team);
      data.push(teamHeaderRow);

      const maxPlayers = Math.max(...categories.map(category =>
        category.getPlayers().filter(player => player.getTeam() === team).length
      ));

      for (let i = 0; i < maxPlayers; i++) {
        const playerRow = PlayerStatsUtils.buildPlayerRow(categories, team, categoryNames, i);
        data.push(playerRow);
      }
    });

    return data;
  }

  /**
   * Builds a player row for the current team and index.
   * @param {Array} categories The array of categories.
   * @param {string} team The team name.
   * @param {Array} categoryNames The array of category names.
   * @param {number} index The index of the player row.
   * @returns {Array} The player row.
   */
  static buildPlayerRow(categories, team, categoryNames, index) {
    const playerRow = categoryNames.map(() => '');

    categories.forEach((category, columnIndex) => {
      const players = category.getPlayers().filter(player => player.getTeam() === team);
      if (players[index]) {
        playerRow[columnIndex] = `${players[index].getName()} [${players[index].getStat()}]`;
      }
    });

    return playerRow;
  }

  /**
   * Retrieve series statistics and update categories with player data.
   *
   * @param {Array<string>} [teams=[]] - Optional array of team names to filter by.
   * @returns {PlayerStats|null} - An instance of Stats containing the updated categories and players, or null if categories are empty.
   */
  static getStats(teams = []) {
    const categories = PlayerStatsUtils.getStatsCategories();
    if (isEmpty_(categories)) {
      return null;
    }

    PlayerStatsUtils.updateCategoryPlayers(categories, teams);

    const stats = new PlayerStats(categories);
    return stats;
  }

  /**
   * Update players for each category based on the provided raw data and optional team filters.
   *
   * @param {Array<StatsCategory>} categories - The array of Category instances.
   * @param {Array<string>} [teams=[]] - Optional array of team names to filter by.
   */
  static updateCategoryPlayers(categories, teams = []) {
    const urls = categories.map(category => category.getUrl());
    const cheerioInstances = getCheerioInstances_(urls);

    categories.forEach((category, index) => {
      const $ = cheerioInstances[index];
      const data = PlayerStatsUtils.getTableDataFromCheerio_($);
      if (isNotEmpty_(data)) {
        const players = PlayerStatsUtils.getPlayersByCategory(category, data, teams);
        category.setPlayers(players);
      }
    });
  }

  /**
   * Retrieves players for a specific category from raw data.
   *
   * @param {StatsCategory} category - The category object containing headers and sort order.
   * @param {Array<Array<string|number>>} rawData - The raw data containing player information.
   * @param {Array<string>} teams - The array of team abbreviations to match against.
   * @returns {Array<Player>} - An array of Player instances.
   */
  static getPlayersByCategory(category, rawData, teams) {
    const headers = rawData[0];
    const playerIndex = headers.indexOf(category.getPlayerNameHeader());
    const statsIndex = headers.indexOf(category.getPlayerStatHeader());

    const filteredData = rawData.slice(1); // Remove the top row
    if (isEmpty_(filteredData)) return [];

    const sortOrder = category.getStatsOrder();
    const data = filteredData.sort((a, b) =>
      sortOrder === StatsOrder.DESCENDING ? b[statsIndex] - a[statsIndex] : a[statsIndex] - b[statsIndex]
    );

    return data
      .filter(row => !isEmpty_(teams) && teams.includes(PlayerStatsUtils.extractTeamName_(row[playerIndex])))
      .map(row => new Player(row[playerIndex], row[statsIndex], PlayerStatsUtils.extractTeamName_(row[playerIndex])));
  }

  /**
   * 
   */
  static getTableDataFromCheerio_($) {
    if (isEmpty_($)) { return []; }
    return extractTableDataByIndex_($, 0);
  }

  /**
   * Extract the team abbreviation from the player name.
   * @param {string} playerName - The player name in the format "Player Name (TEAM)".
   * @return {string} The team abbreviation.
   */
  static extractTeamName_(playerName) {
    if (isEmpty_(playerName)) { return; }

    const match = playerName.match(/\(([^)]+)\)/);
    return match ? match[1] : '';
  }

  /**
   * Retrieves the stats categories from the home page URL.
   *
   * @returns {Array<StatsCategory>} - An array of Category instances.
   */
  static getStatsCategories() {
    const homeURL = PlayerStatsUtils.extractAllStatsHomeURL_();
    if (isEmpty_(homeURL)) {
      return [];
    }

    const categories = PlayerStatsUtils.getStatsCategoriesByHomeURL_(homeURL, SERIES_STATS_CATEGORIES_LIST);
    return categories;
  }

  /**
   * Extracts the URLs for the specified categories from the HTML content of the home page
   * and returns an array of Category instances.
   *
   * @param {string} homeURL - The URL of the home page.
   * @param {Array} categoriesList - An array of category objects containing the key-value pairs.
   * @returns {Array<StatsCategory>} - An array of Category instances.
   */
  static getStatsCategoriesByHomeURL_(homeURL, categoriesList) {
    const $ = getCheerioInstances_(homeURL);
    if (isEmpty_($)) {
      return [];
    }

    const categoryInstances = [];
    categoriesList.forEach(function (categoryObj) {
      const categoryName = categoryObj.value;

      const url = appendBaseURL_($(`a:contains(${categoryName})`).attr('href'));

      const categoryInstance = new StatsCategory(categoryName, categoryObj.key, url, categoryObj.playerNameHeader, categoryObj.playerStatHeader, categoryObj.statsOrder);
      categoryInstances.push(categoryInstance);
    });

    return categoryInstances;
  }

  /**
   * Extracts the URL for the overall stats page.
   *
   * @returns {string} - The URL for the overall stats page.
   */
  static extractAllStatsHomeURL_() {
    const overallStatsURL = PlayerStatsUtils.getSeriesStatsURL_();
    if (isEmpty_(overallStatsURL)) {
      return;
    }

    const $ = getCheerioInstances_(overallStatsURL);
    if (isEmpty_($)) {
      return;
    }

    let url = $(`a:contains(${KEY_VIEW_ALL_STATS})`).attr('href');
    return appendBaseURL_(url);
  }

  /**
   * 
   */
  static getStatsCategoriesURLs() {
    const homeURL = PlayerStatsUtils.extractAllStatsHomeURL_();
    if (isEmpty_(homeURL)) { return; }

    const categoriesURLs = PlayerStatsUtils.extractStatsCategoriesURLs_(homeURL, SERIES_STATS_CATEGORIES_LIST);
    return categoriesURLs;
  }

  /**
   * Extracts the URLs for the specified categories from the HTML content of the home page.
   *
   * @param {string} homeURL - The URL of the home page.
   * @param {Array} categories - An array of category objects containing the key-value pairs.
   * @returns {Array<Object>} - An array of objects, each containing:
   *    @property {string} key - The key of the category.
   *    @property {string} url - The corresponding URL for the category.
   */
  static extractStatsCategoriesURLs_(homeURL, categories) {
    const $ = getCheerioInstances_(homeURL);
    if (isEmpty_($)) {
      return;
    }

    const urls = [];
    categories.forEach(function (categoryObj) {
      const category = categoryObj.value;
      let url = $(`a:contains(${category})`).attr('href');
      url = appendBaseURL_(url);
      const resultObj = { key: categoryObj.key, url };
      urls.push(resultObj);
    });

    return urls;
  }

  /**
   * Retrieves the URL for the overall stats page.
   *
   * @returns {string|null} - The URL for the overall stats page, or null if the active tournament URL is empty.
   */
  static getSeriesStatsURL_() {
    let url = getActiveSeriesURL_();
    if (isNotEmpty_(url)) {
      url = url + "/stats";
      return url;
    }
    log('No Active Tournament URL Found.', LogLevel.ERROR);
  }
}

/**
 * Class representing the statistics for various categories.
 */
class PlayerStats {
  /**
   * Create a Stats instance.
   * @param {Array<StatsCategory>} [categories=[]] - Optional array of Category instances.
   */
  constructor(categories = []) {
    let categories_ = categories;

    /**
     * @returns {Array<StatsCategory>} The categories.
     */
    this.getCategories = function () {
      return categories_;
    };

    // Setters
    this.setCategories = function (categories) {
      categories_ = categories;
    };
  }

  /**
   * Serializes the Stats instance to a plain object.
   * @returns {Object} The plain object representation of the Stats.
   */
  toObject() {
    return {
      categories: this.getCategories().map(category => category.toObject()),
    };
  }

  /**
   * Static method to create a Stats instance from a plain object.
   * @param {Object} obj - The plain object representation of a Stats.
   * @returns {PlayerStats} The Stats instance.
   */
  static fromObject(obj) {
    const categories = obj.categories.map(categoryObj => StatsCategory.fromObject(categoryObj));
    return new PlayerStats(categories);
  }
}

/**
 * Class representing a series stats category.
 */
class StatsCategory {
  /**
   * Create a category.
   * @param {string} name - The name of the category.
   * @param {string} key - The key of the category.
   * @param {string} url - The URL associated with the category.
   * @param {string} playerNameHeader - The header for player names.
   * @param {string} playerStatHeader - The header for player statistics.
   * @param {string} [statsOrder='descending'] - The order of player statistics ('ascending' or 'descending').
   */
  constructor(name, key, url, playerNameHeader, playerStatHeader, statsOrder = StatsOrder.DESCENDING) {
    let name_ = name;
    let key_ = key;
    let url_ = url;
    let playerNameHeader_ = playerNameHeader;
    let playerStatHeader_ = playerStatHeader;
    let statsOrder_ = statsOrder;
    let players_ = []; // Array of Player instances

    // Getters
    this.getName = function () { return name_; };
    this.getKey = function () { return key_; };
    this.getUrl = function () { return url_; };
    this.getPlayerNameHeader = function () { return playerNameHeader_; };
    this.getPlayerStatHeader = function () { return playerStatHeader_; };
    this.getStatsOrder = function () { return statsOrder_; };
    /**
     * @returns {Array<Player>} The Player list.
     */
    this.getPlayers = function () { return players_; };

    // Setters
    this.setName = function (name) { name_ = name; };
    this.setKey = function (key) { key_ = key; };
    this.setUrl = function (url) { url_ = url; };
    this.setPlayerNameHeader = function (header) { playerNameHeader_ = header; };
    this.setPlayerStatHeader = function (header) { playerStatHeader_ = header; };
    this.setStatsOrder = function (order) { statsOrder_ = order; };
    this.setPlayers = function (players) { players_ = players; };

    // Methods
    this.addPlayer = function (player) { players_.push(player); };
    this.removePlayer = function (player) {
      const index = players_.findIndex(p => p.getName() === player.getName());
      if (index !== -1) {
        players_.splice(index, 1);
      }
    };
  }

  /**
   * Serializes the Category instance to a plain object.
   * @returns {Object} The plain object representation of the Category.
   */
  toObject() {
    return {
      name: this.getName(),
      key: this.getKey(),
      url: this.getUrl(),
      playerNameHeader: this.getPlayerNameHeader(),
      playerStatHeader: this.getPlayerStatHeader(),
      statsOrder: this.getStatsOrder(),
      players: this.getPlayers().map(player => player.toObject()),
    };
  }

  /**
   * Static method to create a Category instance from a plain object.
   * @param {Object} obj - The plain object representation of a Category.
   * @returns {StatsCategory} The Category instance.
   */
  static fromObject(obj) {
    const category = new StatsCategory(obj.name, obj.key, obj.url, obj.playerNameHeader, obj.playerStatHeader, obj.statsOrder);
    const players = obj.players.map(playerObj => Player.fromObject(playerObj));
    category.setPlayers(players);
    return category;
  }
}

/**
 * Class representing a player.
 */
class Player {
  /**
   * Create a player.
   * @param {string} name - The name of the player.
   * @param {number} stat - The statistic associated with the player (e.g., runs, wickets).
   * @param {string} team - The team abbreviation the player is associated with.
   */
  constructor(name, stat, team) {
    let name_ = name;
    let stat_ = stat;
    let team_ = team;

    // Getters
    this.getName = function () { return name_; };
    this.getStat = function () { return stat_; };
    this.getTeam = function () { return team_; };

    // Setters
    this.setName = function (name) { name_ = name; };
    this.setStat = function (stat) { stat_ = stat; };
    this.setTeam = function (team) { team_ = team; };
  }

  /**
   * Serializes the Player instance to a plain object.
   * @returns {Object} The plain object representation of the Player.
   */
  toObject() {
    return {
      name: this.getName(),
      stat: this.getStat(),
      team: this.getTeam(),
    };
  }

  /**
   * Static method to create a Player instance from a plain object.
   * @param {Object} obj - The plain object representation of a Player.
   * @returns {Player} The Player instance.
   */
  static fromObject(obj) {
    return new Player(obj.name, obj.stat, obj.team);
  }
}
