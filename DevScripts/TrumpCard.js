//TrumpCard.gs//

function isTrumpCardUsageEnabled_() {
  return ConfigManager.getConfigClass().TrumpCardConfig.IS_ENABLED;
}

/**
 * Checks if trump card usage is enabled based on the provided match ID and configuration.
 *
 * @param {string|number} matchID - The match ID to check.
 * @returns {boolean} - True if trump card usage is enabled, false otherwise.
 */
function isTrumpCardUsageEnabledByMatchID_(matchID) {
  const config = ConfigManager.getConfigClass().TrumpCardConfig;
  const isEnabled = config.IS_ENABLED;
  const minMatchID = config.MATCH_IDS_RANGE.min;
  const maxMatchID = config.MATCH_IDS_RANGE.max;
  const parsedMatchID = parseInt(matchID);

  return isEnabled && isNumberInRange_(parsedMatchID, [minMatchID, maxMatchID]);
}

/**
 * Retrieves the player names and their count from the provided trump card usage object.
 * @param {Object} trumpCardUsageObj - The object containing trump card usage data for each player.
 * @returns {string[]} An array of strings containing player names and their count.
 */
function getTrumpCardUsageDisplay_(e) {
  const trumpCardUsageObj = getTrumpCardUsageCounts_(e)
  const result = [];

  for (const playerName in trumpCardUsageObj) {
    if (trumpCardUsageObj.hasOwnProperty(playerName)) {
      const playerData = trumpCardUsageObj[playerName];
      result.push(`${playerName}: ${playerData.count}`);
    }
  }

  return result;
}

/**
 * Retrieves and processes the trump card usage counts from the daily results sheet.
 *
 * @returns {Object} The trump card usage counts for each punter's name.
 */
function getTrumpCardUsageCounts_(e) {
  let data = getDailyResultsSheetCachedData_().reverse();

  const matchIdColIndex = ColumnIndices.A;
  const entryInfoColIndex = ColumnIndices.B;
  const punterColIndex = ColumnIndices.C;
  const pointsColIndex = ColumnIndices.N;

  // Filter out rows where punter's name is "-" or entryInfo is "Result"
  data = data.filter(row => row[punterColIndex] !== "-" && row[entryInfoColIndex] !== RESULT);

  var trumpCardUsageObj = {};

  // Populate match IDs and sum of points for each name
  data.forEach(({ [entryInfoColIndex]: entryInfo, [punterColIndex]: punterName, [matchIdColIndex]: matchId, [pointsColIndex]: points }) => {
    if (entryInfo.toString().includes(TrumpCardKeys.INDICATOR_EMOJI.trim())) {
      updateTrumpCardMatchIDs_(trumpCardUsageObj, punterName, matchId, TrumpCardKeys.USED_MATCH_IDS_KEY);

      trumpCardUsageObj[punterName].points = trumpCardUsageObj[punterName].points || [];
      trumpCardUsageObj[punterName].points.push(points);

      trumpCardUsageObj[punterName].gainedPoints = trumpCardUsageObj[punterName].gainedPoints || 0;
      trumpCardUsageObj[punterName].gainedPoints += (points / 2);

      trumpCardUsageObj[punterName].count = trumpCardUsageObj[punterName][TrumpCardKeys.USED_MATCH_IDS_KEY].length;
      trumpCardUsageObj[punterName].reachedMaxLimit = trumpCardUsageObj[punterName].count >= ConfigManager.getConfigClass().TrumpCardConfig.MAX_LIMIT;
    }
  });

  // Sort the trumpCardUsageObj by name
  trumpCardUsageObj = Object.fromEntries(Object.entries(trumpCardUsageObj).sort(([nameA], [nameB]) => nameA.localeCompare(nameB)));

  return trumpCardUsageObj;
}

/**
 * Updates the match IDs for a specific property in the counts object.
 *
 * @param {Object} trumpCardUsageObj - The object containing counts and match IDs.
 * @param {string} punterName - The name of the punter.
 * @param {string|number} matchID - The match ID to be added.
 * @param {string} property - The property to update in the counts object.
 */
function updateTrumpCardMatchIDs_(trumpCardUsageObj, punterName, matchID, property) {
  // Ensure that counts[name] exists and is an object
  trumpCardUsageObj[punterName] = trumpCardUsageObj[punterName] || {};
  matchID = parseInt(matchID);

  /**
   * Check if the match ID is not already present in the array.
   * If not present, add the match ID and sort the array in ascending order.
   */
  if (!trumpCardUsageObj[punterName]?.[property] || !trumpCardUsageObj[punterName][property].includes(matchID)) {
    trumpCardUsageObj[punterName][property] = [...(trumpCardUsageObj[punterName]?.[property] || []), matchID];
  }
}

/**
 * Checks if the specified punter's name can use the trump card based on occurrence counts.
 *
 * @param {Object} trumpCardUsageObj - The occurrence counts for each punter's name.
 * @param {string} punterName - The punter's name to check.
 * @returns {boolean} True if the specified punter's name can use the trump card, false otherwise.
 */
function hasRemainingTrumpCardUsages_(trumpCardUsageObj, punterName, matchID) {
  matchID = parseInt(matchID);

  if (isTrumpCardUsageEnabledByMatchID_(matchID)) {
    const usedMatchIDsKey = TrumpCardKeys.USED_MATCH_IDS_KEY;

    if (matchID && trumpCardUsageObj[punterName]?.[usedMatchIDsKey]?.includes(matchID)) {
      // If match ID is provided and exists in usedMatchIDs, return TRUE
      log(`Punter '${punterName}' has already used the trump card in Match ID '${matchID}'. Ignoring it.`, LogLevel.WARN);
      return true;
    }

    // Check if the specified punter's name has more than max 2 occurrences
    const occurrences = trumpCardUsageObj[punterName] ? trumpCardUsageObj[punterName][usedMatchIDsKey]?.length : 0;
    return occurrences < ConfigManager.getConfigClass().TrumpCardConfig.MAX_LIMIT;
  } else {
    log(`Trump card usage is not enabled for Match ID '${matchID}'`, LogLevel.WARN)
  }
}

/**
 * Checks if a player must use the remaining trump card in the given match.
 * 
 * @param {Object} trumpCardUsageObj - The object containing trump card usage information.
 * @param {string} name - The name of the player.
 * @param {string} matchID - The ID of the match.
 * @returns {boolean} - True if the player must use the remaining trump card, false otherwise.
 */
function mustUseReaminingTrumpCard_(trumpCardUsageObj, name, matchID) {
  matchID = parseInt(matchID);
  if (!isTrumpCardUsageEnabledByMatchID_(matchID) || !ConfigManager.getConfigClass().TrumpCardConfig.AUTO_USE_REMAINING_CARDS) {
    return false;
  }

  const count = trumpCardUsageObj[name]?.[TrumpCardKeys.USED_MATCH_IDS_KEY]?.length || 0;
  const timeToUse = isTimeToUseRemainingTrumpCard_(matchID, count);

  return timeToUse && count < ConfigManager.getConfigClass().TrumpCardConfig.MAX_LIMIT;
}

/**
 * Checks if it's the time to use the remaining trump card in the match.
 * 
 * @param {string} matchID - The ID of the match.
 * @returns {boolean} - True if it's the time to use the remaining trump card, false otherwise.
 */
function isTimeToUseRemainingTrumpCard_(matchID, count) {
  const maxMatchID = ConfigManager.getConfigClass().TrumpCardConfig.MATCH_IDS_RANGE.max;
  const minMatchID = ConfigManager.getConfigClass().TrumpCardConfig.MATCH_IDS_RANGE.max - ConfigManager.getConfigClass().TrumpCardConfig.MAX_LIMIT + 1 + count;
  const range = [maxMatchID, minMatchID];
  const timeToUse = isNumberInRange_(matchID, range);

  return timeToUse;
}

/**
 * Get the failed attempts with names and match IDs from counts object.
 *
 * @param {Object} trumpCardUsageObj - The object containing trump card usage data.
 * @returns {Object} - The failed attempts with names and match IDs.
 */
function getTrumpCardFailedAttempts_(trumpCardUsageObj) {
  const failedAttempts = {};

  for (const name in trumpCardUsageObj) {
    const failedAttemptMatchIDs = trumpCardUsageObj[name]?.[TrumpCardKeys.FAILED_ATTEMP_MATCH_IDS_KEY];
    const usedMatchIDs = trumpCardUsageObj[name]?.[TrumpCardKeys.USED_MATCH_IDS_KEY];

    if (failedAttemptMatchIDs && failedAttemptMatchIDs.length > 0) {
      failedAttempts[name] = {
        usedMatchIDs: usedMatchIDs,
        failedAttemptMatchIDs: failedAttemptMatchIDs,
      };
    }
  }

  return failedAttempts;
}
