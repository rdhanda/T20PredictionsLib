/**
 * Displays an alert asking the user if they want to replace certain sheets with a new one. 
 * If the user confirms, it replaces the sheets.
 * 
 * @param {Event} e - The event parameter for a Google Apps Script trigger
 */
function replaceMatchSheetsWithFirstMenu(e) {
  const matchSheets = getSheetsToReplaceWith1_().join("; ");
  const firstMatchSheet = getFirstMatchSheet_().getName();

  let result = SpreadsheetApp.getUi().alert(`⚠️ Replace Match Sheets`,
    `You're about to replace the following Match sheets:\n\n[${matchSheets}]\nwith\n[${firstMatchSheet}] sheet. \n\nDo you want to continue?`,
    SpreadsheetApp.getUi().ButtonSet.YES_NO);

  let proceed = (result === SpreadsheetApp.getUi().Button.YES);

  if (proceed) {
    replaceMatchSheetsWithFirst_(e);
    postReplace_(e);
  }
}

/**
 * Replaces Match-2, 3 and Match-T with Match-1 
 * 
 * @param {Event} e - The event parameter for a Google Apps Script trigger
 */
function replaceMatchSheetsWithFirst_(e) {
  logSTART_(arguments.callee.name);

  const firstMatchSheet = getFirstMatchSheet_();
  clearMatchSheet_(firstMatchSheet);

  const matchSheets = getSheetsToReplaceWith1_();
  for (const matchSheetName of matchSheets) {
    replaceMatchSheet_(firstMatchSheet, matchSheetName);
  }

  logEND_(arguments.callee.name);
}

/**
 * Returns an array of sheet names to replace in Match-1 sheet.
 *
 * @returns {Array<Array<string>>} An array of sheet names to replace in Match-1 sheet.
 */
function getSheetsToReplaceWith1_() {
  const newArr = getMatchSheetNames_().slice(1).concat(TemplateSheetNames.matchSheetName);

  return newArr;
}

/**
 * Displays an alert asking the user if they want to replace certain sheets with a template sheet. 
 * If the user confirms, it replaces the sheets.
 * 
 * @param {Event} e - The event parameter for a Google Apps Script trigger
 */
function replaceMatchSheetsWithTemplateMenu(e) {
  const matchSheets = getMatchSheetNames_().join("; ");
  const matchTSheet = getTemplateMatchSheet_().getName();

  let result = SpreadsheetApp.getUi().alert(`⚠️ Replace Match Sheets`,
    `You're about to replace the following Match sheets:\n\n[${matchSheets}]\nwith\n[${matchTSheet}] sheet. \n\nDo you want to continue?`,
    SpreadsheetApp.getUi().ButtonSet.YES_NO);

  let proceed = (result === SpreadsheetApp.getUi().Button.YES);

  if (proceed) {
    replaceMatchSheetsWithTemplate_(e);
    postReplace_(e);
  }
}

/**
 * Replaces Match Sheets and Match-T with Match-1 
 * 
 * @param {Event} e - The event parameter for a Google Apps Script trigger
 */
function replaceMatchSheetsWithTemplate_(e) {
  logSTART_(arguments.callee.name);

  const matchTSheet = getTemplateMatchSheet_();
  clearMatchSheet_(matchTSheet);

  for (const matchSheetName of getMatchSheetNames_()) {
    replaceMatchSheet_(matchTSheet, matchSheetName)
  }

  logEND_(arguments.callee.name);
}

/**
 * Replaces the specified match sheet with a new sheet.
 *
 * @param {Sheet} sourceSheet - The sheet to be replaced.
 * @param {string} targetSheetName - The name of the new sheet.
 */
function replaceMatchSheet_(sourceSheet, targetSheetName) {
  replaceSheet_(sourceSheet, targetSheetName);
}

/**
 * Performs necessary actions after replacing sheets, such as clearing the temporary sheet, reordering sheets, and activating Match 1 sheet.
 *
 * @param {Object} e - The event object.
 */
function postReplace_(e) {
  resetAllSheetsSettings_();
  activateSheet_(getFirstMatchSheet_());
}


// Clears Match Sheets //

/**
 * Clears all match sheets from the MENU/UI, including all match and manual prediction data.
 */
function clearAllMatchSheetsMenu(e) {
  activateSheet_(getFirstMatchSheet_());
  clearMatchSheets_(e);
}

/**
 * Clears the active match sheet.
 */
function clearActiveMatchSheetsMenu() {
  clearActiveMatchSheets_();
}

/**
 * Clears the active match sheet.
 */
function clearActiveMatchSheets_() {
  // Get the name of the active sheet
  const activeSheetName = getActiveSpreadsheet_().getActiveSheet().getName();
  const matchSheetName = getMatchSheetNames_().find((matchSheetName) => matchSheetName === activeSheetName);

  if (!matchSheetName) { return };

  // Clear the match sheet
  if (matchSheetName) {
    clearMatchSheet_(getSheetByName_(matchSheetName));
  }
}

/**
 * Clears all match sheets, including match and manual prediction data.
 */
function clearMatchSheets_(e) {
  getMatchSheetNames_().forEach(matchSheetName => clearMatchSheet_(getSheetByName_(matchSheetName)));
  flushAndWait_();
}

/**
 * Clears the given match sheet of all match and manual prediction data.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to clear.
 */
function clearMatchSheet_(sheet) {
  const sheetName = sheet.getName();
  removePlayingXICachedData_(sheetName);
  TriggersUtils.deleteMatchResultsTriggers(sheetName);

  sheet.getRangeList(matchSheetObj.clearRanges).clearContent();
  setMatchResultStatus_(sheet, MatchResultStatus.NA);
  sheet.getRange(matchSheetObj.resultRefreshCounterRange).clearNote();
  sheet.getRange(matchSheetObj.playingXIRefreshCounterRange).clearNote();

  showAllBetsRows_(sheet);
}
