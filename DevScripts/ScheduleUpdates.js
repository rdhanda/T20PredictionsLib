//Schedule Updates//
const SCHEDULE_PER_DAY_MAX_MATCH_LIMIT = 3;

/**
 * UI function that triggers the updating of the schedule sheet.
 * 
 * @param {Event} e - The event that triggered the function.
 */
function updateScheduleSheetMenu(e) {
  updateScheduleSheet_(e);
}

/**
 * Update the Schedule sheet with new data from the tournament sheet.
 * 
 * @param {Event} e - The event that triggered this function.
 */
function updateScheduleSheet_(e) {
  if (!EnvConfigClass.isAutoModeEnabled()) { return; }

  console.time(updateScheduleSheet_.name);

  activateSheet_(getScheduleViewSheet_());

  const url = getScheduleURL_();
  if (isEmpty_(url)) { return; }

  const $ = getCheerioInstances_(url);

  if (isNotEmpty_($)) {
    const scheduleData = extractScheduleDetails_($);
    const allMatches = scheduleData.allMatches;

    doUpdateScheduleSheet_(e, allMatches);
    if (isNotEmpty_(scheduleData.teamIDs)) {
      const actualTeamIDs = scheduleData.teamIDs.filter(teamID => isNotEmpty_(teamID) && teamID !== TEAM_TBA);
      doAfterScheduleSheetUpdate_(e, actualTeamIDs);
    }
  }

  console.timeEnd(updateScheduleSheet_.name);
}

/**
 * Updates the schedule sheet with the provided source data.
 * 
 * @param {Object} e - The event object.
 * @param {Array<Array<any>>} sourceData - The data to update the schedule sheet with.
 */
function doUpdateScheduleSheet_(e, sourceData) {
  const sheet = getScheduleSheet_();
  removeFilter_(sheet);
  const colStartNum = colNameToNum_(scheduleTableSheetObj.matchIDColName);
  const range = sheet.getRange(2, colStartNum, sourceData.length, sourceData[0].length);

  const targetData = range.getValues();
  const updatedRows = [];
  const numRowsToSkip = getScheduleRowsToSkipUpdate_();
  if (isEmpty_(numRowsToSkip)) { return; }

  for (let i = numRowsToSkip; i < sourceData.length; i++) {
    for (let j = 0; j < sourceData[i].length; j++) {
      const sourceCell = sourceData[i][j];
      if (isNotEmpty_(sourceCell)) {
        targetData[i][j] = sourceCell;
      }
    }
    updatedRows.push(i + 1);
  }

  range.setValues(targetData);
  updateRefreshCounter_(sheet, scheduleTableSheetObj.randomNumCell);

  console.info(`Schedule Updated for Match IDs: ${getRangeString_(updatedRows)}`);
}

/**
 * Retrieves the number of rows to skip for updating the schedule.
 * @returns {number} The number of rows to skip for updating the schedule.
 */
function getScheduleRowsToSkipUpdate_() {
  const matchID = getFirstFutureEmptyMatchID_();
  if (matchID === 0) { return 0; }

  // If a future empty match is found, return its ID minus 1; otherwise, return undefined
  return matchID ? matchID - 1 : undefined;//MUST return undefined if all the data is in the past
}

/**
 * Retrieves the ID of the first future empty match.
 * @returns {string|null} The ID of the first future empty match, or null if no such match is found.
 */
function getFirstFutureEmptyMatchID_() {
  const matches = Object.values(ScheduleCache.getAllMatches());
  if (matches.length === 0) { return 0; }

  for (const match of matches) {
    if (match.isEmpty() && isAfterCurrentTime_(match.getStartTimeMST())) {
      return match.getId();
    }
  }

  return null; // Return null if no future empty match is found
}

/**
 * Handles actions after the schedule sheet is updated.
 * 
 * @param {object} e - The event object.
 * @param {Array<string>} teamIDs - An array of team IDs.
 */
function doAfterScheduleSheetUpdate_(e, teamIDs) {
  const playingTeamIDs = getPlayingTeamIDsCachedData_();
  const requiresTeamsUpdate = !containsIdenticalValues_(playingTeamIDs, teamIDs);

  if (requiresTeamsUpdate) {
    updatePlayingTeamIDs_(teamIDs);
    updatePlayers_(e, null);
  }

  ScheduleCache.clearCache();
  ScheduleUtils.hideScheduleViewRows();

  logExceedingMatchLimitError_(SCHEDULE_PER_DAY_MAX_MATCH_LIMIT);
  flushAndWait_();
}

/**
 * Logs an error message if the schedule has matches exceeding the specified limit per day.
 * @param {number} limit - The maximum number of matches allowed per day.
 */
function logExceedingMatchLimitError_(limit) {
  const datesExceedingLimit = getDatesWithMatchCountExceedingLimit_(limit);
  if (datesExceedingLimit.length > 0) {
    const datesList = datesExceedingLimit.map(dateInfo => `${dateInfo.date}: ${dateInfo.count}`);
    const message = `Schedule has matches exceeding the limit of '${limit}' on the following dates: ${datesList.join(', ')}`;
    log(message, LogLevel.ERROR);
  }
}

/**
 * Finds all dates with matches scheduled exceeding the specified limit and lists the dates with their match count.
 * @param {number} limit - The specific limit for the number of matches.
 * @returns {Array<{date: string, count: number}>} - An array of objects containing the date and the count of matches on that date.
 */
function getDatesWithMatchCountExceedingLimit_(limit) {
  const matchObjects = ScheduleCache.getAllMatches();
  const dateMatchCount = {};

  // Count the number of matches for each date
  for (const matchId in matchObjects) {
    const match = matchObjects[matchId];
    const matchDate = match.getShortDate();

    if (match.isEmpty()) {
      if (dateMatchCount[matchDate]) {
        dateMatchCount[matchDate]++;
      } else {
        dateMatchCount[matchDate] = 1;
      }
    }
  }

  // Filter out dates with match count exceeding the specified limit
  const datesExceedingLimit = [];
  for (const date in dateMatchCount) {
    if (dateMatchCount[date] > limit) {
      datesExceedingLimit.push({
        date: date,
        count: dateMatchCount[date]
      });
    }
  }

  return datesExceedingLimit;
}

/**
 * Updates the match status in the schedule sheet to "Open" for a given date.
 *
 * @param {[ScheduleMatch]} matches - The matches update the match status for
 */
function updateMatchesStatusOpen_(matches) {
  updateMatchesStatusByMatches_(matches, MatchScheduleStatus.OPEN, scheduleTableSheetObj.openedTimeColName);
}

/**
 * Updates the match status in the schedule sheet to "Closed" for a given date.
 *
 * @param {[ScheduleMatch]} matches - The matches update the match status for
 */
function updateMatchesStatusClosed_(matches) {
  updateMatchesStatusByMatches_(matches, MatchScheduleStatus.CLOSED, scheduleTableSheetObj.closedTimeColName);
}

function updateMatchesStatusByMatches_(matches, status, timeStampCol) {
  if (isNotEmpty_(matches)) {
    const matchIDs = matches.map(match => match.getId());
    const rowNums = getScheduleRowNumsByMatchIDs_(matchIDs);

    if (rowNums) {
      const statusRanges = rowNums.map(index => `${scheduleTableSheetObj.matchStatusColName}${index}`);
      const timeStampRanges = rowNums.map(index => `${timeStampCol}${index}`);

      updateScheduleRangeList_(statusRanges, status);
      updateScheduleRangeList_(timeStampRanges, getCurrentTimesptamp_());
    } else {
      log(`MatchIDs '${matchIDs}' NOT found to update ${status} timestamp.`, LogLevel.WARN);
    }
  } else {
    log(`Matches NOT found to update ${status} timestamp.`, LogLevel.WARN);
  }
}

/**
 * Updates the status and timestamp of matches in the schedule sheet for a given date
 * @param {string} shortDate - The short date string (dd-MMM-yyyy) for which matches should be updated
 * @param {string} newStatus - The status to set for the matches (OPEN or CLOSED)
 * @param {string} timeStampCol - The column name for the timestamp to update (formOpenedTimeColName or formClosedTimeColName)
 */
function updateMatchesStatusByShortDate_(shortDate, newStatus, oldStatus, timeStampCol) {
  const rowNums = getScheduleRowNumsByDate_(shortDate, oldStatus);

  if (rowNums.length > 0) {
    const statusRanges = rowNums.map(index => `${scheduleTableSheetObj.matchStatusColName}${index}`);
    const timeStampRanges = rowNums.map(index => `${timeStampCol}${index}`);

    updateScheduleRangeList_(statusRanges, newStatus);
    updateScheduleRangeList_(timeStampRanges, getCurrentTimesptamp_());
  } else {
    log(`No matching rows found for date=${shortDate}, newStatus=${newStatus}, oldStatus=${oldStatus}, timeStampCol=${timeStampCol}`, LogLevel.WARN);
  }
}

/**
 * Updates the completion time of a match in the schedule journal sheet.
 * 
 * @param {string} matchID - The ID of the match to update.
 */
function updateMatchCompletedTime_(matchID) {
  if (isEmpty_(matchID)) { return; }

  const rowNums = getScheduleRowNumsByMatchIDs_(matchID);

  if (rowNums) {
    const rangeList = rowNums.map(index => `${scheduleTableSheetObj.completedTimeColName}${index}`);
    updateScheduleRangeList_(rangeList, getCurrentTimesptamp_());
  } else {
    log(`MatchID '${matchID}' NOT found to update completion timestamp.`, LogLevel.WARN);
  }
}

/**
 * Update the reminder sent column in the schedule journal sheet for a match on a given date.
 * 
 * @param {string} shortDate The date of the match.
 * @return {void}
 */
function updateReminderTimestamp_(shortDate) {
  logSTART_(updateReminderTimestamp_.name);

  if (isEmpty_(shortDate)) { return; }

  const rowNums = getScheduleRowNumsByDate_(shortDate, MatchScheduleStatus.OPEN);

  if (rowNums) {
    const rangeList = rowNums.map(index => `${scheduleTableSheetObj.reminderSentColName}${index}`);
    updateScheduleRangeList_(rangeList, getCurrentTimesptamp_());
  } else {
    log(`MatchDate '${shortDate}' NOT found to update reminder timestamp.`, LogLevel.WARN);
  }

  logEND_(updateReminderTimestamp_.name)
}

/**
 * Find the row nums of the match in the schedule sheet for a given match date and status.
 * 
 * @param {string} shortDate - The match date in the format of "ddd-MM"
 * @param {string} matchStatus - The match status to filter by
 * @return {number[]} rowIndexArr - Array of row indexes of the match in the schedule sheet
 */
function getScheduleRowNumsByDate_(shortDate, matchStatus) {
  const matchIDs = getMatchIDsByShortDateAndStatus_(shortDate, matchStatus);
  return convertMatchIDsToRowNums_(matchIDs);
}

/**
 * Retrieves match IDs for matches with a specific short date and status.
 * @param {string} shortDate - The short date of the matches (e.g., "ddd-MM").
 * @param {string} matchStatus - The status of the matches.
 * @returns {string[]} An array of match IDs.
 */
function getMatchIDsByShortDateAndStatus_(shortDate, matchStatus) {
  const matchObjects = ScheduleCache.getAllMatches();
  const matchIDs = [];

  for (const matchId in matchObjects) {
    const match = matchObjects[matchId];
    if (match.getShortDate() === shortDate && match.getStatus() === matchStatus) {
      matchIDs.push(match.getId());
    }
  }

  return matchIDs;
}

/**
 * Excludes matches from the schedule journal by their IDs if their current status is MatchScheduleStatus.EMPTY.
 * @param {Array<string>} matchIDs - An array of match IDs to be excluded.
 */
function excludeMatchesByID_(matchIDs) {
  if (isEmpty_(matchIDs)) { return; }

  const flatMatchIDs = expandAndFlattenArray_(matchIDs);
  const rowNums = getScheduleRowNumsByMatchIDs_(flatMatchIDs);

  if (rowNums.length > 0) {
    const statusRanges = rowNums.map(index => `${scheduleTableSheetObj.matchStatusColName}${index}`);
    updateScheduleRangeList_(statusRanges, MatchScheduleStatus.EXCLUDED);
    console.info(`Updated the rows [${rowNums}] in '${scheduleTableSheetName}' sheet with Match Status as '${MatchScheduleStatus.EXCLUDED}' for Match IDs [${flatMatchIDs}].`);
  }
}

/**
 * Flattens an array containing numbers and ranges into a flat array of numbers.
 *
 * @param {Array} arr - The input array, which may contain numbers and sub-arrays representing ranges.
 * @return {Array} - A flattened array with expanded ranges.
 * 
 * @example
 * // Input array containing numbers and ranges
 * let EXCLUDED_MATCH_IDS_IN_SCHEDULE = [[2, 5], 7, [9, 12], 18, 20, 23, 24, 28, 29, 31, 32, 34, 37, 39];
 * // Call the function
 * let flatArray = flattenArray(EXCLUDED_MATCH_IDS_IN_SCHEDULE);
 * // flatArray is now: [2, 3, 4, 5, 7, 9, 10, 11, 12, 18, 20, 23, 24, 28, 29, 31, 32, 34, 37, 39]
 */
function expandAndFlattenArray_(arr) {
  const flatArr = arr.flatMap(item => {
    if (Array.isArray(item) && item.length >= 2) {
      let range = [];
      for (let i = item[0]; i <= item[1]; i++) {
        range.push(i);
      }
      return range;
    } else {
      return item;
    }
  });

  return [...new Set(flatArr)].sort((a, b) => a - b);
}

/**
 * Find the row index of the match in the schedule sheet for given match IDs and status.
 * 
 * @param {string|string[]} matchIDs - The match ID or an array of match IDs to search for
 * @return {number[]} rowIndexArr - Array of row indexes of the match in the schedule sheet
 */
function getScheduleRowNumsByMatchIDs_(matchIDs) {
  if (!Array.isArray(matchIDs)) {
    matchIDs = [matchIDs]; // Convert single value to array
  }

  return convertMatchIDsToRowNums_(matchIDs);
}

/**
 * Converts an array of match IDs to row nums, i.e. match ID + 1.
 * @param {number[]} matchIDs - An array of match IDs.
 * @returns {number[]} An array of row nums.
 */
function convertMatchIDsToRowNums_(matchIDs) {
  return matchIDs.map(id => id + 1);
}

/**
 * Updates a range of cells in the schedule journal sheet with a specified value.
 * 
 * @param {string} range - The range of cells to update, e.g., "A1:B5"
 * @param {string|number|boolean|Date} value - The value to set in the cells
 */
function updateScheduleRangeList_(range, value) {
  const sheet = getScheduleSheet_();
  sheet.getRangeList(range).setValue(value);

  ScheduleCache.clearCache();
}

/**
 * Retrieves the URL for the schedule of the active tournament.
 * 
 * @returns {string | undefined} The URL for the tournament schedule, or undefined if no active tournament URL is found.
 */
function getScheduleURL_() {
  const url = getActiveSeriesURL_();
  if (isNotEmpty_(url)) {
    return `${url}${URLs.TOURNAMENT_SCHEDULE_URL_PART}`;
  }
  log('No Active Tournament URL Found.', LogLevel.WARN);
}

/**
 * Schedule info is extracted from these <div>'s:
 * Sample Content:
 * 
<div class="ds-p-4 hover:ds-bg-ui-fill-translucent ds-border-t ds-border-line">
    <div class="ds-flex">
        <div class="ds-text-compact-xs ds-font-bold ds-w-24">Sat, 23 Mar '24</div>
        <div class="ds-grow ds-px-4 ds-border-r ds-border-line-default-translucent">
            <a href="/series/indian-premier-league-2024-1410320/punjab-kings-vs-delhi-capitals-2nd-match-1422120/live-cricket-score" class="ds-no-tap-higlight">
                <div class="ds-text-compact-xxs">
                    <div class="ds-relative">
                        <div class="ds-flex ds-justify-between ds-items-center">
                            <div class="ds-truncate">
                                <span class="ds-flex ds-items-center">
                                    <div class="ds-text-tight-s ds-font-regular ds-truncate ds-text-typo-mid3">
                                        <span class="ds-text-tight-s ds-font-medium ds-text-typo">2nd Match (D/N) </span>
                                        •&nbsp;Mullanpur,
                                        <a href="/series/indian-premier-league-2024-1410320" title="IPL 2024" class="ds-inline-flex ds-items-start ds-leading-none !ds-inline">
                                            <span class="ds-text-tight-s ds-font-regular ds-text-typo ds-underline ds-decoration-ui-stroke hover:ds-text-typo-primary hover:ds-decoration-ui-stroke-primary ds-block !ds-inline">Indian Premier League</span>
                                        </a>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="ds-flex ds-justify-between ds-items-center">
                            <div class="ds-flex ds-flex-col ds-mt-2 ds-mb-2">
                                <div class="ci-team-score ds-flex ds-justify-between ds-items-center ds-text-typo ds-my-1">
                                    <div class="ds-flex ds-items-center ds-min-w-0 ds-mr-1" title="Punjab Kings">
                                        <img width="20" height="20" alt="Punjab Kings Flag" class="ds-mr-2" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_80/lsci/db/PICTURES/CMS/317000/317003.png" style="width: 20px; height: 20px;">
                                        <p class="ds-text-tight-m ds-font-bold ds-capitalize ds-truncate">Punjab Kings</p>
                                    </div>
                                </div>
                                <div class="ci-team-score ds-flex ds-justify-between ds-items-center ds-text-typo ds-my-1">
                                    <div class="ds-flex ds-items-center ds-min-w-0 ds-mr-1" title="Delhi Capitals">
                                        <img width="20" height="20" alt="Delhi Capitals Flag" class="ds-mr-2" src="https://img1.hscicdn.com/image/upload/f_auto,t_ds_square_w_80/lsci/db/PICTURES/CMS/313400/313422.logo.png" style="width: 20px; height: 20px;">
                                        <p class="ds-text-tight-m ds-font-bold ds-capitalize ds-truncate">Delhi Capitals</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ds-text-right">
                                <div class="ds-text-tight-m ds-font-bold">4:00 AM</div>
                                <span class="ds-text-tight-xs">
                                    <span>10:00 am GMT</span> | <span>3:30 pm Local</span>
                                </span>
                            </div>
                        </div>
                        <p class="ds-text-tight-s ds-font-medium ds-line-clamp-2 ds-text-typo" title="PBKS vs DC - 2nd Match">
                            <span>Match yet to begin</span>
                        </p>
                    </div>
                </div>
            </a>
            <div></div>
        </div>
        <div class="ds-px-4 ds-self-center ds-w-28">
            <div>
                <a href="/series/indian-premier-league-2024-1410320" title="Indian Premier League" class="ds-inline-flex ds-items-start ds-leading-none">
                    <span class="ds-text-compact-xs ds-text-typo hover:ds-underline hover:ds-decoration-ui-stroke ds-block">Series Home</span>
                </a>
            </div>
        </div>
    </div>
</div>
 * 
 */
