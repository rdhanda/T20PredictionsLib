//FormSubmission.gs//
/**
 * Class to handle form submissions.
 */
class FormSubmission {
  /**
   * Creates an instance of FormSubmission.
   * @param {GoogleAppsScript.Events.SheetsOnFormSubmit} e - The event object from the form submission.
   */
  constructor(e) {
    this.e = e;
    const { range, namedValues, values } = e;
    this.range = range;
    this.namedValues = namedValues;
    this.values = values;
    this.sheet = range.getSheet();
    this.formSheetName = this.sheet.getName();
  }

  /**
   * Handles the form submission process.
   */
  handleFormSubmission() {
    logSTART_(this.handleFormSubmission.name);

    try {
      this.makeBackupEntry();

      const response = this.getFormLatestResponseInfo();
      let email = this.getEmailFromResponse(response);

      let punterName = this.getPunterNameFromResponse(email);

      console.log(`Form entry submitted by "${punterName}" using "${email}" in "${this.formSheetName}", data: "${response?.data || ""}".`);
      console.log(`e = ${JSON.stringify(this.e)}`);

      EmailFormSubmission.sendEmail(this.formSheetName, punterName || email, response?.data || "", JSON.stringify(this.e));
      flushAndWait_();
      this.validatePunterNameAndEmail(punterName, email);

      this.resetDefaultFont();
      this.autoResizeColumns();
    } catch (error) {
      log(`${error}, Stack: ${error.stack}`, LogLevel.ERROR);
    }

    logEND_(this.handleFormSubmission.name);
  }

  /**
   * Creates a backup entry of the form submission data.
   */
  makeBackupEntry() {
    makeBackupEntry_(this.formSheetName, this.values);
  }

  /**
   * Retrieves the punter name from the response using the provided email.
   * @param {string} email - The email address from the response.
   * @returns {string|null} The punter name or null if not found.
   */
  getPunterNameFromResponse(email) {
    console.log(`email=${email}`);

    let punterName = this.namedValues[FormConstants.PUNTER_QUESTION_TITLE];
    if (isEmpty_(punterName)) {
      punterName = PuntersUtils.getPunterNameByEmail_(email);
    }

    console.log(`punterName=${punterName}`);
    return punterName && String(punterName);
  }

  /**
   * Retrieves the email from the response.
   * @param {Object} response - The response object containing the email.
   * @returns {string|null} The email address or null if not found.
   */
  getEmailFromResponse(response) {
    const emailAddressKey = this.getEmailAddressKey();
    let email = this.namedValues[emailAddressKey];
    if (isEmpty_(email) && response) {
      email = response.email;
    }
    return email && String(email);
  }

  /**
   * Retrieves the latest response info from the form.
   * @returns {Object|null} An object containing the latest response info or null if not found.
   */
  getFormLatestResponseInfo() {
    if (this.sheet) {
      const formUrl = this.sheet.getFormUrl();
      if (formUrl) {
        console.time(this.getFormLatestResponseInfo.name);

        const form = FormApp.openByUrl(formUrl);
        console.log('Form Title:', form.getTitle());

        const latestResponse = form.getResponses().pop();
        const itemResponses = latestResponse.getItemResponses();

        const responseItems = itemResponses.reduce((acc, itemResponse) => {
          const question = itemResponse.getItem().getTitle();
          const answer = itemResponse.getResponse();
          acc[question] = acc[question] || [];
          acc[question].push(answer);
          return acc;
        }, {});

        const timestamp = latestResponse.getTimestamp();
        const submitterEmail = latestResponse.getRespondentEmail();
        const data = [timestamp, submitterEmail, ...itemResponses.map(item => item.getResponse())];

        console.log(`LATEST FORM RESPONSE: >> Timestamp: ${timestamp}, Submitter Email: ${submitterEmail}, Response Items: ${JSON.stringify(responseItems)}`);
        console.timeEnd(this.getFormLatestResponseInfo.name);

        return { email: submitterEmail, timestamp, responseItems: JSON.stringify(responseItems), data };
      }
    }
    return null;
  }

  /**
   * Retrieves the key for the email address field from the named values.
   * @returns {string|null} The key for the email address field or null if not found.
   */
  getEmailAddressKey() {
    const namedValues = this.e.namedValues;
    const keys = Object.keys(namedValues);

    // Loop through the keys and check if any of them match "email address"
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i].toLowerCase();
      if (key === FormConstants.EMAIL_ADDRESS_QUESTION_TITLE.toLowerCase()) {
        return keys[i];
      }
    }

    // If no key matches "email address", return null
    return null;
  }

  /**
   * Validates the punter name and email address from the form submission.
   * @param {string} selectedPunterName - The punter name selected in the form.
   * @param {string} submittedEmail - The email address submitted in the form.
   */
  validatePunterNameAndEmail(selectedPunterName, submittedEmail) {
    if (SkipValidatingFormSheetNames.includes(this.formSheetName)) {
      console.log(`NOT Validating the punter "${selectedPunterName}" and email address "${submittedEmail}" for form "${this.formSheetName}".`);
      return;
    }

    console.log(`Validating the punter "${selectedPunterName}" and email address "${submittedEmail}" for form "${this.formSheetName}".`);

    if (isNotEmpty_(submittedEmail)) {
      let registeredPunter = PuntersUtils.getPunterNameByEmail_(submittedEmail);

      if (registeredPunter) {
        if (isNotEmpty_(selectedPunterName)) {
          if (registeredPunter != selectedPunterName) {
            EmailInvalidEntries.sendEmailWrongPunter(registeredPunter, submittedEmail, selectedPunterName, this.formSheetName);
          }
        }
      } else {
        if (isNotEmpty_(selectedPunterName)) {
          EmailInvalidEntries.sendEmailUnknownEmailWithPunter(submittedEmail, selectedPunterName, this.formSheetName);
        } else {
          EmailInvalidEntries.sendEmailUnknownEmailWithoutPunter(submittedEmail, this.formSheetName);
        }
      }
    }
  }

  /**
   * Resets the default font of the sheet.
   */
  resetDefaultFont() {
    resetDefaultFont_(this.sheet);
  }

  /**
   * Auto resizes the columns of the sheet.
   */
  autoResizeColumns() {
    autoResizeColumns_(this.sheet);
  }
}

/**
 * Backs up the form submission data to a separate sheet.
 *
 * @param {string} sheetName The name of the sheet containing the form submission data.
 * @param {Array} data The form submission data to be backed up.
 */
function makeBackupEntry_(sheetName, data) {
  console.time(arguments.callee.name);

  if (dailyBetsSheetName == sheetName) {
    const sheet = getDailyBetsBkpSheet_();
    sheet.appendRow(data);
    updateDashboardBetsLists_();
    resetDefaultFont_(sheet);
    autoResizeColumns_(getSheetByName_(sheetName));
  } else if (seriesBetsSheetName == sheetName) {
    const sheet = getSeriesBetsBkpSheet_();
    sheet.appendRow(data);
    resetDefaultFont_(sheet);
    autoResizeColumns_(sheet);
  }

  console.timeEnd(arguments.callee.name);
}

