//DailyBetsFormStatus.gs//

/**
 * Displays the current status of the Daily Bets form in a menu.
 */
function checkDailyBetsFormStatusMenu() {
  let open = isDailyBetsFormAcceptingResponses_();
  let title = getDailyBetsFormTitle_();
  let message = "The Daily Predictions Form [" + title + "] is now ";
  if (open) {
    message += "OPEN. Accepting responses";
  } else {
    message += "CLOSED. Not accepting responses";
  }
  message += "\n\n";
  message += "Form ID: " + getDailyBetsForm_().getId();
  message += "\n";
  message += getDailyBetsFormPublishedURL_();
  showInfoAlert_(message);
}

/**
 * Opens the Daily Bets form for accepting responses via a menu option.
 */
function startAcceptingDailyBetsMenu() {
  startAcceptingDailyBets_();
}

/**
 * Starts accepting responses for the Daily Bets form.
 */
function startAcceptingDailyBets_() {
  let form = getDailyBetsForm_();
  form.setAcceptingResponses(true);
}

/**
 * Closes the Daily Bets form for accepting responses via a menu option.
 */
function stopAcceptingDailyBetsMenu() {
  stopAcceptingDailyBets_();
}

/**
 * Stops accepting responses for the Daily Bets form.
 */
function stopAcceptingDailyBets_() {
  let form = getDailyBetsForm_();
  form.setAcceptingResponses(false);
}

/**
 * Checks if the Daily Bets form is currently accepting responses.
 * @returns {boolean} True if the form is accepting responses, false otherwise.
 */
function isDailyBetsFormAcceptingResponses_() {
  let form = getDailyBetsForm_();
  return form.isAcceptingResponses();
}
