/**
 * Function to get the Auto Bets Excluded Players from column B
 * @return {Array} List of player names
 */
function getAutoBetsExcludedPlayers_() {
  var data = getPlayersSheetCachedData_();
  
  const playerNameIdx = colNameToIndex_(playersExcludedSheetObj.autoBetsExcludedPlayersColName);

  var players = [];

  for (var i = 0; i < data.length; i++) {
    const playerName = data[i][playerNameIdx];
    if (isNotEmpty_(playerName)) {
      players.push(playerName.trim());
    }
  }

  return players;
}

/**
 * Function to get the Daily Bets Form Excluded Players from column B
 * if column C has a 'Y'
 * @return {Array} List of player names
 */
function getDailyBetsFormExcludedPlayers_() {
  var data = getPlayersSheetCachedData_();

  const playerNameIdx = colNameToIndex_(playersExcludedSheetObj.autoBetsExcludedPlayersColName);
  const isDailyFormExcludedFlagIdx = colNameToIndex_(playersExcludedSheetObj.isDailyFormExcludedFlagColName);

  var players = [];

  for (var i = 0; i < data.length; i++) {
    const playerName = data[i][playerNameIdx];
    const isExcluded = toBoolean_(data[i][isDailyFormExcludedFlagIdx]);
    if (isNotEmpty_(playerName) && isExcluded) {
      players.push(playerName.trim());
    }
  }

  return players;
}
