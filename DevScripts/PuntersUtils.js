
/**
 * Represents a utility class for managing punters data.
 */
class PuntersUtils {
  /**
   * Retrieves the list of punter names from the provided punters data.
   * 
   * @param {Punter[]} puntersData - The punters data, an array of Punter objects.
   * @returns {string[]} The list of punter names.
   */
  static getPunterNamesList_(puntersData) {
    return puntersData.map(punter => punter.getName());
  }

  /**
   * Retrieves the comma-separated string of punter names from the provided punters data.
   * 
   * @param {Punter[]} puntersData - The punters data, an array of Punter objects.
   * @returns {string} The comma-separated string of punter names.
   */
  static getPunterNames_(puntersData) {
    return puntersData.map(punter => punter.getName()).join(', ');
  }

  /**
   * Retrieves the name of the punter associated with the given email address.
   * 
   * @param {string} email - The email address of the punter.
   * @param {string} [spreadsheetId] - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {string|null} The name of the punter, or null if no punter is found with the given email.
   */
  static getPunterNameByEmail_(email, spreadsheetId = null) {
    if (isEmpty_(email)) { return; }

    const puntersData = PuntersCache.getPunters(spreadsheetId);
    const lowerCaseEmail = email.toLowerCase();
    for (const punter of Object.values(puntersData)) {
      const punterEmails = punter.getEmails().map(e => e.toLowerCase());
      if (punterEmails.includes(lowerCaseEmail)) return punter.getName();
    }

    log(`Email '${email}' not found`, LogLevel.WARN);
    return null; // Return null if no punter is found with the given email
  }

  /**
   * Retrieves the email address of the punter associated with the given name.
   * 
   * @param {string} name - The name of the punter.
   * @param {string} [spreadsheetId] - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {string|null} The email address of the punter, or null if no punter is found with the given name.
   */
  static getPunterEmail_(name, spreadsheetId = null) {
    if (isEmpty_(name)) { return; }

    const puntersData = PuntersCache.getPunters(spreadsheetId);
    const lowerCaseName = name.toLowerCase().trim();
    const punter = Object.values(puntersData).find(punter => punter.getName().toLowerCase().trim() === lowerCaseName);
    if (punter) {
      return punter.getEmails()[0];
    } else {
      log(`Punter '${name}' not found`, LogLevel.WARN);
      return null;
    }
  }

  /**
   * Retrieves the list of active punters requiring reminders.
   * 
   * @param {string} [spreadsheetId] - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {string[]} The list of names of active punters requiring reminders.
   */
  static getPuntersRequiringReminder_(spreadsheetId = null) {
    const puntersData = PuntersCache.getPunters(spreadsheetId);
    const activePunters = Object.values(puntersData).filter(punter => punter.isReminderAllowed());
    return activePunters.map(punter => punter.getName());
  }

  /**
   * Retrieves the names of active punters.
   * 
   * @param {boolean} [includeActiveBot=false] - Whether to include active bot punters in the result.
   * @param {string} [spreadsheetId] - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {string[]} The names of active punters.
   */
  static getActivePunterNames_(includeActiveBot = false, spreadsheetId = null) {
    const puntersData = PuntersCache.getPunters(spreadsheetId);
    return Object.values(puntersData)
      .filter(punter => !punter.isInactive() && (includeActiveBot || !punter.isBot()))
      .map(punter => punter.getName());
  }

  /**
   * Checks if the specified punter is a bot.
   * 
   * @param {string} punterName - The name of the punter.
   * @param {string} [spreadsheetId] - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {boolean} True if the punter is a bot, otherwise false.
   */
  static isPunterBot_(punterName, spreadsheetId = null) {
    const punterDetails = PuntersCache.getPunters(spreadsheetId);
    return punterDetails[punterName]?.isBot() ?? false;
  }

  /**
   * Checks if a punter can auto-submit.
   * 
   * @param {Punter} punter - The punter object.
   * @returns {boolean} True if the punter can auto-submit, false otherwise.
   */
  static canPunterAutoSubmit_(punter) {
    if (punter) {
      return punter.isAutoSubmitAllowed() && (punter.isBot() || !hasPunterReachedAutoSubmitLimit_(punter.getName()));
    }
    return false;
  }

  /**
   * Retrieves punters eligible for auto-submission.
   * 
   * @param {Event} e - The event object (context-specific).
   * @param {string} [spreadsheetId] - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {Array<Punter>} An array of Punter instances eligible for auto-submission.
   */
  static getPuntersForAutoSubmission_(e, spreadsheetId = null) {
    const punters = PuntersCache.getPunters(spreadsheetId);
    const puntersForAuto = Object.values(punters)
      .filter(punter => PuntersUtils.canPunterAutoSubmit_(punter));

    const submittedPicks = getPuntersWithSubmittedDailyPicks_(e);
    const startedSubmitting = getPuntersStartedSubmitting_(e);

    const puntersAuto = puntersForAuto.filter(punter => {
      const hasStartedSubmitting = startedSubmitting.includes(punter.getName());
      const hasNotSubmittedPicks = !submittedPicks.includes(punter.getName());
      return punter.isBot() || (hasStartedSubmitting && hasNotSubmittedPicks);
    });

    return puntersAuto;
  }
}

// /**
//  * Represents a utility class for managing punters data.
//  */
// class PuntersUtils {
//   /**
//    * Retrieves the list of punter names from the provided punters data.
//    * 
//    * @param {Punter[]} puntersData - The punters data, an array of Punter objects.
//    * @returns {string[]} The list of punter names.
//    */
//   static getPunterNamesList_(puntersData) {
//     return puntersData.map(punter => punter.getName());
//   }

//   /**
//    * Retrieves the comma-separated string of punter names from the provided punters data.
//    * 
//    * @param {Punter[]} puntersData - The punters data, an array of Punter objects.
//    * @returns {string} The comma-separated string of punter names.
//    */
//   static getPunterNames_(puntersData) {
//     return puntersData.map(punter => punter.getName()).join(', ');
//   }

//   /**
//    * Retrieves the name of the punter associated with the given email address.
//    * 
//    * @param {string} email - The email address of the punter.
//    * @returns {string|null} The name of the punter, or null if no punter is found with the given email.
//    */
//   static getPunterNameByEmail_(email) {
//     if (isEmpty_(email)) { return; }

//     const puntersData = PuntersCache.getPunters_();
//     const lowerCaseEmail = email.toLowerCase();
//     for (const punter of Object.values(puntersData)) {
//       const punterEmails = punter.getEmails().map(e => e.toLowerCase());
//       if (punterEmails.includes(lowerCaseEmail)) return punter.getName();
//     }

//     log(`Email '${email}' not found`, LogLevel.WARN);
//     return null; // Return null if no punter is found with the given email
//   }

//   /**
//    * Retrieves the email address of the punter associated with the given name.
//    * 
//    * @param {string} name - The name of the punter.
//    * @returns {string|null} The email address of the punter, or null if no punter is found with the given name.
//    */
//   static getPunterEmail_(name) {
//     if (isEmpty_(name)) { return; }

//     const puntersData = PuntersCache.getPunters_();
//     const lowerCaseName = name.toLowerCase().trim();
//     const punter = Object.values(puntersData).find(punter => punter.getName().toLowerCase().trim() === lowerCaseName);
//     if (punter) {
//       return punter.getEmails()[0];
//     } else {
//       log(`Punter '${name}' not found`, LogLevel.WARN);
//       return null;
//     }
//   }

//   /**
//    * Retrieves the list of active punters requiring reminders.
//    * 
//    * @returns {string[]} The list of names of active punters requiring reminders.
//    */
//   static getPuntersRequiringReminder_() {
//     const puntersData = PuntersCache.getPunters_();
//     const activePunters = Object.values(puntersData).filter(punter => punter.isReminderAllowed());
//     return activePunters.map(punter => punter.getName());
//   }

//   /**
//    * Retrieves the names of active punters.
//    * 
//    * @param {boolean} [includeActiveBot=false] - Whether to include active bot punters in the result.
//    * @returns {string[]} The names of active punters.
//    */
//   static getActivePunterNames_(includeActiveBot = false) {
//     const puntersData = PuntersCache.getPunters_();
//     return Object.values(puntersData)
//       .filter(punter => !punter.isInactive() && (includeActiveBot || !punter.isBot()))
//       .map(punter => punter.getName());
//   }

//   /**
//    * Checks if the specified punter is a bot.
//    * 
//    * @param {string} punterName - The name of the punter.
//    * @returns {boolean} True if the punter is a bot, otherwise false.
//    */
//   static isPunterBot_(punterName) {
//     const punterDetails = PuntersCache.getPunters_();
//     return punterDetails[punterName]?.isBot() ?? false;
//   }

//   /**
//    * Checks if a punter can auto-submit.
//    * 
//    * @param {Punter} punter - The punter object.
//    * @returns {boolean} True if the punter can auto-submit, false otherwise.
//    */
//   static canPunterAutoSubmit_(punter) {
//     if (punter) {
//       return punter.isAutoSubmitAllowed() && (punter.isBot() || !hasPunterReachedAutoSubmitLimit_(punter.getName()));
//     }
//     return false;
//   }

//   /**
//    * Retrieves punters eligible for auto-submission.
//    * 
//    * @param {Event} e - The event object (context-specific).
//    * @returns {Array<Punter>} An array of Punter instances eligible for auto-submission.
//    */
//   static getPuntersForAutoSubmission_(e) {
//     const punters = PuntersCache.getPunters_();
//     const puntersForAuto = Object.values(punters)
//       .filter(punter => PuntersUtils.canPunterAutoSubmit_(punter));

//     const submittedPicks = getPuntersWithSubmittedDailyPicks_(e);
//     const startedSubmitting = getPuntersStartedSubmitting_(e);

//     const puntersAuto = puntersForAuto.filter(punter => {
//       const hasStartedSubmitting = startedSubmitting.includes(punter.getName());
//       const hasNotSubmittedPicks = !submittedPicks.includes(punter.getName());
//       return punter.isBot() || (hasStartedSubmitting && hasNotSubmittedPicks);
//     });

//     return puntersAuto;
//   }
// }

//TODO - move to new class for DailyBets when created
/**
 * Retrieves punters who have started submitting predictions. At least one submnission already
 * @returns {string[]} An array of punter names who have started submitting predictions.
 */
function getPuntersStartedSubmitting_(e) {
  const data = getDataFromSheet_(dailyBetsClosedSheetName);
  const submissions = new Set(); // Using Set instead of array to ensure uniqueness
  const emailIdx = colNameToIndex_(dailyPredictionsSheetObj.emailColName);
  const nameIdx = colNameToIndex_(dailyPredictionsSheetObj.punterColName);

  for (let i = 0; i < data.length; i++) {
    const punterEmail = data[i][emailIdx];
    let punterName = data[i][nameIdx];
    if (isNotEmpty_(punterEmail) && isEmpty_(punterName)) {
      punterName = PuntersUtils.getPunterNameByEmail_(punterEmail);
    }
    if (isNotEmpty_(punterName)) {
      submissions.add(punterName); // Using add method of Set to add unique values
    }
  }

  return Array.from(submissions).sort(); // Converting Set back to array for return and sorting
}
