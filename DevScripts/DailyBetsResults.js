//

/**
 * Represents a collection of daily bets results.
 * Provides static methods to retrieve information from the daily bets results.
 */
class DailyBetsResults {
  /**
   * Returns the DailyBetsResult object for the given match ID.
   *
   * @param {string} matchID - The ID of the match.
   * @returns {DailyBetsResult|null} The DailyBetsResult object for the given match ID, or null if not found.
   */
  static getResultsRowByMatchId(matchID) {
    const dailyResults = getDailyBetsResults_();

    for (const key in dailyResults) {
      if (dailyResults.hasOwnProperty(key)) {
        const result = dailyResults[key];
        if (result.getIsResultRow() && result.getMatchID() === matchID) {
          return result;
        }
      }
    }

    return null; // Return null if no matching result is found for the given match ID
  }

  /**
   * Returns an array of punter names who are ranked 1 for the given match ID.
   *
   * @param {string} matchID - The ID of the match.
   * @returns {string[]} An array of punter names who are ranked 1 for the given match ID.
   */
  static getPuntersRanked1ForMatch(matchID) {
    if (isEmpty_(matchID)) { return; }

    const dailyResults = getDailyBetsResults_();
    const rank1Punters = [];

    for (const key in dailyResults) {
      if (dailyResults.hasOwnProperty(key)) {
        const result = dailyResults[key];
        if (result.getMatchID() === matchID && result.getRank() === 1) {
          rank1Punters.push(result.getPunterName());
        }
      }
    }

    return rank1Punters;
  }
}
