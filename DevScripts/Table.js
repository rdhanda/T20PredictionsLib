//StandingsTable//

const groupNames = [`GROUP A`, `GROUP B`, "Group C", "Group D", "Group E"];//To be used when more than 1 group exists in the table standings

/**
 * Updates the Standings Table UI by calling the updatePointsTable_ function.
 * @param {Event} e - The event object.
 */
function updateStandingsTableMenu(e) {
  updateStandingsTable_(e);
}

/**
 * Updates the Points Table with the latest data.
 * @param {Event} e - The event object.
 */
function updateStandingsTable_(e) {
  if (!EnvConfigClass.isAutoModeEnabled()) { return; }

  console.time(updateStandingsTable_.name);

  const url = getStandingsTableURL_();
  if (isEmpty_(url)) {
    log(`The URL for Points Table could not be found.`, LogLevel.ERROR);
    return;
  }

  //log(`URL for Points Table '${url}'`, LogLevel.INFO);

  const sheet = getStandingsTableSheet_();
  activateSheet_(sheet);
  const range = sheet.getRange(standingsTableSheetObj.tableRange);
  range.clearContent();

  const $ = getCheerioInstances_(url);

  if (isNotEmpty_($)) {
    //const tablesData = extractPointsTableData_($);
    //const pointsData = tempGetPointsTableData_(tablesData[0].data);//When using one group. IPL
    const pointsData = getPointsTableData_($);//When using multiple groups, WC

    if (isNotEmpty_(pointsData)) {
      const startingRow = range.getRow();
      const startingColumn = range.getColumn();

      sheet.getRange(startingRow, startingColumn, pointsData.length, pointsData[0].length).setValues(pointsData);
      updateRefreshCounter_(sheet, standingsTableSheetObj.randomNumCell);
    } else {
      log(`Points Table Standings with Ranks is not yet available at '${url}'`, LogLevel.WARN);
    }
  } else {
    log(`No contents found for URL: '${url}'.`, LogLevel.ERROR);
  }

  console.timeEnd(updateStandingsTable_.name);
}

/**
 * This is a TEMP Solution, need to fix for multiple groups
 */
function tempGetPointsTableData_(data) {
  const finalData = [];
  if (isEmpty_(data)) { return; }

  const headers = data[0];
  const rankRegex = /^\d+/; // Regular expression to match the rank

  const teamIndex = headers.indexOf('TEAMS');
  const matchesIndex = headers.indexOf('M');
  const winsIndex = headers.indexOf('W');
  const lostIndex = headers.indexOf('L');
  const tiedIndex = headers.indexOf('T');
  const noResultIndex = headers.indexOf('N/R');
  const pointsIndex = headers.indexOf('PT');
  const nrrIndex = headers.indexOf('NRR');

  const tableData = data.slice(1).filter(row => {
    const teamData = row[teamIndex];
    const rankMatch = teamData.match(rankRegex);
    const rank = rankMatch ? rankMatch[0] : '';
    const teamName = rankMatch ? teamData.replace(rankRegex, '').trim() : teamData.trim();

    return isNotEmpty_(rank) && isNotEmpty_(teamName) && isNotEmpty_(matchesIndex) && isNotEmpty_(pointsIndex) && isNotEmpty_(nrrIndex);
  }).map(row => {
    const teamData = row[teamIndex];
    const rankMatch = teamData.match(rankRegex);
    const rank = rankMatch ? rankMatch[0] : '';
    const teamName = rankMatch ? teamData.replace(rankRegex, '').trim() : teamData.trim();
    return [rank, teamName, getTeamIdByName_(teamName), row[matchesIndex], row[winsIndex], row[lostIndex], row[tiedIndex], row[noResultIndex], row[pointsIndex], row[nrrIndex]];
  });

  finalData.push(...tableData);

  return finalData;
}

/**
 * Gets the Points Table data from the specified URL.
 * @param {string} url - The URL to fetch the Points Table data from.
 * @returns {Array[]} The filtered Points Table data.
 */
function getPointsTableData_($) {

  /**
   * div.ds-flex.ds-px-4.ds-border-b.ds-border-line.ds-py-3 > div > span.ds-text-title-xs.ds-font-bold.ds-text-typo
   * div ds-flex ds-px-4 ds-border-b ds-border-line ds-py-3
   */

  /**
   * div.ds-mb-4 > 
   * div.ds-w-full ds-bg-fill-content-prime ds-overflow-hidden ds-rounded-xl ds-border ds-border-line > //We Need This CSS
   * div.ds-p-0 ds-border-b ds-border-line last:ds-border-b-0 >
   * table >
   * 
   * TODO: It is not finding correct grouping when there are more than one group.
   * This was working when we had more than one groups, using div > span
   * const cssSelector = 'div.ds-flex.ds-px-4.ds-border-b.ds-border-line.ds-py-3 span.ds-text-tight-s.ds-font-bold.ds-uppercase';
   * 
   */

  //When Only One Group
  //const cssSelector = 'div.ds-w-full.ds-bg-fill-content-prime.ds-overflow-hidden.ds-rounded-xl.ds-border.ds-border-line';

  //When more then One Group
  const cssSelector = 'div.ds-flex.ds-px-4.ds-border-b.ds-border-line.ds-py-3 span.ds-text-tight-s.ds-font-bold.ds-uppercase';

  const groups = extractElementsWithCSSSelector_($, cssSelector);
  if (isEmpty_(groups)) {
    log("No Groups Found. Check the CSS Selector, it might have changed.", LogLevel.WARN);
    return;
  }

  console.log(`Groups for Points Table '${groups}'`);

  const finalData = [];

  for (const [index, groupName] of groups.entries()) {
    //console.log(`Table Index = '${index}', Group = '${groupName}'`);

    const data = extractTableDataByIndex_($, index);

    if (isEmpty_(data)) { return; }

    const headers = data[0];
    const rankRegex = /^\d+/; // Regular expression to match the rank

    const teamIndex = headers.indexOf('TEAMS');
    const matchesIndex = headers.indexOf('M');
    const winsIndex = headers.indexOf('W');
    const lostIndex = headers.indexOf('L');
    const tiedIndex = headers.indexOf('T');
    const noResultIndex = headers.indexOf('N/R');
    const pointsIndex = headers.indexOf('PT');
    const nrrIndex = headers.indexOf('NRR');

    const tableData = data.filter(row => {
      const teamData = row[teamIndex];
      const rankMatch = teamData.match(rankRegex);
      const rank = rankMatch ? rankMatch[0] : '';
      const teamName = rankMatch ? teamData.replace(rankRegex, '').trim() : teamData.trim();

      return isNotEmpty_(rank) && isNotEmpty_(teamName) && isNotEmpty_(matchesIndex) && isNotEmpty_(pointsIndex) && isNotEmpty_(nrrIndex);
    }).map(row => {
      const teamData = row[teamIndex];
      const rankMatch = teamData.match(rankRegex);
      const rank = rankMatch ? rankMatch[0] : '';
      const teamName = rankMatch ? teamData.replace(rankRegex, '').trim() : teamData.trim();

      return [rank, teamName, getTeamIdByName_(teamName), row[matchesIndex], row[winsIndex], row[lostIndex], row[tiedIndex], row[noResultIndex], row[pointsIndex], row[nrrIndex]];
    });

    //Add the Group Header when more than 1 group. TODO: Fix this
    if (groups.length > 1) {
      finalData.push([undefined, groupName.toUpperCase(), undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined]);
    }
    finalData.push(...tableData);
  }

  return finalData;
}

/**
 * Finds the indices of elements from the provided array in the specified groups array.
 * If the groups array is empty or not an array, returns an array with a single value of 0.
 *
 * @param {Array} allGroupsArray - The array in which to find the indices of elements.
 * @returns {Array} An array containing the indices of the elements from the groups array,
 *                  or an array with a single value of 0 if the groups array is empty or not an array.
 */
function findGroupTableIndices_(allGroupsArray) {
  if (!Array.isArray(allGroupsArray) || allGroupsArray.length === 0) {
    return [0];
  }

  return groupNames.map(group => findIndexCaseInsensitive_(allGroupsArray, group));
}

/**
 * Case-insensitive version of Array.prototype.indexOf().
 *
 * @param {Array} array - The array to search.
 * @param {*} searchElement - The element to locate in the array.
 * @returns {number} The first index at which a given element can be found in the array, or -1 if it is not present.
 */
function findIndexCaseInsensitive_(array, searchElement) {
  const lowerCaseSearch = searchElement.toLowerCase();
  return array.findIndex(item => item.toLowerCase() === lowerCaseSearch);
}

/**
 * Gets the URL for the Points Table based on the active tournament.
 * @returns {string|undefined} The URL for the Points Table, or undefined if no active tournament URL is found.
 */
function getStandingsTableURL_() {
  let url = getActiveSeriesURL_();
  if (isNotEmpty_(url)) {
    url = url + "/points-table-standings";
    return url;
  }
  log('No Active Tournament URL Found.', LogLevel.ERROR);
}
