//EnvSetup.gs//

/**
 * Environment configurations object containing various settings.
 * @typedef {Object} EnvConfigs
 * @property {string} ENV_DEFAULT_CONFIG_GROUP - The default configuration group.
 * @property {string} ENV_NAME - The environment name.
 * @property {string[]} ENV_ADMIN_EMAIL_LIST - The list of admin email addresses.
 * @property {boolean} ENV_SEND_EMAILS_FROM_NON_PROD - Whether to send emails from non-production environment.
 * @property {string} ENV_CURRENT_LOG_LEVEL - The current log level.
 * @property {boolean} ENV_GET_PROD_FORM_RESPONSES - Whether to get production form responses.
 * @property {boolean} ENV_TEMP_HALT_TRIGGER_EXECUTIONS - Whether to temporarily halt trigger executions.
 * @property {number} ENV_UPDATE_MATCH_RESULTS_TRIGGER_EVERY_MINUTES - The interval for updating match results triggers (in minutes).
 * @property {number} ENV_UPDATE_MATCH_RESULTS_MAX_PER_MINUTE - The maximum number of match results updates per minute.
 * @property {Object} MATCH_START_TIME - Object containing settings related to match start time.
 * @property {number} MATCH_START_TIME.AddMinutesToCloseDailyBets - Number of minutes to add to close daily bets.
 * @property {number} MATCH_START_TIME.AddMinutesToSendReminders - Number of minutes to add to send reminders.
 */
var EnvConfigs = {
  ENV_DEFAULT_CONFIG_GROUP: EnvConfigConstants.ENV_GROUP_AUCHEM,
  ENV_NAME: EnvConfigConstants.ENV_NAME_DEV,
  ENV_ADDITIONAL_ADMIN_EMAIL_LIST: null,
  ENV_SEND_EMAILS_FROM_NON_PROD: false,
  ENV_CURRENT_LOG_LEVEL: EnvConfigConstants.ENV_LOG_LEVEL_DEBUG,
  ENV_GET_PROD_FORM_RESPONSES: false,
  ENV_TEMP_HALT_TRIGGER_EXECUTIONS: false,
  ENV_UPDATE_MATCH_RESULTS_TRIGGER_EVERY_MINUTES: 5,
  ENV_UPDATE_MATCH_RESULTS_MAX_PER_MINUTE: 2,
  MATCH_START_TIME: {
    AddMinutesToCloseDailyBets: -32, // Toss Time is 32 minutes before start time
    AddMinutesToSendReminders: -90,  // 90 minutes before match start time
  },
  ENV_AUTO_MODE: true,
};

/**
 * This function is called automatically when the Google Sheets document is opened.
 * It initializes the environment and logs the start and end of the function execution.
 * @function
 * @name onOpen
 * @returns {void}
 */
function onOpen() {
  console.time(onOpen.name);

  setupEnv();

  console.timeEnd(onOpen.name);
}

/**
 * 
 */
function promoteAllForms(e) {
  promoteAllForms_(e);
}

/**
 * 
 */
function renameSheets() {
  renameSheets_();
}
