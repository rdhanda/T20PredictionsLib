//Scorecard Live//
//Lice Score scorllbar limit
const LIVE_SCORE_SCROLLBAR_CHAR_LIMIT = 120;//This is max available on live scorecard

function testGetLiveScorecardStatsByMatchID() {
  const matchID = getMatchID_(getFirstMatchSheet_())
  const urlLive = getLiveScorecardURL_(matchID);
  let $live = getCheerioInstances_(urlLive);

  const stats = getLiveScorecardStats_($live);
  log(stats)

  //log(extractLiveOversProgress_($live))

}

function testGetLiveScorecardStatsByUrl() {
  const urlLive = 'https://www.espncricinfo.com/series/county-championship-division-one-2024-1410191/lancashire-vs-kent-20th-match-1410212/live-cricket-score';
  let $live = getCheerioInstances_(urlLive);

  const stats = getLiveScorecardStats_($live);

  log(stats);
}

/**
 * Retrieves live scorecard statistics for a given match ID.
 *
 * @param {string} url - The URL the match for which to fetch statistics.
 * @returns {string} A formatted string containing player statistics.
 */
function getLiveScorecardStats_($) {
  if (isEmpty_($)) {
    return;
  }

  const currentBattersBowlersStats = extractCurrentBattersBowlersStats_($) || '';
  const oversProgress = extractLiveOversProgress_($) || '';
  const additionalInfo = extractPartnershipLastbatInfo_($) || '';

  return { part1: currentBattersBowlersStats, part2: oversProgress, part3: additionalInfo };
}

/**
 * Extracts and formats the current batters and bowlers stats from HTML content.
 * @param {string} $ - The HTML content containing the match stats table.
 * @returns {string} - The formatted current batters and bowlers stats.
 */
function extractCurrentBattersBowlersStats_($) {
  try {
    const table = extractTableDataByIndex_($, 0);
    let battersFormatted = '';
    let bowlersFormatted = '';
    let isBattersSection = false;
    let isBowlersSection = false;

    for (let i = 0; i < table.length; i++) {
      const row = table[i];
      if (row[0] === PlayerTypes.BATTERS) {
        isBattersSection = true;
        isBowlersSection = false;
      } else if (row[0] === PlayerTypes.BOWLERS) {
        isBattersSection = false;
        isBowlersSection = true;
      } else if (isBattersSection) {
        battersFormatted += `🏏${sanitizePlayerName_(row[0])}   ${row[1]} (${row[2]}b ${row[3]}x4 ${row[4]}x6) SR: ${row[5]}\n`;
      } else if (isBowlersSection) {
        bowlersFormatted += `⚪️${sanitizePlayerName_(row[0])}   ${row[1]}-${row[2]}-${row[3]}-${row[4]} Econ: ${row[5]}\n`;
      }
    }

    if (!battersFormatted || !bowlersFormatted) { return "Live score not available"; }

    return `${battersFormatted.trim()}\n\n${bowlersFormatted.trim()}`;
  } catch (error) {
    log(error, LogLevel.ERROR);
    return ''; // Return empty string in case of error
  }
}

/**
 * Extracts the live overs progress from the given Cheerio object.
 *
 * @param {CheerioStatic} $ - The Cheerio object containing the HTML content.
 * @returns {string} The extracted and formatted live overs progress string.
 */
function extractLiveOversProgress_($) {
  try {
    const cssSelectorBalls = 'div.ds-space-x-2 div.ds-py-2 span:not([class])';
    const cssSelectorOver = 'span.ds-text-tight-s.ds-font-regular.ds-bg-fill-content-prime.ds-z-\\[1\\]';
    const cssSelector = `${cssSelectorBalls}, ${cssSelectorOver}`;
    const elements = $(cssSelector).get().reverse();

    let progress = [[]]; // Initialize an array to hold the progress in 2D format
    let currentIndex = 0;

    elements.forEach(element => {
      const $element = $(element);
      const text = $element.text();
      const isOverSpan = $element.is(cssSelectorOver);

      if (isOverSpan) {
        progress[currentIndex].unshift(`${text}:`); // Prepend the text wrapped in brackets at the beginning
        progress.push([]);
        currentIndex++;
      } else {
        progress[currentIndex].push(text);
      }
    });

    let lastFourOvers = progress.reverse();//.slice(0, 6);
    // Join each element inside the inner array by space and each inner array inside the outer array by '\n'
    const progressStr = lastFourOvers.map(over => over.join(' ')).join('\n');

    return progressStr.trim();
  } catch (error) {
    log(error, LogLevel.ERROR);
    return ''; // Return empty string in case of error
  }
}


/**
 * Extracts partnership and last bat information from the provided content.
 * 
 * @param {any} $ - The content from which to extract information.
 * @returns {string} The extracted partnership and last bat information.
 */
function extractPartnershipLastbatInfo_($) {
  try {
    const cssSelector = 'div.ds-text-tight-s.ds-font-regular.ds-px-4.ds-py-2.ds-border-line.ds-text-typo-mid1';
    const data = extractElementsWithCSSSelector_($, cssSelector);
    const progressStr = data.flatMap(innerArray => innerArray.filter(item => item !== '•').join(' ') + '\n').join('');

    return progressStr.trim();
  } catch (error) {
    log(error, LogLevel.ERROR);
    return ''; // Return empty string in case of error
  }
}
