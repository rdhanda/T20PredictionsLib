const MARGIN_POINTS = 2;

/**
 * Returns the current closest margins in the specified sheet for both runs and wickets.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to update.
 */
function getCurrentClosestMargins_(sheet, runsMarginResult, wicketsMarginResult, firstBattingTeam, secondBattingTeam, matchWinTeam) {
  flushAndWait_();

  //For a TIED match, the closest margin is runs=0
  if (runsMarginResult === 0) {
    const runsMargin = 0;
    const wicketsMargin = null;
    return { runsMargin, wicketsMargin };
  }

  let teamToKeepRunsMargin;
  let teamToKeepWicketsMargin;

  if (isNotEmpty_(matchWinTeam)) {
    if (isNotEmpty_(runsMarginResult)) {
      teamToKeepRunsMargin = matchWinTeam;
    }
    if (isNotEmpty_(wicketsMarginResult)) {
      teamToKeepWicketsMargin = matchWinTeam;
    }
  } else {
    teamToKeepRunsMargin = firstBattingTeam;
    teamToKeepWicketsMargin = secondBattingTeam;
  }

  const runsMargin = getCurrentHighestClosestMarginByColNum_(sheet, runsMarginResult, colNameToNum_(matchSheetObj.runsMarginEntriesColName), teamToKeepRunsMargin);
  const wicketsMargin = getCurrentHighestClosestMarginByColNum_(sheet, wicketsMarginResult, colNameToNum_(matchSheetObj.wicketsMarginEntriesColName), teamToKeepWicketsMargin);

  var closestMargins = { runsMargin, wicketsMargin };

  return closestMargins;
}

/**
 * Retrieves the current closest margin based on the specified margin entries column.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet containing the data.
 * @param {number} marginEntriesColNum - The column num for margin entries.
 * @returns {number|null} - The current closest margin value or null if not found.
 */
function getCurrentHighestClosestMarginByColNum_(sheet, marginResult, marginEntriesColNum, teamToKeep) {
  if (isNotEmpty_(marginResult)) {
    const teamEntries = sheet.getRange(rowStartNum2, colNameToNum_(matchSheetObj.teamWinEntriesColName), maxPuntersNum14, 1).getValues().flat();
    const marginEntries = sheet.getRange(rowStartNum2, marginEntriesColNum, maxPuntersNum14, 1).getValues().flat();
    const filteredMarginEntries = getFilteredValuesByTeam_(teamEntries, marginEntries, teamToKeep);

    if (filteredMarginEntries.length > 0) {
      const botExludedEntries = excludeBotMarginEntries_(filteredMarginEntries, sheet);
      const entries = isAllEmptyArray_(botExludedEntries) ? filteredMarginEntries : botExludedEntries;

      const lowestVal = findLowestClosestValue_(marginResult, entries);
      const highestVal = findHighestClosestValue_(marginResult, entries);

      //return lowestVal == highestVal ? lowestVal : `${lowestVal},${highestVal}`;
      const uniqueValues = new Set([lowestVal, highestVal, marginResult]);
      const resultArray = [...uniqueValues];

      return resultArray.join(',');
    }
  }

  return null;
}

/**
 * Retrieves an array of filtered values based on a specific team.
 * @param {string[]} teams - An array of team names.
 * @param {number[]} margins - An array of corresponding margins.
 * @param {string} teamToKeep - The name of the team to filter values for.
 * @returns {Array.<number|undefined>} - An array of filtered values matching the specified team, with undefined values for non-matching teams.
 */
function getFilteredValuesByTeam_(teams, margins, teamToKeep) {
  return margins.map((value, index) => teams[index] == teamToKeep ? value : undefined);
}

/**
 * Gets the filtered margin entries based on team win points.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to retrieve data from.
 * @param {number} marginEntriesCI - The column index of the margin entries.
 * @param {number} winPointsCI - The column index of the win points.
 * @returns {Array<Array<number>|null>} The filtered margin entries.
 */
function getFilteredMarginEntries_(sheet, marginEntriesCI, winPointsCI) {
  const teamWinPoints = sheet.getRange(rowStartNum2, winPointsCI, maxPuntersNum14, 1).getValues();
  const puntersMarginEnties = sheet.getRange(rowStartNum2, marginEntriesCI, maxPuntersNum14, 1).getValues();
  const filteredMarginEntries = teamWinPoints.map((val, i) => (isNotEmpty_(val) && val > 0) ? puntersMarginEnties[i] : null);

  return filteredMarginEntries;
}

/**
 * Creates a copy of an array with elements removed at indices returned by getBotEntriesIndices_ function.
 *
 * @param {Array} arr - The array to remove elements from.
 * @param {Object} sheet - The sheet object to get indices from.
 * @returns {Array} The new array with elements removed.
 */
function excludeBotMarginEntries_(arr, sheet) {
  const indices = getBotEntriesIndices_(sheet);
  const newArr = arr.slice(); // create a copy of the original array
  indices.forEach(i => {
    if (isNotEmpty_(newArr[i])) {
      newArr[i] = null;
    }
  });
  return newArr;
}

/**
 * Returns an array of 0-based row indices where the punter name contains the substring "bot" (case insensitive)
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to search for entries with punter names containing "bot"
 * @return {Array} An array of 0-based row indices where the punter name contains the substring "bot"
 */
function getBotEntriesIndices_(sheet) {
  const punterNames = sheet.getRange(rowStartNum2, colNameToNum_(matchSheetObj.punterNameColName), maxPuntersNum14, 1).getValues().flat();
  const indices = punterNames.map((name, index) => index).filter((index) => PuntersUtils.isPunterBot_(punterNames[index]));
  return indices;
}

/**
 * Clears the margin points in the specified sheet.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to clear margin points in.
 */
function clearMarginPoints_(sheet) {
  sheet.getRange(matchSheetObj.marginPointsRange).clearContent();
}
