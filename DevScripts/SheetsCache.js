//Sheets Cache//
const SheetsCachedData = {};

/**
 * Retrieves cached data for a specific sheet.
 * @param {string} sheetName - The name of the sheet.
 * @param {boolean} [removeHeader=false] - Whether to remove the header row from the fetched data.
 * @returns {any} The cached data for the sheet.
 */
function getSheetCachedData_(sheetName, removeHeader = false) {
  if (SheetsCachedData.hasOwnProperty(sheetName)) {
    return SheetsCachedData[sheetName];
  } else {
    const data = getDataFromSheet_(sheetName, removeHeader);
    SheetsCachedData[sheetName] = data;
    return data;
  }
}

/**
 * Removes the cached data for a specific sheet.
 * @param {string} sheetName - The name of the sheet.
 */
function removeSheetCachedData_(sheetName) {
  console.time(`${removeSheetCachedData_.name}${sheetName}`);

  if (SheetsCachedData.hasOwnProperty(sheetName)) {
    delete SheetsCachedData[sheetName];
    console.timeEnd(`${removeSheetCachedData_.name}${sheetName}`);
  }
}

/**
 * Fetches data from the specified sheet in the given spreadsheet.
 * 
 * @param {string} sheetName - The name of the sheet.
 * @param {boolean} [removeHeader=false] - Whether to remove the header row from the fetched data.
 * @param {string|null} [spreadsheetId=null] - The ID of the spreadsheet to fetch data from. If null, uses the default spreadsheet.
 * @returns {Array<Array<any>>} The fetched data from the sheet.
 */
function getDataFromSheet_(sheetName, removeHeader = false, spreadsheetId = null) {
  const funcName = `${getDataFromSheet_.name}${sheetName}${spreadsheetId ? '_' + spreadsheetId : ''}`;
  console.time(funcName);

  const sheet = getSheetByName_(sheetName, spreadsheetId);
  const range = sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns());
  const values = range.getValues();
  const data = values.filter(row => row.some(cell => isNotEmpty_(cell)));

  console.timeEnd(funcName);
  return removeHeader ? data.slice(1) : data;
}

/**
 * Retrieves the cached data from the teams sheet.
 * 
 * @returns {Array} The cached data from the teams sheet.
 */
function getTeamsSheetCachedData_() {
  return getSheetCachedData_(teamsSheetName);
}

/**
 * Removes the cached data from the teams sheet.
 */
function removeTeamsSheetCachedData_() {
  removeSheetCachedData_(teamsSheetName);
}

/**
 * Retrieves the cached data from the daily results sheet.
 * 
 * @returns {Array} The cached data from the daily results sheet.
 */
function getDailyResultsSheetCachedData_() {
  return getSheetCachedData_(dailyBetsResultsSheetName, true);
}

/**
 * Removes the cached data from the daily results sheet.
 */
function removeDailyResultsSheetCachedData_() {
  removeSheetCachedData_(dailyBetsResultsSheetName);
}

/**
 * Retrieves the cached data from the results journal sheet.
 * 
 * @returns {Array} The cached data from the results journal sheet.
 */
function getResultsJournalSheetCachedData_() {
  return getSheetCachedData_(dailyBetsResultsJournalSheetName);
}

/**
 * Removes the cached data from the results journal sheet.
 */
function removeResultsJournalSheetCachedData_() {
  removeSheetCachedData_(dailyBetsResultsJournalSheetName);
}

/**
 * 
 */
function getPlayersSheetCachedData_() {
  var data = getSheetCachedData_(playersExcludedSheetName, true);
  return data;
}

/**
 * 
 */
function removePlayersSheetCachedData_() {
  removeSheetCachedData_(playersExcludedSheetName);
}
