/**
 * Define all sheets here
 */
const spreadsheetFileNameSuffix = "BatBallBets";//Do not change it to Picks, this is the spreadsheet name
const dailyBetsFormNameSuffix = "DailyBets";//Bets
const seriesBetsFormNameSuffix = "SeriesBets";//SeriesBets

const homeSheetName = "Home";

const match1SheetName = "M1";
const match2SheetName = "M2";
const match3SheetName = "M3";
const matchTSheetName = "MT";

const dailyBetsSheetName = "Bets";//
const dailyBetsClosedSheetName = "BetsClosed";//
const dailyBetsBackupSheetName = "BetsBackup";//
const dailyBetsPointsSheetName = "Points";
const dailyTeamResultsSheetName = "TeamResults";//
const dailyBetsResultsSheetName = "Results";//
const dailyBetsResultsJournalSheetName = "BetsResultsJ";//
const dailyAutoBetsLogSheetName = "AutoBetsLog";//
const dailyTopBetsSheetName = "TopBets";//

const seriesBetsSheetName = "SeriesBets";//
const seriesBetsBackupSheetName = "SeriesBetsBackup";//
const seriesBetsPointsSheetName = "SeriesPoints";//
const seriesBetsModSheetName = "SeriesBetsMod";//
const seriesBetsFormSheetName = "SeriesBetsForm"; //

const tableSheetName = "Table";
const predictoSheetName = "Predicto";
const playerStatsSheetName = "PlayerStats";

const firstInningsScorePointsJSheetName = "1stInnsScorePts";
const firstInningsScoreSheetName = "1stInnsScores";

const playersSheetName = "Players";
const playersExcludedSheetName = "PlayersX";
const scheduleViewSheetName = "ScheduleV";//RENAME
const scheduleTableSheetName = "ScheduleT";//NEW

const puntersSheetName = "Punters";
const teamsSheetName = "Teams";
const seriesViewSheetName = "SeriesV";//
const seriesTableSheetName = "SeriesT";//
const countriesSheetName = "Countries";
const winnersSheetName = "Winners";
const systemLogsSheetName = "Logs";

const FormSheetMap = {
  [dailyBetsSheetName]: dailyBetsFormNameSuffix,
  [seriesBetsSheetName]: seriesBetsFormNameSuffix,
};

const SkipValidatingFormSheetNames = [];

const SheetCurrentAndNewNamesMap = [
  { currentNames: 'Home', newName: homeSheetName },
  { currentNames: 'Table', newName: tableSheetName },
  { currentNames: 'Predicto', newName: predictoSheetName },
  { currentNames: 'M1', newName: match1SheetName },
  { currentNames: 'M2', newName: match2SheetName },
  { currentNames: 'M3', newName: match3SheetName },
  { currentNames: 'MT', newName: matchTSheetName },
  { currentNames: 'D-Picks', newName: dailyBetsSheetName },
  { currentNames: 'DailyPicksClosed', newName: dailyBetsClosedSheetName },
  { currentNames: 'DailyPicksBkp', newName: dailyBetsBackupSheetName },
  { currentNames: 'O-Picks', newName: seriesBetsSheetName },
  { currentNames: 'OverallPicksBkp', newName: seriesBetsBackupSheetName },
  { currentNames: 'Points', newName: dailyBetsPointsSheetName },
  { currentNames: 'T-Results', newName: dailyTeamResultsSheetName },
  { currentNames: 'Results', newName: dailyBetsResultsSheetName },
  { currentNames: 'O-Results', newName: seriesBetsPointsSheetName },
  { currentNames: 'OverallPicksMod', newName: seriesBetsModSheetName },
  { currentNames: 'OverallPicksForm', newName: seriesBetsFormSheetName },
  { currentNames: 'Players', newName: playersSheetName },
  { currentNames: 'Scores', newName: firstInningsScoreSheetName },
  { currentNames: 'Schedule', newName: scheduleTableSheetName },
  { currentNames: 'Punters', newName: puntersSheetName },
  { currentNames: 'AutoPicksLog', newName: dailyAutoBetsLogSheetName },
  { currentNames: 'ScorePts', newName: firstInningsScorePointsJSheetName },
  { currentNames: 'TopPicks', newName: dailyTopBetsSheetName },
  { currentNames: 'ResultsJ', newName: dailyBetsResultsJournalSheetName },
  { currentNames: 'TeamsM', newName: teamsSheetName },
  { currentNames: 'TournamentsM', newName: seriesTableSheetName },
  { currentNames: 'CountriesM', newName: countriesSheetName },
  { currentNames: 'Winners', newName: winnersSheetName },
];

const MatchSheetConfigPunterColName = 'C';

/**
 * Contains information about matches, including the match sheet names and column names for each match from predictions sheet.
 * Steps to add a new match sheet
 * 1. Create a new sheet in the spreadsheet
 * 2. Define a new sheet name const
 * 3. Define a new trigger function e.g. updateMatch4ResultsT
 * 4a. Define a new start function e.g. startUpdateMatch4ResultsT - trigger that starts the exec update trigger at scheduled time
 * 4a. Define a new exec function e.g. execUpdateMatch4ResultsT - trigger that runs every 5 minutes until done
 * 5. Add a new entry in this {MatchSheetConfigs} const.
 */
const MatchSheetConfigs = [
  {
    sheetName: match1SheetName,
    titleIDColName: 'D',
    questionsStartColName: 'E',
    questionsEndColName: 'N',
    startTriggerName: startUpdateMatch1ResultsT.name,
    execTriggerFuncName: execUpdateMatch1ResultsT.name,
  },
  {
    sheetName: match2SheetName,
    titleIDColName: 'O',
    questionsStartColName: 'P',
    questionsEndColName: 'Y',
    startTriggerName: startUpdateMatch1ResultsT.name,
    execTriggerFuncName: execUpdateMatch2ResultsT.name,
  },
  {
    sheetName: match3SheetName,
    titleIDColName: 'Z',
    questionsStartColName: 'AA',
    questionsEndColName: 'AJ',
    startTriggerName: startUpdateMatch1ResultsT.name,
    execTriggerFuncName: execUpdateMatch3ResultsT.name,
  },
];

/**
 * An object that contains the name of a match sheet in a template.
 *
 * @type {{matchSheetName: string}}
 */
const TemplateSheetNames = {
  matchSheetName: matchTSheetName,
};

//**************************** DEFINE VARIABLES HERE, CELL REF HERE */
const rowStartNum2 = 2;
const maxPuntersNum14 = 14;//Same as number of punters
const finalResultRowNum16 = maxPuntersNum14 + rowStartNum2; //14 Results row index number in Match sheets
const betsLastRowNum15 = finalResultRowNum16 - 1;
const scoreDisplayDataRowNum17 = finalResultRowNum16 + 1;

const smudgingPlayersListRowNum18 = scoreDisplayDataRowNum17 + 1;
const playingXIListRowNum19 = smudgingPlayersListRowNum18 + 1;
const impactPlayersListRowNum20 = playingXIListRowNum19 + 1;
const notInPlayingXIListRowNum21 = impactPlayersListRowNum20 + 1;

const firstSecondBattingTeamsRowNum22 = notInPlayingXIListRowNum21 + 1;
const closestMarginsRowNum23 = firstSecondBattingTeamsRowNum22 + 1;

const manualSmudgingBeginRowNum27 = closestMarginsRowNum23 + 4;//First row where player name begins in C Column
const manualSmudgingEndRowNum63 = manualSmudgingBeginRowNum27 + 36;//Last row of manual smudging List

const columnStartNum = 1;//Column num where to start copying from
const columnNumForPoints = 14;//Where total points for each punter are
const columns2CopyForScore = 26;//Number of columns to copy from Match sheets to DailyResults sheet

/****** Sheet Objects ********/

//Dashboard sheet obj
const dashboardSheetObj = {
  get punterStartRow() { return 4 },
  get punterLastRow() { return this.punterStartRow + maxPuntersNum14 },//17
  get overallAndDailyDataRange() { return `B${this.punterStartRow - 1}:J${this.punterLastRow}` },//start one row beofre punter to include header row
  get lastMatchPointsDataRange() { return `Z${this.punterStartRow}:AB${this.punterLastRow}` },
  get dailySubmittedEntriesRange() { return `AF${this.punterStartRow}:AF${this.punterLastRow}` },
  get dailyMissingEntriesRange() { return `AG${this.punterStartRow}:AG${this.punterLastRow}` },
}

const predictoSheetObj = {
  predictoColName: "B",
  potmColName: "D",
  economyColName: "H",

  get clearRangesPredicto() {
    return [`${getRangeByColNameWithHeader_(this.predictoColName)}`];
  },

  get clearRangesPlayerCategories() {
    return [`${getRangeByColNameNoHeader_(this.potmColName, this.economyColName)}`];
  }
}

//
const standingsTableSheetObj = {
  randomNumCell: "R1",
  tableRange: 'A3:J40',

  get clearRanges() {
    return [this.randomNumCell, this.tableRange];
  },
}

//DailyResponses sheet obj
const dailyPredictionsSheetObj = {
  emailColName: "B",
  punterColName: "C",

  //To reset after tournament is done, before starting new tournament
  deleteFromRowNum: 2,
  emptyRowsToKeep: 10,
}

//Overall Predcitions Sheet Obj
const overallPredictionsSheetObj = {
  deleteFromRowNum: 2,
  emptyRowsToKeep: 10,
  hideColRange: [4, 14],
}

//OverallResults sheet cell refs
const overallResultsSheetObj = {
  randomNumCell: "L1",
  resultsAndProbablesRange: 'G18:J19',

  get clearRanges() {
    return [this.randomNumCell, this.resultsAndProbablesRange, 'AT2:AT13', 'AW2:AW5', 'G29:J76']
  },
}

const overallPredictionsModSheetObj = {
  clearRanges: ["A2:L"],//
}

const dailyPointsSheetObj = {
  bonusPointsColName: "CE",

  get clearRanges() {
    return [getRangeByColNameNoHeader_(this.bonusPointsColName),];
  },
}

//Daily Results sheet cell refs
const dailyResultsSheetObj = {
  topMatchIDCell: "A2",
  matchIDColName: "A",
  matchIDRange: "A:A",
  entriesIDsColumRange: "B:B",
  entriesInfoColName: "B",
  punterNameColName: "C",
  potmColName: "I",

  teamWinColName: "F",
  potmPointsColName: "V",

  //To reset after tournament is done, before starting new tournament
  deleteFromRowNum: 2,
  emptyRowsToKeep: 10,
}

//FormData Sheet obj
const dailyPredictionsFormSheetObj = {
  clearRanges: ["A2"],

  readyToProcessStatusCell: "B7",

  matchDateCell: "A2",
  numOfMatchesCell: "B2",

  formTitleCell: "B10",

  //The following MUST match the MAX number of matches can be setup in the form. IPL = 2 matches, WC = 3 matches
  matchesInfoRange: "D2:J4",//3 matches only for now. MatchID in [0], MatchTitle in [1], TeamA in [2], TeamB in [3], Desc in [4], Stage in [5]
}

//OverallPredictionsFrom Sheet obj
const overallPredictionsFormSheetObj = {
  randomNumCell: "G15",

  allTeamsRange: "B2:B",

  group1TeamsRange: "C2:C",
  group2TeamsRange: "D2:D",
}

//Match sheet obj
const matchSheetObj = {
  punterNameColName: "C",
  matchResultStatusCell: `C${finalResultRowNum16}`,
  resultStatusColName: "C",//Where result is set to Off,On,Done,Copied. This will be replace with - when copying

  resultMatchIDCell: `A${finalResultRowNum16}`,
  resultTossCell: `D${finalResultRowNum16}`,
  resultFirstInningsScoreCell: `E${finalResultRowNum16}`,
  resultWinCell: `F${finalResultRowNum16}`,
  resultRunsMarginCell: `G${finalResultRowNum16}`,
  resultWktsMarginCell: `H${finalResultRowNum16}`,
  resultPOTMCell: `I${finalResultRowNum16}`,
  resultMaxRunsCell: `J${finalResultRowNum16}`,
  resultStrikeRateCell: `K${finalResultRowNum16}`,
  resultMaxWicketsCell: `L${finalResultRowNum16}`,
  resultEconomyCell: `M${finalResultRowNum16}`,

  runsMarginResultTied: `Tied`,

  teamWinEntriesColName: "F",
  runsMarginEntriesColName: "G",
  wicketsMarginEntriesColName: "H",

  winPointsSumCell: `S${finalResultRowNum16}`,
  marginPointsSumCell: `T${finalResultRowNum16}`,
  marginPointsRange: `T${rowStartNum2}:U${betsLastRowNum15}`,

  //To get the data from scorecard data using urlFtech()
  scoreResultCopyRange: `D${finalResultRowNum16}:M${finalResultRowNum16}`,
  matchNameCell: `A${scoreDisplayDataRowNum17}`,//A15
  scorecardStatusRange: `C${scoreDisplayDataRowNum17}`,//C15
  teamsScoreTargetForecastRange: `D${scoreDisplayDataRowNum17}:H${scoreDisplayDataRowNum17}`,
  matchRunningStatusRange: `I${scoreDisplayDataRowNum17}:I${scoreDisplayDataRowNum17}`,
  liveScorecardRange1: `J${scoreDisplayDataRowNum17}:K${scoreDisplayDataRowNum17}`,
  liveScorecardRange2: `L${scoreDisplayDataRowNum17}`,
  liveScorecardRange3: `M${scoreDisplayDataRowNum17}`,
  resultRefreshCounterRange: `N${scoreDisplayDataRowNum17}`,//N15
  playersCountCell: `O${scoreDisplayDataRowNum17}`,
  flagsRange: `Q${scoreDisplayDataRowNum17}:S${scoreDisplayDataRowNum17}`,
  scoreDisplayDataRange: `C${scoreDisplayDataRowNum17}:M${scoreDisplayDataRowNum17}`,//used to clear
  potmProbableSmudgingListsRange: `I${smudgingPlayersListRowNum18}:M${smudgingPlayersListRowNum18}`,
  playingXIRange: `I${playingXIListRowNum19}:L${playingXIListRowNum19}`,
  playingXIRefreshCounterRange: `N${playingXIListRowNum19}`,
  impactPlayersRange: `I${impactPlayersListRowNum20}:L${impactPlayersListRowNum20}`,
  notInPlayingXIRange: `I${notInPlayingXIListRowNum21}:L${notInPlayingXIListRowNum21}`,
  closestMarginsRange: `G${closestMarginsRowNum23}:H${closestMarginsRowNum23}`,
  firstSecondBattingTeamsRange: `G${firstSecondBattingTeamsRowNum22}:H${firstSecondBattingTeamsRowNum22}`,
  //
  playingXITeam1Range: `J${playingXIListRowNum19}`,
  playingXITeam2Range: `K${playingXIListRowNum19}`,

  clearRanges: [
    `A${rowStartNum2}:M${betsLastRowNum15}`,
    `A${finalResultRowNum16}`,
    `D${finalResultRowNum16}:M${finalResultRowNum16}`,
    `A${scoreDisplayDataRowNum17}:Z${scoreDisplayDataRowNum17}`,
    `I${smudgingPlayersListRowNum18}:N${smudgingPlayersListRowNum18}`,
    `I${playingXIListRowNum19}:N${playingXIListRowNum19}`,
    `I${impactPlayersListRowNum20}:N${impactPlayersListRowNum20}`,
    `I${notInPlayingXIListRowNum21}:N${notInPlayingXIListRowNum21}`,
    `G${closestMarginsRowNum23}:H${closestMarginsRowNum23}`,
    `G${firstSecondBattingTeamsRowNum22}:H${firstSecondBattingTeamsRowNum22}`,
    `I${manualSmudgingBeginRowNum27}:M${manualSmudgingEndRowNum63}`,
  ],

  htmlDataTableRange: `A1:N${finalResultRowNum16}`,
}

//Players sheet object
const playersSheetObj = {
  randomNumCell: "V1",
  teamIDsRowNum: 1,

  minPlayersCountRequired: 12,//Check for min number of players

  teamColumnStartColName: "B",
  teamColumnEndColName: "U",

  playerCategories: {
    "source": { startRow: 2, endRow: 36 },
    "manual": { startRow: 37, endRow: 41 },
    "excluded": { startRow: 42, endRow: 46 },
  },

  get teamIDsRange() {
    return `${this.teamColumnStartColName}${this.teamIDsRowNum}:${this.teamColumnEndColName}${this.teamIDsRowNum}`
  },

  get sourcePlayersRange() {
    const sourceCategory = this.playerCategories["source"];
    return `${this.teamColumnStartColName}${sourceCategory.startRow}:${this.teamColumnEndColName}${sourceCategory.endRow}`;
  },

  get manualPlayersRange() {
    const manualCategory = this.playerCategories["manual"];
    return `${this.teamColumnStartColName}${manualCategory.startRow}:${this.teamColumnEndColName}${manualCategory.endRow}`;
  },

  get excludedPlayersRange() {
    const excludedCategory = this.playerCategories["excluded"];
    return `${this.teamColumnStartColName}${excludedCategory.startRow}:${this.teamColumnEndColName}${excludedCategory.endRow}`;
  },

  get clearRanges() {
    return [this.teamIDsRange, this.randomNumCell, this.sourcePlayersRange, this.manualPlayersRange, this.excludedPlayersRange];
  },
}

//Players Excluded Sheet Obj
const playersExcludedSheetObj = {
  autoBetsExcludedPlayersColName: "B",
  isDailyFormExcludedFlagColName: "C",

  get clearRanges() {
    return [getRangeByColNameNoHeader_(this.autoBetsExcludedPlayersColName, this.isDailyFormExcludedFlagColName),];
  },
}

//Will be used for Teams and TeamM sheet
const teamsSheetObj = {
  teamIDColName: "B",
  teamNameColName: "C",
  otherTeamNamesColName: "D",
}

//Schedule sheet obj
const scheduleTableSheetObj = {
  randomNumCell: "Q1",

  matchIDColName: "A",
  matchTitleColName: "B",
  matchShortDateColName: "C",
  teamAColName: "D",
  teamBColName: "E",
  scorecardURLColName: "F",
  stageColName: "G",
  venueColName: "H",
  startTimeLocalColName: "I",
  startTimeMSTColName: "J",
  startTimeISTColName: "K",
  matchStatusColName: "L",
  openedTimeColName: "M",
  closedTimeColName: "N",
  completedTimeColName: "O",
  reminderSentColName: "P",

  get matchIDColRange() { return getRangeByColNameNoHeader_(this.matchIDColName) },
  get matchDateRange() { return getRangeByColNameNoHeader_(this.matchShortDateColName) },

  get clearRanges() {
    return [getRangeByColNameNoHeader_(this.matchIDColName, this.reminderSentColName), this.randomNumCell];
  },
}

//Punter sheet cell refs
const puntersSheetObj = {
  get clearRanges() {
    return ["B2:E"];
  },

  nameColName: "B",
  nickNameColName: "C",
  emailColName: "D",
  isInactiveColName: "E",
  isReminderAllowedColName: "F",
  isAutoSubmitAllowedColName: "G",
  isBotColName: "H",
  isSameAsBotColName: "I",
}

//Auto Submission Log
const autoBetsLogSheetObj = {
  punterNameColName: "B",
  matchDateColName: "D",

  get clearRanges() { return [getRangeByColNameNoHeader_(this.punterNameColName, this.matchDateColName)] },
}

const resultsJournalSheetObj = {
  potmColName: "B",
  maxRunColName: "C",
  strikeRateColName: "D",
  maxWicketsColName: "E",
  economyColName: "F",

  runsMarginRange: [9, 35],
  wicketsMarginRange: [3, 7],
}

//TournamentMaster sheet cell refs
const seriesSheetObj = {
  idColName: 'A',
  nameColName: 'B',
  activeStatusColName: 'C',
  urlColName: 'D',
  hostCountryColName: 'E',
  matchCategoryColName: 'F',
  impactPlayerAllowedColName: 'G',

  activeStatusRange: "C2:C",
}

const countriesMasterSheetObj = {
  idColName: 'A',
  countryColName: 'B',
  timezoneColName: 'C',
}

////////////////////////////////////////////////////////

/**
 * Retrieves the active spreadsheet.
 * @return {GoogleAppsScript.Spreadsheet.Spreadsheet} The active spreadsheet.
 * @private
 */
function getActiveSpreadsheet_() {
  return SpreadsheetApp.getActiveSpreadsheet();
}

/**
 * Retrieves the sheet by its name from the specified or default spreadsheet.
 * 
 * @param {string} sheetName - The name of the sheet to retrieve.
 * @param {string|null} [spreadsheetId=null] - The ID of the spreadsheet to fetch the sheet from. If null, uses the default spreadsheet.
 * @returns {GoogleAppsScript.Spreadsheet.Sheet} The sheet with the specified name.
 */
function getSheetByName_(sheetName, spreadsheetId = null) {
  const spreadsheet = spreadsheetId ? SpreadsheetApp.openById(spreadsheetId) : getActiveSpreadsheet_();
  const sheet = spreadsheet.getSheetByName(sheetName);
  if (!sheet) {
    throw new Error(`Sheet with name "${sheetName}" not found.`);
  }
  return sheet;
}

/**
 * Retrieves the "Daily Predictions" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Daily Predictions" sheet.
 * @private
 */
function getDailyBetsSheet_() {
  return getSheetByName_(dailyBetsSheetName);
}

/**
 * Retrieves the "Daily Predictions Closed" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Daily Predictions Closed" sheet.
 * @private
 */
function getDailyBetsClosedSheet_() {
  return getSheetByName_(dailyBetsClosedSheetName);
}

/**
 * Retrieves the "Daily Predictions Backup" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Daily Predictions Backup" sheet.
 * @private
 */
function getDailyBetsBkpSheet_() {
  return getSheetByName_(dailyBetsBackupSheetName);
}

/**
 * Retrieves the "Overall Predictions" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Overall Predictions" sheet.
 * @private
 */
function getSeriesBetsSheet_() {
  return getSheetByName_(seriesBetsSheetName);
}

/**
 * Retrieves the "Overall Predictions Backup" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Overall Predictions Backup" sheet.
 * @private
 */
function getSeriesBetsBkpSheet_() {
  return getSheetByName_(seriesBetsBackupSheetName);
}

/**
 * Retrieves the "Dashboard" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Dashboard" sheet.
 * @private
 */
function getDashboardSheet_() {
  return getSheetByName_(homeSheetName);
}


function getStandingsTableSheet_() {
  return getSheetByName_(tableSheetName);
}

/**
 * Retrieves the "Daily Points" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Daily Points" sheet.
 * @private
 */
function getDailyPointsSheet_() {
  return getSheetByName_(dailyBetsPointsSheetName);
}

/**
 * Retrieves the "Team Results" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Team Results" sheet.
 * @private
 */
function getTeamResultsSheet_() {
  return getSheetByName_(dailyTeamResultsSheetName);
}

/**
 * Retrieves the "Overall Results" sheet.
 * @return {GoogleAppsScript.Spreadsheet.Sheet} The "Overall Results" sheet.
 * @private
 */
function getSeriesResultsSheet_() {
  return getSheetByName_(seriesBetsPointsSheetName);
}

/**
 * Returns the overall predictions modification sheet.
 * 
 * @returns {Object} The overall predictions modification sheet.
 */
function getOverallPredictionsModSheet_() {
  return getSheetByName_(seriesBetsModSheetName);
}

/**
 * Returns the overall predictions form sheet.
 * 
 * @returns {Object} The overall predictions form sheet.
 */
function getOverallPredictionsFormSheet_() {
  return getSheetByName_(seriesBetsFormSheetName);
}

/**
 * Returns the daily results sheet.
 * 
 * @returns {Object} The overall predictions form sheet.
 */
function getDailyResultsSheet_() {
  return getSheetByName_(dailyBetsResultsSheetName);
}

/**
 * Returns the players sheet.
 * 
 * @returns {Object} The players sheet.
 */
function getPlayersSheet_() {
  return getSheetByName_(playersSheetName);
}

function getPlayersExcludedSheet_() {
  return getSheetByName_(playersExcludedSheetName);
}

/**
 * Get the sheet containing the schedule data
 * @return {Sheet} - The sheet object containing schedule data
 */
function getScheduleSheet_() {
  return getSheetByName_(scheduleTableSheetName);
}

function getScheduleViewSheet_() {
  return getSheetByName_(scheduleViewSheetName);
}

/**
 * Get the sheet containing the punters data
 * @return {Sheet} - The sheet object containing punters data
 */
function getPuntersSheet_() {
  return getSheetByName_(puntersSheetName);
}

/**
 * Get the sheet containing the punters data
 * @return {Sheet} - The sheet object containing punters data
 */
function getAutoPicksLogSheet_() {
  return getSheetByName_(dailyAutoBetsLogSheetName);
}

function getTopBetsSheet_() {
  return getSheetByName_(dailyTopBetsSheetName);
}

/**
 * Get the sheet containing the master list of teams
 * @return {Sheet} - The sheet object containing master list of teams
 */
function getTeamsMasterSheet_() {
  return getSheetByName_(teamsSheetName);
}

/**
 * Get the sheet containing the master list of tournaments
 * @return {Sheet} - The sheet object containing master list of tournaments
 */
function getSeriesViewSheet_() {
  return getSheetByName_(seriesViewSheetName);
}

/**
 * Get the sheet containing the master list of tournaments
 * @return {Sheet} - The sheet object containing master list of tournaments
 */
function getSeriesTableSheet_() {
  return getSheetByName_(seriesTableSheetName);
}

/**
 * Get the sheet containing the past winners data
 * @return {Sheet} - The sheet object containing past winners data
 */
function getPastWinnersSheet_() {
  return getSheetByName_(winnersSheetName);
}

/**
 * Get the sheet containing the past winners data
 * @return {Sheet} - The sheet object containing past winners data
 */
function getResultsJournalSheet_() {
  return getSheetByName_(dailyBetsResultsJournalSheetName);
}

function getPredictoSheet_() {
  return getSheetByName_(predictoSheetName);
}

function getLogsSheet_() {
  return getSheetByName_(systemLogsSheetName);
}