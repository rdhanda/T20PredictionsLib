//ScheduleDataExtract.gs//

const SCHEDULE_MATCH_ROW_CSS = 'div.ds-p-4.hover\\:ds-bg-ui-fill-translucent';//<div class="ds-p-4 hover:ds-bg-ui-fill-translucent ds-border-t ds-border-line">
const MATCH_START_DATE_CSS = 'div.ds-text-compact-xs.ds-font-bold.ds-w-24';//<div class="ds-text-compact-xs ds-font-bold ds-w-24">Sat, 23 Mar '24</div>
const MATCH_START_TIME_CSS = 'div.ds-text-tight-m.ds-font-bold';//<div class="ds-text-tight-m ds-font-bold">4:00 AM</div>
const MATCH_TEAMS_NAMES_CSS = '.ci-team-score p';//<div class="ci-team-score ds-flex ds-justify-between ds-items-center ds-text-typo ds-my-1">
const MATCH_SCORECARD_URL_CSS_REGEX = /href="(.*?)"/;//<a href="/series/indian-premier-league-2024-1410320/punjab-kings-vs-delhi-capitals-2nd-match-1422120/live-cricket-score" class="ds-no-tap-higlight">
const MATCH_STAGE_VENUE_CSS = '.ds-text-tight-s.ds-font-regular.ds-truncate.ds-text-typo-mid3';//<div class="ds-text-tight-s ds-font-regular ds-truncate ds-text-typo-mid3">
const MATCH_TITLE_CSS = 'p.ds-text-tight-s.ds-font-medium.ds-line-clamp-2.ds-text-typo';
const MATCH_TITLE_ATTR = 'title';

/**
 * Extracts schedule details from HTML content.
 * 
 * @param {string} $ - The HTML content to extract details from.
 * @returns {Array} An array containing extracted schedule details.
 */
function extractScheduleDetails_($) {
  const allMatches = [];
  const localTimezone = getActiveSeriesTimezone_();
  const uniqueTeams = new Set(); // To store unique teams

  let previousMatchDate = null;

  $(SCHEDULE_MATCH_ROW_CSS).each((index, element) => {
    const matchID = index + 1;
    const matchDetails = extractMatchDetails_($, element, matchID, localTimezone, previousMatchDate);
    previousMatchDate = matchDetails.date;

    uniqueTeams.add(matchDetails.team1);
    uniqueTeams.add(matchDetails.team2);
    allMatches.push(matchDetails.details);
  });

  const teamIDs = Array.from(uniqueTeams).sort();
  return { allMatches, teamIDs };
}

/**
 * Extracts details of a single match.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @param {number} matchID - The ID of the match.
 * @param {string} localTimezone - The local timezone.
 * @param {string} previousMatchDate - The previous match temp start date.
 * @returns {Array} An array containing details of the match.
 */
function extractMatchDetails_($, element, matchID, localTimezone, previousMatchDate) {
  const teamNames = extractMatchTeamNames_($, element);
  let date = extractMatchStartDate_($, element);
  if (isEmpty_(date)) { date = previousMatchDate; }

  const time = extractMatchStartTime_($, element);
  const url = extractMatchUrl_($, element);
  const stageAndVenue = extractMatchStageAndVenue_($, element);
  const startTimeUTC = combineMatchDateAndTime_(date, time);

  const shortDate = determineMatchShortDate_(startTimeUTC, date, localTimezone, matchID);
  const matchTitle = constructMatchTitle_(matchID, shortDate, teamNames.team1, teamNames.team2, stageAndVenue.stage, stageAndVenue.venue);

  const startTimeLocal = startTimeUTC ? convertToTimezone_(startTimeUTC, localTimezone, SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) : '';
  const startTimeMST = startTimeUTC ? convertToMST_(startTimeUTC, SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) : '';
  const startTimeIST = startTimeUTC ? convertToIST_(startTimeUTC, SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) : '';

  const details = [matchID, matchTitle, shortDate, teamNames.team1, teamNames.team2, url, stageAndVenue.stage, stageAndVenue.venue, startTimeLocal, startTimeMST, startTimeIST];
  return { date, details, team1: teamNames.team1, team2: teamNames.team2 };
}

/**
 * Extracts team names from the match element.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @returns {Array} An array containing team names.
 */
function extractMatchTeamNames_($, element) {
  const teamNames = $(element).find(MATCH_TEAMS_NAMES_CSS).map((_, el) => $(el).text()).get();
  const team1 = getTeamIdByName_(teamNames[0].toString());
  const team2 = getTeamIdByName_(teamNames[1].toString());

  return { team1, team2 };
}

/**
 * Extracts date from the match element.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @returns {string} The extracted date.
 */
function extractMatchStartDate_($, element) {
  return $(element).find(MATCH_START_DATE_CSS).text().trim();
}

/**
 * Extracts match title from HTML element using Cheerio.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @returns {string} The extracted match title.
 */
function extractMatchTitleInfo_($, element) {
  const titleElement = $(element).find(MATCH_TITLE_CSS);
  return titleElement.attr(MATCH_TITLE_ATTR) || '';
}

/**
 * Extracts time from the match element.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @returns {string} The extracted time.
 */
function extractMatchStartTime_($, element) {
  const timeElement = $(element).find(MATCH_START_TIME_CSS).filter(function () {
    return $(this).text().trim().includes(':');
  }).first();
  return timeElement.text().trim();
}

/**
 * Extracts URL from the match element.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @returns {string} The extracted URL.
 */
function extractMatchUrl_($, element) {
  const urlRegex = MATCH_SCORECARD_URL_CSS_REGEX;
  const urlMatch = $(element).html().match(urlRegex);
  return urlMatch && urlMatch[1] ? urlMatch[1] : '';
}

/**
 * Extracts match stage and venue from match information.
 * 
 * @param {CheerioStatic} $ - Cheerio instance.
 * @param {Element} element - The HTML element of the match.
 * @returns {Object} An object containing stage and venue.
 */
function extractMatchStageAndVenue_($, element) {
  const matchInfo = $(element).find(MATCH_STAGE_VENUE_CSS).text().trim();
  const matchInfoRegex = /(.*?)\s*•\s*([^,]+)\s*,/;
  const matchStageAndVenue = matchInfo.match(matchInfoRegex);

  const stage = (matchStageAndVenue && matchStageAndVenue[1]) ? matchStageAndVenue[1].trim() : '';
  const venue = (matchStageAndVenue && matchStageAndVenue[2]) ? matchStageAndVenue[2].trim() : '';

  return { stage, venue };
}

/**
 * Combines date and time into a UTC datetime string.
 * 
 * @param {string} date - The date.
 * @param {string} time - The time.
 * @returns {string} The combined datetime string.
 */
function combineMatchDateAndTime_(date, time) {
  return date && time ? `${date} ${time}` : '';
}

/**
 * Determines the short date based on various factors.
 * 
 * @param {string} startTimeUTC - The UTC start time.
 * @param {string} date - The date.
 * @param {string} localTimezone - The local timezone.
 * @param {number} matchID - The ID of the match.
 * @returns {string} The determined short date.
 */
function determineMatchShortDate_(startTimeUTC, date, localTimezone, matchID) {
  let shortDate = '';
  if (startTimeUTC) {
    shortDate = convertToShortMatchDateFormatTZ_(startTimeUTC, localTimezone);
  } else if (date) {
    shortDate = convertToShortMatchDateFormatTZ_(date, localTimezone, SCHEDULE_SOURCE_INPUT_DATE_FORMAT);
  } else {
    log(`No match date found for Match ID ${matchID}.`, LogLevel.WARN);
  }
  return shortDate;
}

/**
 * Constructs a match title based on provided parameters.
 * 
 * @param {number} matchID - The ID of the match.
 * @param {string} shortDate - The short date of the match.
 * @param {string} team1 - The name of the first team.
 * @param {string} team2 - The name of the second team.
 * @param {string} stage - The stage of the match.
 * @param {string} venue - The venue of the match.
 * @returns {string} Returns the constructed match title.
 * @private
 */
function constructMatchTitle_(matchID, shortDate, team1, team2, stage, venue) {
  //stage = isPlayoff_(stage) ? ` - ${stage}` : '';//Ignore 1st Match, 28th Match, but keep Semi-Final, Eliminator, Final etc
  stage = ` - ${stage}`;
  venue = isEmpty_(venue) ? '' : ` at ${venue}`;
  return `[#${matchID}] ${shortDate}: ${team1} vs ${team2}${stage}${venue}`;
}

/**
 * Checks if the stage contains a playoff substring in a case-insensitive manner.
 * @param {string} stage - The stage to check.
 * @returns {boolean} True if the stage contains a playoff substring, false otherwise.
 */
function isPlayoff_(stage) {
  if (isEmpty_(stage)) {
    return false; // Return false if stage is null
  }
  const lowercaseStage = stage.toLowerCase(); // Convert stage to lowercase
  for (const playoff of PLAYOFFS_LIST) {
    const lowercasePlayoff = playoff.toLowerCase(); // Convert playoff to lowercase
    if (lowercaseStage.includes(lowercasePlayoff)) {
      return true;
    }
  }
  return false;
}

