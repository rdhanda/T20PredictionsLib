//////////////////////////////////////////////////////////////////////
//Moment API Spreadsheet Script - 
//https://docs.google.com/spreadsheets/d/1DPhG9NQKtUo19eOsrSGOpOaNBPetcrat1L4EXcYwkZA/edit#gid=0 (my own sheet)
//USE Deployed Version 2, DO NOT USE Development Mode/HEAD (will cause issues when used in another account/gmail id)
//HEAD version rerquires WRITE access, Deployed Version requires READ only version
//Moment Script ID 1e9QVCAfWc-IIUnyGyaIGksWmalLkeZENHiHE42hgcHBddb2IH6fdTvCa

const SCHEDULE_DISPLAY_DATE_FORMAT = "DD-MMM-YYYY hh:mm A";
const SCHEDULE_DISPLAY_LONG_DATE_FORMAT = "DD-MMM-YYYY hh:mm:ss A";

const SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT = "ddd, DD MMM 'YY h:mm A";
const SCHEDULE_SOURCE_INPUT_DATE_FORMAT = "ddd, DD MMM 'YY";

const LONG_TIMESTAMP_FORMAT = "DD-MMM-YYYYTHH:mm:ss";
const SCHEDULE_SHORT_DATE_FORMAT = "MMM-DD";//MUST Match Schedule sheet Column C format

const ICS_CALENDAR_DATE_INPUT_FORMAT = "YYYYMMDDTHHmmssZ";

const MST_TZ = "America/Edmonton";
const IST_TZ = "Asia/Kolkata";

/**
 * Checks if a given date is valid based on a specific format.
 * @param {string} aDate - The date to validate.
 * @returns {boolean} - Returns true if the date is valid, otherwise false.
 *
 * @example
 * var scheduleDateTimeFormat = "DD-MMM-YYYY hh:mm A";
 * var dateToCheck = "25-May-2023 10:30 AM";
 * var isValid = isValidDate_(dateToCheck); // Returns true
 */
function isValidDate_(aDate) {
  const momentObj = MomentAPI.moment(aDate, SCHEDULE_DISPLAY_DATE_FORMAT, true); // The "true" argument enables strict parsing
  return momentObj.isValid();
}

/**
 * 
 */
function isNotValidDate(aDate) {
  return !isValidDate_(aDate);
}

/**
 * Converts the given date to Mountain Standard Time (MST).
 *
 * @param {string} utcDateString - The UTC date string to convert.
 * @returns {string} The converted date in the format specified by scheduleDateTimeFormat.
 */
function convertToMST_(utcDateString, inputDateFormat = SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) {
  return convertToTimezone_(utcDateString, MST_TZ, inputDateFormat);
}

/**
 * Converts the given date to Indian Standard Time (IST).
 *
 * @param {string} utcDateString - The UTC date string to convert.
 * @returns {string} The converted date in the format specified by scheduleDateTimeFormat.
 */
function convertToIST_(utcDateString, inputDateFormat = SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) {
  return convertToTimezone_(utcDateString, IST_TZ, inputDateFormat);
}

/**
 * Converts the given date to the specified timezone.
 *
 * @param {string} utcDateString - The UTC date string to convert.
 * @param {string} tz - The timezone to convert the date to.
 * @returns {string} The converted date in the format specified by scheduleDateTimeFormat.
 */
function convertToTimezone_(utcDateString, tz, inputDateFormat = SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) {
  return MomentAPI.moment.utc(utcDateString, inputDateFormat).tz(tz).format(SCHEDULE_DISPLAY_DATE_FORMAT);
}

/**
 * Converts the given date to a short match date format and returns it as a string.
 *
 * @param {Date} date - The date to be converted.
 * @returns {string} The converted date in short match format (in uppercase).
 */
function toShortMatchDateFormat_(date) {
  return MomentAPI.moment(date).format(SCHEDULE_SHORT_DATE_FORMAT).toString().toUpperCase();
}

/**
 * Converts a UTC date to a short match date format in the timezone.
 * @param {string} dateUTC - The UTC date.
 * @param {string} timezone - The timezone.
 * @returns {string} The formatted date string in the timezone.
 */
function convertToShortMatchDateFormatTZ_(dateUTC, timezone, inputDateFormat = SCHEDULE_SOURCE_INPUT_DATETIME_FORMAT) {
  // Convert the UTC start time to the local timezone and format it to short date format
  return MomentAPI.moment.utc(dateUTC, inputDateFormat)
    .tz(timezone)
    .format(SCHEDULE_SHORT_DATE_FORMAT)
    .toUpperCase();
}

/**
 * Converts the given date to the date time format used by the scheduler.
 *
 * @param {Date} date - The date to format.
 * @returns {string} The formatted date in the format specified by scheduleDateTimeFormat.
 */
function toScheduleDateTimeFormat_(date) {
  return MomentAPI.moment(date).format(SCHEDULE_DISPLAY_DATE_FORMAT);
}

/**
 * Converts the given date to the long timestamp format.
 *
 * @param {Date} date - The date to format.
 * @returns {string} The formatted date in the format specified by longTimestampFormat.
 */
function toLongTimestampFormat_(date) {
  return MomentAPI.moment(date).format(LONG_TIMESTAMP_FORMAT);
}

/**
 * Gets the current formatted long timestamp.
 *
 * @returns {string} The current formatted date in the format specified by longTimestampFormat.
 */
function getCurrentLongTimesptamp_() {
  return toLongTimestampFormat_(new Date());
}

/**
 * Gets the current formatted timestamp.
 *
 * @returns {string} The current formatted date in the format specified by scheduleDateTimeFormat.
 */
function getCurrentTimesptamp_() {
  return MomentAPI.moment().format(SCHEDULE_DISPLAY_LONG_DATE_FORMAT);
}

/**
 * Adds the specified number of minutes to the given time.
 *
 * @param {Date} aTime - The time to add minutes to.
 * @param {number} minutesToadd - The number of minutes to add.
 * @returns {Date} The resulting time after adding the specified minutes.
 */
function addMinutes_(aTime, minutesToadd) {
  return MomentAPI.moment(aTime).add(minutesToadd, "minutes").toDate();
}

/**
 * Adds the specified number of days to the given time.
 *
 * @param {Date} aTime - The time to add days to.
 * @param {number} daysToadd - The number of days to add.
 * @returns {Date} The resulting time after adding the specified days.
 */
function addDays_(aTime, daysToadd) {
  return MomentAPI.moment(aTime).add(daysToadd, "days");
}

/**
 * Determines if the given time is before the current time.
 *
 * @param {Date} aTime - The time to compare.
 * @returns {boolean} Whether the given time is before the current time.
 */
function isBeforeCurrentTime_(aTime) { 
  if (!aTime) {
    log(`Invalid Time Provided '${aTime}'`, LogLevel.WARN);
    return;
  }

  if (typeof aTime === "string") {
    aTime = toDate_(aTime);
  }
  return MomentAPI.moment(aTime).isBefore(MomentAPI.moment());
}

/**
 * Determines if the given time is after the current time.
 *
 * @param {Date} aTime - The time to compare.
 * @returns {boolean} Whether the given time is after the current time.
 */
function isAfterCurrentTime_(aTime) {
  if (!aTime) {
    log(`Invalid Time Provided '${aTime}'`, LogLevel.WARN);
    return;
  }

  if (typeof aTime === "string") {
    aTime = toDate_(aTime);
  }
  return MomentAPI.moment(aTime).isAfter(MomentAPI.moment());
}

/**
 * Converts a date string to a moment object.
 *
 * @param {string} aDateString - The date string to convert.
 * @returns {Object} - The moment object representing the date.
 */
function toDate_(aDateString) {
  var momentObj = MomentAPI.moment(aDateString);

  // Check if the moment object is valid
  if (momentObj.isValid()) {
    return momentObj;
  } else {
    // If the moment object is invalid, try parsing with the specified format
    return MomentAPI.moment(aDateString, SCHEDULE_DISPLAY_DATE_FORMAT);
  }
}

/**
 * Determines if the current time is between the given start and end times.
 *
 * @param {Date} startTime - The start time to compare.
 * @param {Date} endTime - The end time to compare.
 * @returns {boolean} Whether the current time is between the given start and end
 */
function isBetweenTimes_(startTime, endTime) {
  return MomentAPI.moment().isBetween(startTime, endTime);
}

/**
 * Calculates the time difference in minutes between two moments.
 * 
 * @param {Moment} timeA - The first moment to compare.
 * @param {Moment} timeB - The second moment to compare.
 * @returns {number|undefined} - The time difference in minutes if both moments are not empty, otherwise undefined.
 */
function getTimeDiffereneceInMinutes_(timeA, timeB) {
  if (isNotEmpty_(timeA) && isNotEmpty_(timeB)) {
    return timeA.diff(timeB, "minutes");
  }
}

/**
 * Returns the score timestamp value.
 * @returns {string} The score timestamp value with the timestamp in long format followed by 'MST'.
 */
function getScoreUpdatedTimestamp_() {
  return `Last Updated: \n${getCurrentTimestampTimeOnly_()}`;
}

/**
 * Returns the current timestamp (time only, e.g. 10:19:01) as a string.
 * @returns {string|null} The current timestamp, or null if it couldn't be extracted.
 */
function getCurrentTimestampTimeOnly_() {
  const dateString = getCurrentLongTimesptamp_().toString();
  return dateString.includes('T') ? dateString.split('T')[1] + ' MST' : '';
}
