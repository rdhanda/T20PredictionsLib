/**
 * Adds all the menus for setup, forms, and results to the UI.
 * 
 */
function addAllMenus_() {
  const ui = SpreadsheetApp.getUi();

  addSetupMenu_(ui);
  addBetsMenu_(ui);
  addResultsMenu_(ui);
}

/**
 * Adds an item to a menu with a specified label and function.
 * 
 * @param {GoogleAppsScript.Base.Menu} menu - The menu to which the item will be added.
 * @param {string} label - The label of the menu item.
 * @param {Function} func - The function associated with the menu item.
 */
function addMenuItem_(menu, label, func) {
  const libraryID = getLibraryID_();
  menu.addItem(label, libraryID + func.name);
}

/**
 * Adds the setup menu and its submenus to the UI.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance to which the setup menu will be added.
 */
function addSetupMenu_(ui) {
  const setupMenu = ui.createMenu("🏏 Setup");

  const seriesSubMenu = getSeriesSubMenu(ui);
  setupMenu.addSubMenu(seriesSubMenu);
  setupMenu.addSeparator();

  const sheetsSubMenu = getSheetsSubMenu_(ui);
  setupMenu.addSubMenu(sheetsSubMenu);
  setupMenu.addSeparator();

  addMenuItem_(setupMenu, "👥 Update Players", updatePlayersMenu);
  addMenuItem_(setupMenu, "🗓️ Update Schedule", updateScheduleSheetMenu);
  setupMenu.addSeparator();

  if (isNotProdEnv_()) {
    addMenuItem_(setupMenu, "🔄 Refresh Data from PROD", refreshDataFromProdMenu);
  }

  addMenuItem_(setupMenu, "🗑️ Remove All Cached Data", removeAllCachedDataMenu);
  setupMenu.addSeparator();

  addMenuItem_(setupMenu, `❌ Empty ${systemLogsSheetName} Sheet`, emptyLogsSheetMenu);
  setupMenu.addSeparator();

  addMenuItem_(setupMenu, "⚙️ Display Config Info", displayConfigInfoMenu);
  addMenuItem_(setupMenu, "⚙️ Display Env Setup Info", displayEnvInfoMenu);
  setupMenu.addSeparator();

  addMenuItem_(setupMenu, "🕑 Create All Triggers", createTriggersMenu);
  addMenuItem_(setupMenu, "❌ Delete All Triggers", deleteAllTriggersMenu);

  setupMenu.addToUi();
}

/**
 * Gets the Series submenu.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance.
 * @returns {GoogleAppsScript.Base.Menu} - The tournament submenu.
 */
function getSeriesSubMenu(ui) {
  const subMenu = ui.createMenu("🏆 Series");

  addMenuItem_(subMenu, "🆕 Setup a new Series", setupNewSeriesMenu);
  addMenuItem_(subMenu, "💾 Backup & Reset Current Series", backupAndResetSeriesSheetsMenu);
  addMenuItem_(subMenu, "🗑️ Clear Current Series", clearSeriesMenu);
  addMenuItem_(subMenu, "🔄 Reset Series Info", resetActiveSeriesInfoMenu);

  return subMenu;
}

/**
 * Gets the sheets submenu.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance.
 * @returns {GoogleAppsScript.Base.Menu} - The sheets submenu.
 */
function getSheetsSubMenu_(ui) {
  const subMenu = ui.createMenu("📑 Sheets");
  addMenuItem_(subMenu, "📄 Show All Sheets", showAllSheetsMenu);
  addMenuItem_(subMenu, "🔄 Reset All Sheets Settings", resetAllSheetsSettingsMenu);
  subMenu.addSeparator();

  if (isNotTestEnv_()) {
    addMenuItem_(subMenu, "📑 Promote Sheets from TEST", promoteSheetsAllMenu);
    subMenu.addSeparator();
  }
  addMenuItem_(subMenu, "📑 Replace Match Sheets with 1", replaceMatchSheetsWithFirstMenu);
  addMenuItem_(subMenu, "📑 Replace Match Sheets with T", replaceMatchSheetsWithTemplateMenu);

  return subMenu;
}

/**
 * Adds the forms menu and its submenus to the UI.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance to which the forms menu will be added.
 */
function addBetsMenu_(ui) {
  const betsMenu = ui.createMenu("🎰 Bets");

  const dailyBetsSubMenu = getDailyBetsFormSubMenu_(ui);
  betsMenu.addSubMenu(dailyBetsSubMenu);
  betsMenu.addSeparator();

  const seriesBetsSubMenu = getSeriesBetsFormSubMenu_(ui);
  betsMenu.addSubMenu(seriesBetsSubMenu);
  betsMenu.addSeparator();

  addMenuItem_(betsMenu, "🟢 Import By Open Matches", importByOpenMatchesMenu);
  addMenuItem_(betsMenu, "🔒 Import By Last Closed Matches", importByLastClosedMatchesMenu);
  addMenuItem_(betsMenu, "🆔 Import By Match ID", importByMatchIdMenu);
  betsMenu.addSeparator();

  addMenuItem_(betsMenu, "🟢/🔒 Toggle Bets", toggleBetsMenu);
  addMenuItem_(betsMenu, "🤖 Auto Submit Bets", autoSubmitBetsMenu);
  addMenuItem_(betsMenu, "🔄 Update Daily Bets Submissions", updateDashboardBetsListsMenu);
  addMenuItem_(betsMenu, "🔄 Update Top Bets", updateTopBetsSheetMenu);
  betsMenu.addSeparator();

  addMenuItem_(betsMenu, "🔄 Update Predicto Sheet", updatePredictoSheetMenu);

  betsMenu.addToUi();
}

/**
 * Gets the daily predictions submenu.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance.
 * @returns {GoogleAppsScript.Base.Menu} - The daily predictions submenu.
 */
function getDailyBetsFormSubMenu_(ui) {
  const subMenu = ui.createMenu("📝 Daily Bets Form");

  addMenuItem_(subMenu, "🔄 Update Form", updateDailyBetsFormMenu);
  subMenu.addSeparator();

  addMenuItem_(subMenu, "🔎 Check Status", checkDailyBetsFormStatusMenu);
  addMenuItem_(subMenu, "🟢 Open Form - Start Accepting responses", startAcceptingDailyBetsMenu);
  addMenuItem_(subMenu, "🔒 Close Form - Stop Accepting responses", stopAcceptingDailyBetsMenu);
  subMenu.addSeparator();

  addMenuItem_(subMenu, "🔖 Update Title & Description", updateDailyBetsFormTitleDescMenu);
  addMenuItem_(subMenu, "👥 Update Punters", updateDailyBetsFormPuntersMenu);
  addMenuItem_(subMenu, "🔄 Update Match Questions", updateDailyBetsFormMatchQuestionsMenu);
  subMenu.addSeparator();

  addMenuItem_(subMenu, "🔄 Reset Form", resetDailyBetsFormMenu);

  return subMenu;
}

/**
 * Gets the Series Bets submenu.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance.
 * @returns {GoogleAppsScript.Base.Menu} - The Series bets submenu.
 */
function getSeriesBetsFormSubMenu_(ui) {
  const subMenu = ui.createMenu("📊 Series Bets Form");
  addMenuItem_(subMenu, "🔄 Update Form", updateSeriesBetsFormMenu);
  addMenuItem_(subMenu, "🔄 Recreate Form Questions", recreateSeriesBetsFormQuestionsMenu);
  subMenu.addSeparator();

  addMenuItem_(subMenu, "🔎 Check Status", checkSeriesBetsFormStatusMenu);
  addMenuItem_(subMenu, "🟢 Open Form - Start Accepting responses", startAcceptingSeriesBetsMenu);
  addMenuItem_(subMenu, "🔒 Close Form - Stop Accepting responses", stopAcceptingSeriesBetsMenu);
  subMenu.addSeparator();

  addMenuItem_(subMenu, "👥 Update Punters", updateSeriesBetsFormPuntersMenu);
  addMenuItem_(subMenu, "🔄 Update Question Titles", updateSeriesBetsFormQuestionTitlesMenu);
  addMenuItem_(subMenu, "👥 Update Teams", updateSeriesBetsFormTeamsMenu);
  subMenu.addSeparator();

  addMenuItem_(subMenu, "📝 Reset Form", resetSeriesBetsFormMenu);

  return subMenu;
}

/**
 * Adds the results menu and its submenus to the UI.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance to which the results menu will be added.
 */
function addResultsMenu_(ui) {
  const resultsMenu = ui.createMenu("🏆 Results");

  addMenuItem_(resultsMenu, "📋 Copy Match Results", copyAllMatchResultsMenu);
  addMenuItem_(resultsMenu, "📊 Update Match Results", updateAllMatchResultsMenu);
  addMenuItem_(resultsMenu, "🔃 Force Refresh Active Match Sheet Results", forceRefreshActiveMatchResultsMenu);
  resultsMenu.addSeparator();

  addMenuItem_(resultsMenu, "🔄 Refresh Series Results", updateSeriesResultsMenu);
  addMenuItem_(resultsMenu, "📊 Update Player Stats", updatePlayerStatsMenu);
  addMenuItem_(resultsMenu, "📊 Update Standings Table", updateStandingsTableMenu);
  resultsMenu.addSeparator();

  addMenuItem_(resultsMenu, "🗑️ Clear All Match Sheets", clearAllMatchSheetsMenu);
  addMenuItem_(resultsMenu, "🗑️ Clear Active Match Sheet", clearActiveMatchSheetsMenu); resultsMenu.addSeparator();
  resultsMenu.addSeparator();

  addMenuItem_(resultsMenu, "🚮 Delete Results of Recent Match ID", deleteResultsByTopMostMatchIDMenu);
  addMenuItem_(resultsMenu, "🚮 Delete Results by Match ID", deleteResultsByMatchIDMenu);

  resultsMenu.addToUi();
}

/**
 * Gets the delete daily results submenu.
 * 
 * @param {GoogleAppsScript.Base.Ui} ui - The UI instance.
 * @returns {GoogleAppsScript.Base.Menu} - The delete daily results submenu.
 */
function getDeleteDailyResultsSubMenu_(ui) {
  const subMenu = ui.createMenu("❌ Delete DailyResults");

  addMenuItem_(subMenu, "🚮 Delete Results of Recent Match ID", deleteResultsByTopMostMatchIDMenu);
  addMenuItem_(subMenu, "🚮 Delete Results by Match ID", deleteResultsByMatchIDMenu);

  return subMenu;
}
