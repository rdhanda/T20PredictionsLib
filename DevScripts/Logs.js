//Logs.gs//

/**
 * Empties all system logs from the logs sheet.
 * This function serves as a menu item handler.
 * 
 * @param {Object} e - The event object containing information about the menu item action.
 */
function emptyLogsSheetMenu(e) {
  const sheet = getLogsSheet_();
  const wasHidden = sheet.isSheetHidden();
  activateSheet_(sheet);

  flushAndWait_();

  let ui = SpreadsheetApp.getUi();

  let result = ui.alert(
    "Empty All System Logs:",
    "Are you sure you want to delete all logs? This action cannot be undone.",
    ui.ButtonSet.OK_CANCEL);

  if (result == ui.Button.OK) {
    emptyLogsSheet_(e);
  }

  if (wasHidden) {
    sheet.hideSheet();
  }
}

/**
 * Empties all system logs from the logs sheet.
 * 
 * @param {Object} e - The event object containing information about the trigger.
 */
function emptyLogsSheet_(e) {
  const sheet = getLogsSheet_();

  deleteRows_(sheet, 1, 10);
  sheet.setTabColor(null);
  //const range = sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns());
  //range.setFontColor(null);

  sheet.hideSheet();
}

/**
 * Inserts a log row into the logs sheet.
 * If the log level is ERROR, it highlights the sheet tab in red and makes it visible.
 * 
 * @param {Object} row - The log row object containing log details.
 * @param {string} row.level - The log level (e.g., 'INFO', 'WARN', 'ERROR').
 * @param {string} row.message - The log message.
 * @param {string} row.stack - The stack trace or additional log details.
 */
function insertLogRow_(row) {
  console.time(insertLogRow_.name);

  const sheet = getLogsSheet_();
  const rowValues = [new Date(), row.level, row.message, row.stack];

  // Insert rows into the sheet
  sheet.insertRows(1, 1);
  const range = sheet.getRange(1, 1, 1, rowValues.length);
  range.setValues([rowValues]);

  const hasErrorLevel = row.level == LogLevel.ERROR.text;
  if (hasErrorLevel) {
    sheet.setTabColor("#FF0000");//red color
    sheet.showSheet();
    //range.setFontColor('#FF0000');
  } else {
    //range.setFontColor(null);
  }

  console.timeEnd(insertLogRow_.name);
}
