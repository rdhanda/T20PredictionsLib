// DAILY RESULTS //

/**
 * Copy score data from Match sheet 1 into DailyScores sheet
 */
function copyAllMatchResultsMenu(e) {
  for (const match of MatchSheetConfigs) {
    copyAllMatchResults_(e, getSheetByName_(match.sheetName));
  }
}

/**
 * 
 */
function copyAllMatchResults_(e, matchSheet) {
  const done = copyMatchResult_(e, matchSheet);
  if (done == MatchResultStatus.DONE) {
    activateSheet_(getDashboardSheet_());
  }
}

/**
 * Copys the scores for a match into DailyScore sheet from the given Match sheet
 */
function copyMatchResult_(e, matchSheet) {
  console.time(copyMatchResult_.name);

  let predictionsToCopyNum = findActualPredictionsNum_(matchSheet);

  if (predictionsToCopyNum > 0 && isResultDecided_(matchSheet)) {
    let predictionsData = matchSheet.getRange(rowStartNum2, columnStartNum, predictionsToCopyNum, columns2CopyForScore).getValues();
    if (predictionsData.length > 0 && isNotEmpty_(predictionsData[0][0])) {
      activateSheet_(getDailyResultsSheet_());
      let matchID = parseInt(predictionsData[0][0]);
      if (isMatchResultNotRecorded_(matchID)) {
        copyToDailyResultsSheet_(matchSheet, predictionsToCopyNum, predictionsData);
        flushAndWait_();
        doAfterCopyingResults_(e, matchSheet, matchID);
        log(`[${matchSheet.getName()}] Scores succesfully copied."`, LogLevel.DEBUG);
        flushAndWait_();
        logEND_(copyMatchResult_.name);
        return MatchResultStatus.DONE;
      }
    }
  }
  
  console.timeEnd(copyMatchResult_.name);
}

/**
 * 
 */
function copyToDailyResultsSheet_(matchSheet, predictionsToCopyNum, predictionsData) {
  sortPredictionsData_(predictionsData);

  let resultsData = matchSheet.getRange(finalResultRowNum16, columnStartNum, 1, columns2CopyForScore).getValues();
  resultsData[0][colNameToIndex_(matchSheetObj.resultStatusColName)] = "-";

  const dailyResultsSheet = getDailyResultsSheet_();
  dailyResultsSheet.insertRowsBefore(rowStartNum2, predictionsToCopyNum + 1);//Insert for all enteries + one more for results
  //First copy Predictions data
  dailyResultsSheet.getRange(rowStartNum2, columnStartNum, predictionsToCopyNum, columns2CopyForScore).setValues(predictionsData);
  //Second copy results row
  dailyResultsSheet.getRange((rowStartNum2 + predictionsToCopyNum), columnStartNum, 1, columns2CopyForScore).setValues(resultsData);

  //Copy the formulas for Punters points
  //dailyResultsSheet.getRange(rowStartNum2, columnNumForPoints, predictionsToCopyNum, 1).setFormulasR1C1(matchSheet.getRange(rowStartNum2, columnNumForPoints, predictionsToCopyNum, 1).getFormulasR1C1());

  //Set column as Text format instead of automatic (number and string). The default does not work with QUERY function on TeamResults/Dashboard sheets
  dailyResultsSheet.getRange(dailyResultsSheetObj.entriesIDsColumRange).setNumberFormat('@STRING@');
}

/**
 * Sorts the given predictions data by points in descending order, and then by punter name in ascending order.
 * 
 * @param {Array} predictionsData - The array of predictions data to sort.
 * @returns {Array} The sorted predictions data array.
 */
function sortPredictionsData_(predictionsData) {
  const pointsIdx = columnNumForPoints - 1;
  const nameIndex = colNameToIndex_(matchSheetObj.punterNameColName);

  predictionsData.sort((a, b) => {
    if (a[pointsIdx] !== b[pointsIdx]) {
      return b[pointsIdx] - a[pointsIdx];
    } else {
      return a[nameIndex].localeCompare(b[nameIndex]);
    }
  });

  return predictionsData;
}

/**
 * 
 */
function doAfterCopyingResults_(e, matchSheet, matchID) {
  //Match sheet, set score copied status
  setMatchResultStatus_(matchSheet, MatchResultStatus.DONE);

  //Match completion steps
  doAfterMatchCompletion_(e, matchSheet, matchID);

  //Delete any empty rows
  deleteEmptyRows_(getDailyResultsSheet_(), dailyResultsSheetObj.emptyRowsToKeep);

  //Update punters top choices
  updateTopBetsSheet_(e);

  //Reset cache
  removeDailyResultsSheetCachedData_();
}

/**
 * Get missing result for questions
 */
function getMissingResultQuestions_(sheet) {
  let arr = [];
  if (!isTossDecided_(sheet)) {
    arr.push("Toss");
  }
  if (!isFirstInningsScoreDecided_(sheet)) {
    arr.push("First Inning Score");
  }
  if (!isWinnerDecided_(sheet)) {
    arr.push("Win");
  }
  if (!isMarginDecided_(sheet)) {
    arr.push("Runs OR Wickets Margin");
  }
  if (!isPOTMDecided_(sheet)) {
    arr.push("POTM");
  }
  if (!isMaxRunsDecided_(sheet)) {
    arr.push("Max Runs");
  }
  if (!isStrikeRateDecided_(sheet)) {
    arr.push("S/R");
  }
  if (!isMaxWicketsDecided_(sheet)) {
    arr.push("Max Wickets");
  }
  if (!isEconomyDecided_(sheet)) {
    arr.push("Economy");
  }
  if (!isMarginPointsAllocated_(sheet)) {
    arr.push("Margin Points Allocated to Punters. Try 'Calculate Match Margin Points' from Menu");
  }
  return arr;
}

/**
 * Finds the actual number of Predictions 
 */
function findActualPredictionsNum_(sheet) {
  //Get data only for Predictions IDs, in Column 2
  let data = sheet.getRange(rowStartNum2, columnStartNum + 1, maxPuntersNum14, 1).getValues();
  let count = data.filter(function (e) { return e[0] != ""; }).length;
  return count;
}


///////////////////////////DELETE RESULTS //////////////////////////
/**
 * Deletes the rows from the responses sheet
 */
function deleteResultsByTopMostMatchIDMenu() {
  //Get top most match ID
  activateSheet_(getDailyResultsSheet_());
  let matchID = getDailyResultsSheet_().getRange(dailyResultsSheetObj.topMatchIDCell).getValue();//Row number is 2
  deleteDailyResults_(matchID);
}

/**
 * Deletes the scores rows from the DailyScores sheet by match ID 
 */
function deleteResultsByMatchIDMenu() {
  let ui = SpreadsheetApp.getUi();
  let result = ui.prompt(
    "Delete Score for Match ID:",
    "Enter the Match ID to delete scores data in [" + dailyBetsResultsSheetName + "] sheet",
    ui.ButtonSet.OK_CANCEL);

  let button = result.getSelectedButton();
  if (button == ui.Button.OK) {
    let matchID = result.getResponseText();
    activateSheet_(getDailyResultsSheet_());
    deleteDailyResults_(matchID);
  }
}

/**
 * Starting row num
 * Last row num
 * Rows to delete
 * Range define
 * 
 */
function deleteDailyResults_(matchID) {
  if (isInvalidNumber_(matchID) || isMatchResultNotRecorded_(matchID)) {
    showWarningAlert_(`Match ID '${matchID}' NOT found in the [${dailyBetsResultsSheetName}] sheet.`);
    return;
  }

  const sheet = getDailyResultsSheet_();
  let firstRowMatchID = findRowOfFirstCell_(matchID, sheet.getRange(dailyResultsSheetObj.matchIDRange));
  let lastRowMatchID = findRowOfLastCell_(matchID, sheet.getRange(dailyResultsSheetObj.matchIDRange));
  if (sheet.getLastRow() === lastRowMatchID) {
    addEmptyRowsToBottom_(sheet, 2);
  }
  let rows2Delete = (lastRowMatchID - firstRowMatchID + 1);
  let range = dailyResultsSheetObj.matchIDColName + (firstRowMatchID) + ":" + dailyResultsSheetObj.matchIDColName + (lastRowMatchID);

  let sameValue = isRangeValuesIdentical_(sheet.getRange(range));
  if (!sameValue) {
    showErrorAlert_(`Scores for Match ID in the [${dailyBetsResultsSheetName}] are NOT in order. Check the data and try again.`);
  } else {
    let result = SpreadsheetApp.getUi().alert("⚠️ Delete", "You're about to delete ALL scores for Match ID " + matchID + " in [" + dailyBetsResultsSheetName + "] sheet. \n\nDo you want to continue?", SpreadsheetApp.getUi().ButtonSet.YES_NO);
    // Process the user's response.
    if (result === SpreadsheetApp.getUi().Button.YES) {
      sheet.deleteRows((firstRowMatchID), rows2Delete);
      //Set column as Text format instead of automatic (number and string). The default does not work with QUERY function on TeamResults/Dashboard sheets
      sheet.getRange(dailyResultsSheetObj.entriesIDsColumRange).setNumberFormat('@STRING@');

      removeDailyResultsSheetCachedData_();
    }
  }
}

/**
 * Retrieves the counts of wins for each team based on daily results.
 * @param {any} e - The event object.
 * @returns {Object} An object containing team names and their respective win counts.
 */
function getTeamWinCounts_(e) {
  const data = getDailyResultsSheetCachedData_();
  const teamCounts = {};

  const entryInfoColIndex = colNameToIndex_(dailyResultsSheetObj.entriesInfoColName);
  const teamWinColIndex = colNameToIndex_(dailyResultsSheetObj.teamWinColName);

  data
    .filter(row => row[entryInfoColIndex] == RESULT)
    .forEach(row => teamCounts[row[teamWinColIndex]] = (teamCounts[row[teamWinColIndex]] || 0) + 1);

  return teamCounts;
}
