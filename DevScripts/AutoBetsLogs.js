//AutoBetsLogs.gs//

class AutoBetsLog {
  constructor(punterName, usedCount) {
    this._punterName = punterName;
    this._usedCount = usedCount;
  }

  // Getter for punterName
  get punterName() { return this._punterName; }

  // Setter for punterName
  set punterName(punterName) { this._punterName = punterName; }

  // Getter for usedCount
  get usedCount() { return this._usedCount; }

  // Setter for usedCount
  set usedCount(usedCount) { this._usedCount = usedCount; }
}

/**
 * Cache for Auto Bets Logs
 */
const autoBetsLogsCache_ = {
  autoBets: null,
};

/**
 * Retrieves information about auto picks logs, counting unique entries per punter for each match date.
 * 
 * @returns {Array<{ name: string, count: number }>} Array of objects containing punter name and unique entry count.
 */
function getAutoBetsLogsCache_() {
  if (autoBetsLogsCache_.autoBets == null) {
    refreshAutoBetsLogsCache_();
  }

  return autoBetsLogsCache_.autoBets;
}

function refreshAutoBetsLogsCache_() {
  const data = getDataFromSheet_(dailyAutoBetsLogSheetName, true); // Excluding header row

  const nameColumnIndex = colNameToIndex_(autoBetsLogSheetObj.punterNameColName);
  const dateColumnIndex = colNameToIndex_(autoBetsLogSheetObj.matchDateColName);

  const punterEntries = {};

  data.forEach(row => {
    const name = row[nameColumnIndex]; // Adjusting column index to array index
    const date = row[dateColumnIndex]; // Adjusting column index to array index

    punterEntries[name] = punterEntries[name] || {};
    punterEntries[name][date] = true;
  });

  // Assuming punterEntries is your object containing entries
  autoBetsLogsCache_.autoBets = Object.entries(punterEntries)
    .map(([punterName, entries]) => ({
      punterName,
      usedCount: Object.keys(entries).length,
    }))
    .sort((a, b) => a.punterName.localeCompare(b.punterName));
}

/**
 * Checks if a punter has reached the auto-submit limit.
 * @param {string} punterName - The name of the punter.
 * @returns {boolean} True if the punter has reached the auto-submit limit, otherwise false.
 */
function hasPunterReachedAutoSubmitLimit_(punterName) {
  const { limit, usedCount } = getAutoSubmitCountStats_(punterName);
  return usedCount >= limit;
}

/**
 * Retrieves the auto-submit count statistics for a punter.
 * @param {string} punterName - The name of the punter.
 * @returns {Object} An object containing the used count, remaining count, and limit.
 */
function getAutoSubmitCountStats_(punterName) {
  const punters = getAutoBetsLogsCache_();
  const punter = punters.find(item => item.punterName === punterName);
  const usedCount = punter ? punter.usedCount : 0;
  const puntersLimit = ConfigManager.getConfigClass().AUTO_SUBMIT_BETS_LIMIT_PER_PUNTER;
  const limit = PuntersUtils.isPunterBot_(punterName) ? Infinity : puntersLimit;
  const remainingCount = PuntersUtils.isPunterBot_(punterName) ? Infinity : (puntersLimit - usedCount);

  return { limit, usedCount, remainingCount, };
}
