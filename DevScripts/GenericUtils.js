//GenericUtlities.gs//

/**
 * Logs a message with the specified log level.
 * @param {*} message The message to be logged. If it's an Error object, its stack trace will be used.
 * @param {LogLevel} [level=LogLevel.DEBUG] The log level of the message.
 */
function log(message, level = LogLevel.DEBUG) {
  toast_("Logs", message);

  if (level.value >= EnvConfigClass.getEnvCurrentLogLevel().value) {
    const isError = message instanceof Error;
    level = isError ? LogLevel.ERROR : level;

    const errorMessage = isError ? message.message : message;
    const stackTrace = (() => {
      const sliceIndex = isError ? 1 : 3;
      return `\n>>at:` + (isError ? message.stack : new Error().stack).split('\n').slice(sliceIndex, -1).map(line => line.replace(/\s+at\s+/, '')).join(', ');
    })();

    const logFunction = getLogFunction(level);
    logFunction(`${level.text}:`, errorMessage, stackTrace);

    appendLog_(level, errorMessage, stackTrace);
  }
}

/**
 * Retrieves the appropriate logging function based on the provided log level.
 * 
 * @param {LogLevel} level - The log level.
 * @returns {function} The logging function corresponding to the log level.
 */
function getLogFunction(level) {
  switch (level) {
    case LogLevel.DEBUG:
      return console.log;
    case LogLevel.INFO:
      return console.info;
    case LogLevel.WARN:
      return console.warn;
    case LogLevel.ERROR:
      return console.error;
    default:
      return console.log; // Default to console.log if log level is unknown
  }
}

/**
 * Conditionally appends log row to system log cache for warning or error levels.
 * 
 * @param {LogLevel} level - The log level.
 * @param {string} errorMessage - The error message.
 * @param {string} stackTrace - The stack trace.
 */
function appendLog_(level, errorMessage, stackTrace) {
  if (level === LogLevel.WARN || level === LogLevel.ERROR) {
    const logRow = createLogRow_(level.text, errorMessage, stackTrace);
    //appendToLogCache_(logRow);
    insertLogRow_(logRow);
  }
}

/**
 * Creates a log row object with the provided text, message, stack, and count.
 * 
 * @param {string} level - The log level associated with the log row.
 * @param {string} message - The message associated with the log row.
 * @param {string} stack - The stack trace associated with the log row.
 * @param {number} [count=1] - The count associated with the log row. Defaults to 1 if not provided.
 * @returns {Object} - The created log row object containing properties for date, text, message, stack, and count.
 */
function createLogRow_(level, message, stack, count = 1) {
  return {
    date: new Date(),
    level: level,
    message: message ? message.toString().trim() : '',
    stack: stack ? stack.trim() : '',
    count: count
  };
}

/**
 * Logs the start time of a function and returns the current time.
 *
 * @param {string} funcName - The name of the function to log.
 */
function logSTART_(funcName) {
  //toast_("START", funcName);
  funcName = `${funcName}()`
  console.info(`${funcName}: START`);
  console.time(funcName);
}

/**
 * Logs the end time of a function and the time it took to execute.
 *
 * @param {string} funcName - The name of the function to log.
 */
function logEND_(funcName) {
  //toast_("END", funcName);
  funcName = `${funcName}()`;
  console.timeEnd(funcName);
  console.info(`${funcName}: END`);
}

/**
 * Executes a function, retrying it a certain number of times with a delay between each retry.
 *
 * @param {Function} func - The function to execute.
 * @param {number} maxRetries - The maximum number of retries.
 * @param {number} waitTimeSeconds - The wait time between retries in seconds.
 * @return {*} - The return value of the function.
 * @throws - If the function throws an error after all retries have been exhausted.
 */
function executeWithRetry_(func) {
  console.time(`${executeWithRetry_}${func.name}`)
  flushAndWait_();
  const maxRetries = 3, waitTimeSeconds = 5;

  if (maxRetries < 1) {
    return func();
  }
  var numRetries = 0;
  while (numRetries < maxRetries) {
    numRetries++;
    //log(`Running ${func} [${numRetries}] times`, LogLevel.INFO);
    try {
      return func();
    } catch (error) {
      log(`${func} failed after [${numRetries}] attempts. Error message: ${error.message}. Stack: ${error.stack || ''}`, LogLevel.ERROR);
      if (numRetries >= maxRetries) {
        throw error;
      }
      //flushAndWait_();
      Utilities.sleep(waitTimeSeconds * 1000);
    }
  }
  console.timeEnd(`${executeWithRetry_}${func.name}`)
}

/**
 * Handles errors and warnings. Shows alerts if called from menu, or logs errors and warnings if called from trigger.
 *
 * @param {Error|undefined} e - The error object, if any.
 * @param {LogLevel} logLevel - The log level for the message (WARN or ERROR).
 * @param {string} message - The message to display or log.
 */
function handleWarningAndErrors_(e, logLevel, message) {
  log(message, logLevel);
  if (!e || !e.triggerUid) {
    if (logLevel === LogLevel.WARN) {
      showWarningAlert_(message);
    } else if (logLevel === LogLevel.ERROR) {
      showErrorAlert_(message);
    }
  }
}

/**
 * For real errors to be notified via email, use this 
 */
function throwError_(subject, error) {
  log(`subject=${subject}`, LogLevel.ERROR);
  log(`error=${error}`, LogLevel.ERROR);

  throw error;
}

/**
 * Checks if a value is empty or not.
 * 
 * @param {*} val - The value to check.
 * @returns {boolean} - True if the value is empty, false otherwise.
 */
function isEmpty_(val) {
  return (
    val === undefined ||
    val === null ||
    String(val).trim() === `` || //DO NOT CHANGE THIS IMPL
    (Array.isArray(val) && val.length === 0) ||
    (val instanceof Map && val.size === 0) ||
    (val instanceof Set && val.size === 0) ||
    (typeof val === 'object' && Object.keys(val).length === 0 && !(val instanceof Date)) //Added to check for an object, excluding Date
  );
}

/**
 * Checks if a value is not empty.
 * 
 * @param {*} val - The value to check.
 * @returns {boolean} - True if the value is not empty, false otherwise.
 */
function isNotEmpty_(val) {
  return !isEmpty_(val);
}

/**
 * Checks if all elements of an array (including nested arrays) are empty.
 * 
 * @param {*} array - The array to check.
 * @returns {boolean} - True if all elements and nested elements are empty, false otherwise.
 */
function isAllEmptyArray_(array) {
  if (isEmpty_(array)) return true; // Empty array or undefined

  // Check if all elements are either empty or nested empty arrays
  return array.every(element => isEmpty_(element) || (Array.isArray(element) && isAllEmptyArray_(element)));
}

/**
 * Checks if a value is a valid number.
 * 
 * @param {*} num - The value to check.
 * @returns {boolean} - True if the value is a valid number, false otherwise.
 */
function isValidNumber_(num) {
  return !isNaN(parseInt(num)) && isFinite(num);
}

/**
 * Checks if a value is an invalid number.
 * 
 * @param {*} num - The value to check.
 * @returns {boolean} - True if the value is an invalid number, false otherwise.
 */
function isInvalidNumber_(num) {
  return !isValidNumber_(num);
}

/**
 * Flushes any pending changes to the spreadsheet and waits for all data executions to be completed before continuing.
 *
 * @returns {void}
 */
function flushAndWait_() {
  try {
    SpreadsheetApp.flush();
    getActiveSpreadsheet_().waitForAllDataExecutionsCompletion(120);
  } catch (e) {
    log(`Error message: ${e.message}`, LogLevel.ERROR);
  }
}

/**
 * Shows a info alert window with title and message with OK button
 */
function showInfoAlert_(message) {
  SpreadsheetApp.getUi().alert("ℹ️ Info", message, SpreadsheetApp.getUi().ButtonSet.OK);
}

/**
 * Shows a info alert window with title and message with OK button
 */
function showCustomInfoAlert_(header, message) {
  SpreadsheetApp.getUi().alert("ℹ️ " + header, message, SpreadsheetApp.getUi().ButtonSet.OK);
}

/**
 * Shows a warning alert window with title and message with OK button
 */
function showWarningAlert_(message) {
  SpreadsheetApp.getUi().alert("⚠️ Warning", message, SpreadsheetApp.getUi().ButtonSet.OK);
}

/**
 * Shows an error alert window with title and message with OK button
 */
function showErrorAlert_(message) {
  SpreadsheetApp.getUi().alert("🛑 ERROR", message, SpreadsheetApp.getUi().ButtonSet.OK);
}

/**
 * Displays a toast notification with a given title and message.
 *
 * @param {string} title - The title of the toast notification.
 * @param {string} msg - The message to be displayed in the toast notification.
 */
function toast_(title, msg) {
  getActiveSpreadsheet_().toast(msg, title);
}

/**
 * Converts the given value to a boolean value.
 *
 * @param {*} value - The value to convert.
 * @returns {boolean} - The boolean value of the given value.
 */
function toBoolean_(value) {
  //return (typeof value === "boolean") ? value : (isNotEmpty_(value) ? "true" === value.toString().toLowerCase() : false);
  if (typeof value === "boolean") {
    return value;
  } else if (isNotEmpty_(value)) {
    const lowercaseValue = value.toString().toLowerCase();
    return lowercaseValue === "true" || lowercaseValue === "y";
  } else {
    return false;
  }
}

/**
 * Updates a random number in a given cell to refresh the score.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet containing the cell to update.
 * @param {string} counterCell - The cell reference (e.g., "A1") of the cell to update.
 */
function updateRefreshCounter_(sheet, counterCell) {
  const cell = sheet.getRange(counterCell);
  let num = cell.getValue();
  num = isValidNumber_(num) ? num + 1 : 1;
  cell.setValue(num);
  cell.setNote(`Last Updated:\n${getCurrentLongTimesptamp_()}`);

  flushAndWait_();
}

/**
 * Converts a given number of milliseconds to a string formatted as "MM:SS.mmm".
 *
 * @param {number} millis - The number of milliseconds to convert.
 * @returns {string} A string representing the given number of milliseconds in the format "MM:SS.mmm".
 */
function convertMilliSeconds_(millis) {
  let minutes = Math.floor(millis / 60000);
  let seconds = ((millis % 60000) / 1000).toFixed(4);
  seconds = seconds.slice(0, -1);
  return (
    (minutes < 10 ? "0" : "") + minutes + ":" +
    (seconds < 10 ? "0" : "") + seconds + " (mm:ss.sss)"
  );
}

/**
 * Adds spaces before each capital letter that follows a lowercase letter in a string.
 *
 * @param {string} str - The input string to add spaces to.
 * @returns {string} The input string with spaces added before each capital letter that follows a lowercase letter.
 */
function addSpacesToCamelCase_(str) {
  if (isEmpty_(str)) { return '' };
  return str.replace(/([a-z])([A-Z])/g, '$1 $2');
}

/**
 * Appends pre prod env name and tournament name to the given text
 * This is used for email subject, file names etc
 */
function appendEnvAndTournamentName_(text) {
  let preProdEnvName = "";
  if (isNotProdEnv_()) {
    preProdEnvName = EnvConfigClass.getEnvName() + ":";
  }
  //const result = preProdEnvName + getActiveSeriesName_() + ":" + text;
  //const result = `${preProdEnvName}${getActiveSeriesName_()}:${EnvConfigClass.getEnvDefaultConfigGroup()}:${text}`;
  const result = `${preProdEnvName}${EnvConfigClass.getEnvDefaultConfigGroup()}:${getActiveSeriesName_()}:${text}`;

  return result;
}

/**
 * Return true if a team name has specific words in it, otherwise false
 */
function isInvalidTeamName_(teamName) {
  return INVALID_TEAMS_ARR.filter(function (item) { return teamName.toLowerCase().includes(item.toLowerCase()) }).length > 0;
}

/**
 * Converts a one-based column index number to a one-based column name in Excel format (e.g. A, B, AA, AB, etc.).
 * 
 * @param {number} colNum - The one-based column index number to convert.
 * @returns {string} The Excel-style column name.
 */
function colNumToName_(colNum) {
  let temp, letter = '';
  while (colNum > 0) {
    temp = (colNum - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    colNum = Math.floor((colNum - temp - 1) / 26);
  }
  return letter;
}

/**
 * Converts an Excel-style column name to a one-based column index number (e.g. A=1, B=2, AA=27, AB=28, etc.).
 * 
 * @param {string} colName - The column name to convert (case-insensitive).
 * @returns {number} The one-based column index number.
 */
function colNameToNum_(colName) {
  let column = 0;
  for (let i = 0; colName && i < colName.length; i++) {
    column += (colName.charCodeAt(i) - 64) * Math.pow(26, colName.length - i - 1);
  }
  return column;
}

/**
 * Converts column name to JS (0 based) index
 * 
 * @param {string} colName - The column name to convert (case-insensitive).
 * @returns {number} The zero-based column index number.
 */
function colNameToIndex_(colName) {
  let columnNum = colNameToNum_(colName);
  if (columnNum > 0) {
    return columnNum - 1;
  }
  return -1;
}

/**
 * Returns the range string for a given column name and row range.
 *
 * @param {string} colName - The column name, e.g. "A", "B", "C", etc.
 * @param {number} startRowNum - The starting row number.
 * @param {number} endRowNum - The ending row number.
 * @returns {string} The range string, e.g. "A1:A10".
 */
function getColumnRange_(colName, startRowNum, endRowNum) {
  return `${colName}${startRowNum}:${colName}${endRowNum}`;
}

/**
 * Returns the hyperlink to the active Google Spreadsheet's dashboard sheet.
 *
 * @return {string} The hyperlink to the dashboard sheet.
 */
function getDashboardSheetLink_() {
  var spreadsheet = getActiveSpreadsheet_();
  var sheet = spreadsheet.getSheetByName(homeSheetName);
  if (sheet) {
    var sheetUrl = spreadsheet.getUrl().replace('/edit', '/view') + '#gid=' + sheet.getSheetId();
    return '<a href="' + sheetUrl + '">' + spreadsheet.getName() + '</a>';
  }
}

/**
 * Checks if the specified sheet has a valid match ID in the expected location.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to check.
 * @return {boolean} True if the sheet has a valid match ID, false otherwise.
 */
function hasMatchID_(sheet) {
  let matchID = getMatchID_(sheet);
  return isNotEmpty_(matchID) && isValidNumber_(matchID);
}

/**
 * Retrieves the match ID from the specified sheet.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to retrieve the match ID from.
 * @return {number} The match ID, or null if it is not found or not a valid number.
 */
function getMatchID_(sheet) {
  let matchID = sheet.getRange(matchSheetObj.resultMatchIDCell).getValue();
  return matchID;
}

/**
 * Checks if the match result for the specified match ID has been recorded into the daily results sheet.
 * 
 * @param {string} matchID - The match ID to check.
 * @return {boolean|undefined} True if the match result has been recorded, false if it has not been recorded, or undefined if the match ID is empty.
 */
function isMatchResultRecorded_(matchID) {
  if (isEmpty_(matchID)) {
    return undefined;
  }
  let matches = getDailyResultsSheet_().getRange(dailyResultsSheetObj.matchIDRange).getValues().filter(function (row) { return row[0] == matchID });
  return (matches.length > 0);
}

/**
 * Checks if the match result for the specified match ID has not been recorded in the daily results sheet.
 * 
 * @param {string} matchID - The match ID to check.
 * @return {boolean|undefined} True if the match result has not been recorded, false if it has been recorded, or undefined if the match ID is empty.
 */
function isMatchResultNotRecorded_(matchID) {
  if (isEmpty_(matchID)) {
    return undefined;
  }
  return !isMatchResultRecorded_(matchID);
}

/**
 * Counts the number of cells in a range that match a given text.
 * 
 * @param {Sheet} sheet - The sheet containing the range to search.
 * @param {string} range - The range to search, in A1 notation.
 * @param {string} text - The text to search for.
 * @returns {number} - The number of cells in the range that match the text.
 */
function countMatchingCells_(sheet, range, text) {
  return sheet.getRange(range).getValues().filter(row => row[0] === text).length;
}

/**
 * Finds a cell in a sheet based on its column value and returns the value of another cell in the same row.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to search.
 * @param {number} searchColumnIndex - The index (1-based) of the column to search for the cell value to match.
 * @param {string} cellValueToMatch - The cell value to match.
 * @param {number} returnColumnIndex - The index (1-based) of the column to return the value of.
 * @param {boolean} [returnOnFirstMatch=false] - Whether to return the first match found or all matches.
 * 
 * @return {string|Array} If `returnOnFirstMatch` is `true`, returns the value of the first cell that matches the cell value to match in the specified column.
 *                        If `returnOnFirstMatch` is `false`, returns an array of values of all cells that match the cell value to match in the specified column.
 *                        If no cell value matches, returns an empty string or an empty array, depending on `returnOnFirstMatch`.
 */
function filterSheetByCellValue_(sheet, searchColumnIndex, cellValueToMatch, returnColumnIndex, returnOnFirstMatch = false) {
  let data = sheet.getDataRange().getValues();
  let retCellValues = returnOnFirstMatch ? "" : [];
  for (let i = 0; i < data.length; i++) {
    if (data[i][searchColumnIndex - 1] == cellValueToMatch) {
      let cellValue = sheet.getRange(i + 1, returnColumnIndex).getValue();
      if (returnOnFirstMatch) {
        retCellValues = cellValue;
        break;
      } else {
        retCellValues.push(cellValue);
      }
    }
  }
  return retCellValues;
}

/**
 * Retrieves the row number of a specific cell value in a given sheet.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet object where the search is performed.
 * @param {number} searchColIdx - The index of the column where the search is performed (0-based index).
 * @param {*} cellValueToMatch - The cell value to match.
 * @returns {number|undefined} The row number of the matched cell value, or undefined if no match is found.
 */
function getSheetRowNum_(sheet, searchColIdx, cellValueToMatch) {
  let data = sheet.getDataRange().getValues();
  for (let i = 0; i < data.length; i++) {
    if (data[i][searchColIdx] == cellValueToMatch) {
      return i + 1;
    }
  }
}

/**
 * Checks if the range has minimum value in all cells
 */
function checkRangeHasMinValue_(valuesRange, conditionsRange, minValueToCompare) {
  let values = valuesRange.getValues();
  let conditions = conditionsRange.getValues();
  for (let row in values) {
    for (let col in values[row]) {
      let cellValue = values[row][col];
      if ((STATUS_Y == conditions[0][col]) && isValidNumber_(cellValue) && isValidNumber_(minValueToCompare) && cellValue < minValueToCompare) {
        return false;
      }
    }
  }
  return true;
}

/**
 * Return the row number of the first cell where the cell value matches  with @cellValue in the given @range
 */
function findRowOfFirstCell_(cellValue, dataRange) {
  let values = dataRange.getValues();
  for (let i = 0; i < values.length; i++) {
    for (let j = 0; j < values[i].length; j++) {
      if (values[i][j] == cellValue) {
        return (i + 1); // This is your row number within the data range
      }
    }
  }
}

/**
 * Return the row number of the last cell where the cell value matches  with @cellValue in the given @range
 */
function findRowOfLastCell_(cellValue, dataRange) {
  let values = dataRange.getValues();
  let lastRowNum = 0;
  for (let i = 0; i < values.length; i++) {
    for (let j = 0; j < values[i].length; j++) {
      if (values[i][j] == cellValue) {
        lastRowNum = (i + 1); // This is your row number within the data range
      }
    }
  }
  return lastRowNum;
}

/**
 * Checks whether all values in a range are identical.
 *
 * @param {Range} dataRange - The range to check.
 * @returns {boolean} `true` if all values in the range are identical, `false` otherwise.
 *
 * @example
 * // Assume that cell A1 to C3 contains the value "hello".
 * isRangeValuesIdentical_(sheet.getRange("A1:C3")); // Returns `true`.
 *
 * // Assume that cell A1 to C3 contains different values.
 * isRangeValuesIdentical_(sheet.getRange("A1:C3")); // Returns `false`.
 */
function isRangeValuesIdentical_(dataRange) {
  const values = dataRange.getValues().flat();
  return new Set(values).size === 1;
}

/**
 * Returns an array of unique values with their counts from the input array.
 *
 * @param {Array} arr - The input array.
 * @returns {Array} An array of unique values with their counts.
 *
 * @example
 * removeDuplicatesAndAddCounts_([1, 2, 2, 3, 3, 3]); // returns ["1", "2 (2)", "3 (3)"]
 */
function getUniqueValuesWithCounts_(arr) {
  const counts = {};
  const lastIndex = {};

  // Iterate in reverse to capture the last occurrence index
  for (let i = arr.length - 1; i >= 0; i--) {
    const val = arr[i];
    counts[val] = (counts[val] || 0) + 1;
    if (!(val in lastIndex)) {
      lastIndex[val] = i;
    }
  }

  // Sort the unique values based on their last occurrence index
  const sortedUniqueValues = Object.keys(counts).sort((a, b) => lastIndex[a] - lastIndex[b]);

  // Format the output string
  const formattedStr = sortedUniqueValues.map(val => {
    return counts[val] > 1 ? `${val} (${counts[val]})` : val;
  });

  return formattedStr;
}

/**
 * Sorts a 2D array by a specified column.
 * @param {Array<Array>} data - The 2D array to sort.
 * @param {number} column - The zero-based index of the column to sort by.
 * @param {boolean} [ascending=true] - Whether to sort in ascending order (true) or descending order (false).
 * @returns {Array<Array>} The sorted 2D array.
 */
function sortArrayByColumn_(data, column, ascending = true) {
  const sortOrder = ascending ? 1 : -1;

  data.sort((a, b) => {
    const compareResult = (a[column] < b[column]) ? -1 : (a[column] > b[column]) ? 1 : 0;
    return compareResult * sortOrder;
  });

  return data;
}

/**
 * Generates a random UUID using the Utilities class provided by Google Apps Script.
 *
 * @returns {string} A randomly generated UUID.
 */
function generateGUID_() {
  var uuid = Utilities.getUuid();
  return uuid;
}

/**
 * Checks whether a string matches the pattern of a GUID (Globally Unique Identifier).
 *
 * @param {string} value - The string to be tested.
 * @returns {boolean} Returns true if the input string matches the GUID pattern, false otherwise.
 *
 * @example
 * var isGuidResult = isGuid('123e4567-e89b-12d3-a456-426614174001');
 * Logger.log(isGuidResult); // Outputs: true
 */
function isGuid(value) {
  // Define the regular expression pattern for a GUID
  var guidPattern = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

  // Test the value against the GUID pattern
  return guidPattern.test(value);
}

/**
 * Splits a string input into an array of numbers.
 * @param {string|number} input - The input string or number to be split.
 * @returns {number[]} An array of numbers extracted from the input.
 */
function splitInputIntoNumbers_(input) {
  if (!input) {
    return [];
  }
  const inputStr = String(input);
  const numbers = inputStr.split(/[^0-9]+/).map(Number);
  return numbers.filter(Boolean);
}

/**
 * Returns an array of common elements between two arrays.
 *
 * @param {Array} arr1 - The first array of values.
 * @param {Array} arr2 - The second array of values.
 * @returns {Array} - An array of common values between the two input arrays.
 */
function getCommonValues_(arr1, arr2) {
  const commonElements = arr1.filter(element => arr2.includes(element));
  return commonElements;
}

/**
 * Returns a random value between the two input values.
 *
 * @param {any} value1 - The first value to choose from
 * @param {any} value2 - The second value to choose from
 * @returns {any} The randomly chosen value
 */
function getRandomValue_(value1, value2) {
  return Math.random() < 0.5 ? value1 : value2;
}

/**
 * Returns a random integer between the minimum and maximum values (inclusive).
 *
 * @param {number} min - The minimum value to choose from
 * @param {number} max - The maximum value to choose from
 * @returns {number} The randomly chosen integer
 */
function getRandomNumber_(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * Returns a random value from an array.
 *
 * @param {Array} arr - The array to select a random value from.
 * @returns {*} - A random value from the array or null if the array is empty.
 */
function getRandomValueFromArray_(arr) {
  if (arr.length > 0) {
    const randomIndex = Math.floor(Math.random() * arr.length);
    return arr[randomIndex];
  }
  return null; // return null if the array is empty
}

/**
 * Gets a random value from an array, excluding values containing specified characters.
 * 
 * @param {Array} arr - The array to pick a random value from.
 * @param {Array} excludeChars - An array of characters to exclude from values.
 * @return {*} A random value from the array, excluding values containing the specified characters.
 */
function getRandomValueFromArrayWithExclusion_(arr, excludeChars) {
  // Filter out values containing the specified characters
  const filteredArr = arr.filter(value => !excludeChars.some(char => value.includes(char)));

  if (filteredArr.length > 0) {
    const randomIndex = Math.floor(Math.random() * filteredArr.length);
    return filteredArr[randomIndex];
  }

  return null; // return null if the filtered array is empty
}

/**
 * Returns a string representing the range of row indices provided in an array.
 * 
 * @param {number[]} rowIndices - An array of row indices.
 * @returns {string} A string representing the range of row indices.
 * @example
 * const rangeString = getRangeString_([1,2,3,4,5,7,8,9,12,15]);
 * Logger.log(rangeString); // Output: "1-5, 7-9, 12, 15"
 */
function getRangeString_(rowIndices) {
  if (isNotEmpty_(rowIndices)) {
    const separator = '-';
    rowIndices.sort(function (a, b) { return a - b; });

    let ranges = [];
    let start = rowIndices[0];
    for (let i = 1; i < rowIndices.length; i++) {
      if (rowIndices[i] != rowIndices[i - 1] + 1) {
        ranges.push(start == rowIndices[i - 1] ? start.toString() : `${start}${separator}${rowIndices[i - 1]}`);
        start = rowIndices[i];
      }
    }
    ranges.push(start == rowIndices[rowIndices.length - 1] ? start.toString() : `${start}${separator}${rowIndices[rowIndices.length - 1]}`);
    return ranges.join(', ');
  }
}

/**
 * Checks if a number is within the range defined by two numbers, regardless of their order.
 *
 * @param {number} number - The number to check.
 * @param {Array<number>} range - The range defined by an array containing two numbers.
 * @returns {boolean} True if the number is within the range, otherwise false.
 */
function isNumberInRange_(number, range) {
  const sortedRange = range.slice().sort((a, b) => a - b); // Make a copy and sort the range
  return number >= sortedRange[0] && number <= sortedRange[1];
}

/**
 * Checks if the input string contains the substring '(2x)'.
 * 
 * @param {string} matchName - The match name to check.
 * @returns {boolean} True if the match name contains '(2x)', false otherwise.
 */
function checkForDoublePoints_(matchName) {
  return matchName.includes(TrumpCardKeys.INDICATOR_EMOJI.trim());
}

/**
 * Logs a two-dimensional array as a table.
 * If the input is not an array, it will be logged as is.
 * @param {any} input - The input to log as a table.
 */
function logArrayAsTable(input) {
  if (!Array.isArray(input)) {
    console.log(input);
    // Logger.log(input);
    return;
  }

  const columnWidths = input[0].map((_, colIndex) =>
    Math.max(...input.map((row) => String(row[colIndex]).length))
  );

  const table = input
    .map((row, rowIndex) =>
      row
        .map((cell, colIndex) => {
          const paddedCell = String(cell).padEnd(columnWidths[colIndex], ' ');
          return rowIndex === 0 ? `${paddedCell}   ` : paddedCell;
        })
        .join('\t')
    )
    .join('\n');

  const underlineRow = columnWidths.map((width) => '-'.repeat(width)).join('\t');
  const output = table.replace(/\n/, `\n${underlineRow}\n`);
  console.log(output);
  // Logger.log(output);
}

/**
 * Synchronizes all columns in the given data by filling empty cells to match the maximum number of columns.
 * 
 * @param {Array<Array<any>>} data - The data to synchronize columns.
 * @returns {Array<Array<any>>} The synchronized data with all rows having the same number of columns.
 */
function syncAllColumns_(data) {
  if (isEmpty_(data)) { return; }

  var maxColumns = data.reduce(function (max, row) {
    return Math.max(max, row.length);
  }, 0);

  return data.map(function (row) {
    while (row.length < maxColumns) {
      row.push("");
    }
    return row;
  });
}

/**
 * Gets the maximum numeric value in a specific column of a Google Sheets spreadsheet.
 * Skips NaN values.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet containing the column.
 * @param {string} columnName - The column's letter identifier (e.g., "A", "B").
 * @return {number} The maximum numeric value in the specified column, excluding NaN values.
 */
function getMaxValueInColumn_(sheet, columnName) {
  var data = sheet.getRange(columnName + ":" + columnName).getValues(); // Assuming data starts from row 2
  var max = -Infinity;

  data.forEach(function (row) {
    var value = row[0];
    if (typeof value === "number" && !isNaN(value)) {
      max = Math.max(max, value);
    }
  });

  return max;
}

/**
 * Appends BASE URL
 */
function appendBaseURL_(url) {
  if (url.indexOf(URLs.BASE_URL) !== 0) {
    url = URLs.BASE_URL + url;
  }
  return url;
}

/**
 * Finds the highest closest value in the haystack to the needle
 * 
 * @param {number} needle - The value to find the closest match to
 * @param {Array<number>} haystack - The array of values to search
 * 
 * @returns {number} The highest closest value
 */
function findHighestClosestValue_(needle, haystack) {
  return haystack.reduce((a, b) => {
    if (isEmpty_(a)) {
      return b;
    } else if (isEmpty_(b)) {
      return a;
    }
    let aDiff = Math.abs(a - needle);
    let bDiff = Math.abs(b - needle);

    if (aDiff == bDiff) {
      return a > b ? a : b;
    } else {
      return bDiff < aDiff ? b : a;
    }
  });
}

/**
 * Finds the lowest closest value in the haystack to the needle
 * 
 * @param {number} needle - The value to find the closest match to
 * @param {Array<number>} haystack - The array of values to search
 * 
 * @returns {number} The lowest closest value
 */
function findLowestClosestValue_(needle, haystack) {
  return haystack.reduce((a, b) => {
    if (isEmpty_(a)) {
      return b;
    } else if (isEmpty_(b)) {
      return a;
    }
    let aDiff = Math.abs(a - needle);
    let bDiff = Math.abs(b - needle);

    if (aDiff == bDiff) {
      return a < b ? a : b;
    } else {
      return bDiff < aDiff ? b : a;
    }
  });
}

/**
 * Replaces placeholders in a string with actual values obtained by calling corresponding methods.
 *
 * @param {string} originalString - The original string containing placeholders.
 * @param {Object} replacements - An object where keys are placeholders and values are functions or static values.
 * @returns {string} The updated string with replaced placeholders.
 */
function replacePlaceholders_(originalString, replacements) {
  let updatedString = originalString;

  for (const placeholder in replacements) {
    if (replacements.hasOwnProperty(placeholder)) {
      const replacement = replacements[placeholder];

      if (typeof replacement === 'function') {
        // If the replacement is a function, call it to get the actual value
        const replacementValue = replacement();
        updatedString = updatedString.replace(new RegExp(placeholder, 'g'), replacementValue);
      } else {
        // If the replacement is a static value, directly replace it
        updatedString = updatedString.replace(new RegExp(placeholder, 'g'), replacement);
      }
    }
  }

  return updatedString;
}

/**
 * Checks if a column name is a possible valid column name in Google Sheets.
 *
 * @param {string} columnName - The column name to check.
 * @returns {boolean} True if the column name is a possible valid column name, otherwise false.
 */
function isValidColumnName_(columnName) {
  // Regular expression to match valid column names
  var regex = /^[A-Z]+$/;

  return regex.test(columnName);
}

/**
 * Checks if a column name is an invalid column name in Google Sheets.
 *
 * @param {string} columnName - The column name to check.
 * @returns {boolean} True if the column name is an invalid column name, otherwise false.
 */
function isInvalidColName_(columnName) {
  return !isValidColumnName_(columnName);
}

/**
 * Generates a range string based on the given start and end column names, with header.
 *
 * @param {string} startColName - The starting column name.
 * @param {string} [endColName=startColName] - The ending column name (optional, defaults to startColName).
 * @returns {string|undefined} The range string in A1 notation, or undefined if startColName is empty or invalid.
 */
function getRangeByColNameWithHeader_(startColName, endColName) {
  return getRangeByColName_(startColName, endColName, true);
}

/**
 * Generates a range string based on the given start and end column names.
 *
 * @param {string} startColName - The starting column name.
 * @param {string} [endColName=startColName] - The ending column name (optional, defaults to startColName).
 * @returns {string|undefined} The range string in A1 notation, or undefined if startColName is empty.
 */
function getRangeByColNameNoHeader_(startColName, endColName) {
  return getRangeByColName_(startColName, endColName, false);
}

/**
 * Generates a range string based on the given start and end column names, with or without header.
 *
 * @param {string} startColName - The starting column name.
 * @param {string} [endColName=startColName] - The ending column name (optional, defaults to startColName).
 * @param {boolean} [includeHeader=false] - Whether to include the header row in the range (optional, defaults to false).
 * @returns {string|undefined} The range string in A1 notation, or undefined if startColName is empty or invalid.
 */
function getRangeByColName_(startColName, endColName, includeHeader) {
  if (isInvalidColName_(startColName) || (isNotEmpty_(endColName) && isInvalidColName_(endColName))) { return; }
  if (isEmpty_(endColName)) { endColName = startColName; }

  var startRow = includeHeader ? '' : 2;
  var range = startColName.toUpperCase() + startRow + ":" + endColName.toUpperCase();
  return range;
}

/**
 * Calculates the column length (number of columns) between two column names.
 *
 * @param {string} startColName - The starting column name.
 * @param {string} endColName - The ending column name.
 * @returns {number} The column length (number of columns).
 */
function getColLength_(startColName, endColName) {
  const startColumn = colNameToNum_(startColName);
  const endColumn = colNameToNum_(endColName);
  return Math.abs(endColumn - startColumn) + 1;
}

/**
 * Checks if the input string starts with a number.
 * @param {string} inputString - The input string to check.
 * @returns {string|null} Returns the input string if it starts with a number, otherwise null.
 */
function startsWithNumber_(inputString) {
  return /^\d/.test(inputString) ? inputString : null;
}

/**
 * Appends data to a Google Sheets range.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet The sheet to set values to.
 * @param {Array<Array>} data The 2D array of values to set.
 * @param {number} [startColumnNum=1] The column number to start setting values from. Defaults to 1.
 */
function appendDataToSheet_(sheet, data, startColumnNum = 1) {
  if (isNotEmpty_(data)) {
    const numRows = data.length;
    const numCols = data[0].length;
    const lastRow = sheet.getLastRow();

    const range = sheet.getRange(lastRow + 1, startColumnNum, numRows, numCols);
    range.setValues(data);
  }
}

/**
 * Extracts the short date (MMM-DD) from the provided title string.
 * The title string is expected to be in the format [#] MMM-DD: ...
 * 
 * @param {string} title - The title string from which to extract the short date.
 * @returns {string|null} The extracted short date (MMM-DD), or null if not found.
 */
function extractMatchShortDate_(title) {
  const regex = /\] (\w{3}-\d{2}):?/;
  const match = regex.exec(title);
  return match ? match[1] : null;
}

/**
 * Compares two arrays for equality.
 * @param {Array} array1 The first array to compare.
 * @param {Array} array2 The second array to compare.
 * @returns {boolean} True if the arrays are equal, false otherwise.
 */
function containsIdenticalValues_(array1, array2) {
  if (isEmpty_(array1) && isEmpty_(array2)) { return true; }
  if ((isEmpty_(array1) && isNotEmpty_(array2) || isNotEmpty_(array1) && isEmpty_(array2))) { return false; }

  return array1.length === array2.length &&
    array1.slice().every((value, index) => value === array2.slice()[index]);
}

/**
 * Shows all rows in the given sheet.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to operate on.
 */
function showAllRows_(sheet) {
  var lastRow = sheet.getMaxRows();
  sheet.showRows(1, lastRow);
}

/**
 * Get the plain object representation of an instance.
 * @param {Object} instance - The instance of the class.
 * @returns {Object} The plain object representation of the instance.
 */
function toObject_(instance) {
  if (typeof instance.toObject === 'function') {
    return instance.toObject();
  } else {
    log(`The provided instance ${JSON.stringify(instance)} does not have a toObject() method.`, LogLevel.ERROR);
  }
}

/**
 * Logs the objects stored in different types of instances.
 * @param {*} instances - The instances to be logged.
 */
function logObjects_(instances) {
  if (!instances) { return; }

  /**
   * Log the plain object representation of an array of instances.
   * @param {Array<Object>} instances - The array of instances of the class.
   */
  function logObjectsArray_(instances) {
    const objectRepresentations = instances.map(instance => toObject_(instance));
    console.log(objectRepresentations);
  }

  /**
   * Logs the objects stored in a Map.
   * @param {Map} instances - The Map containing the instances to be logged.
   */
  function logObjectsMap_(instances) {
    const objectRepresentations = {};
    for (const [key, instance] of instances.entries()) {
      objectRepresentations[key] = toObject_(instance);
    }

    console.log(objectRepresentations);
  }

  /**
   * Log the plain object representation of a JavaScript object.
   * @param {Object} instances - The JavaScript object to be logged.
   */
  function logJSObject_(instances) {
    const objectRepresentations = {};

    for (const key in instances) {
      if (instances.hasOwnProperty(key)) {
        const instance = instances[key];
        objectRepresentations[key] = toObject_(instance);
      }
    }

    console.log(objectRepresentations);
  }

  if (Array.isArray(instances)) {
    logObjectsArray_(instances);
  } else if (instances instanceof Map) {
    logObjectsMap_(instances);
  } else if (typeof instances === 'object') {
    logJSObject_(instances);
  } else {
    console.error("Unsupported type. Only arrays, maps, and objects are supported.");
  }
}

/**
 * Retrieves the name of the calling function.
 * 
 * @returns {string} - The name of the calling function.
 */
function getFuncName_() {
  return arguments.callee.caller.name;
}

///////////////////// EXPERIMENTAL ////////////////////////

function sendWhatsAppMessage_(phoneNumber, message) {
  var ACCOUNT_SID = "AC9c8c5c88ea4655fea540906c6f5f4f1c";
  var ACCOUNT_TOKEN = "0907f8d52f07857d601f9dff80df9062";
  var options = {
    "method": "post",
    'headers': {
      "Authorization": "Basic " + Utilities.base64Encode(ACCOUNT_SID + ":" + ACCOUNT_TOKEN),
    },
    'payload': {
      'Body': message,
      'To': 'whatsapp:+' + phoneNumber,
      'From': 'whatsapp:+14155238886',
    },
    'muteHttpExceptions': true
  };
  var url = "https://api.twilio.com/2010-04-01/Accounts/" + ACCOUNT_SID + "/Messages.json";
  var response = UrlFetchApp.fetch(url, options);
  Logger.log(response);
}
