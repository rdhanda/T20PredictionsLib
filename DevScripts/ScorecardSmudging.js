//Smudging.gs//

/**
 * Retrieves smudging lists based on various criteria.
 *
 * @param {Object} allTablesObj - Object containing all relevant tables.
 * @param {Array} team1PlayingXI - Array of players in team 1's playing XI.
 * @param {Array} team2PlayingXI - Array of players in team 2's playing XI.
 * @param {boolean} firstInningsCompleted - Indicates if the first innings is completed.
 * @param {number} maxRunsObj - Maximum runs criteria.
 * @param {number} maxWickets - Maximum wickets criteria.
 * @param {number} runs2Win - Runs required to win.
 * @param {number} wickets2TakeObj - Wickets required to take.
 * @returns {Object} - Object containing smudging lists for max runs, highest SR, max wickets, and best economy.
 */
function getAllSmudgingLists_(allTablesObj, team1PlayingXI, team2PlayingXI, team1ImpactPlayers, team2ImpactPlayers, firstInningsCompleted, maxRunsObj, maxWickets, runs2Win, wickets2TakeObj) {
  if (isEmpty_(allTablesObj) || isEmpty_(team1PlayingXI) || isEmpty_(team2PlayingXI) || isEmpty_(firstInningsCompleted)) {
    return [];
  }

  const maxRunsList = getSmudgingListForMaxRuns_(allTablesObj, firstInningsCompleted, maxRunsObj, runs2Win, team2PlayingXI);
  const highestSRList = getSmudgingListForHighestSR_(allTablesObj, firstInningsCompleted);
  const maxWicketsList = getSmudgingListForMaxWickets_(allTablesObj, firstInningsCompleted, maxWickets, wickets2TakeObj, team1PlayingXI);
  const bestEconomyList = getSmudgingListForBestEconomy_(allTablesObj, firstInningsCompleted);

  if (firstInningsCompleted) {
    const innings1DidNotBatPlayers = getDidNotBatPlayers_(allTablesObj.battingTables[0], team1PlayingXI.concat(team1ImpactPlayers));
    const innings1DidNotBowlPlayers = getDidNotBowlPlayers_(allTablesObj.bowlingTables[0], team2PlayingXI.concat(team2ImpactPlayers));

    maxRunsList.unshift(...innings1DidNotBatPlayers);
    highestSRList.unshift(...innings1DidNotBatPlayers);
    maxWicketsList.unshift(...innings1DidNotBowlPlayers);
    bestEconomyList.unshift(...innings1DidNotBowlPlayers);

    maxRunsList.unshift(`${innings1str}DNB`);
    highestSRList.unshift(`${innings1str}DNB`);
    maxWicketsList.unshift(`${innings1str}DNB`);
    bestEconomyList.unshift(`${innings1str}DNB`);
  }

  const maxRunsListDisplay = getDisplayList_(maxRunsList);
  const highestSRListDisplay = getDisplayList_(highestSRList);
  const maxWicketsListDisplay = getDisplayList_(maxWicketsList);
  const bestEconomyListDisplay = getDisplayList_(bestEconomyList);

  return {
    maxRunsListDisplay,
    highestSRListDisplay,
    maxWicketsListDisplay,
    bestEconomyListDisplay,
  }
}

/**
 * Retrieves the list of batters below the maximum runs limit from the appropriate batting table(s) based on the first innings completion status.
 * @param {object} allTablesObj - An object containing all the tables data.
 * @param {boolean} firstInningsCompleted - A boolean value indicating if the first innings is completed.
 * @param {number} maxRuns - The maximum runs limit.
 * @returns {Array<string>} An array of player names who scored below the maximum runs limit.
 */
function getSmudgingListForMaxRuns_(allTablesObj, firstInningsCompleted, maxRunsObj, runs2Win, team2PlayingXI) {
  const maxRuns = maxRunsObj.value;
  if (isEmpty_(allTablesObj) || isEmpty_(firstInningsCompleted) || isEmpty_(maxRuns)) { return []; }

  const smudging = [];

  if (!firstInningsCompleted) {
    const smudging1OutOnly = getBattersBelowMaxRuns_(allTablesObj.battingTables[0], maxRuns, BattersToSelect.OutOnly);
    smudging.push(`${innings1str}OUT<${maxRuns} runs`, ...smudging1OutOnly);
  } else {
    const smudging1All = getBattersBelowMaxRuns_(allTablesObj.battingTables[0], maxRuns, BattersToSelect.All);
    smudging.push(`${innings1str}ALL< ${maxRuns} runs`, ...smudging1All);

    const maxAnyBatterCanScoreMoreRuns = (runs2Win + 6);
    let checkMaxPossibleRuns = runs2Win && (maxAnyBatterCanScoreMoreRuns < maxRuns);

    //Out and lower than Max Runs
    const smudging2OutOnly = getBattersBelowMaxRuns_(allTablesObj.battingTables[1], maxRuns, BattersToSelect.OutOnly);
    smudging.push(`${innings2str}OUT< ${maxRuns} runs`, ...smudging2OutOnly);

    if (checkMaxPossibleRuns) {
      const maxPossibleRuns = maxRuns - maxAnyBatterCanScoreMoreRuns;
      const smudging3NotOutOnly = getBattersBelowMaxRuns_(allTablesObj.battingTables[1], maxPossibleRuns, BattersToSelect.NotOutOnly);
      smudging.push(`${innings2str}Can't Score ${maxRuns} runs`, ...smudging3NotOutOnly);

      const smudging2DNB = getDidNotBatPlayers_(allTablesObj.battingTables[1], team2PlayingXI);
      smudging.push(`${innings2str}DNB<`, ...smudging2DNB);
    }
  }

  return smudging;
}

/**
 * Retrieves a list of batters from a table with runs below a certain threshold and based on specific conditions.
 * 
 * @param {string[][]} table - The table containing batting data.
 * @param {number} maxRuns - The maximum number of runs allowed for a batter to be included.
 * @param {string} battersToSelect - The type of batters to select ('OUT_ONLY', 'NOT_OUT_ONLY', or any other value).
 * @returns {string[]} The list of batters below the maximum runs threshold and based on the specified conditions.
 */
function getBattersBelowMaxRuns_(table, maxRuns, battersToSelect) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BattingTableIndex.Player, conditions: { exclude: BATTING_TABLE_NON_PLAYERS_LIST } },
    { index: BattingTableIndex.Runs, conditions: { lt: maxRuns } },//Less than max runs
  ];

  if (battersToSelect === BattersToSelect.OutOnly) {
    headerConditions.push({ index: BattingTableIndex.HowOut, conditions: { exclude: [BATTER_NOT_OUT] } });
  } else if (battersToSelect === BattersToSelect.NotOutOnly) {
    headerConditions.push({ index: BattingTableIndex.HowOut, conditions: { include: [BATTER_NOT_OUT] } });
  } else {
    headerConditions.push({ index: BattingTableIndex.HowOut, conditions: {} });
  }

  const battingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    battingData.push(sanitizePlayerName_(row[0])); // Add only the batter name to the result array
  }

  return battingData;
}

/**
 * Retrieves the players who did not bat from the playing XI list.
 *
 * @param {string[]} battersWhoBatted - The list of batters who batted.
 * @param {string[]} playingXIList - The list of players in the playing XI.
 * @returns {string[]} The players who did not bat.
 */
function getDidNotBatPlayers_(battingTable, playingXIList) {
  if (isEmpty_(playingXIList)) {
    return [];
  }

  const playersWhoBatted = getPlayersWhoBatted_(battingTable);
  const didNotBatPlayers = [];

  for (const player of playingXIList) {
    if (!playersWhoBatted?.includes(player)) {
      didNotBatPlayers.push(player);
    }
  }

  return didNotBatPlayers.sort();
}

/**
 * Retrieves the names of batters who batted.
 *
 * @param {string[][]} table - The batting table.
 * @returns {string[]} The names of batters who batted.
 */
function getPlayersWhoBatted_(table) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BattingTableIndex.Player, conditions: { exclude: BATTING_TABLE_NON_PLAYERS_LIST } },
  ];

  const battingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    battingData.push(sanitizePlayerName_(row[0])); // Add only the batter name to the result array
  }

  return battingData;
}

/**
 * Retrieves a list of players with the highest smudging value based on strike rate.
 * @param {Object} allTablesObj - The object containing all batting tables.
 * @param {boolean} firstInningsCompleted - Indicates if the first innings has been completed.
 * @returns {Array} - The combined list of players with the highest smudging value.
 */
function getSmudgingListForHighestSR_(allTablesObj, firstInningsCompleted) {
  if (isEmpty_(allTablesObj) || isEmpty_(firstInningsCompleted)) {
    return [];
  }
  const smudging = [];

  // For all innings
  const battingData = extractBattingData_(allTablesObj.battingTables);
  // Exclude non out batters
  const highestSR = getPlayersMaxStrikeRateData_(battingData, true).value;

  if (!firstInningsCompleted) {
    const smudging1LessThanMinBalls = getBattersPlayedLessThanMinBalls_(allTablesObj.battingTables[0]);
    const smudging2BelowSR = getBattersBelowHighestSR_(allTablesObj.battingTables[0], highestSR, true);//Out, and below Highest SR

    smudging.push(`${innings1str}OUT<${getMinBallsForBatterSR_()} balls`, ...smudging1LessThanMinBalls);
    smudging.push(`${innings1str}OUT< ${highestSR} SR`, ...smudging2BelowSR);
  } else {
    // Get the maximum strike rate from all batters of the first innings (including not out)
    const firstInningsBattingData = extractBattingData_(allTablesObj.battingTables[0]);
    const firstInningsHighestSR = getPlayersMaxStrikeRateData_(firstInningsBattingData, false).value;

    // Maximum strike rate from the first innings and from both innings (excluding not outs)
    const maxHighestSR = isEmpty_(highestSR) ? firstInningsHighestSR : Math.max(highestSR, firstInningsHighestSR);

    const smudging1All = getBattersNotEqualToHighestSR_(allTablesObj.battingTables[0], maxHighestSR);
    smudging.push(`${innings1str}ALL<>${maxHighestSR} SR`, ...smudging1All);

    // Batters who played less than the minimum number of balls in the second innings
    const smudging2 = getBattersPlayedLessThanMinBalls_(allTablesObj.battingTables[1]);
    smudging.push(`${innings2str}OUT<${getMinBallsForBatterSR_()} balls`, ...smudging2);

    // Batters who are out, played the minimum balls, and had a lower strike rate than the maximum in the second innings
    const smudging3 = getBattersBelowHighestSR_(allTablesObj.battingTables[1], maxHighestSR, true);
    smudging.push(`${innings2str}OUT<${maxHighestSR} SR`, ...smudging3);
  }

  return smudging;
}

/**
 * Retrieves the names of batters who have SR below the highest SR.
 *
 * @param {string[][]} table - The batting table.
 * @param {number} maxRuns - The maximum runs allowed.
 * @param {boolean} excludeNotOut - Flag to exclude 'not out' players.
 * @returns {string[]} The names of batters below the highest SR.
 */
function getBattersBelowHighestSR_(table, highestSR, excludeNotOut) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BattingTableIndex.Player, conditions: { exclude: BATTING_TABLE_NON_PLAYERS_LIST } },
    { index: BattingTableIndex.HowOut, conditions: { exclude: excludeNotOut ? [BATTER_NOT_OUT] : [] } },
    { index: BattingTableIndex.BallsFaced, conditions: { gte: getMinBallsForBatterSR_() } },
    { index: BattingTableIndex.StrikeRate, conditions: { lt: highestSR } },
  ];

  const battingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    battingData.push(sanitizePlayerName_(row[0])); // Add only the batter name to the result array
  }

  return battingData;
}

/**
 * 
 */
function getBattersNotEqualToHighestSR_(table, highestSR) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BattingTableIndex.Player, conditions: { exclude: BATTING_TABLE_NON_PLAYERS_LIST } },
    { index: BattingTableIndex.StrikeRate, conditions: { ne: highestSR } },
  ];

  const battingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    battingData.push(sanitizePlayerName_(row[0])); // Add only the batter name to the result array
  }

  return battingData;
}

/**
 * 
 */
function getBattersPlayedLessThanMinBalls_(table) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BattingTableIndex.Player, conditions: { exclude: BATTING_TABLE_NON_PLAYERS_LIST } },
    { index: BattingTableIndex.HowOut, conditions: { exclude: [BATTER_NOT_OUT] } },//not out or how out
    { index: BattingTableIndex.BallsFaced, conditions: { lt: getMinBallsForBatterSR_() } },//Bowled faced
  ];

  const battingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    battingData.push(sanitizePlayerName_(row[0])); // Add only the batter name to the result array
  }

  return battingData;
}

/**
 * Retrieves the list of bowlers below the maximum wickets limit from the appropriate bowling table(s) based on the first innings completion status.
 * @param {object} allTablesObj - An object containing all the tables data.
 * @param {boolean} firstInningsCompleted - A boolean value indicating if the first innings is completed.
 * @param {number} maxWickets - The maximum wickets limit.
 * @returns {Array<string>} An array of player names who took wickets below the maximum limit.
 */
function getSmudgingListForMaxWickets_(allTablesObj, firstInningsCompleted, maxWickets, wickets2TakeObj, team1PlayingXI) {
  if (isEmpty_(allTablesObj) || isEmpty_(firstInningsCompleted) || isEmpty_(maxWickets)) {
    return [];
  }
  const smudging = [];

  if (!firstInningsCompleted) {
    const wickets2Take = wickets2TakeObj.innings1WicketsToTake;
    const smudging1MaxOvers = getBowlersBelowMaxWickets_(allTablesObj.bowlingTables[0], maxWickets, BowlersToSelect.MaxOvers);

    smudging.push(`${innings1str}<${maxWickets} wkts (=max overs)`, ...smudging1MaxOvers);

    const { belowMaxOvers, oversStr } = checkBelowMaxOvers_(wickets2Take, maxWickets);
    if (belowMaxOvers) {
      const moreWicketsPossible = maxWickets - wickets2Take;
      const smudging2BelowMaxOvers = getBowlersBelowMaxWickets_(allTablesObj.bowlingTables[0], moreWicketsPossible, BowlersToSelect.BelowMaxOvers);

      smudging.push(`${innings1str}<${maxWickets} wkts ${oversStr}`, ...smudging2BelowMaxOvers);
    }
  } else {
    const smudging1All = getBowlersBelowMaxWickets_(allTablesObj.bowlingTables[0], maxWickets, BowlersToSelect.All);
    const smudging2MaxOvers = getBowlersBelowMaxWickets_(allTablesObj.bowlingTables[1], maxWickets, BowlersToSelect.MaxOvers);

    smudging.push(`${innings1str}<${maxWickets} wkts`, ...smudging1All);
    smudging.push(`${innings2str}<${maxWickets} wkts (=max overs)`, ...smudging2MaxOvers);

    const wickets2Take = wickets2TakeObj.innings2WicketsToTake;
    const { belowMaxOvers, oversStr } = checkBelowMaxOvers_(wickets2Take, maxWickets);

    if (belowMaxOvers) {
      const moreWicketsPossible = maxWickets - wickets2Take;
      const smudging2BelowMaxOvers = getBowlersBelowMaxWickets_(allTablesObj.bowlingTables[1], moreWicketsPossible, BowlersToSelect.BelowMaxOvers);
      const smudging4DNB = getDidNotBowlPlayers_(allTablesObj.bowlingTables[1], team1PlayingXI);

      smudging.push(`${innings2str}<${maxWickets} wkts ${oversStr}`, ...smudging2BelowMaxOvers);
      smudging.push(`${innings2str}<DNB`, ...smudging4DNB);
    }
  }

  return smudging;
}

/**
 * Retrieves the bowlers below the specified maximum wickets from the given table.
 * 
 * @param {Array} table - The input table containing bowling data.
 * @param {number} maxWickets - The maximum number of wickets to filter by.
 * @param {boolean} bowlersToSelect - Whether to include all bowlers or only bowlers with overs greater than equal to the MAX_OVERS_BOWLER constant.
 * @returns {Array|undefined} - The array of bowlers below the max wickets or undefined if the table is empty.
 */
function getBowlersBelowMaxWickets_(table, maxWickets, bowlersToSelect) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BowlingTableIndex.Player, conditions: {} },//Name
    { index: BowlingTableIndex.Wickets, conditions: { lt: maxWickets } },//Wickets
  ];

  if (BowlersToSelect.All === bowlersToSelect) {
    headerConditions.push({ index: BowlingTableIndex.Overs, conditions: { gte: 0.1 } });
  } else if (BowlersToSelect.BelowMaxOvers === bowlersToSelect) {
    headerConditions.push({ index: BowlingTableIndex.Overs, conditions: { lt: getMaxOversPerBowler_() } });
  } else if (BowlersToSelect.MaxOvers === bowlersToSelect) {
    headerConditions.push({ index: BowlingTableIndex.Overs, conditions: { gte: getMaxOversPerBowler_() } });
  }

  const bowlingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    if (isNotEmpty_(row[0]) && isNotEmpty_(row[2])) {//Check if Wickets exists
      bowlingData.push(sanitizePlayerName_(row[0])); // Add only the bowler name to the result array
    }
  }

  return bowlingData;
}

/**
 * Calculates includeAllBowlers and oversStr based on the provided wickets2Take.
 * @param {number} wickets2Take - The number of wickets to take.
 * @param {number} maxWickets - The maximum number of wickets possible.
 * @returns {Object} An object containing includeAllBowlers and oversStr.
 */
function checkBelowMaxOvers_(wickets2Take, maxWickets) {
  let belowMaxOvers = false;
  let oversStr = "(=max overs)";

  if (isValidNumber_(wickets2Take) && wickets2Take < maxWickets) {
    belowMaxOvers = true;
    oversStr = "(<max overs)";
  }

  return { belowMaxOvers, oversStr };
}

/**
 * Retrieves the players from the playing XI list who did not bowl in the first innings.
 * @param {object} allTablesObj - An object containing all the tables data.
 * @param {Array<string>} playingXIList - The list of players in the playing XI.
 * @returns {Array<string>} An array of player names who did not bowl in the first innings.
 */
function getDidNotBowlPlayers_(bowlingTable, playingXIList) {
  if (isEmpty_(playingXIList)) {
    return [];
  }

  const didNotBowlPlayers = [];

  const playersWhoBolwed = getPlayersWhoBowled_(bowlingTable);
  for (const player of playingXIList) {
    if (!playersWhoBolwed?.includes(player)) {
      didNotBowlPlayers.push(player);
    }
  }

  return didNotBowlPlayers.sort();
}

/**
 * Retrieves the names of players who bowled from the given table.
 * @param {Array<Array<string>>} table - The table containing the bowling data.
 * @returns {Array<string>} An array of player names who bowled.
 */
function getPlayersWhoBowled_(table) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BowlingTableIndex.Player, conditions: {} },
    { index: BowlingTableIndex.Overs, conditions: { gt: 0 } },
  ];

  const bowlingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    bowlingData.push(sanitizePlayerName_(row[0])); // Add only the batter name to the result array
  }

  return bowlingData;
}

/**
 * Retrieves the list of bowlers with smudging best economy.
 * 
 * @param {Object} allTablesObj - The object containing all tables data.
 * @param {boolean} firstInningsCompleted - Indicates if the first innings is completed.
 * @returns {Array} The list of bowlers with smudging best economy.
 */
function getSmudgingListForBestEconomy_(allTablesObj, firstInningsCompleted) {
  if (isEmpty_(allTablesObj) || isEmpty_(firstInningsCompleted)) { return []; }

  const bowlingData = extractBowlingData_(allTablesObj.bowlingTables);
  let bestEconomyObjMaxOvers = getPlayersBestEconomyData_(bowlingData, getMaxOversPerBowler_());
  if (isEmpty_(bestEconomyObjMaxOvers) && !firstInningsCompleted) { return []; }
  let bestEconomy = bestEconomyObjMaxOvers.value;
  if (isEmpty_(bestEconomy) && !firstInningsCompleted) { return []; }
  let runsCondeded = calculateBowlerRunsConcededInMaxOvers_(bestEconomy);

  const smudging = [];

  if (!firstInningsCompleted) {
    const smudging1 = getBowlersAboveBestEconomy_(allTablesObj.bowlingTables[0], bestEconomy, getMaxOversPerBowler_());
    const smudging2 = getBowlersAboveBestEconomyRuns_(allTablesObj.bowlingTables[0], runsCondeded);

    smudging.push(`${innings1str}>${bestEconomy} eco`, ...smudging1);
    smudging.push(`${innings1str}>${runsCondeded} runs`, ...smudging2);
  } else {
    const smudging1BelowMinOvers = getBowlersBelowMinOvers_(allTablesObj.bowlingTables[0]);
    const firstInningsBowlingData = extractBowlingData_(allTablesObj.bowlingTables[0]);
    const firstInningsBestEconomyObjMinOvers = getPlayersBestEconomyData_(firstInningsBowlingData, getMinOversForBowlerEconomy_());

    if (isNotEmpty_(firstInningsBestEconomyObjMinOvers)) {
      const firstInningsBestEconomy = firstInningsBestEconomyObjMinOvers.value;
      const firstInningsRunsConceded = calculateBowlerRunsConcededInMaxOvers_(firstInningsBestEconomy);

      if (isNotEmpty_(firstInningsBestEconomy) && isNotEmpty_(firstInningsRunsConceded)) {
        bestEconomy = isEmpty_(bestEconomy) ? Infinity : bestEconomy;
        runsCondeded = isEmpty_(runsCondeded) ? Infinity : runsCondeded;

        bestEconomy = Math.min(bestEconomy, firstInningsBestEconomy);
        runsCondeded = Math.min(runsCondeded, firstInningsRunsConceded);
      }
    }

    const smudging1MinOversAboveBestEconomy = getBowlersAboveBestEconomy_(allTablesObj.bowlingTables[0], bestEconomy, getMinOversForBowlerEconomy_());
    const smudging2MaxOversAboveBestEconomy = getBowlersAboveBestEconomy_(allTablesObj.bowlingTables[1], bestEconomy, getMaxOversPerBowler_());
    const smudging2AboveBestEconomyRuns = getBowlersAboveBestEconomyRuns_(allTablesObj.bowlingTables[1], runsCondeded);

    smudging.push(`${innings1str}<${getMinOversForBowlerEconomy_()} overs`, ...smudging1BelowMinOvers);
    smudging.push(`${innings1str}>${bestEconomy} eco`, ...smudging1MinOversAboveBestEconomy);
    smudging.push(`${innings2str}>${bestEconomy} eco`, ...smudging2MaxOversAboveBestEconomy);
    smudging.push(`${innings2str}>${runsCondeded} runs`, ...smudging2AboveBestEconomyRuns);
  }

  return smudging;
}

/**
 * Retrieves the bowlers below the specified best economy from the given table.
 * @param {Array} table - The input table containing bowling data.
 * @param {number} bestEconomy - The best economy to filter by.
 * @param {boolean} oversBowled - Whether to include all bowlers or only bowlers with overs greater than the MAX_OVERS_PER_BOWLER constant.
 * @returns {Array|undefined} - The array of bowlers below the best economy or undefined if the table is empty.
 */
function getBowlersAboveBestEconomy_(table, bestEconomy, oversBowled) {
  if (isEmpty_(table) || isEmpty_(bestEconomy) || isEmpty_(oversBowled) || oversBowled <= 0) { return []; }

  const headerConditions = [
    { index: BowlingTableIndex.Player, conditions: {} },//Name
    { index: BowlingTableIndex.Overs, conditions: { gte: oversBowled } },
    { index: BowlingTableIndex.Economy, conditions: { gt: bestEconomy } },
  ];

  const bowlingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    bowlingData.push(sanitizePlayerName_(row[0])); // Add only the bowler name to the result array
  }

  return bowlingData;
}

/**
 * 
 */
function getBowlersAboveBestEconomyRuns_(table, runs) {
  if (isEmpty_(table) || isEmpty_(runs) || runs <= 0) { return []; }

  const headerConditions = [
    { index: BowlingTableIndex.Player, conditions: {} },
    { index: BowlingTableIndex.Runs, conditions: { gt: runs } },
  ];

  const bowlingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    if (isNotEmpty_(row[0]) && isNotEmpty_(row[1])) {
      bowlingData.push(sanitizePlayerName_(row[0])); // Add only the bowler name to the result array
    }
  }

  return bowlingData;
}

/**
 * 
 */
function getBowlersBelowMinOvers_(table) {
  if (isEmpty_(table)) { return []; }

  const headerConditions = [
    { index: BowlingTableIndex.Player, conditions: {} },//Name
    { index: BowlingTableIndex.Overs, conditions: { lt: getMinOversForBowlerEconomy_() } },
  ];

  const bowlingData = [];
  const data = filterHtmlTableDataByConditions_(table, headerConditions, false);

  for (const row of data) {
    if (isNotEmpty_(row[0]) && isNotEmpty_(row[1])) {
      bowlingData.push(sanitizePlayerName_(row[0])); // Add only the bowler name to the result array
    }
  }

  return bowlingData;
}
