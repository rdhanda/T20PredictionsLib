//PromoteSgeets.gs//

function promoteSheetsAllMenu(e) {
  promoteSheetsAll_(e);
}

/**
 * Promotes sheets from a specific batch by calling `promoteSheetsBatch_` function with the corresponding promotion batch enum.
 */
function promoteSheetsBatch1_(e) {
  promoteSheetsBatch_(e, getPromotionSheetsBatch_(Promotion.BATCH1));
}

/**
 * Promotes sheets from a specific batch by calling `promoteSheetsBatch_` function with the corresponding promotion batch enum.
 */
function promoteSheetsBatch2_(e) {
  promoteSheetsBatch_(e, getPromotionSheetsBatch_(Promotion.BATCH2));
}

/**
 * Promotes sheets from a specific batch by calling `promoteSheetsBatch_` function with the corresponding promotion batch enum.
 */
function promoteSheetsBatch3_(e) {
  promoteSheetsBatch_(e, getPromotionSheetsBatch_(Promotion.BATCH3));
}

/**
 * Promotes sheets from the excluded promotion batch by calling `promoteSheetsBatch_` function with the corresponding promotion batch enum.
 */
function promoteSheetsExcluded_(e) {
  promoteSheetsBatch_(e, getPromotionSheetsBatch_(Promotion.EXCLUDED));
}

/**
 * Promotes sheets from all three promotion batches by calling `promoteSheetsBatch_` function with the merged promotion sheets.
 */
function promoteSheetsAll_(e) {
  let batch = getPromotionSheetsBatch_(Promotion.BATCH1).concat(getPromotionSheetsBatch_(Promotion.BATCH2), getPromotionSheetsBatch_(Promotion.BATCH3));
  promoteSheetsBatch_(e, batch);
}

/**
 * Gets the list of sheet names belonging to a promotion batch.
 *
 * @param {Promotion} batch - The promotion batch to retrieve the sheet names for.
 * @return {string[]} An array of sheet names belonging to the specified promotion batch.
 */
function getPromotionSheetsBatch_(batch) {
  const sheetNames = [];
  for (const sheet of getFinalSheetConfigs()) {
    if (sheet.promotion === batch) {
      sheetNames.push(sheet.name);
    }
  }
  return sheetNames.reverse();
}

/**
 * Promotes the given sheets to the active spreadsheet.
 * If the active spreadsheet is not the test environment, it first activates the dashboard sheet and then promotes the sheets.
 * If the active spreadsheet is the test environment, it logs an error message.
 *
 * @param {string[]} sheetList - The list of sheet names to promote.
 */
function promoteSheetsBatch_(e, sheetList) {
  logSTART_(arguments.callee.name);
  log("Source sheet id " + ConfigManager.getConfigClass().TEST_ENV_SPREADSHEET_ID, LogLevel.DEBUG);
  if (isNotTestEnv_()) {
    activateSheet_(getDashboardSheet_());
    let sourceSpreadsheet = SpreadsheetApp.openById(ConfigManager.getConfigClass().TEST_ENV_SPREADSHEET_ID);

    promoteSheets_(sourceSpreadsheet, sheetList);
    doAfterPromoteSheets_(e);
  } else {
    log("ERROR: Sheets promotion is allowed in non TEST envs only. Current env is " + EnvConfigClass.getEnvName(), LogLevel.ERROR);
  }

  activateSheet_(getDashboardSheet_());
  logEND_(arguments.callee.name);
}

/**
 * 
 */
function doAfterPromoteSheets_(e) {
  //Reset Sheet and Form names
  updateActiveSpreadsheetName_();
  updateAttachedFormNames_();

  clearMatchSheets_();
  emptyLogsSheet_();
}

/**
 * Creates missing sheets in the active spreadsheet based on a list of excluded sheet names from a promotion batch.
 * 
 * @returns {void}
 */
function createMissingSheets_() {
  /** @type {string[]} */
  let excludedSheetsList = getPromotionSheetsBatch_(Promotion.EXCLUDED);
  log(`Excluded Sheets List = ${excludedSheetsList}`, LogLevel.DEBUG)

  for (let i = 0; i < excludedSheetsList.length; i++) {
    let sheetName = excludedSheetsList[i];
    let sheet = getSheetByName_(sheetName);
    if (isEmpty_(sheet)) {
      let newSheet = getActiveSpreadsheet_().insertSheet();
      newSheet.setName(sheetName);
      console.log("Sheet [" + sheetName + "] created.");
    }
  }
}

/**
 * Checks if the source and target spreadsheet are not the same.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Spreadsheet} sourceSpreadsheet - The source spreadsheet to compare against the active spreadsheet.
 * 
 * @returns {boolean} Returns true if the source and active spreadsheets are not the same, false otherwise.
 */
function canMigrateSheets_(sourceSpreadsheet) {
  if (sourceSpreadsheet) {
    return (sourceSpreadsheet.getId() != getActiveSpreadsheet_().getId());
  } else {
    return false;
  }
}

/**
 * Promotes a list of sheets from the source spreadsheet to the active spreadsheet.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Spreadsheet} sourceSpreadsheet - The source spreadsheet to copy sheets from.
 * @param {string[]} sheetList - The list of sheet names to promote from the source spreadsheet to the active spreadsheet.
 * 
 * @returns {void}
 */
function promoteSheets_(sourceSpreadsheet, sheetList) {
  console.info(`Sheets List>> ${sheetList}`);

  if (Array.isArray(sheetList)) {
    let overallStartTime = Date.now();
    console.log(`Total Sheets Count "${sheetList.length}"`);
    for (let i = 0; i < sheetList.length; i++) {
      let startTime = Date.now();
      let sheetName = sheetList[i];

      let sourceSheet = sourceSpreadsheet.getSheetByName(sheetName);
      replaceSheet_(sourceSheet, sheetName);

      let endTime = Date.now();
      console.log(`Completed ${i + 1} of ${sheetList.length}, [${sheetName}] in ${convertMilliSeconds_(endTime - startTime)}. Total ${convertMilliSeconds_(endTime - overallStartTime)}.`);

      //If not last sheet, and total time more than 5 minutes, time to stop 
      if ((i + 1) != sheetList.length && (endTime - overallStartTime) > (5 * 60 * 1000)) {
        log("5 minutes up, stopping it now.", LogLevel.WARN);
        throw ("WARNING: It took more than 5 minutes, stopping now. Rerun the migration for remaining batch(es)");
      }
      flushAndWait_();
    }
    createMissingSheets_();
    resetAllSheetsSettings_();
    addAllMenus_();
  }
}

/**
 * Replaces a sheet in the active spreadsheet with a source sheet from the source spreadsheet.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sourceSheet - The sheet to copy from the source spreadsheet.
 * @param {string} targetSheetName - The name of the sheet to replace in the active spreadsheet.
 * 
 * @returns {GoogleAppsScript.Spreadsheet.Sheet} Returns the new sheet that was created from the source sheet.
 */
function replaceSheet_(sourceSheet, targetSheetName) {
  let newSheet = sourceSheet.copyTo(getActiveSpreadsheet_());

  if (newSheet) {
    deleteSheet_(targetSheetName);
    newSheet.setName(targetSheetName);
    resetVisibilityAndProtection_(newSheet);
  }

  return newSheet;
}

/**
 * Deletes a sheet with a given name from the active spreadsheet, if it exists.
 *
 * @param {string} targetSheetName - The name of the sheet to delete.
 */
function deleteSheet_(targetSheetName) {
  let targetSheet = getActiveSpreadsheet_().getSheetByName(targetSheetName);
  if (targetSheet) {
    getActiveSpreadsheet_().deleteSheet(targetSheet);
  } else {
    console.info("Sheet " + targetSheetName + " NOT found.");
  }
}
