//ClearSeries.gs//

/**
 * Activates the schedule sheet and clears the tournament UI, if possible.
 * @function
 * @name clearTournamentUI
 * @param {Event} e - The event object.
 * @returns {boolean} - Returns `true` if the tournament was cleared successfully.
 */
function clearSeriesMenu(e) {
  activateSheet_(getScheduleViewSheet_());
  return tryClearSeries_(e);
}

/**
 * Attempts to clear the current tournament.
 * @function
 * @name tryClearTournament_
 * @param {Event} e - The event object.
 * @returns {boolean} - Returns `true` if the tournament was cleared successfully.
 */
function tryClearSeries_(e) {
  if (isNotProdEnv_() || isSeriesNotActive_() || canClearWithoutConfirmation_()) {
    clearSeries_();
    return true;
  } else {
    handleWarningAndErrors_(e, LogLevel.WARN, "Can not clear the current Series. There are entries in some of [" + dailyBetsSheetName + "], [" + dailyBetsClosedSheetName + "], [" + seriesBetsSheetName + "], [" + seriesBetsModSheetName + "], [" + dailyBetsResultsSheetName + "] sheets. You need to backup and reset tournament first.");
    return false;
  }
}

/**
 * Checks if the sheets can be cleared without user confirmation.
 * @function
 * @name canClearWithoutConfirmation_
 * @returns {boolean} - Returns `true` if the sheets can be cleared without user confirmation.
 */
function canClearWithoutConfirmation_() {
  const dailyOpen = getDailyBetsSheet_().getDataRange().getValues().length;
  const dailyClosed = getDailyBetsClosedSheet_().getDataRange().getValues().length;
  const overallPredictions = getSeriesBetsSheet_().getDataRange().getValues().length;
  const dailyResults = getDailyResultsSheet_().getDataRange().getValues().length;

  return dailyOpen <= 1 && dailyClosed <= 1 && overallPredictions <= 1 && dailyResults <= 1;
}


/**
 * Clears the tournament sheets and data.
 * @function
 * @name clearTournament_
 */
function clearSeries_(e) {
  logSTART_(clearSeries_.name);

  clearMatchSheets_(e);

  clearContent_(getScheduleSheet_(), scheduleTableSheetObj.clearRanges);
  clearContent_(getPlayersSheet_(), playersSheetObj.clearRanges);
  clearContent_(getSeriesResultsSheet_(), overallResultsSheetObj.clearRanges);
  clearContent_(getOverallPredictionsModSheet_(), overallPredictionsModSheetObj.clearRanges);
  clearContent_(getAutoPicksLogSheet_(), autoBetsLogSheetObj.clearRanges);
  clearContent_(getStandingsTableSheet_(), standingsTableSheetObj.clearRanges);
  clearContent_(getDailyPointsSheet_(), dailyPointsSheetObj.clearRanges);
  clearContent_(getPlayersExcludedSheet_(), playersExcludedSheetObj.clearRanges);
  clearContent_(getPredictoSheet_(), predictoSheetObj.clearRangesPredicto);
  clearContent_(getPredictoSheet_(), predictoSheetObj.clearRangesPlayerCategories);
  clearDashboardBetsLists_();

  deleteRows_(getDailyResultsSheet_(), dailyResultsSheetObj.deleteFromRowNum, dailyResultsSheetObj.emptyRowsToKeep);
  deleteRows_(getDailyBetsSheet_(), dailyPredictionsSheetObj.deleteFromRowNum, dailyPredictionsSheetObj.emptyRowsToKeep);
  deleteRows_(getDailyBetsClosedSheet_(), 1, dailyPredictionsSheetObj.emptyRowsToKeep);
  deleteRows_(getDailyBetsBkpSheet_(), 1, dailyPredictionsSheetObj.emptyRowsToKeep);
  deleteRows_(getSeriesBetsSheet_(), overallPredictionsSheetObj.deleteFromRowNum, overallPredictionsSheetObj.emptyRowsToKeep);
  deleteRows_(getSeriesBetsBkpSheet_(), overallPredictionsSheetObj.deleteFromRowNum, overallPredictionsSheetObj.emptyRowsToKeep);

  clearTopBetsSheet_(e);
  emptyLogsSheet_(e);

  clearAttachedForms_(e);
  clearActiveSeriesStatus_();
  removeActiveSeriesFromCache_();
  flushAndWait_();

  updateActiveSpreadsheetName_();
  updateAttachedFormNames_();

  removeAllCachedData_(e);

  flushAndWait_();
  logEND_(clearSeries_.name);
}

/**
 * Clears the responses in the attached forms for daily and overall predictions sheets
 * and resets the form settings.
 * 
 * @returns {void}
 */
function clearAttachedForms_(e) {
  toast_("Clear Attached Forms", "Starting");

  let dailyPredictionsSheetForm = getDailyBetsForm_();
  removeCachedFormTitles_(dailyPredictionsSheetForm);

  dailyPredictionsSheetForm.deleteAllResponses();
  resetDailyBetsForm_(e, dailyPredictionsSheetForm);

  let overallPredictionsSheetForm = getSeriesBetsForm_();
  removeCachedFormTitles_(overallPredictionsSheetForm);

  overallPredictionsSheetForm.deleteAllResponses();
  resetOverallPredictionsForm_(overallPredictionsSheetForm);

  toast_("Clear Attached Forms", "Done");
}

/**
 * Clears content of the sheet in the rnage list specified
 */
function clearContent_(sheet, clearSetupRangeList) {
  toast_("Clearing Content", sheet.getName());
  removeSheetProtection_(sheet);
  activateSheet_(sheet);

  sheet.getRangeList(clearSetupRangeList).clearContent();
  sheet.getRangeList(clearSetupRangeList).clearNote();
}

/**
 * Delete rows from enteries or results sheets
 */
function deleteRows_(sheet, deleteFromRowNum, emptyRowsToKeep) {
  toast_("Deleting Rows", sheet.getName());
  removeSheetProtection_(sheet);
  activateSheet_(sheet);

  let maxRows = sheet.getMaxRows();
  let lastRow = sheet.getLastRow();
  let startRowNum = deleteFromRowNum + emptyRowsToKeep;

  if (lastRow >= deleteFromRowNum && maxRows >= deleteFromRowNum) {
    sheet.insertRowsBefore(deleteFromRowNum, emptyRowsToKeep);
    let rows2Delete = sheet.getMaxRows() - startRowNum + 1;
    sheet.deleteRows(startRowNum, rows2Delete);
  } else if (lastRow === 0 && maxRows > emptyRowsToKeep) {
    let rows2Delete = maxRows - startRowNum + 1;
    sheet.deleteRows(startRowNum, rows2Delete);
  }
}

/**
 * Clears the content of the active series status range in the series table sheet.
 */
function clearActiveSeriesStatus_() {
  activateSheet_(getSeriesViewSheet_());
  getSeriesTableSheet_().getRange(seriesSheetObj.activeStatusRange).clearContent();
}
