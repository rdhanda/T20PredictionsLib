//Bot Submissions//

/**
 * Handles the UI event for auto-submitting predictions.
 * @param {Event} e - The UI event.
 */
function autoSubmitBetsMenu(e) {
  autoSubmitBets_(e);
}

/**
 * Automatically submits predictions for eligible punters and sends emails.
 * @param {EventObject} e The event object triggering the function.
 */
function autoSubmitBets_(e) {
  if (!ConfigManager.getConfigClass().AUTO_SUBMIT_BETS_ENABLED) {
    return;
  }

  console.time(autoSubmitBets_.name);

  const punters = PuntersUtils.getPuntersForAutoSubmission_(e);
  if (isNotEmpty_(punters)) {
    console.log(`Auto-submitting predictions for punters '${PuntersUtils.getPunterNames_(punters)}'`);

    const betsMap = getBetsMap_(e, punters);
    submitBets_(betsMap);
    EmailAutoSubmission.sendEmails(betsMap, ScheduleUtils.getFirstOpenMatchShortDate());
  }

  console.timeEnd(autoSubmitBets_.name);
}

/**
 * Prepares the bets map for auto submission.
 * 
 * @param {Object} e - The event object.
 * @param {Array<Punter>} punters - The array of punters.
 * @returns {Map} The predictions map.
 */
function getBetsMap_(e, punters) {
  const betsMap = new Map();
  const teamWinCounts = getTeamWinCounts_(e);
  const openMatches = ScheduleUtils.getOpenMatches();

  for (const openMatch of openMatches) {
    const title = openMatch.getTitle();
    const homeTeam = openMatch.getHomeTeam()
    const awayTeam = openMatch.getAwayTeam();

    const newTitle = title + BOT_SUBMITTED_EMOJI;

    const allTeamPlayers = getAllProbablePlayers_(homeTeam, awayTeam);
    const botBets = getAllBets_(newTitle, homeTeam, awayTeam, allTeamPlayers, teamWinCounts).values;

    for (const punter of punters) {
      if (punter.isBot() || punter.isAutoSubmitSameAsBot()) {//Assumption: There will alwaybe ONE Bot, if there are more, and we need diffeent, then remove punter.isBot, only check sameAsBot
        addBetsToPunterMap_(punter.getName(), botBets, betsMap);
        console.info(`Using SAME AS bot bets for '${punter.getName()}'`);
      } else {
        const ownBets = getAllBets_(`${newTitle}${STAR_CHAR}`, homeTeam, awayTeam, allTeamPlayers, teamWinCounts).values;
        addBetsToPunterMap_(punter.getName(), ownBets, betsMap);
        console.log(`Using THEIR OWN random bets for '${punter.getName()}'`);
      }
    }
  }

  return betsMap;
}

/**
 * Adds bets to the specified punter's predictions map.
 * If the punter already exists in the map, the predictions are appended to their existing predictions.
 * If the punter does not exist in the map, a new entry is created with the punter's name and predictions.
 * @param {string} punterName The name of the punter.
 * @param {Array} bets The predictions to add to the punter's map entry.
 * @param {Map} betsMap The map containing punter names as keys and their predictions as values.
 */
function addBetsToPunterMap_(punterName, bets, betsMap) {
  const previousMatchBets = betsMap.get(punterName) || [new Date(), undefined, punterName];
  betsMap.set(punterName, previousMatchBets.concat(bets));
}

/**
 * Submits predictions data to Google Sheets and logs the submission.
 * 
 * @param {Map<string, Array>} betsMap A map containing punter names as keys and their predictions as values.
 */
function submitBets_(betsMap) {
  const sheetsData = { dailyPicks: [], dailyPicksBkp: [], autoPicksLog: [] };

  for (const [punterName, bets] of betsMap.entries()) {
    console.info(`Submitting Auto Predictions for Punter "${punterName}", Predictions Used: ${JSON.stringify(bets)}`);

    sheetsData.dailyPicks.push(bets);
    sheetsData.dailyPicksBkp.push(bets);

    const punter = bets[2];
    const submissionTime = bets[0];
    const matchDate = extractMatchShortDate_(bets[3]);
    sheetsData.autoPicksLog.push([punter, submissionTime, matchDate]);
  }

  appendDataToSheet_(getDailyBetsSheet_(), sheetsData.dailyPicks);
  appendDataToSheet_(getDailyBetsBkpSheet_(), sheetsData.dailyPicksBkp);
  appendDataToSheet_(getAutoPicksLogSheet_(), sheetsData.autoPicksLog, colNameToNum_(autoBetsLogSheetObj.punterNameColName));
}
