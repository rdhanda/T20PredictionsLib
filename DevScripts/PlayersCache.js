//Teams and Players Cache Objects//

const teamIdsPlayersCache_ = {
  playingTeamIDs: null,
  players: null
};

/**
 * Removes the cached data for playing team IDs and invalidates the cache.
 */
function removePlayingTeamIDsCachedData_() {
  teamIdsPlayersCache_.playingTeamIDs = null;
}

/**
 * Removes the cached data for players and invalidates the cache.
 */
function removePlayersCachedData_() {
  teamIdsPlayersCache_.players = null;
}

/**
 * Retrieves the cached data for playing team IDs. If not initialized, refreshes the cache first.
 * @returns {Array<string> | null} Cached data for playing team IDs.
 */
function getPlayingTeamIDsCachedData_() {
  if (teamIdsPlayersCache_.playingTeamIDs == null) {
    refreshPlayingTeamIDsCachedData_();
  }
  return teamIdsPlayersCache_.playingTeamIDs;
}

/**
 * Retrieves the cached data for players. If not initialized, refreshes the cache first.
 * @returns {Object<string, Object>} Cached data for players.
 */
function getPlayersCachedData_() {
  if (teamIdsPlayersCache_.players == null) {
    refreshPlayersCachedData_();
  }
  return teamIdsPlayersCache_.players;
}

/**
 * Refreshes the cached data for playing team IDs.
 */
function refreshPlayingTeamIDsCachedData_() {
  const allData = getDataFromSheet_(playersSheetName);

  const teamColStartIdx = colNameToIndex_(playersSheetObj.teamColumnStartColName);
  const teamColEndIdx = colNameToIndex_(playersSheetObj.teamColumnEndColName);

  const teamIDs = allData[0].slice(teamColStartIdx, teamColEndIdx + 1).filter(teamID => isNotEmpty_(teamID));

  teamIdsPlayersCache_.playingTeamIDs = teamIDs;
}

/**
 * Refreshes the cached data for players.
 */
function refreshPlayersCachedData_() {
  const allData = getDataFromSheet_(playersSheetName);

  const teamColStartIdx = colNameToIndex_(playersSheetObj.teamColumnStartColName);
  const teamColEndIdx = colNameToIndex_(playersSheetObj.teamColumnEndColName);
  const playerCategories = playersSheetObj.playerCategories;

  const playersData = {};

  for (let i = teamColStartIdx; i < teamColEndIdx + 1; i++) {
    const teamName = allData[0][i].trim();

    if (isEmpty_(teamName)) { continue; }
    const teamPlayers = {};

    for (const [category, { startRow, endRow }] of Object.entries(playerCategories)) {
      const categoryPlayers = [];

      for (let j = startRow; j <= endRow; j++) {
        const cellValue = String(allData[j - 1][i]);

        if (cellValue) {
          const [playerName, playerType = PlayerTypes.ALLROUNDERS] = cellValue.split("\n");
          categoryPlayers.push({ playerName, playerType });
        }
      }
      teamPlayers[category] = categoryPlayers;
    }
    playersData[teamName] = teamPlayers;
  }

  teamIdsPlayersCache_.players = playersData;
}
