//CacheUtils.gs//

const MaxCacheExpirationInSeconds = 21600;
const FormTitlesCacheKeyPrefix = `FormTitles_`;
const PlayingXIDataCacheKeyPrefix = "PlayingXI_";
const MatchSheetDataCacheKeyPrefix = "MatchSheet_";
const TeamsPlayersDataCacheKey = "TeamsPlayers";
const SystemLogsDataCacheKey = "SystemLogs";
const LockTimestampPrefix = `LockTimestamp_`;
const ActiveSeriesCacheKey = "ActiveSeries";

/**
 * Gets the document cache.
 * @returns {Cache} The script cache.
 */
function getDocumentsCache_() {
  return CacheService.getDocumentCache();
}

/**
 * Stores an object property in the script cache.
 * 
 * @param {string} key - The key under which the object will be stored.
 * @param {Object} obj - The object to be stored.
 */
function putInDocumentCache_(key, obj, expirationInSeconds = MaxCacheExpirationInSeconds) {
  const funcName = `${putInDocumentCache_.name}${key}`;

  const cache = getDocumentsCache_();
  const objJSON = JSON.stringify(obj);
  cache.put(key, objJSON, expirationInSeconds);

  console.timeEnd(funcName);
}

/**
 * Retrieves an object property from the script cache.
 * @param {string} key - The key of the object to be retrieved.
 * @returns {Object} The retrieved object.
 */
function getFromDocumentCache_(key) {
  const cache = getDocumentsCache_();
  const value = cache.get(key);
  if (value) { return JSON.parse(value); }
  else { return; }
}

/**
 * Deletes a property from the environment properties.
 * @param {string} key - The key of the property to be deleted.
 */
function removeFromDocumentCache_(key) {
  console.time(`${removeFromDocumentCache_.name}${key}`);

  const cache = getDocumentsCache_();
  const value = cache.get(key);
  if (value) {
    cache.remove(key);
    console.timeEnd(`${removeFromDocumentCache_.name}${key}`);
  }
}

//FORM CACHE UTILS//

/**
 * Generates a cache key for form titles using the form ID.
 *
 * @param {string} formId - The ID of the Google Form.
 * @returns {string} - The generated cache key.
 */
function generateFormTitlesCacheKey_(formId) {
  return `${FormTitlesCacheKeyPrefix}${formId}`;
}

/**
 * Retrieves form titles from the script cache or fetches and caches them if not present.
 *
 * @param {Form} form - The Google Form object.
 * @returns {Array<string>} An array of form titles.
 */
function getFormTitles_(form) {
  const cacheKey = generateFormTitlesCacheKey_(form.getId());
  let formTitles = getFromDocumentCache_(cacheKey);

  if (!formTitles) { formTitles = setFormTitlesCache_(form, cacheKey); }

  return formTitles;
}

/**
 * Caches form titles in the script cache.
 *
 * @param {Form} form - The Google Form object.
 * @param {string} cacheKey - The cache key for form titles.
 * @returns {string} - The JSON string of form titles.
 */
function setFormTitlesCache_(form, cacheKey) {
  const items = form.getItems();
  const titles = items.map(item => item.getTitle());

  putInDocumentCache_(cacheKey, titles)
  return titles;
}

/**
 * Clears the cached form titles in the script cache.
 *
 * @param {Form} form - The Google Form object.
 */
function removeCachedFormTitles_(form) {
  if (form) {
    const cacheKey = generateFormTitlesCacheKey_(form.getId());
    removeFromDocumentCache_(cacheKey);
  }
}

//
function removeAllCachedDataMenu(e) {
  removeAllCachedData_(e);
}
/**
 * Removes all cached data.
 * 
 * @param {Event} e - The event object.
 */
function removeAllCachedData_(e) {
  removeActiveSeriesFromCache_();
}

//Playing XI Cache//

/**
 * Generates a cache data key for playing XI data.
 * 
 * @param {string} sheetName - The name of the sheet.
 * @returns {string} The cache data key for playing XI data.
 */
function generatePlayingXICacheDataKey_(sheetName) {
  return `${PlayingXIDataCacheKeyPrefix}${sheetName}`;
}

function generateMatchSheetCacheDataKey_(sheetName) {
  return `${MatchSheetDataCacheKeyPrefix}${sheetName}`;
}
function getMatchSheetCachedData_(sheetName) {
  const objKey = generateMatchSheetCacheDataKey_(sheetName);
  return getFromDocumentCache_(objKey);
}
function setMatchSheetCachedData_(sheetName, matchSheetData) {
  const objKey = generateMatchSheetCacheDataKey_(sheetName);
  putInDocumentCache_(objKey, matchSheetData);
}
function removeMatchSheetCachedData_(sheetName) {
  const objKey = generateMatchSheetCacheDataKey_(sheetName);
  removeFromDocumentCache_(objKey);
}

/**
 * Retrieves the cached playing XI data for a specific sheet.
 * 
 * @param {string} sheetName - The name of the sheet.
 * @returns {Object} The cached playing XI data for the specified sheet.
 */
function getPlayingXICachedData_(sheetName) {
  const objKey = generatePlayingXICacheDataKey_(sheetName);
  return getFromDocumentCache_(objKey);
}

/**
 * Sets the cached playing XI data for a specific sheet.
 * 
 * @param {string} sheetName - The name of the sheet.
 * @param {Object} playingXIData - The playing XI data to be cached.
 */
function setPlayingXICachedData_(sheetName, playingXIData) {
  const objKey = generatePlayingXICacheDataKey_(sheetName);
  putInDocumentCache_(objKey, playingXIData);
}

/**
 * Removes the cached playing XI data for a specific sheet.
 * 
 * @param {string} sheetName - The name of the sheet.
 */
function removePlayingXICachedData_(sheetName) {
  const objKey = generatePlayingXICacheDataKey_(sheetName);
  removeFromDocumentCache_(objKey);
}

//Active Series Cache//

/**
 * Puts the active series in the cache.
 * @param {Series} activeSeries - The active series object to cache.
 */
function putActiveSeriesInCache_(activeSeries) {
  putInDocumentCache_(ActiveSeriesCacheKey, activeSeries.toObject());
}

/**
 * Gets the active series from the cache.
 * @returns {Series|null} The active series object from the cache, or null if not found.
 */
function getActiveSeriesFromCache_() {
  const cachedObject = getFromDocumentCache_(ActiveSeriesCacheKey);
  if (cachedObject) {
    return Series.fromObject(cachedObject);
  }
  return null;
}

/**
 * Removes the active tournament object from the environment properties.
 */
function removeActiveSeriesFromCache_() {
  removeFromDocumentCache_(ActiveSeriesCacheKey);
}

//Logs cache//
/**
 * Retrieves the cached system logs data.
 * 
 * @returns {Array} The cached system logs data.
 */
function getLogsCachedData_() {
  return getFromDocumentCache_(SystemLogsDataCacheKey)
}

/**
 * Stores the system logs data in the cache.
 * 
 * @param {Array} logRows - The system logs data to be stored.
 */
function putLogsInCache_(logRows) {
  putInDocumentCache_(SystemLogsDataCacheKey, logRows);
}

/**
 * Removes the cached system logs data.
 * IMP: Do not remove as part removing all other logs
 */
function removeLogsFromCache_() {
  removeFromDocumentCache_(SystemLogsDataCacheKey);
}
