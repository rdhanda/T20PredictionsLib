//TriggersExecution.gs//

/**
 * Triggers the process of updating form submission data.
 * @param {Event} e - The event object triggered by the script.
 * @throws {Error} If the `onFormSubmit_` function is not defined.
 */
function onFormSubmitT(e) {
  Triggers.executeOnFormSubmit(e, onFormSubmitT.name);
}

//Match Results - START
/**
 * Creates a trigger to execute the update function for match 1 results at intervals defined in the environment configuration.
 * Deletes any existing triggers for this function to prevent duplicates.
 */
function startUpdateMatch1ResultsT() {
  Triggers.createMinuteTrigger(execUpdateMatch1ResultsT.name, EnvConfigClass.getUpdateMatchResultsTriggerEveryMinutes());
  TriggersUtils.deleteTriggerByFuncName(startUpdateMatch1ResultsT.name);
}

/**
 * Creates a trigger to execute the update function for match 2 results at intervals defined in the environment configuration.
 * Deletes any existing triggers for this function to prevent duplicates.
 */
function startUpdateMatch2ResultsT() {
  Triggers.createMinuteTrigger(execUpdateMatch2ResultsT.name, EnvConfigClass.getUpdateMatchResultsTriggerEveryMinutes());
  TriggersUtils.deleteTriggerByFuncName(startUpdateMatch2ResultsT.name);
}

/**
 * Creates a trigger to execute the update function for match 3 results at intervals defined in the environment configuration.
 * Deletes any existing triggers for this function to prevent duplicates.
 */
function startUpdateMatch3ResultsT() {
  Triggers.createMinuteTrigger(execUpdateMatch3ResultsT.name, EnvConfigClass.getUpdateMatchResultsTriggerEveryMinutes());
  TriggersUtils.deleteTriggerByFuncName(startUpdateMatch3ResultsT.name);
}

/**
 * Triggers the process of updating match results data.
 * @param {Event} e - The event object triggered by the script.
 * @throws {Error} If the `updateAllMatchResults_` function is not defined.
 */
function execUpdateMatch1ResultsT(e) {
  Triggers.executeRecurringTaskFunction(e, execUpdateMatch1ResultsT.name, execUpdateMatch1Results);
}

/**
 * Triggers the process of updating match results data.
 * @param {Event} e - The event object triggered by the script.
 * @throws {Error} If the `updateAllMatchResults_` function is not defined.
 */
function execUpdateMatch2ResultsT(e) {
  Triggers.executeRecurringTaskFunction(e, execUpdateMatch2ResultsT.name, execUpdateMatch2Results);
}

/**
 * Triggers the process of updating match results data.
 * @param {Event} e - The event object triggered by the script.
 * @throws {Error} If the `updateAllMatchResults_` function is not defined.
 */
function execUpdateMatch3ResultsT(e) {
  Triggers.executeRecurringTaskFunction(e, execUpdateMatch3ResultsT.name, execUpdateMatch3Results);
}
//Match Results - END

//Bets Open/Close - START
/**
 * Creates daily scheduled triggers.
 * @param {Event} e - The event object triggered by the script.
 */
function creatDailyScheduledTriggersT(e) {
  Triggers.executeCreateDailyScheduledTriggers(e);
}

/**
 * Triggers the bets close process.
 * @param {Event} e - The event object triggered by the script.
 */
function closeDailyBetsScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, closeDailyBetsScheduledT.name, Bets.closeBets);
}

/**
 * Triggers the bets open process.
 * @param {Event} e - The event object triggered by the script.
 */
function openDailyBetsScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, openDailyBetsScheduledT.name, Bets.openBets);
}

/**
 * Triggers sending reminders.
 * @param {Event} e - The event object triggered by the script.
 */
function sendRemindersScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, sendRemindersScheduledT.name, sendReminders_);
}
//Bets Open/Close - END

//Generic Updates - START
/**
 * Updates the schedule sheet based on a scheduled trigger.
 * @param {Event} e - The event object triggered by the script.
 */
function updateScheduleSheetScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, updateScheduleSheetScheduledT.name, updateScheduleSheet_);
}

/**
 * Updates the players list based on a scheduled trigger.
 * @param {Event} e - The event object triggered by the script.
 */
function updatePlayersScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, updatePlayersScheduledT.name, updatePlayers_);
}

/**
 * Updates the series results based on a scheduled trigger.
 * @param {Event} e - The event object triggered by the script.
 */
function updateSeriesResultsScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, updateSeriesResultsScheduledT.name, updateSeriesResults_);
}

/**
 * Updates the standings table based on a scheduled trigger.
 * @param {Event} e - The event object triggered by the script.
 */
function updateStandingsTableScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, updateStandingsTableScheduledT.name, updateStandingsTable_);
}

/**
 * Updates the player stats based on a scheduled trigger.
 * @param {Event} e - The event object triggered by the script.
 */
function updatePlayerStatsScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, updatePlayerStatsScheduledT.name, updatePlayerStats_);
}

/**
 * Updates the Predicto sheet based on a scheduled trigger.
 * 
 * @param {Event} e - The event object triggered by the script.
 */
function updatePredictoSheetScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, updatePredictoSheetScheduledT.name, updatePredictoSheet_);
}

/**
 * Resets all sheet settings based on a scheduled trigger.
 * @param {Event} e - The event object triggered by the script.
 */
function resetAllSheetSettingsScheduledT(e) {
  Triggers.executeOneTimeTaskFunction(e, resetAllSheetSettingsScheduledT.name, resetAllSheetsSettings_);
}

/**
 * Triggers the daily update process, which updates information such as scores and stats.
 * @param {Event} e - The event object triggered by the script.
 * @throws {Error} If the `doDailyUpdates_` function is not defined.
 */
function doDailyUpdatesT(e) {
  Triggers.executeRecurringTaskFunction(e, doDailyUpdatesT.name, doDailyupdates_);
}

/**
 * Executes a series of daily updates in a Google Spreadsheet.
 * The updates include refreshing the tournament sheet, updating the schedule sheet teams and URL, updating players sheets,
 * updating the overall results sheets and updating the overall results.
 * @param {Event} e - The event object provided by the Google Apps Script runtime.
 */
function doDailyupdates_(e) {
  if (!EnvConfigClass.isAutoModeEnabled()) { return; }

  logSTART_(arguments.callee.name);

  Triggers.createAllScheduledUpdateTriggers();

  flushAndWait_();
  logEND_(arguments.callee.name);
}
//Generic Updates - END
