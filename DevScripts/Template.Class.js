//Template.Class.gs//

/**
 * Represents a Template.
 * @class
 */
class TemplateClass {
  /**
   * Creates an instance of TemplateClass.
   * @param {number} id - The ID of the TemplateClass.
   * @param {string} name - The name of the TemplateClass.
   */
  constructor(id, name) {
    var id_ = id;
    var name_ = name;
    
    /**
     * Retrieves the ID of the TemplateClass.
     * @returns {number} The ID of the TemplateClass.
     */
    this.getId = function () { return id_; };
    
    /**
     * Retrieves the name of the TemplateClass.
     * @returns {string} The name of the TemplateClass.
     */
    this.getName = function () { return name_; };
    
    /**
     * Sets the ID of the TemplateClass.
     * @param {number} id - The new ID value to set.
     */
    this.setId = function (id) { id_ = id; };
    
    /**
     * Sets the name of the TemplateClass.
     * @param {string} name - The new name value to set.
     */
    this.setName = function (name) { name_ = name; };
  }

  /**
   * Serializes the TemplateClass instance to a plain object.
   * @returns {Object} The plain object representation of the TemplateClass.
   */
  toObject() {
    return {
      id: this.getId(),
      name: this.getName(),
    };
  }

  /**
   * Static method to create a TemplateClass instance from a plain object.
   * @param {Object} obj - The plain object representation of a TemplateClass.
   * @returns {TemplateClass} The TemplateClass instance.
   */
  static fromObject(obj) {
    return new TemplateClass(
      obj.id,
      obj.name,
    );
  }
}
