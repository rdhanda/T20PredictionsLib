//TriggesCRUD.gs//

/**
 * Creates the triggers menu.
 * 
 * @param {Event} e - The event object triggered by the script.
 */
function createTriggersMenu(e) {
  Triggers.createTriggers(e);
}

/**
 * Deletes all triggers via the menu.
 * 
 * @param {Event} e - The event object triggered by the script.
 */
function deleteAllTriggersMenu(e) {
  TriggersUtils.deleteAllTriggers(e);
}

/**
 * Class for triggers creation and execution functions
 */
class Triggers {
  /**
   * Creates all necessary triggers for the application.
   */
  static createTriggers(e) {
    logSTART_(this.createTriggers.name);

    TriggersUtils.deleteAllTriggers(e);

    this.createOnFormSubmitTrigger();
    this.createDailyMainTrigger(0, 30);
    this.createDailyUpdatesTrigger(1, 30);
    this.executeCreateDailyScheduledTriggers(e);
    this.createMatchResultsTriggers(e);

    logEND_(this.createTriggers.name);
  }

  /**
   * Creates a new trigger for the given function to run on form submission in the active spreadsheet.
   *
   */
  static createOnFormSubmitTrigger() {
    const funcName = onFormSubmitT.name;
    if (!TriggersUtils.isTriggerExists(funcName)) {
      const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
      let trigger = ScriptApp.newTrigger(fullFuncName)
        .forSpreadsheet(getActiveSpreadsheet_())
        .onFormSubmit()
        .create();

      const newTriggerUniqueId = TriggersUtils.saveTrigger(trigger);
      console.info(`CREATED Trigger for "${fullFuncName}" with ID "${newTriggerUniqueId}"`);
    }
  }

  /**
   * Schedules the main daily trigger at a specific hour and nearMinute.
   * 
   * @param {number} hour - The hour at which to schedule the trigger.
   * @param {number} nearMinute - The approximate minute at which to schedule the trigger.
   */
  static createDailyMainTrigger(hour, nearMinute) {
    this.createDailyTrigger(creatDailyScheduledTriggersT.name, hour, nearMinute);
  }

  /**
   * Creates a trigger that runs the doDailyUpdatesT function daily at the specified hour and near minute.
   * @param {number} hour - The hour of the day when the function should run, in the time zone of the script.
   * @param {number} nearMinute - The minute of the hour when the function should run, in the time zone of the script.
   */
  static createDailyUpdatesTrigger(hour, nearMinute) {
    this.createDailyTrigger(doDailyUpdatesT.name, hour, nearMinute);
  }

  /**
   * Executes the main daily tasks, scheduling subsequent reminders and betting actions.
   * 
   * @param {Object} e - The event object passed by the trigger.
   */
  static executeCreateDailyScheduledTriggers(e) {
    const funcName = Triggers.executeCreateDailyScheduledTriggers.name;
    console.time(funcName);

    try {
      if (!ScheduleUtils.isAnyMatchOpen()) {
        openDailyBetsScheduledT(e);
      }

      if (ScheduleUtils.isAnyMatchOpen()) {
        const matchStartTime = ScheduleUtils.getFirstOpenMatchStartTimeMST();
        if (matchStartTime) {
          const betsReminderTime = addMinutes_(matchStartTime, EnvConfigClass.getAddMinutesToSendReminders());
          const betsCloseTime = addMinutes_(matchStartTime, EnvConfigClass.getAddMinutesToCloseDailyBets());

          this.createScheduledTrigger(sendRemindersScheduledT.name, betsReminderTime);
          this.createScheduledTrigger(closeDailyBetsScheduledT.name, betsCloseTime);
          this.createScheduledTrigger(openDailyBetsScheduledT.name, matchStartTime);
        }
      } else {
        log(`NO Open matches found.`, LogLevel.WARN);
      }

      TriggersUtils.updateTriggerExecutionTime(funcName);
    } catch (error) {
      log(`${error}, Stack: ${error.stack}`, LogLevel.ERROR);
    } finally {
    }

    console.timeEnd(funcName);
  }

  /**
   * Creates triggers for updating match results based on the match schedule.
   * 
   * @param {Object} e - The event object triggered by the script.
   */
  static createMatchResultsTriggers(e) {
    getMatchSheetNames_().forEach((sheetName) => {
      const sheet = getSheetByName_(sheetName);
      const matchID = getMatchID_(sheet);
      const resultStatus = getMatchResultStatus_(sheet);
      if (isNotEmpty_(matchID) && ActiveMatchResultStatusList.includes(resultStatus)) {
        const matchScheduledClosedTime = ScheduleUtils.getMatchScheduleClosedTime(matchID);
        Triggers.createMatchResultsStartScheduledTrigger(sheetName, matchScheduledClosedTime);
      }
    });
  }

  /**
   * Creates a scheduled trigger for updating match results based on the specified sheet name.
   * @param {string} sheetName - The name of the sheet for which the trigger should be created.
   * @param {number} triggerTime - The time interval in minutes for the trigger.
   * @throws {Error} If no matching configuration is found for the sheetName.
   */
  static createMatchResultsStartScheduledTrigger(sheetName, triggerTime) {
    let execFuncName = TriggersUtils.getMatchResultsExecTriggerFuncName(sheetName);
    if (execFuncName) {
      if (!TriggersUtils.getTriggerByFuncName(execFuncName)) {
        let startFuncName = TriggersUtils.getMatchResultsStartTriggerFuncName(sheetName);
        if (startFuncName) {
          this.createScheduledTrigger(startFuncName, triggerTime);
        }
      }
    }
  }

  /**
   * Creates a scheduled trigger to update schedule sheet
   */
  static createUpdateScheduleSheetScheduledTrigger() {
    const triggerTime = new Date();
    this.createScheduledTrigger(updateScheduleSheetScheduledT.name, triggerTime);
  }

  /**
   * Creates a scheduled trigger to update players
   */
  static createUpdatePlayersScheduledTrigger() {
    const triggerTime = new Date();
    this.createScheduledTrigger(updatePlayersScheduledT.name, triggerTime);
  }

  /**
   * Creates a scheduled trigger to update series results
   */
  static createUpdateSeriesResultsScheduledTrigger() {
    const triggerTime = new Date();
    this.createScheduledTrigger(updateSeriesResultsScheduledT.name, triggerTime);
  }

  /**
   * Creates a scheduled trigger to update standings table
   */
  static createUpdateStandingsTableScheduledTrigger() {
    const triggerTime = new Date();
    this.createScheduledTrigger(updateStandingsTableScheduledT.name, triggerTime);
  }

  /**
    * Creates a scheduled trigger for updating player stats.
    * 
    * This method sets up a scheduled trigger to run the `updatePlayerStatsScheduledT` function at a specified time.
    */
  static createUpdatePlayerStatsScheduledTrigger() {
    const triggerTime = new Date();
    this.createScheduledTrigger(updatePlayerStatsScheduledT.name, triggerTime);
  }

  /**
   * Creates a scheduled trigger for updating the Predicto sheet.
   * 
   * This method sets up a scheduled trigger to run the `updatePredictoSheetScheduledT` function at a specified time.
   */
  static createUpdatePredictoSheetScheduledTrigger() {
    const triggerTime = new Date();
    this.createScheduledTrigger(updatePredictoSheetScheduledT.name, triggerTime);
  }

  /**
   * Creates a scheduled trigger to reset all sheet settings
   */
  static createResetAllSheetSettingsScheduledTrigger() {
    const triggerTime = addMinutes_(new Date(), 5);//After 5 minutes
    this.createScheduledTrigger(resetAllSheetSettingsScheduledT.name, triggerTime);
  }

  //******************************************************************************************//
  /**
   * Creates a daily trigger for a function.
   *
   * @param {string} funcName The name of the function to trigger.
   * @param {number} hour The hour of the day when the trigger should fire.
   * @param {number} nearMinute The minute of the hour when the trigger should fire.
   */
  static createDailyTrigger(funcName, hour, nearMinute) {
    if (!TriggersUtils.isTriggerExists(funcName)) {
      const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
      const trigger = ScriptApp.newTrigger(fullFuncName)
        .timeBased()
        .atHour(hour)
        .nearMinute(nearMinute)
        .everyDays(1)
        .create();

      const newTriggerUniqueId = TriggersUtils.saveTrigger(trigger);
      console.info(`CREATED Trigger for "${fullFuncName}" with ID "${newTriggerUniqueId}". It will run daily at ${hour}:${nearMinute.toString().padStart(2, '0')}.`);
    }
  }

  /**
   * Creates a time-based trigger that runs a specified function every N minutes.
   *
   * @param {string} funcName - The name of the function to run.
   * @param {number} minutes - The interval in minutes.
   * @returns {void}
   */
  static createMinuteTrigger(funcName, minutes) {
    if (!TriggersUtils.isTriggerExists(funcName)) {
      const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
      const trigger = ScriptApp.newTrigger(fullFuncName)
        .timeBased()
        .everyMinutes(minutes)
        .create();

      const newTriggerUniqueId = TriggersUtils.saveTrigger(trigger);
      console.info(`CREATED Trigger for "${fullFuncName}" with ID "${newTriggerUniqueId}". It will run every ${minutes} minute(s).`);
    }
  }

  /**
   * Creates a new hourly trigger for the specified function.
   *
   * @param {string} funcName - The name of the function to create a trigger for.
   * @param {number} hour - The hour of the day to run the trigger (0-23).
   */
  static createHourlyTrigger(funcName, hour) {
    if (!TriggersUtils.isTriggerExists(funcName)) {
      const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
      const trigger = ScriptApp.newTrigger(fullFuncName)
        .timeBased()
        .everyHours(hour)
        .create();

      const newTriggerUniqueId = TriggersUtils.saveTrigger(trigger);
      console.info(`CREATED Trigger for "${fullFuncName}" with ID "${newTriggerUniqueId}". It will run every ${hour} hour(s).`);
    }
  }

  /**
   * Creates a new scheduled trigger for the specified function at the given date and time.
   * 
   * @param {string} funcName - The name of the function to be triggered.
   * @param {Date} scheduledTime - The specific date and time at which the function should be triggered.
   */
  static createScheduledTrigger(funcName, scheduledTime) {
    try {
      if (TriggersUtils.hasFunctionExecuted(funcName)) {
        TriggersUtils.deleteTriggerByFuncName(funcName);
      }

      if (isBeforeCurrentTime_(scheduledTime)) {
        scheduledTime = new Date();
      }

      if (!TriggersUtils.isTriggerExists(funcName)) {
        const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
        const trigger = ScriptApp.newTrigger(fullFuncName)
          .timeBased()
          .at(scheduledTime)
          .create();

        const newTriggerUniqueId = TriggersUtils.saveTrigger(trigger);
        console.info(`CREATED Trigger for "${fullFuncName}" with ID "${newTriggerUniqueId}". It will run at ${toScheduleDateTimeFormat_(scheduledTime)}.`);
      }
    } catch (error) {
      log(`${error}, Stack: ${error.stack}`, LogLevel.ERROR);
    }
  }

  /**
   * Creates an immediate trigger to run a specified function.
   *
   * @param {string} funcName - The name of the function to trigger.
   */
  static createImmediateTrigger(funcName) {
    const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
    ScriptApp.newTrigger(fullFuncName)
      .timeBased()
      .after(1) // After 1 millisecond
      .create();
  }

  /**
   * Executes a recurring task function with retries and trigger management.
   * 
   * @param {Object} e - The event object triggered by the script.
   * @param {string} triggerFuncName - The name of the trigger function.
   * @param {Function} execFunction - The function to execute the main task.
   */
  static executeRecurringTaskFunction(e, triggerFuncName, execFunction) {
    if (!TriggersUtils.canExecuteTrigger()) { return; }
    console.time(triggerFuncName);

    try {
      executeWithRetry_(() => {
        execFunction(e);
      });

      TriggersUtils.updateTriggerExecutionTime(triggerFuncName);
    } catch (error) {
      log(`${error}, Stack: ${error.stack}`, LogLevel.ERROR);
    } finally {
      console.timeEnd(triggerFuncName);
    }
  }

  /**
   * Executes a one-time task function with retries and trigger management.
   * 
   * @param {Object} e - The event object triggered by the script.
   * @param {string} triggerFuncName - The name of the trigger function.
   * @param {Function} execFunction - The function to execute the main task.
   */
  static executeOneTimeTaskFunction(e, triggerFuncName, execFunction) {
    if (!TriggersUtils.canExecuteTrigger()) { return; }
    console.time(triggerFuncName);

    this.recreateScheduledTrigger(triggerFuncName, 6); // If timed out, try in 6 minutes

    /** @type {TriggerExecutionResult} */
    let result = null;
    try {
      executeWithRetry_(() => {
        result = execFunction(e);
      });
    } catch (error) {
      log(`${error}, Stack: ${error.stack}`, LogLevel.ERROR);
    } finally {
      if (result instanceof TriggerExecutionResult && result.needsRetry()) {
        this.recreateScheduledTrigger(triggerFuncName, result.retryAfterMinutes());
      } else {
        TriggersUtils.deleteTriggerByFuncName(triggerFuncName);
      }

      console.timeEnd(triggerFuncName);
    }
  }

  /**
   * Recreates a trigger for the specified function to run after a specified number of minutes.
   * 
   * @param {string} funcName - The name of the function to be triggered.
   * @param {number} minutes - The number of minutes after which the function should be triggered.
   */
  static recreateScheduledTrigger(funcName, minutes) {
    TriggersUtils.deleteTriggerByFuncName(funcName);
    const scheduledTime = new Date(new Date().getTime() + minutes * 60 * 1000);
    this.createScheduledTrigger(funcName, scheduledTime);
  }

  /**
   * Triggers the process of updating form submission data.
   * @param {Event} e - The event object triggered by the script.
   * @throws {Error} If the `onFormSubmit_` function is not defined.
   */
  static executeOnFormSubmit(e, funcName) {
    console.time(funcName);

    try {
      executeWithRetry_(function () {
        const formSubmission = new FormSubmission(e);
        formSubmission.handleFormSubmission();
      });

      TriggersUtils.updateTriggerExecutionTime(funcName);
    } catch (error) {
      log(`${error}, Stack: ${error.stack}`, LogLevel.ERROR);
    }

    console.timeEnd(funcName);
  }

  /**
     * Creates all scheduled update triggers.
     * 
     * This method sets up the following scheduled triggers:
     * - Update schedule sheet teams and URL
     * - Update players list
     * - Update series results
     * - Update standings table
     * - Reset all sheet settings
     * 
     * It calls the respective functions from the Triggers class to create these triggers.
     * 
     * @example
     * // To create all scheduled update triggers
     * Triggers.createAllScheduledUpdateTriggers();
     */
  static createAllScheduledUpdateTriggers() {
    this.createUpdateScheduleSheetScheduledTrigger();
    this.createUpdatePlayersScheduledTrigger();
    this.createUpdateSeriesResultsScheduledTrigger();
    this.createUpdateStandingsTableScheduledTrigger();
    this.createResetAllSheetSettingsScheduledTrigger();
  }
}

/**
 * Represents the result of a trigger execution.
 * @class
 */
class TriggerExecutionResult {
  /**
   * Creates an instance of TriggerExecutionResult.
   * @param {boolean} isSuccess - Indicates if the execution was successful.
   * @param {boolean} [needsRetry=false] - Indicates if the execution needs to be retried.
   * @param {number} [retryAfterMinutes=0] - Specifies after how many minutes the execution should be retried.
   */
  constructor(isSuccess, needsRetry = false, retryAfterMinutes = 0) {
    let isSuccess_ = isSuccess;
    let needsRetry_ = needsRetry;
    let retryAfterMinutes_ = retryAfterMinutes;

    /**
     * Retrieves whether the execution was successful.
     * @returns {boolean} true if the execution was successful, false otherwise.
     */
    this.isSuccess = function () { return isSuccess_; };

    /**
     * Retrieves whether the execution needs to be retried.
     * @returns {boolean} true if the execution needs to be retried, false otherwise.
     */
    this.needsRetry = function () { return needsRetry_; };

    /**
     * Retrieves after how many minutes the execution should be retried.
     * @returns {number} The number of minutes after which the execution should be retried.
     */
    this.retryAfterMinutes = function () { return retryAfterMinutes_; };

    // Setters (if needed)
    // this.setIsSuccess = function (isSuccess) { isSuccess_ = isSuccess; };
    // this.setNeedsRetry = function (needsRetry) { needsRetry_ = needsRetry; };
    // this.setRetryAfterMinutes = function (retryAfterMinutes) { retryAfterMinutes_ = retryAfterMinutes; };
  }

  /**
   * Serializes the TriggerExecutionResult instance to a plain object.
   * @returns {Object} The plain object representation of the TriggerExecutionResult.
   */
  toObject() {
    return {
      isSuccess: this.isSuccess(),
      needsRetry: this.needsRetry(),
      retryAfterMinutes: this.retryAfterMinutes(),
    };
  }

  /**
   * Static method to create a TriggerExecutionResult instance from a plain object.
   * @param {Object} obj - The plain object representation of a TriggerExecutionResult.
   * @returns {TriggerExecutionResult} The TriggerExecutionResult instance.
   */
  static fromObject(obj) {
    return new TriggerExecutionResult(
      obj.isSuccess,
      obj.needsRetry,
      obj.retryAfterMinutes
    );
  }
}
