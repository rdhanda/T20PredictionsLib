//Series Bets Form

const overallPredictionsFormDescription = () => `Additional Points:
● 3 points for each correctly predicted qualifier team in the first 4 questions (up to 12 points total)
● 3 points for each correctly predicted finalist team in the first 2 questions (up to 6 points total)
● 6 bonus points for correctly predicting all 4 qualifying teams

${getPoweredByStr_()}`;

//** Player categories' stats include all matches, including playoffs`;

//● 6 bonus points for answering all 4 of the first questions correctly`;

const question1Champion = "Champion";
const question1WinnerDesc = "(30 points)";

const question2Runnerup = "Runner-up";
const question2RunnerupDesc = "(20 points)";

//const question3TeamFinishing3rd = "Team Finishing 3rd";//Team Eliminated (Eliminator Match# 73)
//const question3TeamFinishing3rdDesc = "(8 points for team finishing 3rd in the tournament, i.e. team losing eliminator match# 73)";

//IPL
// const question3TeamFinishing3rd = "Team Finishing 3rd";
// const question3TeamFinishing3rdDesc = "(8 points for team losing eliminator match# 72)";
// const question4TeamFinishing4th = "Team Finishing 4th";
// const question4TeamFinishing4thDesc = "(6 points for team losing eliminator match# 73)";

//T20WC
const question3TeamFinishing3rd = "Semi Finalist Team 1";
const question3TeamFinishing3rdDesc = "(8 points for team losing either of the semi finals)";
const question4TeamFinishing4th = "Semi Finalist Team 2";
const question4TeamFinishing4thDesc = "(8 points for team losing either of the semi finals)";

const question5OrangeCup = "Orange Cap";
const question5OrangeCupDesc = "(7 points for highest runs scorer of the tournament)";

const question6PurpleCup = "Purple Cap";
const question6PurpleCupDesc = "(7 points for leading wicket-taker of the tournament)";

const question7BestBattingSR = "Highest Batting S/R";
const question7BestBattingSRDesc = () => `(7 points, condition: minimum ${getSeriesMinBallsFacedForSR_()} balls played)`;

const question8BestBowlingEco = "Best Bowling Economy";
const question8BestBowlingEcoDesc = () => `(7 points, condition: minimum ${getSeriesMinOversFacedForEconomy_()} overs bowled)`;

const overallPredictionsFormQuestionsArr = [[FormConstants.PUNTER_QUESTION_TITLE], [question1Champion, FormApp.ItemType.LIST, question1WinnerDesc], [question2Runnerup, FormApp.ItemType.LIST, question2RunnerupDesc], [question3TeamFinishing3rd, FormApp.ItemType.LIST, question3TeamFinishing3rdDesc], [question4TeamFinishing4th, FormApp.ItemType.LIST, question4TeamFinishing4thDesc], [question5OrangeCup, FormApp.ItemType.TEXT, question5OrangeCupDesc], [question6PurpleCup, FormApp.ItemType.TEXT, question6PurpleCupDesc], [question7BestBattingSR, FormApp.ItemType.TEXT, question7BestBattingSRDesc()], [question8BestBowlingEco, FormApp.ItemType.TEXT, question8BestBowlingEcoDesc()]];

//Define all teams questions arr OR group teams questions arr
const multipleGroups = false;//or true

const allTeamsQuestionsArr = [question1Champion, question2Runnerup, question3TeamFinishing3rd, question4TeamFinishing4th]
const group1TeamsQuestionsArr = [question1Champion, question2Runnerup];
const group2TeamsQuestionsArr = [question3TeamFinishing3rd, question4TeamFinishing4th];

/**
 * Update overall Predictions form
 */
function updateSeriesBetsFormMenu(e) {
  updateSeriesBetsForm_(e);
}

/**
 * TO ADD TO MENU
 */
function updateSeriesBetsFormQuestionTitlesMenu(e) {
  updateOverallPredictionsFormQuestionTitles_(e);
}

/**
 * 
 */
function updateOverallPredictionsFormQuestionTitles_(e) {
  const titles = overallPredictionsFormQuestionsArr.map(subArr => subArr[0]);

  updateFormQuestionTitles_(getSeriesBetsForm_(), titles);
}

/**
 * 
 */
function updateSeriesBetsFormPuntersMenu(e) {
  updatePuntersList_(getSeriesBetsForm_());
}

/**
 * 
 */
function updateSeriesBetsFormTeamsMenu(e) {
  updateOverallPredictionsTeams_(getSeriesBetsForm_());
}

/**
 * Update overall Predictions form
 */
function updateSeriesBetsForm_(e) {
  activateSheet_(getOverallPredictionsFormSheet_());

  let form = getSeriesBetsForm_();
  removeCachedFormTitles_(form);

  updateOverallPredictionsFormTitle_();
  updateOverallPredictionsFormDescription_();

  updatePuntersList_(form);
  updateOverallPredictionsTeams_(form);
}

/**
 * 
 */
function updateOverallPredictionsFormTitle_() {
  let form = getSeriesBetsForm_();
  form.setTitle(appendEnvAndTournamentName_(seriesBetsSheetName));
}

function updateOverallPredictionsFormDescription_() {
  let form = getSeriesBetsForm_();
  form.setDescription(overallPredictionsFormDescription());
}

/**
 * Update teams
 */
function updateOverallPredictionsTeams_(form) {
  //updateTeamsList_(form, overallPredictionsFormQuestionsArr);
  const group1Teams = getGroupTeams_(overallPredictionsFormSheetObj.group1TeamsRange);
  const group2Teams = getGroupTeams_(overallPredictionsFormSheetObj.group2TeamsRange);

  if (multipleGroups) {
    updateMultipleListItems_(form, group1TeamsQuestionsArr, group1Teams, true);
    updateMultipleListItems_(form, group2TeamsQuestionsArr, group2Teams, true);
  } else {
    updateMultipleListItems_(form, allTeamsQuestionsArr, group1Teams, true);
  }
}

/**
 * 
 */
function getGroupTeams_(range) {
  return getOverallPredictionsFormSheet_().getRange(range).getValues().filter(function (row) { return isNotEmpty_(row[0]) }).sort();
}

/**
 * Check form status
 */
function checkSeriesBetsFormStatusMenu() {
  let open = isOverallPredictionsFormAcceptingResponses_();
  let title = getSeriesBetsFormTitle_();
  let message = "The Overall Predictions Form [" + title + "] is now ";
  if (open) {
    message += "OPEN. Accepting responses";
  } else {
    message += "CLOSED. Not accepting responses";
  }
  message += "\n\n";
  message += "Form ID: " + getSeriesBetsForm_().getId();
  message += "\n";
  message += getSeriesBetsFormPublishedURL_();

  showInfoAlert_(message);
}

/**
 * Checks if the form is now accepting resposnes
 */
function isOverallPredictionsFormAcceptingResponses_() {
  let form = getSeriesBetsForm_();
  return form.isAcceptingResponses();
}

/**
 * Starts accepting the form responses
 */
function startAcceptingSeriesBetsMenu() {
  let form = getSeriesBetsForm_();
  form.setAcceptingResponses(true);
}

/**
 * Stops accepting the form responses
 */
function stopAcceptingSeriesBetsMenu() {
  stopAcceptingSeriesBets_();
}

/**
 * 
 */
function stopAcceptingSeriesBets_() {
  let form = getSeriesBetsForm_();
  form.setAcceptingResponses(false);
}

/**
 * 
 */
function refreshOverallPredictionsFormUI(e) {
  refreshOverallPredictionsForm_(e);
}

/**
 * 
 */
function refreshOverallPredictionsForm_(e) {
  activateSheet_(getOverallPredictionsFormSheet_());
  updateRefreshCounter_(getOverallPredictionsFormSheet_(), overallPredictionsFormSheetObj.randomNumCell);
  flushAndWait_();
}

// RESET FORM //

/**
 * 
 */
function resetSeriesBetsFormMenu() {
  let form = getSeriesBetsForm_();
  resetOverallPredictionsForm_(form);
}

/**
 * RESET form
 */
function resetOverallPredictionsForm_(form) {
  removeCachedFormTitles_(form);

  form.setTitle(appendEnvAndTournamentName_(seriesBetsFormNameSuffix));
  form.setDescription("");
  const item = getFormItem_(form, FormConstants.PUNTER_QUESTION_TITLE, false);
  updateListItem_(item, FormConstants.PUNTER_QUESTION_DESCRIPTION, NOT_APPLICABLE_ARR, false);

  //For 2 Groups
  if (multipleGroups) {
    updateMultipleListItems_(form, group1TeamsQuestionsArr, NOT_APPLICABLE_ARR, false);
    updateMultipleListItems_(form, group2TeamsQuestionsArr, NOT_APPLICABLE_ARR, false);
  } else {
    updateMultipleListItems_(form, allTeamsQuestionsArr, NOT_APPLICABLE_ARR, false);
  }
}

/**
 * 
 */
function recreateSeriesBetsFormQuestionsMenu(e) {
  let form = getSeriesBetsForm_();
  recreateOverallPredictionsFormQuestions_(form);
  updateSeriesBetsForm_(e);
}

/**
 * 
 */
function recreateOverallPredictionsFormQuestions_(form) {
  removeCachedFormTitles_(form);

  deleteFormResponses_(form);
  deleteFormQuestions_(form);
  createOverallPredictionsFormQuestions_(form);

  let sheet = getSeriesBetsSheet_();
  reattachForm_(form, sheet);

  flushAndWait_();
}

/**
 * 
 */
function createOverallPredictionsFormQuestions_(form) {
  //Start with index i=1 (exclude first Punter question)
  for (let i = 1; i < overallPredictionsFormQuestionsArr.length; i++) {
    const itemType = overallPredictionsFormQuestionsArr[i][1];
    const title = overallPredictionsFormQuestionsArr[i][0];
    const values = NOT_APPLICABLE_ARR;
    const description = overallPredictionsFormQuestionsArr[i].length >= 3 ? overallPredictionsFormQuestionsArr[i][2] : null;
    addFormItem_(form, itemType, title, values, description);
  }
}
