//DailyPredictionSheets.gs//

/**
 * Resets the number of rows and columns in the Daily Predictions sheet
 *
 * Performs the following tasks:
 * - Activates the Daily Predictions Open Sheet
 * - Deletes excess empty rows in the Daily Predictions Open Sheet
 * - Hides unnecessary columns in the Daily Predictions Open Sheet
 * - Hides unnecessary rows in the Daily Predictions Open Sheet
 * - Resets match ID formulas in the Daily Predictions Open Sheet
 * - Automatically resizes columns in the Daily Predictions Open Sheet, Closed Sheet, and Backup Sheet
 */
function resetBetsSheets_(e) {
  console.time(resetBetsSheets_.name);
  flushAndWait_();

  const sheet = getDailyBetsSheet_();
  const closedSheet = getDailyBetsClosedSheet_();
  const backupSheet = getDailyBetsBkpSheet_();

  let wasProtected = isSheetProtected_(sheet);
  if (wasProtected) {
    removeSheetProtection_(sheet);
  }
  //activateSheet_(sheet);

  deleteDailyBetsExcessEmptyRows_();
  hideAllBetsSheetsColumns_(e);
  syncSheetColumns_(sheet, backupSheet);

  autoResizeColumns_(sheet);
  autoResizeColumns_(closedSheet);
  autoResizeColumns_(backupSheet);

  if (wasProtected) {
    addSheetProtection_(sheet);
  }

  flushAndWait_();
  console.timeEnd(resetBetsSheets_.name);
}

/**
 * showDailyPredictionsSheetColumns_ shows all columns of the DailyResopnses sheet.
 * @param {Object} e - The event object for the trigger.
 */
function showDailyBetsSheetColumns_(e) {
  let sheet = getDailyBetsSheet_();
  sheet.showColumns(1, sheet.getLastColumn());   // Show all columns in the sheet 
  //enableCopy_();
  autoResizeColumns_(sheet);
  activateSheet_(sheet);
}

/**
 * Hides specified columns on the main sheet and backup sheet if there are enough columns to hide.
 *
 * @param {GoogleAppsScript.Events.SheetsOnFormSubmit} e - The form submit event object.
 */
function hideAllBetsSheetsColumns_(e) {
  let sheet = getDailyBetsSheet_();
  const backupSheet = getDailyBetsBkpSheet_();

  // Hide columns on main sheet
  hideBetsSheetColumns_(sheet);
  // Hide columns on backup sheet
  hideBetsSheetColumns_(backupSheet);
}

/**
 * Hides specified columns on a given sheet if there are enough columns to hide.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet on which to hide columns.
 */
function hideBetsSheetColumns_(sheet) {
  flushAndWait_();

  const startColumn = colNameToNum_(MatchSheetConfigPunterColName);//
  hideColumnsAfter(sheet, startColumn);
}

/**
 * Moves daily picks from the source sheet to the closed sheet.
 * After appending the data, sorts the closed sheet by column A in descending order.
 * 
 * @param {Object} e - The event object.
 */
function moveDailyBetsToClosedSheet_(e) {
  console.time(moveDailyBetsToClosedSheet_.name);

  flushAndWait_();
  var sourceSheet = getDailyBetsSheet_();
  var destinationSheet = getDailyBetsClosedSheet_();

  var dataToAppend = sourceSheet.getDataRange().getValues();
  var notesToAppend = sourceSheet.getDataRange().getNotes();
  dataToAppend.splice(0, 1); // Remove the first row (header) from the data
  notesToAppend.splice(0, 1); // Remove the first row (header) from the notes

  if (dataToAppend.length > 0) {
    var numRows = dataToAppend.length;
    var numCols = dataToAppend[0].length;
    var insertRow = 1; // Insert data at the top

    dataToAppend.reverse(); // Reverse the data
    notesToAppend.reverse(); // Reverse the notes

    destinationSheet.insertRows(insertRow, numRows);
    destinationSheet.getRange(insertRow, 1, numRows, numCols).setValues(dataToAppend);
    destinationSheet.getRange(insertRow, 1, numRows, numCols).setNotes(notesToAppend); // Set the notes in the target sheet

    // Sort the closed sheet by column A in descending order
    var range = destinationSheet.getDataRange();
    range.sort([{ column: 1, ascending: false }]);

    clearDailyBetsSheet_(e);
  }

  console.timeEnd(moveDailyBetsToClosedSheet_.name);
}

/**
 * Deletes the excess empty rows in the daily predictions open, closed and backup sheets.
 * The number of empty rows to keep is determined by the `emptyRowsToKeep` property of the `dailyPredictionsOpenSheetObj` object.
 */
function deleteDailyBetsExcessEmptyRows_() {
  deleteExcessEmptyRows_(getDailyBetsSheet_(), dailyPredictionsSheetObj.emptyRowsToKeep);
  deleteExcessEmptyRows_(getDailyBetsClosedSheet_(), dailyPredictionsSheetObj.emptyRowsToKeep);
  deleteExcessEmptyRows_(getDailyBetsBkpSheet_(), dailyPredictionsSheetObj.emptyRowsToKeep);
}

/**
 * 
 */
function clearDailyBetsSheet_(e) {
  deleteRows_(getDailyBetsSheet_(), dailyPredictionsSheetObj.deleteFromRowNum, dailyPredictionsSheetObj.emptyRowsToKeep);
}
