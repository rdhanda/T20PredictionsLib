// Env Props

/**
 * Returns the document properties of the current script's container.
 * 
 * @return {GoogleAppsScript.Properties.Properties} The document properties of the current script's container.
 * @note Using DocumentPorperties, these props are for per document. ScriptProperties and UserPorperties were not useful
 */
function getEnvironmentProperties_() {
  return PropertiesService.getDocumentProperties();
}

/**
 * Gets all the environment properties stored.
 * @return {string} A string containing all the environment properties.
 */
function getAllEnvProperties_() {
  let properties = getEnvironmentProperties_().getProperties();
  let propArray = Object.entries(properties).sort().map(([key, value]) => `${key}=${value}`);
  return propArray.length > 0 ? propArray.join("\n") + "\n" : "";
}

/**
 * Deletes all the environment properties.
 */
function deleteAllEnvPropertiesMenu(e) {
  deleteAllEnvProperties_();
}

/**
 * Deletes all the environment properties.
 */
function deleteAllEnvProperties_() {
  getEnvironmentProperties_().deleteAllProperties();
}

/**
 * Stores an object property in the environment properties.
 * @param {string} objKey - The key under which the object will be stored.
 * @param {Object} obj - The object to be stored.
 */
function setEnvProperty_(objKey, obj) {
  const objJSON = JSON.stringify(obj)
  getEnvironmentProperties_().setProperty(objKey, objJSON);

  console.info(`SAVED Env Property. Key=${objKey}. Value=${objJSON}`);
}

/**
 * Retrieves an object property from the environment properties.
 * @param {string} objKey - The key of the object to be retrieved.
 * @returns {Object} The retrieved object.
 */
function getEnvProperty_(objKey) {
  const rawPropertyValue = getEnvironmentProperties_().getProperty(objKey);
  if (rawPropertyValue) {
    return JSON.parse(rawPropertyValue);
  }
  else {
    return {};
  }
}

/**
 * Deletes a property from the environment properties.
 * @param {string} objKey - The key of the property to be deleted.
 */
function deleteEnvProperty_(objKey) {
  getEnvironmentProperties_().deleteProperty(objKey);
  console.info(`DELETED Env Property. Key=${objKey}.`);
}
