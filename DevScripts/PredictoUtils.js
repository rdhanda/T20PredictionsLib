//PredictoUtils.gs//

//Minimum number of players results required in each player category
const MIN_NUM_OF_PLAYERS_REQUIRED_IN_RESULTS = 3;

//Storing Threshold for player categories, will be lowered until the min num of players required is not matched
const POTM_THRESHOLD = 3;
const MAX_RUNS_THRESHOLD = 3;
const SR_THRESHOLD = 3;
const MAX_WICKETS_THRESHOLD = 4;
const ECONOMY_THRESHOLD = 4;

/**
 * Retrieves all the probables for different categories for the given teams.
 * @param {string} teamA The name of the first team.
 * @param {string} teamB The name of the second team.
 * @returns {Object} An object containing the probables for each category.
 * - potmProbables: An array of probables for the 'Player of the Match' category.
 * - maxRunsProbables: An array of probables for the 'Maximum Runs' category.
 * - strikeRateProbables: An array of probables for the 'Strike Rate' category.
 * - maxWicketsProbables: An array of probables for the 'Maximum Wickets' category.
 * - economyProbables: An array of probables for the 'Economy Rate' category.
 */
function getAllProbablePlayers_(teamA, teamB) {
  const allPlayersList = getPlayersList_([teamA, teamB], PlayerTypeCategories.ALL_PLAYER_CATEGORIES);
  const battersList = getPlayersList_([teamA, teamB], PlayerTypeCategories.BATTER_PLAYER_CATEGORIES);
  const bowlersList = getPlayersList_([teamA, teamB], PlayerTypeCategories.BOWLER_PLAYER_CATEGORIES);

  return {
    potmProbables: getPOTMProbables_(allPlayersList),
    maxRunsProbables: getMaxRunsProbables_(battersList),
    strikeRateProbables: getStrikeRateProbables_(battersList),
    maxWicketsProbables: getMaxWicketsProbables_(bowlersList),
    economyProbables: getEconomyProbables_(bowlersList),
  };
}

/**
 * Gets predictions for a given match.
 *
 * @param {string} title - The title of the match.
 * @param {string} teamA - The name of the first team.
 * @param {string} teamB - The name of the second team.
 * @param {Object} allProbablePlayers - Object containing all player statistics.
 * @returns {Array, Object} An array containing the prediction values, and object containg predictions for the match.
 */
function getAllBets_(title, teamA, teamB, allProbablePlayers, teamWinCounts) {
  const toss = getTossPrediction_(teamA, teamB);
  const score = getScorePrediction_();
  const win = getWinPrediction_(teamA, teamB, teamWinCounts);
  const runsMargin = getRunsMarginPrediction_();
  const wicketsMargin = getWicketsMarginPrediction_();

  const potm = getRandomValueFromArray_(allProbablePlayers.potmProbables);
  const maxRuns = getRandomValueFromArray_(allProbablePlayers.maxRunsProbables);
  const strikeRate = getRandomValueFromArray_(allProbablePlayers.strikeRateProbables);
  const maxWickets = getRandomValueFromArray_(allProbablePlayers.maxWicketsProbables);
  const economy = getRandomValueFromArray_(allProbablePlayers.economyProbables);

  // Create predictions object using defined keys
  const predictions = {
    [PredictionKeys.Title]: title,
    [PredictionKeys.Toss]: toss,
    [PredictionKeys.Score]: score,
    [PredictionKeys.Win]: win,
    [PredictionKeys.RunsMargin]: runsMargin,
    [PredictionKeys.WicketsMargin]: wicketsMargin,
    [PredictionKeys.POTM]: potm,
    [PredictionKeys.MaxRuns]: maxRuns,
    [PredictionKeys.StrikeRate]: strikeRate,
    [PredictionKeys.MaxWickets]: maxWickets,
    [PredictionKeys.Economy]: economy
  };

  // Return values and predictions object
  return { values: Object.values(predictions), predictions };
}

/**
 * Returns a random team from the given two teams.
 *
 * @param {string} teamA The name of the first team.
 * @param {string} teamB The name of the second team.
 * @return {string} The name of the randomly selected team.
 */
function getTossPrediction_(teamA, teamB) {
  return getRandomValue_(teamA, teamB);
}

/**
 * Returns a random score prediction within the configured score range.
 *
 * @return {number} The random score prediction.
 */
function getScorePrediction_() {
  let allValues = [];
  getInningsScorePrediction_().forEach(range => {
    for (let i = range[0]; i <= range[1]; i++) {
      allValues.push(i);
    }
  });
  const randomIndex = Math.floor(Math.random() * allValues.length);
  return allValues[randomIndex];
}

/**
 * 
 */
function getInningsScorePrediction_() {
  return ConfigManager.getConfigClass().INNINGS_SCORE_PREDICTION_RANGES[getActiveSeriesMatchCategory_()];
}

/**
 * Returns a random winning team from the given two teams.
 *
 * @param {string} teamA The name of the first team.
 * @param {string} teamB The name of the second team.
 * @return {string} The name of the randomly selected winning team.
 */
// function getWinPrediction_(teamA, teamB) {
//   return getRandomValue_(teamA, teamB);
// }

/**
 * Predicts the winner of a match between two teams based on historical performance.
 * @param {string} teamA - The name of team A.
 * @param {string} teamB - The name of team B.
 * @param {Object} teamWinCounts - An object containing the win counts for each team.
 * @returns {string} The predicted winner of the match between teamA and teamB.
 */
function getWinPrediction_(teamA, teamB, teamWinCounts) {
  if (isEmpty_(teamWinCounts)) { teamWinCounts = [] }

  const teamsArray = Array(teamWinCounts[teamA] || 1).fill(teamA)
    .concat(Array(teamWinCounts[teamB] || 1).fill(teamB));

  // Fisher-Yates shuffle algorithm
  for (let i = teamsArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [teamsArray[i], teamsArray[j]] = [teamsArray[j], teamsArray[i]];
  }

  return teamsArray[Math.floor(Math.random() * teamsArray.length)];
}

/**
 * Returns a random runs margin prediction within the configured range.
 *
 * @return {number} The random runs margin prediction.
 */
function getRunsMarginPrediction_() {
  return getRandomNumber_(resultsJournalSheetObj.runsMarginRange[0], resultsJournalSheetObj.runsMarginRange[1]);
}

/**
 * Returns a random wickets margin prediction within the configured range.
 *
 * @return {number} The random wickets margin prediction.
 */
function getWicketsMarginPrediction_() {
  return getRandomNumber_(resultsJournalSheetObj.wicketsMarginRange[0], resultsJournalSheetObj.wicketsMarginRange[1]);
}

/**
 * Returns an array of probable players for the Player of the Match category.
 * @param {string[]} allTeamPlayers - An array of players in the team.
 * @returns {string[]} An array of probable players.
 */
function getPOTMProbables_(allTeamPlayers) {
  return getPlayersByCategory_(resultsJournalSheetObj.potmColName, POTM_THRESHOLD, allTeamPlayers);
}

/**
 * Returns an array of probable players for the Maximum Runs category.
 * @param {string[]} allTeamPlayers - An array of players in the team.
 * @returns {string[]} An array of probable players.
 */
function getMaxRunsProbables_(allTeamPlayers) {
  return getPlayersByCategory_(resultsJournalSheetObj.maxRunColName, MAX_RUNS_THRESHOLD, allTeamPlayers);
}

/**
 * Returns an array of probable players for the Strike Rate category.
 * @param {string[]} allTeamPlayers - An array of players in the team.
 * @returns {string[]} An array of probable players.
 */
function getStrikeRateProbables_(allTeamPlayers) {
  return getPlayersByCategory_(resultsJournalSheetObj.strikeRateColName, SR_THRESHOLD, allTeamPlayers);
}

/**
 * Returns an array of probable players for the Maximum Wickets category.
 * @param {string[]} allTeamPlayers - An array of players in the team.
 * @returns {string[]} An array of probable players.
 */
function getMaxWicketsProbables_(allTeamPlayers) {
  return getPlayersByCategory_(resultsJournalSheetObj.maxWicketsColName, MAX_WICKETS_THRESHOLD, allTeamPlayers);
}

/**
 * Returns an array of probable players for the Economy category.
 * @param {string[]} allTeamPlayers - An array of players in the team.
 * @returns {string[]} An array of probable players.
 */
function getEconomyProbables_(allTeamPlayers) {
  return getPlayersByCategory_(resultsJournalSheetObj.economyColName, ECONOMY_THRESHOLD, allTeamPlayers);
}

/**
 * Returns an array of players in the specified column, excluding any values in the exclusion list, and only that appears more than once
 * 
 * @param {string} colName - The name of the column to get players from.
 * @param {number} threshold - The minimum number of occurrences for a value to be included in the result. Default is 1.
 * @returns {string[]} An array of players.
 */
function getPlayersByCategory_(colName, threshold, allTeamPlayers) {
  const players = getAllPlayersByCategory_(colName);

  return getMultiOccurrences_(players, threshold, allTeamPlayers);
}

/**
 * 
 */
function getAllPlayersByCategory_(colName) {
  const values = getResultsJournalSheetCachedData_();
  const colIndex = colNameToIndex_(colName);

  const exclusionList = getAutoBetsExcludedPlayers_();
  const players = values.map(row => row[colIndex].toString().split('\n').map(value => value.trim())).flat();

  return players.filter(value => !exclusionList.includes(value));
}

/**
 * Finds values in the array that occur multiple times and meet a given threshold.
 * If no values are found, the threshold is lowered until some values are found.
 *
 * @param {Array} arr - The array to search for occurrences.
 * @param {number} [initialThreshold=2] - The initial threshold to check for occurrences.
 * @param {Array} allTeamPlayers - An array of all team players.
 * @returns {Array} - An array containing the values that meet the threshold.
 */
function getMultiOccurrences_(arr, initialThreshold = 2, allTeamPlayers) {
  let threshold = initialThreshold;
  let results = [];

  // Exclude empty values from the array
  const filteredArr = arr.filter(val => isNotEmpty_(val));

  const countMap = filteredArr.reduce((acc, val) => acc.set(val, (acc.get(val) || 0) + 1), new Map());

  while (results.length < MIN_NUM_OF_PLAYERS_REQUIRED_IN_RESULTS && threshold > 0) {
    const multiOccurances = [...countMap.entries()].filter(([key, value]) => value >= threshold).map(([key, value]) => key);
    results = getCommonValues_(multiOccurances, allTeamPlayers);
    threshold--;
  }

  if (results.length === 0) {
    log(`No players were found for multiple occurrences. Returning all players instead.`, LogLevel.DEBUG);
    results = allTeamPlayers;
  }
  return results;
}
