//Refresh data from PROD

function refreshDataFromProdMenu(e) {
  refreshDataFromProd_(e);
}

/**
 * Refreshes the target sheets with data (excluding header row) from the production environment.
 */
function refreshDataFromProd_(e) {
  logSTART_(refreshDataFromProd_.name);

  if (isNotProdEnv_()) {
    const sourceSpreadsheet = SpreadsheetApp.openById(ConfigManager.getConfigClass().PROD_ENV_SPREADSHEET_ID);
    const sheetNames = getProdSheetsForDataRefresh_();

    // Loop through sheet names
    for (const sheetName of sheetNames) {
      // Get source sheet and range
      const sourceSheet = sourceSpreadsheet.getSheetByName(sheetName);
      if (sourceSheet) {
        const sourceValues = sourceSheet.getDataRange().getValues();
        refreshSheetWithProdData_(sheetName, sourceValues);
      } else {
        log(`Did NOT find sheet "${sheetName}" in source PROD spreadsheet.`, LogLevel.WARN);
      }
    }

    doAfterDataRefresh_(e);
  } else {
    log(`Can't refresh data from PROD into PROD env. You must run this from a non PROD env only.`, LogLevel.WARN);
  }

  logEND_(refreshDataFromProd_.name);
}

/**
 * Retrieves the sheet names for data refresh from the production environment.
 * @returns {string[]} The sheet names to be refreshed.
 */
function getProdSheetsForDataRefresh_() {
  const sheetNames = getPromotionSheetsBatch_(Promotion.EXCLUDED);
  console.log(sheetNames);

  return sheetNames;
}

/**
 * Performs actions after data refresh.
 * This function resets the match ID formulas in the daily predictions sheet.
 */
function doAfterDataRefresh_(e) {
  console.time(doAfterDataRefresh_.name);

  //Update dashboard entries
  updateDashboardBetsLists_(e);

  //Update Top Picks Sheet
  updateTopBetsSheet_(e);

  console.timeEnd(doAfterDataRefresh_.name);
}

/**
 * Refreshes the target sheet with data from the production environment.
 * 
 * @param {string} sheetName - The name of the target sheet.
 * @param {Array[]} sourceValues - The data to be copied to the target sheet.
 */
function refreshSheetWithProdData_(sheetName, sourceValues) {
  console.time(refreshSheetWithProdData_.name + sheetName);

  const sheetInfo = getFinalSheetConfigs().find(info => info.name === sheetName);
  let startRowNum = 1;
  //Remove the top header row values
  if (!sheetInfo.replaceHeader) {
    sourceValues.shift();
    startRowNum = 2;
  }

  var spreadsheet = getActiveSpreadsheet_();
  // Get target sheet
  var targetSheet = spreadsheet.getSheetByName(sheetName);

  // Check if target sheet exists, create if not
  if (!targetSheet) {
    targetSheet = spreadsheet.insertSheet(sheetName);
    log(`Sheet "${sheetName}" NOT found, created a new one. Make sure to update the header row.`, LogLevel.WARN);
  }

  // Get target range
  var targetLastRow = targetSheet.getMaxRows();
  var targetLastColumn = targetSheet.getMaxColumns();

  //Target range - exclude header row
  var targetRange = targetSheet.getRange(startRowNum, 1, targetLastRow - 1, targetLastColumn);

  // Clear existing data in target range
  targetRange.clearContent();

  var sourceLastRow = sourceValues.length;
  if (sourceLastRow > 0) {
    var sourceLastColumn = sourceValues[0].length;

    // Resize target sheet if necessary
    if (sourceLastRow > targetLastRow) {
      targetSheet.insertRowsAfter(targetLastRow, sourceLastRow - targetLastRow);
    }
    if (sourceLastColumn > targetLastColumn) {
      targetSheet.insertColumnsAfter(targetLastColumn, sourceLastColumn - targetLastColumn);
    }

    targetRange = targetSheet.getRange(startRowNum, 1, sourceLastRow, sourceLastColumn);
    // Copy data from source to target
    targetRange.setValues(sourceValues);
  }

  console.timeEnd(refreshSheetWithProdData_.name + sheetName);
}
