//Top Picks//
const TopBetsMinCount = 3;

/**
 * Updates the Punters Top Choices sheet with player counts.
 */
function updateTopBetsSheetMenu(e) {
  updateTopBetsSheet_(e);
}

/**
 * Updates the Punters Top Choices sheet with player counts.
 * @param {Event} e - The event object.
 */
function updateTopBetsSheet_(e) {
  var sheet = getTopBetsSheet_();
  clearTopBetsSheet_(sheet);

  const sortedPlayers = getPuntersTopBets_(TopBetsMinCount);
  // Convert sortedPlayers to 2D array
  const data = convertTo2DArray_(sortedPlayers);
  const range = sheet.getRange(1, 2, data.length, data[0].length);

  range.setValues(data);
}

/**
 * Clears Top Bets Sheet
 */
function clearTopBetsSheet_(e) {
  var sheet = getTopBetsSheet_();
  sheet.getRange(1, 2, sheet.getMaxRows(), sheet.getMaxColumns() - 1).clearContent();
}

/**
 * Retrieves the top picks for punters based on the minimum count of player selections.
 * @param {number} minCount - The minimum count of player selections to consider.
 * @returns {Object} An object containing the top picks for each punter.
 */
function getPuntersTopBets_(minCount) {
  var data = getDailyResultsSheetCachedData_();

  var players = {}; // Object to store player counts for each punter
  const punterNameColIndex = colNameToIndex_(dailyResultsSheetObj.punterNameColName);
  const potmColIndex = colNameToIndex_(dailyResultsSheetObj.potmColName);
  const potmPointsColIndex = colNameToIndex_(dailyResultsSheetObj.potmPointsColName);
  const categories = Object.values(PlayerQuestionCategories);

  data.forEach(row => {
    var punter = row[punterNameColIndex];
    if (isEmpty_(punter) || punter == "-") return;

    categories.forEach((category, index) => {
      var player = row[index + potmColIndex]; // Start index for POTM
      var points = row[index + potmPointsColIndex];

      addPlayerToCategory_(players, punter, category, player, points);
    });
  });

  // Filter and sort players
  filterAndSortPlayers_(players, minCount);

  // Sort players by punter name
  var sortedPlayers = sortTopBetsByPunter_(players);

  return sortedPlayers;
}

/**
 * Adds a player to the category for a specific punter, counting the player's selections and wins.
 * @param {Object} players - The object containing player counts for each punter.
 * @param {string} punter - The name of the punter.
 * @param {string} category - The category of the player.
 * @param {string} player - The name of the player.
 * @param {number} points - The points earned by the player.
 */
function addPlayerToCategory_(players, punter, category, player, points) {
  players[punter] = players[punter] || {};
  players[punter][category] = players[punter][category] || {};
  if (!players[punter][category][player]) {
    players[punter][category][player] = { name: player, count: 1, winCount: points > 0 ? 1 : 0 };
  } else {
    players[punter][category][player].count++;
    players[punter][category][player].winCount += points > 0 ? 1 : 0;
  }
}

/**
 * Filters and sorts the players based on the minimum count.
 * @param {Object} players - The object containing player counts for each punter.
 * @param {number} minCount - The minimum count of player selections to consider.
 */
function filterAndSortPlayers_(players, minCount) {
  for (var punter in players) {
    for (var category in players[punter]) {
      players[punter][category] = Object.values(players[punter][category])
        .filter(player => (player.count >= minCount || player.winCount > 0))
        .sort((a, b) => {
          if (a.count !== b.count) {
            return b.count - a.count;
          }
          return a.name.localeCompare(b.name);
        });
    }
  }
}

/**
 * Sorts the players by punter name.
 * @param {Object} players - The object containing player counts for each punter.
 * @returns {Object} The object containing sorted players.
 */
function sortTopBetsByPunter_(players) {
  var sortedPlayers = {};
  Object.keys(players).sort().forEach(punter => {
    sortedPlayers[punter] = players[punter];
  });
  return sortedPlayers;
}

/**
 * Converts the sortedPlayers object to a 2D array suitable for updating a sheet.
 * @param {Object} sortedPlayers - The sorted players object.
 * @returns {Array<Array<string>>} The 2D array.
 */
function convertTo2DArray_(sortedPlayers) {
  var result = [];
  const categories = Object.values(PlayerQuestionCategories);

  // Add headers
  var headersRow = [FormConstants.PUNTER_QUESTION_TITLE].concat(categories);
  result.push(headersRow);

  // Iterate over each punter
  for (var punter in sortedPlayers) {
    var punterRow = [punter];
    categories.forEach(category => {
      var players = sortedPlayers[punter][category];
      if (players && players.length > 0) {
        var playersString = players.map(player => `${player.name}: ${player.count}${player.winCount > 0 ? ` (🏆${player.winCount})` : ''}`).join('\n');
        punterRow.push(playersString);
      } else {
        punterRow.push(''); // Add an empty string if playersCount is null or undefined
      }
    });
    result.push(punterRow);
  }

  return result;
}
