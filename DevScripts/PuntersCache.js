const PuntersCacheObj = {};

/**
 * PuntersCache class
 */
class PuntersCache {
  /**
   * Retrieves cached punters data. If not already cached, fetches from the sheet.
   * 
   * @param {string} spreadsheetId - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {Object<string, Punter>} The cached punters data, where keys are punter names and values are Punter objects.
   */
  static getPunters(spreadsheetId = null) {
    const cacheKey = spreadsheetId || 'default';
    if (!PuntersCacheObj[cacheKey]) {
      PuntersCacheObj[cacheKey] = PuntersCache.getPuntersFromSheet(spreadsheetId);
    }
    return PuntersCacheObj[cacheKey];
  }

  /**
   * Fetches punters data from the sheet and processes it.
   * 
   * @private
   * @param {string} spreadsheetId - The ID of the spreadsheet to fetch data from. If null, uses the current spreadsheet.
   * @returns {Object<string, Punter>} The processed punters data, where keys are punter names and values are Punter objects.
   */
  static getPuntersFromSheet(spreadsheetId = null) {
    var data = getDataFromSheet_(puntersSheetName, true, spreadsheetId);

    var punters = {};
    const { id, name, nickName, emails, isInactive, isReminderAllowed, isAutoSubmitAllowed, isBot, isSameAsBot } = PuntersCache.getPunterSheetIndices();

    for (let row of data) {
      var id_ = row[id];
      var name_ = row[name].trim();
      var nickName_ = row[nickName];
      var emails_ = row[emails].split(EMAIL_LIST_SEPARATOR).map(email => email.trim().toLowerCase());
      var isInactive_ = toBoolean_(row[isInactive]);
      var isReminderAllowed_ = !isInactive_ && toBoolean_(row[isReminderAllowed]);
      var isAutoSubmitAllowed_ = !isInactive_ && toBoolean_(row[isAutoSubmitAllowed]);
      var isAutoSubmitSameAsBot_ = toBoolean_(row[isSameAsBot]);
      var isBot_ = toBoolean_(row[isBot]);

      var punter = new Punter(id_, name_, nickName_, emails_, isInactive_, isReminderAllowed_, isAutoSubmitAllowed_, isAutoSubmitSameAsBot_, isBot_);
      punters[name_] = punter;
    }

    var sortedPuntersArray = Object.entries(punters).sort(([nameA,], [nameB,]) => nameA.localeCompare(nameB));

    var sortedPunters = {};
    sortedPuntersArray.forEach(([name, punter]) => {
      sortedPunters[name] = punter;
    });

    return sortedPunters;
  }

  /**
  * Retrieves the column indices for the punters sheet.
  * @returns {Object} An object containing column indices for various properties.
  */
  static getPunterSheetIndices() {
    return {
      id: ColumnIndices.A,
      name: ColumnIndices.B,
      nickName: ColumnIndices.C,
      emails: ColumnIndices.D,
      isInactive: ColumnIndices.E,
      isReminderAllowed: ColumnIndices.F,
      isAutoSubmitAllowed: ColumnIndices.G,
      isBot: ColumnIndices.H,
      isSameAsBot: ColumnIndices.I,
    };
  }

  /**
   * Clears the cached punters data for a specific spreadsheet.
   * 
   */
  static clearCache() {
    PuntersCacheObj = {};
  }
}

/**
 * Represents a Punter.
 * @class
 */
class Punter {
  /**
   * Creates an instance of Punter.
   * @param {string} id - The ID of the punter.
   * @param {string} name - The name of the punter.
   * @param {string} nickName - The nickname of the punter.
   * @param {string[]} emails - The emails associated with the punter.
   * @param {boolean} isInactive - Whether the punter is inactive.
   * @param {boolean} isReminderAllowed - Whether reminders are allowed for the punter.
   * @param {boolean} isAutoSubmitAllowed - Whether auto submission is allowed for the punter.
   * @param {boolean} isAutoSubmitSameAsBot - Whether auto submission is same as bot for the punter.
   * @param {boolean} isBot - Whether the punter is a bot.
   */
  constructor(id, name, nickName, emails, isInactive, isReminderAllowed, isAutoSubmitAllowed, isAutoSubmitSameAsBot, isBot) {
    var id_ = id;
    var name_ = name;
    var nickName_ = nickName;
    var emails_ = emails;
    var isInactive_ = isInactive;
    var isReminderAllowed_ = isReminderAllowed;
    var isAutoSubmitAllowed_ = isAutoSubmitAllowed;
    var isAutoSubmitSameAsBot_ = isAutoSubmitSameAsBot;
    var isBot_ = isBot;

    // Getters
    this.getId = function () { return id_; };
    this.getName = function () { return name_; };
    this.getNickName = function () { return nickName_; };
    this.getEmails = function () { return emails_; };
    this.isInactive = function () { return toBoolean_(isInactive_); };
    this.isReminderAllowed = function () { return !this.isInactive() && toBoolean_(isReminderAllowed_); };
    this.isAutoSubmitAllowed = function () { return !this.isInactive() && toBoolean_(isAutoSubmitAllowed_); };
    this.isAutoSubmitSameAsBot = function () { return !this.isInactive() && toBoolean_(isAutoSubmitSameAsBot_); };
    this.isBot = function () { return !this.isInactive() && toBoolean_(isBot_); };

    // Setters
    this.setId = function (newValue) { id_ = newValue; };
    this.setName = function (newValue) { name_ = newValue; };
    this.setNickName = function (newValue) { nickName_ = newValue; };
    this.setEmails = function (newValue) { emails_ = newValue; };
    this.setIsInactive = function (newValue) { isInactive_ = newValue; };
    this.setIsReminderAllowed = function (newValue) { isReminderAllowed_ = newValue; };
    this.setIsAutoSubmitAllowed = function (newValue) { isAutoSubmitAllowed_ = newValue; };
    this.setIsAutoSubmitSameAsBot = function (newValue) { isAutoSubmitSameAsBot_ = newValue; };
    this.setIsBot = function (newValue) { isBot_ = newValue; };
  }

  /**
   * Serializes the Punter instance to a plain object.
   * @returns {Object} The plain object representation of the Punter.
   */
  toObject() {
    return {
      id: this.getId(),
      name: this.getName(),
      nickName: this.getNickName(),
      emails: this.getEmails(),
      isInactive: this.isInactive(),
      isReminderAllowed: this.isReminderAllowed(),
      isAutoSubmitAllowed: this.isAutoSubmitAllowed(),
      isAutoSubmitSameAsBot: this.isAutoSubmitSameAsBot(),
      isBot: this.isBot(),
    };
  }

  /**
   * Static method to create a Punter instance from a plain object.
   * @param {Object} obj - The plain object representation of a Punter.
   * @returns {Punter} The Punter instance.
   */
  static fromObject(obj) {
    return new Punter(
      obj.id,
      obj.name,
      obj.nickName,
      obj.emails,
      obj.isInactive,
      obj.isReminderAllowed,
      obj.isAutoSubmitAllowed,
      obj.isAutoSubmitSameAsBot,
      obj.isBot
    );
  }
}
