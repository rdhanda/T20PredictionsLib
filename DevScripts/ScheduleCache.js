/**
 * @typedef {Object} ScheduleMatchesCacheObj
 * @property {boolean} initialized - Indicates if the cache is initialized.
 * @property {Object.<string, ScheduleMatch>|null} matches - Object containing scheduled matches.
 */

/** @type {ScheduleMatchesCacheObj} */
const ScheduleCacheObj = {
  initialized: false,
  matches: null,
};

/**
 * Class representing a cache for scheduled matches.
 */
class ScheduleCache {

  /**
   * Retrieves the scheduled matches from the cache.
   * @returns {Object.<string, ScheduleMatch>|null} An object containing match objects with match IDs as keys, or null if not initialized.
   */
  static getAllMatches() {
    ScheduleCache.init();
    return ScheduleCacheObj.matches;
  }

  /**
   * Initializes the cache with match data from the schedule sheet.
   * @returns {void}
   */
  static init() {
    if (ScheduleCacheObj.initialized) { return; }

    const scheduleData = getDataFromSheet_(scheduleTableSheetName, true);
    const colIndices = ScheduleCache.getScheduleColIndices_();

    const scheduledMatches = {};

    scheduleData.forEach(row => {
      const match = new ScheduleMatch(
        row[colIndices.id],
        row[colIndices.title],
        row[colIndices.shortDate],
        row[colIndices.homeTeam],
        row[colIndices.awayTeam],
        row[colIndices.scorecardURL],
        row[colIndices.stage],
        row[colIndices.venue],
        row[colIndices.startTimeLocal],
        row[colIndices.startTimeMST],
        row[colIndices.startTimeIST],
        row[colIndices.status],
        row[colIndices.openedTime],
        row[colIndices.closedTime],
        row[colIndices.completedTime],
        row[colIndices.reminderSent]
      );

      // Adding the match object to the result with the match ID as the key
      scheduledMatches[match.getId()] = match;
    });

    ScheduleCacheObj.matches = scheduledMatches;
    ScheduleCacheObj.initialized = true;
  }

  /**
   * Retrieves the column indices for the schedule sheet.
   * @returns {Object.<string, number>} An object mapping column names to their indices.
   */
  static getScheduleColIndices_() {
    const colIndices = {
      id: colNameToIndex_(scheduleTableSheetObj.matchIDColName),
      title: colNameToIndex_(scheduleTableSheetObj.matchTitleColName),
      shortDate: colNameToIndex_(scheduleTableSheetObj.matchShortDateColName),
      homeTeam: colNameToIndex_(scheduleTableSheetObj.teamAColName),
      awayTeam: colNameToIndex_(scheduleTableSheetObj.teamBColName),
      scorecardURL: colNameToIndex_(scheduleTableSheetObj.scorecardURLColName),
      stage: colNameToIndex_(scheduleTableSheetObj.stageColName),
      venue: colNameToIndex_(scheduleTableSheetObj.venueColName),
      startTimeLocal: colNameToIndex_(scheduleTableSheetObj.startTimeLocalColName),
      startTimeMST: colNameToIndex_(scheduleTableSheetObj.startTimeMSTColName),
      startTimeIST: colNameToIndex_(scheduleTableSheetObj.startTimeISTColName),
      status: colNameToIndex_(scheduleTableSheetObj.matchStatusColName),
      openedTime: colNameToIndex_(scheduleTableSheetObj.openedTimeColName),
      closedTime: colNameToIndex_(scheduleTableSheetObj.closedTimeColName),
      completedTime: colNameToIndex_(scheduleTableSheetObj.completedTimeColName),
      reminderSent: colNameToIndex_(scheduleTableSheetObj.reminderSentColName)
    };
    return colIndices;
  }

  /**
   * Clears the cache by setting match data to null.
   * @returns {void}
   */
  static clearCache() {
    ScheduleCacheObj.initialized = false;
    ScheduleCacheObj.matches = null;
  }
}

/**
 * Represents a Match.
 */
class ScheduleMatch {
  /**
   * Creates an instance of Match.
   * @param {number} id - The ID of the match.
   * @param {string} title - The title of the match.
   * @param {string} shortDate - The short date of the match.
   * @param {string} homeTeam - The home team of the match.
   * @param {string} awayTeam - The away team of the match.
   * @param {string} url - The URL of the match.
   * @param {string} stage - The stage of the match.
   * @param {string} venue - The venue of the match.
   * @param {string} startTimeLocal - The local start time of the match.
   * @param {string} startTimeMST - The MST start time of the match.
   * @param {string} startTimeIST - The IST start time of the match.
   * @param {string} status - The status of the match.
   * @param {string} openedTime - The opened time of the match.
   * @param {string} closedTime - The closed time of the match.
   * @param {string} completedTime - The completed time of the match.
   * @param {string} remindedTime - The reminded time of the match.
   */
  constructor(id, title, shortDate, homeTeam, awayTeam, url, stage, venue, startTimeLocal, startTimeMST, startTimeIST, status, openedTime, closedTime, completedTime, remindedTime) {
    var id_ = id;
    var title_ = title;
    var shortDate_ = shortDate;
    var homeTeam_ = homeTeam;
    var awayTeam_ = awayTeam;
    var url_ = url;
    var stage_ = stage;
    var venue_ = venue;
    var startTimeLocal_ = startTimeLocal;
    var startTimeMST_ = startTimeMST;
    var startTimeIST_ = startTimeIST;
    var status_ = status;
    var openedTime_ = openedTime;
    var closedTime_ = closedTime;
    var completedTime_ = completedTime;
    var remindedTime_ = remindedTime;

    // Getters and setters for each attribute
    this.getId = function () { return id_; };
    this.setId = function (newValue) { id_ = newValue; };

    this.getTitle = function () { return title_; };
    this.setTitle = function (newValue) { title_ = newValue; };

    this.getShortDate = function () { return shortDate_; };
    this.setShortDate = function (newValue) { shortDate_ = newValue; };

    this.getHomeTeam = function () { return homeTeam_; };
    this.setHomeTeam = function (newValue) { homeTeam_ = newValue; };

    this.getAwayTeam = function () { return awayTeam_; };
    this.setAwayTeam = function (newValue) { awayTeam_ = newValue; };

    this.getUrl = function () { return url_; };
    this.setUrl = function (newValue) { url_ = newValue; };

    this.getStage = function () { return stage_; };
    this.setStage = function (newValue) { stage_ = newValue; };

    this.getVenue = function () { return venue_; };
    this.setVenue = function (newValue) { venue_ = newValue; };

    this.getStartTimeLocal = function () { return startTimeLocal_; };
    this.setStartTimeLocal = function (newValue) { startTimeLocal_ = newValue; };

    this.getStartTimeMST = function () { return startTimeMST_; };
    this.setStartTimeMST = function (newValue) { startTimeMST_ = newValue; };

    this.getStartTimeIST = function () { return startTimeIST_; };
    this.setStartTimeIST = function (newValue) { startTimeIST_ = newValue; };

    this.getStatus = function () { return status_; };
    this.setStatus = function (newValue) { status_ = newValue; };

    this.getOpenedTime = function () { return openedTime_; };
    this.setOpenedTime = function (newValue) { openedTime_ = newValue; };

    this.getClosedTime = function () { return closedTime_; };
    this.setClosedTime = function (newValue) { closedTime_ = newValue; };

    this.getCompletedTime = function () { return completedTime_; };
    this.setCompletedTime = function (newValue) { completedTime_ = newValue; };

    this.getRemindedTime = function () { return remindedTime_; };
    this.setRemindedTime = function (newValue) { remindedTime_ = newValue; };

    this.isEmpty = function () { return MatchScheduleStatus.EMPTY === this.getStatus() }
    this.isOpen = function () { return MatchScheduleStatus.OPEN === this.getStatus() }
    this.isClosed = function () { return MatchScheduleStatus.CLOSED === this.getStatus() }
    this.isExcluded = function () { return MatchScheduleStatus.EXCLUDED === this.getStatus() }

    /**
     * Converts the match object to a string representation.
     * @returns {string} A string representation of the match.
     */
    this.toObject = function () {
      return {
        id: this.getId(),
        title: this.getTitle(),
        shortDate: this.getShortDate(),
        homeTeam: this.getHomeTeam(),
        awayTeam: this.getAwayTeam(),
        url: this.getUrl(),
        stage: this.getStage(),
        venue: this.getVenue(),
        startTimeLocal: this.getStartTimeLocal(),
        startTimeMST: this.getStartTimeMST(),
        startTimeIST: this.getStartTimeIST(),
        status: this.getStatus(),
        openedTime: this.getOpenedTime(),
        closedTime: this.getClosedTime(),
        completedTime: this.getCompletedTime(),
        remindedTime: this.getRemindedTime(),
      };
    }

    /**
     * Converts the match object to a JSON string.
     * @returns {string} A JSON string representing the match.
     */
    this.toStringify = function () {
      return JSON.stringify(this.toObject());
    };
  }
}
