//SheetUtils.gs//

/**
 * UI function for showing all available sheets
 *
 * Calls the underlying function showAllSheets_ for the actual implementation.
 */
function showAllSheetsMenu(e) {
  showAllSheets_(e);
}

/**
 * UI/Menu function for resetting the order of sheets
 *
 * Calls the underlying function resetAllSheetsSettings_ for the actual implementation.
 */
function resetAllSheetsSettingsMenu(e) {
  resetAllSheetsSettings_(e);
}

/**
 * Hides all columns after the specified column number in the given sheet.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to hide columns in.
 * @param {number} startColNum - The column number after which to hide all columns.
 */
function hideColumnsAfter_(sheet, startColNum) {
  var numCols = sheet.getMaxColumns();
  var columnsToHide = sheet.getMaxColumns() - startColNum + 1;

  if (startColNum <= numCols) {
    sheet.hideColumns(startColNum, columnsToHide);
  }
}


/**
 * showAllSheets_ is the function that shows all the sheets in the active spreadsheet.
 */
function showAllSheets_() {
  const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  const sheets = activeSpreadsheet.getSheets();

  sheets.forEach(sheet => sheet.showSheet());
  flushAndWait_();
}

/**
 * Reorders all the sheets in the active spreadsheet.
 * It updates the name of the active spreadsheet, disables copying the spreadsheet, 
 * resets the daily predictions sheets, reorders all sheets,
 * resets the show/hide protection of all sheets, shows the active match sheets,
 * flushes the changes made to the spreadsheet, and activates the dashboard sheet.
 * Logs the start and end time of the process, and the total time taken.
 * 
 * @param {Event} e - The event object triggered by a user action.
 */
function resetAllSheetsSettings_(e) {
  const funcName = resetAllSheetsSettings_.name;
  console.time(funcName);

  flushAndWait_();

  updateActiveSpreadsheetName_();
  updateAttachedFormNames_();
  disableSpreadsheetCopy_(e);

  resetBetsSheets_(e);
  resetSeriesBetsSheets_();

  resetSheetsOrder_(e);
  resetSheetsProperties_();

  updateDashboardBetsLists_(e);
  resetAllSheetsFont_(e);

  activateSheet_(getDashboardSheet_());
  console.timeEnd(funcName);
}

/**
 * Resets the order of sheets in the active spreadsheet based on the sheetConfigs array.
 * 
 * @param {Event} e - The trigger event object.
 */
function resetSheetsOrder_(e) {
  const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();

  getFinalSheetConfigs().forEach((sheetObj, index) => {
    const sheetName = sheetObj.name;
    const sheet = spreadsheet.getSheetByName(sheetName);
    if (sheet) {
      if (isSheetProtected_(sheet)) {
        removeSheetProtection_(sheet);
      }
      if (sheet.getIndex() !== index + 1) {
        spreadsheet.setActiveSheet(sheet);
        spreadsheet.moveActiveSheet(index + 1);
      }
    }
  });

  flushAndWait_();
}

/**
 * Resets the properties of all sheets in the active spreadsheet
 * according to their corresponding sheet configurations.
 */
function resetSheetsProperties_() {
  const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();

  getFinalSheetConfigs().forEach(sheetObj => {
    const sheetName = sheetObj.name;
    const sheet = spreadsheet.getSheetByName(sheetName);
    if (sheet) {
      const wasProtected = isSheetProtected_(sheet);
      if (wasProtected) {
        removeSheetProtection_(sheet);
      }

      if (isMatchSheet_(sheetName) && hasMatchID_(sheet)) {
        sheet.showSheet();
      }
      else {
        setSheetVisibility_(sheet, sheetObj.visibility);
      }

      setSheetProtection_(sheet, sheetObj.protection);

      if (wasProtected) {
        addSheetProtection_(sheet);
      }
    }
  });

  flushAndWait_();
}

/**
 * Sets the visibility of a sheet based on the specified visibility value.
 * If the visibility value is set to PROTECTED, the sheet will also be protected except for the first cell.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to set visibility for.
 * @param {SheetVisibility} visibility - The visibility value to set for the sheet.
 */
function setSheetVisibility_(sheet, visibility) {
  if (visibility === SheetVisibility.SHOW) {
    sheet.showSheet();
  } else if (visibility === SheetVisibility.HIDE || visibility === SheetVisibility.ALWAYS_HIDDEN) {
    sheet.hideSheet();
  }
}

/**
 * Sets the protection of a sheet based on the specified protection value.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to set protection for.
 * @param {Protection} protection - The protection value to set for the sheet.
 */
function setSheetProtection_(sheet, protection) {
  if (protection === Protection.ON) {
    addSheetProtection_(sheet);
  } else if (protection === Protection.OFF) {
    removeSheetProtection_(sheet);
  }
}

/**
 * Shows all match sheets that have a match ID in them.
 *
 * @param {Event} e - The event parameter from the onOpen() trigger.
 */
function showActiveMatchSheets_(e) {
  for (const match of MatchSheetConfigs) {
    let matchSheet = getSheetByName_(match.sheetName);
    hasMatchID_(matchSheet) ? matchSheet.showSheet() : matchSheet.hideSheet();
  }
}

/**
 * Resets the visibility and protection of the given sheet based on its corresponding sheet info.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to reset.
 */
function resetVisibilityAndProtection_(sheet) {
  resetSheetVisibility_(sheet);
  resetSheetProtection_(sheet);
}

/**
 * Resets the visibility of the given sheet based on its corresponding sheet info.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to reset visibility for.
 */
function resetSheetVisibility_(sheet) {
  const sheetInfo = getFinalSheetConfigs().find(info => info.name === sheet.getName());
  if (sheetInfo) {
    setSheetVisibility_(sheet, sheetInfo.visibility);
  }
}

/**
 * Resets the protection of the given sheet based on its corresponding sheet info.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to reset protection for.
 */
function resetSheetProtection_(sheet) {
  const sheetInfo = getFinalSheetConfigs().find(info => info.name === sheet.getName());
  if (sheetInfo) {
    setSheetProtection_(sheet, sheetInfo.protection);
  }
}


/**
 * Add protection to the given sheet.
 * @function
 * @param {object} sheet - The sheet to add protection to.
 */
function addSheetProtection_(sheet) {
  toast_("Adding Protection", sheet.getName());

  let protection = sheet.protect().setDescription("Protected " + sheet.getName());
  protection.setWarningOnly(true);
}

/**
 * Remove protection from the given sheet.
 * @function
 * @param {object} sheet - The sheet to remove protection from.
 */
function removeSheetProtection_(sheet) {
  let protection = sheet.getProtections(SpreadsheetApp.ProtectionType.SHEET)[0];
  if (protection && protection.canEdit()) {
    toast_("Removing Protection", sheet.getName());
    protection.remove();
  }
}

/**
 * Check if the given sheet has protection enabled.
 * @function
 * @param {object} sheet - The sheet to check if it is protected.
 * @return {boolean} - True if the sheet has protection enabled, false otherwise.
 */
function isSheetProtected_(sheet) {
  const protections = sheet.getProtections(SpreadsheetApp.ProtectionType.SHEET);
  return protections.length > 0;
}

/**
 * Activates the specified sheet if it is not set to always hidden.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to activate.
 */
function activateSheet_(sheet) {
  if (sheet) {
    let sheetName = sheet.getName();
    let doActivate = true;
    if (isSheetAlwaysHidden_(sheetName)) {
      doActivate = false;
    }
    if (doActivate) {
      let isProtected = isSheetProtected_(sheet);
      if (isProtected) {
        removeSheetProtection_(sheet);
      }
      sheet.activate();
      if (isProtected) {
        addSheetProtection_(sheet);
      }
    }
    flushAndWait_();
  }
}

/**
 * Checks if the sheet with the given name is always hidden.
 *
 * @param {string} sheetName - The name of the sheet to check.
 * @returns {boolean} `true` if the sheet is always hidden, `false` otherwise.
 */
function isSheetAlwaysHidden_(sheetName) {
  return SheetVisibility.ALWAYS_HIDDEN === getDefaultSheetVisibility_(sheetName);
}

/**
 * Gets the default visibility of the sheet with the given name.
 *
 * @param {string} sheetName - The name of the sheet to get the visibility of.
 * @returns {SheetVisibility} The default visibility of the sheet.
 */
function getDefaultSheetVisibility_(sheetName) {
  const sheetInfo = getFinalSheetConfigs().find(info => info.name === sheetName);
  if (sheetInfo) {
    return sheetInfo.visibility;
  } else {
    return SheetVisibility.SHOW;
  }
}

/**
 * Enables the copying of data in the current Spreadsheet.
 */
function enableSpreadsheetCopy_() {
  let id = SpreadsheetApp.getActiveSpreadsheet().getId();
  Drive.Files.patch({ viewersCanCopyContent: true, copyRequiresWriterPermission: false, writersCanShare: true }, id);
}

/**
 * Disables the copying of data in the current Spreadsheet.
 */
function disableSpreadsheetCopy_() {
  let id = SpreadsheetApp.getActiveSpreadsheet().getId();
  Drive.Files.patch({ viewersCanCopyContent: false, copyRequiresWriterPermission: true, writersCanShare: true }, id);
}

/**
 * Updates the name of the active Spreadsheet.
 */
function updateActiveSpreadsheetName_() {
  DriveApp.getFileById(getActiveSpreadsheet_().getId()).setName(getActiveSpreadsheetNewName_());
}

/**
 * Returns the new name of the active Spreadsheet.
 * 
 * @returns {string} The new name of the active Spreadsheet.
 */
function getActiveSpreadsheetNewName_() {
  return appendEnvAndTournamentName_(spreadsheetFileNameSuffix);
}

/**
 * Copies and archives the active spreadsheet and all attached forms, if any.
 * The archived spreadsheet and forms are moved to the tournament archive folder.
 *
 * @return {string} the name of the archived spreadsheet
 */
function copyAndArchiveActiveSpreadsheet_() {
  logSTART_(arguments.callee.name);

  let archivedSheetName = getActiveSpreadsheet_().getName() + ARCHIVED_FILE_SUFFIX;
  let archivedSpreadsheetFile = DriveApp.getFileById(getActiveSpreadsheet_().getId()).makeCopy(archivedSheetName);
  let archivedSpreadsheet = SpreadsheetApp.openById(archivedSpreadsheetFile.getId());

  let seriesArchiveFolder = getSeriesArchiveFolder_();

  // Archive all attached forms.
  let sheets = archivedSpreadsheet.getSheets();
  for (let i = 0; i < sheets.length; i++) {
    let formUrl = sheets[i].getFormUrl();
    archiveAttachedForm_(formUrl, seriesArchiveFolder);
  }

  // Move the archived spreadsheet to the archive folder
  archivedSpreadsheetFile.moveTo(seriesArchiveFolder);

  logEND_(arguments.callee.name);
  return archivedSheetName;
}

/**
 * Archives the form with the specified URL.
 *
 * @param {string} formUrl - The URL of the form to be archived
 * @param {GoogleAppsScript.Drive.Folder} archiveFolder - The folder to which the archived form should be moved
 */
function archiveAttachedForm_(formUrl, archiveFolder) {
  if (formUrl) {
    logSTART_(arguments.callee.name);

    let form = FormApp.openByUrl(formUrl);
    let formId = form.getId();

    // Rename the form file
    let formFile = DriveApp.getFileById(formId);
    let formFileName = formFile.getName().replace("Copy of ", "") + ARCHIVED_FILE_SUFFIX;
    formFile.setName(formFileName);

    formFile.moveTo(archiveFolder);

    log(`Archived the Form as [${formFileName}]`, LogLevel.DEBUG);
    logEND_(arguments.callee.name);
  }
}

// /**
//  * Get the archive folder for predictions.
//  *
//  * @return {GoogleAppsScript.Drive.Folder} - The archive folder for predictions.
//  */
// function getBetsArchiveFolder_() {
//   let predictionsFolder = getBetsRootFolder_();

//   let predictionsArchiveFolder = predictionsFolder.getFoldersByName(DRIVE_ARCHIVED_SUB_FOLDER);

//   if (predictionsArchiveFolder.hasNext()) {
//     return predictionsArchiveFolder.next();
//   } else {
//     return predictionsFolder.createFolder(DRIVE_ARCHIVED_SUB_FOLDER);
//   }
// }

/**
 * Returns the base folder for the series, creating any necessary folders along the way.
 *
 * @returns {GoogleAppsScript.Drive.Folder} - The base folder for the series.
 */
function getSeriesBaseFolder_() {
  const DRIVE_SERIES_BASE_FOLDER_PATH = [DRIVE_ROOT_FOLDER];
  return getOrCreateFolderPath_(DRIVE_SERIES_BASE_FOLDER_PATH);
}

/**
 * Returns the archive folder for the series, creating any necessary folders along the way.
 *
 * @returns {GoogleAppsScript.Drive.Folder} - The archive folder for the series.
 */
function getSeriesArchiveFolder_() {
  const DRIVE_SERIES_ARCHIVE_SUB_FOLDER_PATH = [DRIVE_ARCHIVED_SUB_FOLDER];
  return getOrCreateFolderPath_(DRIVE_SERIES_ARCHIVE_SUB_FOLDER_PATH);
}

/**
 * Returns the folder containing the current Google Spreadsheet.
 * If the spreadsheet is in the root folder, it returns the base series folder.
 *
 * @returns {GoogleAppsScript.Drive.Folder} - The folder containing the current spreadsheet or the base series folder if it's in the root folder.
 */
function getCurrentSpreadsheetFolder_() {
  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var spreadsheetId = spreadsheet.getId();
  var file = DriveApp.getFileById(spreadsheetId);

  var folders = file.getParents();

  if (folders.hasNext()) {
    return folders.next();
  } else {
    return getSeriesBaseFolder_();
  }
}

/**
 * Navigates through or creates the necessary folders based on the provided folder path.
 * Returns the final folder in the specified path.
 *
 * @param {string[]} folderPath - Array of folder names representing the path.
 * @returns {GoogleAppsScript.Drive.Folder} - The last folder in the path.
 */
function getOrCreateFolderPath_(folderPath) {
  var parentFolder = getCurrentSpreadsheetFolder_();
  var currentFolder = parentFolder;

  folderPath.forEach(function (name) {
    var folders = currentFolder.getFoldersByName(name);
    if (folders.hasNext()) {
      currentFolder = folders.next();
    } else {
      currentFolder = currentFolder.createFolder(name);
    }
  });

  return currentFolder;
}

// /**
//  * Get the folder for archiving the tournament sheet and forms.
//  *
//  * @return {GoogleAppsScript.Drive.Folder} - The folder for archiving the tournament sheet and forms.
//  */
// function getSeriesArchiveFolder2_() {
//   let seriesName = getActiveSeriesName_();
//   let betsArchiveFolder = getBetsArchiveFolder_();
//   let seriesFolder = betsArchiveFolder.getFoldersByName(seriesName);

//   if (seriesFolder.hasNext()) {
//     return seriesFolder.next();
//   } else {
//     return betsArchiveFolder.createFolder(seriesName);
//   }
// }

/**
 * Auto Resize Columns function resizes all columns in the specified sheet
 * to their optimal width based on their content.
 *
 * @param {Sheet} sheet - The sheet object to auto resize columns in.
 */
function autoResizeColumns_(sheet) {
  if (sheet) {
    sheet.autoResizeColumns(1, sheet.getMaxColumns());
  }
}

/**
 * getMatchDataTable retrieves the HTML table from the specified match sheet.
 *
 * @param {Sheet} sheet - The match sheet object to retrieve the HTML table from.
 * @return {String} The HTML string representation of the table.
 */
function getMatchDataTable_(sheet) {
  return getHtmlTable_(sheet, matchSheetObj.htmlDataTableRange);
}

/**
 * getDashboardLeaderboardTable retrieves the HTML table from the dashboard leaderboard.
 *
 * @return {String} The HTML string representation of the dashboard leaderboard table.
 */
function getDashboardLeaderboardTable_() {
  return getHtmlTable_(getDashboardSheet_(), dashboardSheetObj.overallAndDailyDataRange);
}

/**
 * getDashboardLeaderboardTable retrieves the HTML table from the dashboard last match points.
 *
 * @return {String} The HTML string representation of the dashboard last match points table.
 */
function getDashboardLastMatchPointsTable_() {
  return getHtmlTable_(getDashboardSheet_(), dashboardSheetObj.lastMatchPointsDataRange);
}

/**
 * Get sheet+range table in HTML format
 *
 * @function
 * @param {Object} sheet - The sheet object to retrieve data from.
 * @param {String} rangeName - The range in A1 notation to retrieve data from.
 * @returns {String} - The HTML string representation of the table.
 */
function getHtmlTable_(sheet, rangeName) {
  let ss_data = getHtmlData_(sheet, rangeName);
  let data = ss_data[0];
  let background = ss_data[1];
  let fontColor = ss_data[2];
  let fontStyles = ss_data[3];
  let fontWeight = ss_data[4];
  let fontSize = ss_data[5];
  let htmlTable = "<table border='0px'>";
  for (let i = 0; i < data.length; i++) {
    // Check if the row is empty
    if (data[i].every(cell => cell === '')) {
      continue; // Skip the row if all cells are empty
    }
    
    htmlTable += "<tr>"
    for (let j = 0; j < data[i].length; j++) {
      htmlTable += "<td style='height:20px;background:" + background[i][j] + ";color:" + fontColor[i][j] + ";font-style:" + fontStyles[i][j] + ";font-weight:" + fontWeight[i][j] + ";font-size:" + (fontSize[i][j] + 0) + "px;'>" + data[i][j] + "</td>";
    }
    htmlTable += "</tr>";
  }
  htmlTable += "</table>";
  return htmlTable;
}

/**
 * Get data values and formatting
 *
 * @function
 * @param {Object} sheet - The sheet object to retrieve data from.
 * @param {String} rangeName - The range in A1 notation to retrieve data from.
 * @returns {Array} - An array of values, background, font color, font style, font weight, and font size.
 */
function getHtmlData_(sheet, rangeName) {
  let range = sheet.getRange(rangeName);
  let background = range.getBackgrounds();
  let val = range.getDisplayValues();
  let fontColor = range.getFontColors();
  let fontStyles = range.getFontStyles();
  let fontWeight = range.getFontWeights();
  let fontSize = range.getFontSizes();
  return [val, background, fontColor, fontStyles, fontWeight, fontSize];
}

/**
 * Deletes empty rows in a given sheet, keeping a minimum number of rows.
 * 
 * @function
 * @param {Object} sheet - The sheet object to delete empty rows from.
 * @param {Number} keepMinRows - The minimum number of rows to keep in the sheet.
 */
function deleteEmptyRows_(sheet, keepMinRows) {
  let maxRowsNum = sheet.getMaxRows();
  let lastRowNum = sheet.getLastRow();

  if (lastRowNum > keepMinRows && maxRowsNum - lastRowNum > 0) {
    sheet.deleteRows(lastRowNum + 1, maxRowsNum - lastRowNum);
  }
}

/**
 * Deletes excess empty rows in a given sheet, keeping a specified number of empty rows.
 * 
 * @function
 * @param {Object} sheet - The sheet object to delete excess empty rows from.
 * @param {Number} emptyRowsToKeep - The number of empty rows to keep in the sheet.
 */
function deleteExcessEmptyRows_(sheet, emptyRowsToKeep) {
  if (sheet) {
    let lastRow = sheet.getLastRow();
    let maxRows = sheet.getMaxRows();
    let emptyRows = maxRows - lastRow;
    let deleteFromRow = emptyRowsToKeep + lastRow;
    let rows2Delete = maxRows - deleteFromRow;

    if (emptyRows > emptyRowsToKeep) {
      sheet.deleteRows(deleteFromRow, rows2Delete);
    }
  }
}

/**
 * Syncs the columns of the target sheet with those of the source sheet by adding any missing columns.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sourceSheet - The source sheet to compare against.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} targetSheet - The target sheet to sync columns for.
 */
function syncSheetColumns_(sourceSheet, targetSheet) {
  // Get the number of columns in each sheet
  var sourceNumCols = sourceSheet.getMaxColumns();
  var targetNumCols = targetSheet.getMaxColumns();

  // If the target sheet has fewer columns than the source sheet, add any missing columns
  if (targetNumCols < sourceNumCols) {
    var numColsToAdd = sourceNumCols - targetNumCols;
    // Uncomment the following line to actually insert the missing columns
    targetSheet.insertColumnsAfter(targetNumCols, numColsToAdd);
  }
}

/**
 * Adds a specified number of empty rows to the bottom of a sheet.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to add the rows to.
 * @param {number} numRowsToAdd - The number of empty rows to add.
 */
function addEmptyRowsToBottom_(sheet, numRowsToAdd) {
  var lastRow = sheet.getLastRow();
  sheet.insertRowsAfter(lastRow, numRowsToAdd);
}

/**
 * Inserts a new row with the same formulas and formatting as the row above.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to insert the new row in.
 * @param {number} rowNum - The row number to insert the new row after.
 */
function insertNewRowWithFormulasAndFormatting_(sheet, rowNum) {
  var lastColumnIndex = sheet.getLastColumn();

  // Get the formulas and formatting from the row above
  var formulasRange = sheet.getRange(rowNum - 1, 1, 1, lastColumnIndex).getFormulas();
  var formatRange = sheet.getRange(rowNum - 1, 1, 1, lastColumnIndex).getBackgrounds();

  // Insert the new row and copy the formulas and formatting
  sheet.insertRowAfter(rowNum - 1);
  sheet.getRange(rowNum, 1, 1, lastColumnIndex).setFormulas(formulasRange);
  sheet.getRange(rowNum, 1, 1, lastColumnIndex).copyFormatToRange(sheet, 1, lastColumnIndex, rowNum, rowNum);
  sheet.getRange(rowNum, 1, 1, lastColumnIndex).setBackgrounds(formatRange);
}

/**
 * Returns an array of two-element arrays representing the ranges of columns to hide for each match.
 * 
 * The first element of each inner array is the column index to start hiding from,
 * and the second element is the number of columns to hide.
 * @returns {number[][]} An array of two-element arrays representing the ranges of columns to hide for each match.
 */
function getMatchesHideRanges_() {
  const lastTwoColumnsArr = MatchSheetConfigs.map(config => {
    const startNum = colNameToNum_(config.questionsStartColName);
    const numOfColumnsToHide = colNameToNum_(config.questionsEndColName) - startNum + 1;
    return [startNum, numOfColumnsToHide];
  });

  return lastTwoColumnsArr;
}

/**
 * Returns an array of all match sheet names from the given matches.
 *
 * @param {Array<Object>} matches - The array of match objects.
 * @returns {Array<string>} An array of match sheet names.
 */
function getMatchSheetNames_() {
  return MatchSheetConfigs.map(match => match.sheetName);
}

/**
 * Retrieves the first match sheet from the matchData array.
 *
 * @returns {Sheet} The first match sheet.
 */
function getFirstMatchSheet_() {
  const sheetName = getMatchSheetNames_()[0];
  return getSheetByName_(sheetName);
}

/**
 * Retrieves the template match sheet from the matchScoreTemplateSheetNamesArr array.
 *
 * @returns {Sheet} The template match sheet.
 */
function getTemplateMatchSheet_() {
  const sheetName = TemplateSheetNames.matchSheetName;
  return getSheetByName_(sheetName);
}

/**
 * Check if a given sheet name matches any of the matchSheetName values in an array of objects.
 *
 * @param {string} sheetName - The name of the sheet to check.
 * @returns {boolean} - True if sheetName matches any matchSheetName values, otherwise false.
 */
function isMatchSheet_(sheetName) {
  return MatchSheetConfigs.some(data => data.sheetName === sheetName);
}

/**
 * Removes filter from the specified sheet.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet object to remove filter from.
 */
function removeFilter_(sheet) {
  const filter = sheet.getFilter();
  if (filter) {
    filter.remove();
  }
}

/**
 * Renames sheets based on sheetCurrentAndNewNamesMap.
 *
 */
function renameSheets_() {
  const spreadsheet = getActiveSpreadsheet_();
  const sheets = spreadsheet.getSheets();
  console.log(`Number of Sheets in Spreadsheet is [${sheets.length}]`);
  console.log(`Number of Sheets in Names Map is [${SheetCurrentAndNewNamesMap.length}]`);

  SheetCurrentAndNewNamesMap.forEach(({ currentNames, newName }) => {
    if (currentNames && newName) {
      const currentNamesArray = currentNames.split(',');
      currentNamesArray.forEach((currentName) => {
        currentName = currentName.trim();
        if (currentName !== newName) {
          const sheet = sheets.find((s) => s.getName() === currentName);
          if (sheet) {
            sheet.setName(newName);
            console.log(`Renamed sheet "${currentName}" to "${newName}".`);
          } else {
            //log(`Sheet "${currentName}" not found.`, LogLevel.INFO);
          }
        }
      });
    }
  });
}

/**
 * Compares sheet in the ative sheets vs defined in sheetCurrentAndNewNamesMap
 */
function compareSheets() {
  const spreadsheet = getActiveSpreadsheet_();
  const sheets = spreadsheet.getSheets();
  const sheetCurrentNamesInMap = SheetCurrentAndNewNamesMap
    .flatMap(({ currentNames }) => currentNames.split(",").map(name => name.trim()));
  const sheetNewNamesInMap = SheetCurrentAndNewNamesMap.map(({ newName }) => newName);
  const sheetNamesInSpreadsheet = sheets.map(sheet => sheet.getName());

  const missingSheetNamesInMap = sheetNamesInSpreadsheet.filter(name => !sheetCurrentNamesInMap.includes(name));
  const missingCurrentSheetNamesInSpreadsheet = sheetCurrentNamesInMap.filter(name => !sheetNamesInSpreadsheet.includes(name));
  const missingNewSheetNamesInSpreadsheet = sheetNewNamesInMap.filter(name => !sheetNamesInSpreadsheet.includes(name));

  if (missingSheetNamesInMap.length > 0) {
    console.log(`Sheets not found in sheetCurrentAndNewNamesMap: ${missingSheetNamesInMap.join(', ')}`);
  }
  if (missingCurrentSheetNamesInSpreadsheet.length > 0) {
    console.log(`Current Sheet Names not found in spreadsheet: ${missingCurrentSheetNamesInSpreadsheet.join(', ')}`);
  }
  if (missingNewSheetNamesInSpreadsheet.length > 0) {
    console.log(`New Sheet Names not found in spreadsheet: ${missingNewSheetNamesInSpreadsheet.join(', ')}`);
  }
}

/**
 * Logs the names of all sheets in the active spreadsheet, one per line.
 */
function logAllSheetNames() {
  const sheets = getActiveSpreadsheet_().getSheets();
  const sheetNames = sheets.map(sheet => sheet.getName()).join('\n');
  console.log(`Sheet Names: \n${sheetNames}`);
}

/**
 * Updates the specified ranges in a sheet with corresponding values.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet object to update.
 * @param {Object[]} rangesToUpdate - An array of objects representing the ranges and values to be updated.
 * @param {string} rangesToUpdate[].range - The range to update in A1 notation (e.g., "D14:M14").
 * @param {any[][]} [rangesToUpdate[].values] - The values to set in the range.
 * @param {any} [rangesToUpdate[].value] - The value to set in the range.
 */
function updateSheetRanges_(sheet, rangesToUpdate) {
  rangesToUpdate.forEach(({ range, values, value }) => {
    if (values) {
      sheet.getRange(range).setValues(values);
    } else if (value) {
      sheet.getRange(range).setValue(value);
    }
  });

  flushAndWait_();
}

/**
 * Gets the minimum and maximum recorded match IDs from the daily results sheet.
 *
 * @returns {Object} - An object with the minimum and maximum recorded match IDs.
 * @property {number} minMatchID - The minimum recorded match ID.
 * @property {number} maxMatchID - The maximum recorded match ID.
 */
function getMinAndMaxRecordedMatchIDs_() {
  const sheet = getDailyResultsSheet_();
  const matchIDs = sheet.getRange(dailyResultsSheetObj.matchIDRange).getValues().flat().filter(id => isValidNumber_(id));
  return matchIDs.length ? { minMatchID: Math.min(...matchIDs), maxMatchID: Math.max(...matchIDs) } : {};
}

/********* Sheet By Names */

/**
 * Retrieves a sheet in the active spreadsheet by its name, without using any cached references.
 * 
 * @param {string} sheetName - The name of the sheet to retrieve.
 * @return {Sheet} The sheet with the specified name.
 */
function getSheetByNameNoCache_(sheetName) {
  let allSheets = getActiveSpreadsheet_().getSheets();
  for (let i = 0; isNotEmpty_(sheetName) && i < allSheets.length; i++) {
    let sheet = allSheets[i];
    if (sheetName === sheet.getName()) {
      return sheet;
    }
  }
}

//Sheet References in Other Sheets - checks all formuals if any sheet is being used

/**
 * Logs references to a given sheet in the active spreadsheet.
 *
 * @param {string} sheetName - The name of the sheet to check references for.
 */
function logSheetReferences_(sheetName) {
  log(`Starting to check references for Sheet: ${sheetName}`);

  const spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  const sheets = spreadsheet.getSheets();
  const references = [];

  sheets.forEach(sheet => {
    if (sheet.getName() !== sheetName) {
      const referencedCells = findReferencedSheets_(sheet, sheetName);
      if (referencedCells.length > 0) {
        references.push({
          sheetName: sheet.getName(),
          cells: referencedCells.join(", ")
        });
      }
    }
  });

  if (references.length > 0) {
    references.forEach(reference => {
      log(`Sheet: ${reference.sheetName}`);
      log(`  Cells: ${reference.cells}`);
    });
  } else {
    log(`No references found for Sheet: ${sheetName}`);
  }

  log(`Reference check complete for Sheet: ${sheetName}`);
}

/**
 * Finds cells in a sheet that reference a given sheet.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to check for references.
 * @param {string} sheetName - The name of the sheet to find references to.
 * @returns {string[]} An array of cell addresses that reference the given sheet.
 */
function findReferencedSheets_(sheet, sheetName) {
  const dataRange = sheet.getDataRange();
  const formulas = dataRange.getFormulas();
  const referencedCells = [];

  formulas.forEach((row, rowIndex) => {
    row.forEach((formula, columnIndex) => {
      if (typeof formula === 'string' && formula.includes(sheetName)) {
        referencedCells.push(getCellAddress_(rowIndex + 1, columnIndex + 1));
      }
    });
  });

  return referencedCells;
}

/**
 * Gets the address of a cell given its row and column indices.
 *
 * @param {number} row - The row index (1-based).
 * @param {number} column - The column index (1-based).
 * @returns {string} The cell address.
 */
function getCellAddress_(row, column) {
  const columnLetter = String.fromCharCode(64 + column);
  return `${columnLetter}${row}`;
}

/**
 * Returns the index of the column immediately following the last question end column
 * defined in MatchSheetConfigs.
 * 
 * @returns {number} The index of the next column after the last question end column.
 */
function getMatchEntryColIdx_() {
  // Get the index of the last question end column in MatchSheetConfigs
  const lastQueIndex = colNameToIndex_(MatchSheetConfigs[MatchSheetConfigs.length - 1].questionsEndColName);
  // Return the index of the column immediately following the last question end column
  return lastQueIndex + 1;
}

/**
 * Returns the number of the column immediately following the last question end column
 * defined in MatchSheetConfigs.
 * 
 * @returns {number} The number of the next column after the last question end column.
 */
function getMatchEntryColNum_() {
  // Get the index of the column immediately following the last question end column
  return getMatchEntryColIdx_() + 1;
}

/**
 * Hides columns within a specified range in a Google Sheets spreadsheet.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet in which to hide columns.
 * @param {number} startColumn - The starting column index (1-based) of the range to hide.
 * @param {number} endColumn - The ending column index (1-based) of the range to hide.
 */
function hideColumnsInRange(sheet, startColumn, endColumn) {
  const numColumns = endColumn - startColumn + 1;
  if (numColumns <= 0 || sheet.getMaxColumns() < endColumn) {
    return;
  }

  // Hide the columns in the specified range
  sheet.hideColumns(startColumn, numColumns);
}

/**
 * Hides all columns after the specified column index.
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet object.
 * @param {number} startColumn - The column index after which to hide all columns.
 */
function hideColumnsAfter(sheet, startColumn) {
  const numColumns = sheet.getMaxColumns() - startColumn;
  if (numColumns <= 0) {
    return;
  }

  // Hide the columns after the specified column
  sheet.hideColumns(startColumn + 1, numColumns);
}

/**
 * Changes the font of all sheets in the active spreadsheet to Lato.
 * 
 * @returns {void}
 */
function resetAllSheetsFont_(e) {
  console.time(resetAllSheetsFont_.name);

  var spreadsheet = SpreadsheetApp.getActiveSpreadsheet();
  var sheets = spreadsheet.getSheets();

  sheets.forEach(function (sheet) {
    resetDefaultFont_(sheet);
  });

  console.timeEnd(resetAllSheetsFont_.name);
}

/**
 * Changes the font of the sheet Lato.
 * 
 * @returns {void}
 */
function resetDefaultFont_(sheet) {
  var range = sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns());

  // Set the font family for the entire range
  range.setFontFamily(FontConstants.DEFAULT_FONT_FAMILY);
  console.timeEnd(sheet.getName());
}
