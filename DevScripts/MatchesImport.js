//** IMPORT, COPY Functions */

function testImportMatch(e) {
  importByMatchID_(e, 1)
}

/**
 * UI Menu to import Open  Matches
 */
function importByOpenMatchesMenu(e) {
  activateSheet_(getFirstMatchSheet_());
  importByOpenMatches_(e);
}

/**
 * UI Menu to import Open Matches.
 *
 * @param {EventObject} e - The event object.
 */
function importByOpenMatches_(e) {
  const matchID = ScheduleUtils.getFirstOpenMatchID();
  if (isNotEmpty_(matchID)) {
    importByMatchID_(e, matchID);
    activateSheet_(getFirstMatchSheet_());
  } else {
    activateSheet_(getScheduleViewSheet_());
    handleWarningAndErrors_(e, LogLevel.WARN, "No match are Open in " + scheduleTableSheetName + " sheet");
  }
}

/**
 * UI Menu to import Last Closed Matches.
 */
function importByLastClosedMatchesMenu(e) {
  activateSheet_(getFirstMatchSheet_());
  importByLastClosedMatches_();
}

/**
 * Imports the last closed match.
 * @param {Event} e - The onOpen event object.
 */
function importByLastClosedMatches_(e) {
  const matchID = ScheduleUtils.getLastClosedMatchID();
  if (isNotEmpty_(matchID)) {
    importByMatchID_(e, matchID);
    activateSheet_(getFirstMatchSheet_());
  } else {
    activateSheet_(getScheduleViewSheet_());
    handleWarningAndErrors_(e, LogLevel.WARN, "No match are Closed in " + scheduleTableSheetName + " sheet");
  }
}

/**
 * UI Menu for importing by Match ID
 */
function importByMatchIdMenu(e) {
  activateSheet_(getScheduleViewSheet_());

  let ui = SpreadsheetApp.getUi();
  let lastScoredMatchID = getDailyResultsSheet_().getRange(dailyResultsSheetObj.topMatchIDCell).getValue();
  if (isEmpty_(lastScoredMatchID)) {
    lastScoredMatchID = 0;
  }

  let result = ui.prompt(
    "Import by Match ID",
    "Enter the Match ID to import data:\nHighest Match ID scored in " + dailyBetsResultsSheetName + " is [" + lastScoredMatchID + "]\n\n",
    ui.ButtonSet.OK_CANCEL);
  let button = result.getSelectedButton();
  let text = result.getResponseText();
  if (button == ui.Button.OK) {
    let matchID = text;
    if (isValidNumber_(matchID)) {
      importByMatchID_(e, parseInt(matchID));
    }
  }
}

/**
 * Imports match data by match ID.
 * 
 * @param {any} e - Event object (if any) needed for further processing.
 * @param {string} matchID - The ID of the match to import data for.
 * @returns {boolean} - Indicates whether the import operation was successful.
 */
function importByMatchID_(e, matchID) {
  console.time(arguments.callee.name);
  clearMatchSheets_(e);

  const data = getSubmittedMatchData_(e, matchID);
  const success = isNotEmpty_(data) && copyIntoMatchSheets_(e, data);

  console.timeEnd(arguments.callee.name);
  return success;
}

/**
 * Retrieves the submitted match data for a given match ID.
 * 
 * @param {any} e - Event object (if any) needed for further processing.
 * @param {string} matchID - The ID of the match to retrieve data for.
 * @returns {Array|null} - An array containing the submitted match data, or null if an error occurred.
 */
function getSubmittedMatchData_(e, matchID) {
  let data = getMatchDataFromAnySourceSheet_(matchID);
  if (data.length === 0) {
    handleWarningAndErrors_(e, LogLevel.WARN, `No picks were found in ${dailyBetsSheetName} or ${dailyBetsClosedSheetName} sheet for Match ID: ${matchID}`);
    return null;
  }

  data = filterPredictions_(data);
  if (data.length > maxPuntersNum14) {
    throwError_(`Match ID [${matchID}] - More than max picks received [${data.length}]`, `There are ${data.length} picks for match id [${matchID}], max expected is ${maxPuntersNum14}. Check the [${dailyBetsSheetName} or ${dailyBetsClosedSheetName}] sheet(s) and delete the extra picks. Then try again from menu.`);
    return null;
  }

  return data;
}

/**
 * Get match data for the given match ID from either the open or closed source sheet, in that order.
 * 
 * @param {number} matchID - The ID of the match to get data for.
 * @returns {Array} An array of match data objects for the given match ID.
*/
function getMatchDataFromAnySourceSheet_(matchID, sorted = true) {
  const matchIDIdxs = MatchSheetConfigs.map(match => colNameToIndex_(match.titleIDColName));

  const dataFromClosed = getDailyPredictionsFormResponsesClosedSheet_().getDataRange().getValues();
  const dataFromOpen = getDailyPredictionsFormResponsesSheet_().getDataRange().getValues().slice(1);

  const filteredDataFromClosed = filterDataByMatchID_(dataFromClosed, matchID, matchIDIdxs);
  const filteredDataFromOpen = filterDataByMatchID_(dataFromOpen, matchID, matchIDIdxs);

  const combinedFilteredData = filteredDataFromClosed.concat(filteredDataFromOpen);

  if (sorted) {
    const timestampColIdx = 0;
    combinedFilteredData.sort(function (a, b) {
      return new Date(a[timestampColIdx]) - new Date(b[timestampColIdx]);
    });
  }

  return combinedFilteredData;
}

/**
 * Filters the data by match ID.
 * @param {Array<Array<any>>} data - The data to filter.
 * @param {string|number} matchID - The match ID to filter by.
 * @param {Array<number>} matchIDIdxs - The column indexes where match IDs could exist.
 * @returns {Array<Array<any>>} - The filtered data.
 */
function filterDataByMatchID_(data, matchID, matchIDIdxs) {
  return data.filter(row => {
    let matchIDFound = false;
    matchIDIdxs.forEach(idx => {
      if (!matchIDFound && idx < row.length && extractMatchID_(row[idx]) == matchID) {
        matchIDFound = true;
      }
    });
    return matchIDFound;
  });
}

/**
 * Copies match data into match sheets and updates the corresponding score sheets.
 *
 * @param {GoogleAppsScript.Events.SheetsOnFormSubmitEvent} e - The form submit event object.
 * @returns {boolean} Returns true if the function executes successfully.
 */
function copyIntoMatchSheets_(e, data) {
  if (data.length > 0) {
    const trumpCardUsageObj = getTrumpCardUsageCounts_(e);

    MatchSheetConfigs.forEach(({ sheetName }, i) => {
      const matchSheet = getSheetByName_(sheetName);
      const matchData = getMatchData_(data, i + 1, trumpCardUsageObj);

      const matchID = copyIntoMatchSheet_(matchSheet, matchData);
      if (isNotEmpty_(matchID)) {
        hideEmptyBetsRows_(matchSheet);
        const matchScheduledClosedTime = ScheduleUtils.getMatchScheduleClosedTime(matchID);
        Triggers.createMatchResultsStartScheduledTrigger(sheetName, matchScheduledClosedTime);
      }
    });

    const trumpCardFailedAttempts = getTrumpCardFailedAttempts_(trumpCardUsageObj);
    doAfterImport_(e, trumpCardFailedAttempts);
  }
  return true;
}

/**
 * Processes match data and updates trump card usage counts and failed attempts.
 *
 * @param {Array} data - The array of match data.
 * @param {number} matchKey - The key of the match.
 * @param {Object} trumpCardUsageObj - The object containing trump card usage counts for punters.
 * @returns {Array} - The processed match data.
 */
function getMatchData_(data, matchKey, trumpCardUsageObj) {
  const match = MatchSheetConfigs[matchKey - 1];

  return data.map(row => {
    const matchName = row[colNameToIndex_(match.titleIDColName)];
    const matchID = extractMatchID_(matchName);//row[colNameToIndex_(mId)];
    let entryInfo = row[getMatchEntryColIdx_()];
    const punterName = row[colNameToIndex_(MatchSheetConfigPunterColName)];
    const attemptedToUseTrumpCard = isNotEmpty_(punterName)
      && isNotEmpty_(matchID)
      && typeof matchName === 'string'
      && matchName.includes(TrumpCardKeys.INDICATOR_EMOJI.trim());

    if (attemptedToUseTrumpCard) {
      if (hasRemainingTrumpCardUsages_(trumpCardUsageObj, punterName, matchID)) {
        entryInfo += `${TrumpCardKeys.INDICATOR_EMOJI}`;
        updateTrumpCardMatchIDs_(trumpCardUsageObj, punterName, matchID, TrumpCardKeys.USED_MATCH_IDS_KEY);
      } else {
        updateTrumpCardMatchIDs_(trumpCardUsageObj, punterName, matchID, TrumpCardKeys.FAILED_ATTEMP_MATCH_IDS_KEY);
      }
    } else if (mustUseReaminingTrumpCard_(trumpCardUsageObj, punterName, matchID)) {
      entryInfo += `${TrumpCardKeys.INDICATOR_EMOJI}`;
    }

    return [matchID, entryInfo, punterName, ...row.slice(colNameToIndex_(match.questionsStartColName), colNameToIndex_(match.questionsEndColName) + 1)];
  });
}

/**
 * Copies match data into a match sheet and updates the corresponding score sheet.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} matchSheet - The match sheet to copy data into.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} scoreSheet - The score sheet to update with the match ID.
 * @param {any[][]} matchData - The match data to copy into the match sheet.
 */
function copyIntoMatchSheet_(matchSheet, matchData) {
  const matchID = matchData[0][0];

  if (matchData.length <= maxPuntersNum14 && isNotEmpty_(matchID)) {
    matchSheet.showSheet();
    activateSheet_(matchSheet);
    const sortCol = colNameToIndex_(matchSheetObj.punterNameColName);
    matchData = sortArrayByColumn_(matchData, sortCol);

    matchSheet.getRange(2, 1, matchData.length, matchData[0].length).setValues(matchData);
    matchSheet.getRange(matchSheetObj.resultMatchIDCell).setValue(matchID);

    const matchTitle = splitMatchDetails_(ScheduleUtils.getMatchTitle(matchID));
    matchSheet.getRange(matchSheetObj.matchNameCell).setValue(matchTitle);

    setMatchResultStatus_(matchSheet, MatchResultStatus.WAITING);

    flushAndWait_();

    console.info(`Imported Data for Match ID [${matchID}] , Match Sheet [${matchSheet.getName()}] Updated.`);
    return matchID;
  }
}

/**
 * Splits the match title into its components and returns them joined with newlines.
 * 
 * @param {string} matchTitle - The title of the match including match number, date, teams, and venue.
 * @returns {string} The split match details joined with newlines.
 */
function splitMatchDetails_(matchTitle) {
  const [matchNumberAndDate, rest] = matchTitle.split(": ");
  const [teams, venue] = rest.split(" at ");
  const time = getCurrentTimestampTimeOnly_();
  return [matchNumberAndDate, `${teams} ${venue}`, time].join("\n");
}

/**
 * Filters the data from the daily predictions sheet and creates an array of unique submissions. 
 * Each submission includes the last one made by each punter, with an entry ID added. The punter's 
 * name is retrieved from the email they submitted with.
 * 
 * @param {Array} data - The data from the daily predictions sheet.
 * @returns {Array} An array of unique submissions, including the last submission made by each punter, 
 * with an entry ID added.
 */
function filterPredictions_(data) {
  const punterColumnIndex = colNameToIndex_(dailyPredictionsSheetObj.punterColName);
  const firstMatchTitleColIdx = colNameToIndex_(MatchSheetConfigs[0].titleIDColName);

  const uniques = [];
  const arrMinLength = getMatchEntryColNum_();

  for (let i = data.length - 1; i >= 0; i--) {
    const row = arrMinLength < data[i].length ? data[i] : data[i].concat(Array(arrMinLength - data[i].length).fill(''));
    let punterName = getPunterNameFromRow_(row, punterColumnIndex);
    if (isEmpty_(punterName)) { continue; }

    row[punterColumnIndex] = punterName;

    if (uniques.some(uniqueRow => uniqueRow[punterColumnIndex] == punterName)) { continue; }

    row[arrMinLength - 1] = getEntryInfo_(i, row[firstMatchTitleColIdx]);
    uniques.push(row);
  }

  return uniques.reverse();
}

/**
 * Returns the entry information for a given row index and match title.
 * If the match title includes the bot submitted emoji, the entry ID is appended with the emoji.
 * 
 * @param {number} i - The row index.
 * @param {string} matchTitle - The title of the match.
 * @returns {string} The entry information.
 */
function getEntryInfo_(i, matchTitle) {
  const entryID = (i + 1);
  const hasEmoji = matchTitle.toString().includes(BOT_SUBMITTED_EMOJI);

  if (hasEmoji) {
    const hasStar = matchTitle.toString().includes(STAR_CHAR);
    return hasStar ? `${entryID}${BOT_SUBMITTED_EMOJI}${STAR_CHAR}` : `${entryID}${BOT_SUBMITTED_EMOJI}`;
  } else {
    return entryID.toString();
  }
}

/**
 * Retrieves the punter name from the specified row using the punter column index.
 * If the punter name is empty, it attempts to fetch the punter name using the email column.
 * 
 * @param {Array} row - The row from which to retrieve the punter name.
 * @param {number} punterColumnIndex - The index of the punter column.
 * @returns {string} The punter name.
 */
function getPunterNameFromRow_(row, punterColumnIndex) {
  const emailIndex = colNameToIndex_(dailyPredictionsSheetObj.emailColName);

  let punterName = row[punterColumnIndex];
  if (isEmpty_(punterName)) {
    punterName = PuntersUtils.getPunterNameByEmail_(row[emailIndex]);
  }
  return punterName;
}

/**
 * Runs the necessary functions after importing match data into the sheets.
 * Activates the Match 1 sheet, updates all match results, and flushes the spreadsheet changes.
 * Sends emails for failed trump card attempts.
 *
 * @param {Object} e - The event object.
 * @param {Object[]} trumpCardFailedAttempts - An array of objects representing failed trump card attempts.
 */
function doAfterImport_(e, trumpCardFailedAttempts) {
  activateSheet_(getFirstMatchSheet_());
  flushAndWait_();

  updateAllMatchResults_(e); // For immediate one time update and creating needed triggers.

  // Email to all failed attempt punters
  EmailTrumpCardUsage.sendEmailsOnFailedAttempt(e, trumpCardFailedAttempts);
  flushAndWait_();
}

/**
 * If ENV_GET_FORM_RESPONSES_FROM_PROD is true in EnvProperties return from prod spreadsheet
 * Otherwise return the current active DailyResponses Open sheet
 */
function getDailyPredictionsFormResponsesSheet_() {
  return getSheetFromProdOrActiveSpreadsheet_(dailyBetsSheetName);
}

/**
 * If ENV_GET_FORM_RESPONSES_FROM_PROD is true in EnvProperties return from prod spreadsheet
 * Otherwise return the current active DailyResponses Closed sheet
 */
function getDailyPredictionsFormResponsesClosedSheet_() {
  return getSheetFromProdOrActiveSpreadsheet_(dailyBetsClosedSheetName);
}

/**
 * 
 */
function getSheetFromProdOrActiveSpreadsheet_(sheetName) {
  if (isNotProdEnv_() && EnvConfigClass.isGettingProdFormResponses()) {
    let sourceSpreadsheet = SpreadsheetApp.openById(ConfigManager.getConfigClass().PROD_ENV_SPREADSHEET_ID);
    let sheet = sourceSpreadsheet.getSheetByName(sheetName);
    return sheet;
  } else {
    return getSheetByName_(sheetName);
  }
}

/**
 * Extracts the match ID from a string containing the ID enclosed in square brackets.
 * If no match ID is found, returns an empty string.
 *
 * @param {string} str - The string to extract the match ID from.
 * @returns {string|number} The match ID, or an empty string if no match ID is found.
 *
 * @example
 * // returns "31"
 * const matchID = extractMatchID("[#31] Oct 12 - SCOT vs WI");
 */
function extractMatchID_(str) {
  const regex = /\[#(.*?)\]/;
  const match = regex.exec(str);
  if (match && match[1]) {
    return parseInt(match[1], 10); // Parse the matched ID as an integer
  }
  return ""; // Return an empty string if no match ID is found
}

/**
 * Hides empty rows while keeping one empty row visible.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to operate on.
 */
function hideEmptyBetsRows_(sheet) {
  var rowStart = rowStartNum2;
  var rowEnd = betsLastRowNum15;
  var columnCount = columnNumForPoints;

  showAllBetsRows_(sheet);

  var range = sheet.getRange(rowStart, 1, rowEnd - rowStart + 1, columnCount);
  var values = range.getValues();

  // Find the last non-empty row
  var lastNonEmptyRow = rowStart - 1;
  for (var i = 0; i < values.length; i++) {
    if (values[i].some(cell => cell !== "")) {
      lastNonEmptyRow = rowStart + i;
    }
  }

  // Hide rows from last non-empty row + 2 to rowEnd
  if (lastNonEmptyRow < rowEnd - 1) {
    sheet.hideRows(lastNonEmptyRow + 2, rowEnd - (lastNonEmptyRow + 1));
  }
}

/**
 * Shows all bet rows in the given sheet.
 * 
 * @param {Sheet} sheet - The sheet object where the bet rows need to be shown.
 */
function showAllBetsRows_(sheet) {
  var rowStart = rowStartNum2;
  var rowEnd = betsLastRowNum15;

  // Ensure all rows are shown before proceeding
  sheet.showRows(rowStart, rowEnd - rowStart + 1);
}
