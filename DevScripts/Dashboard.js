/// DASHBOARD SHEET Functions ////

/**
 * Calls functions to update the daily entries list on the dashboard UI.
 * @param {Object} e - The event object.
 */
function updateDashboardBetsListsMenu(e) {
  logSTART_(updateDashboardBetsListsMenu.name);

  activateSheet_(getDashboardSheet_());
  updateDashboardBetsLists_(e);

  logEND_(updateDashboardBetsListsMenu.name);
}

/**
 * Updates the daily entries list on the dashboard UI.
 * @param {Object} e - The event object.
 */
function updateDashboardBetsLists_(e) {
  console.time(updateDashboardBetsLists_.name);

  clearDashboardBetsLists_();//It takes time to update?
  if (ScheduleUtils.isAnyMatchOpen()) {
    const missingEntries = getMissingDailyPredictions_();
    const submittedEntries = getUniqueValuesWithCounts_(getPuntersWithSubmittedDailyPicks_());

    updateDasboardDailyMissingBets_(missingEntries);
    updateDasboardDailySubmittedBets_(submittedEntries);
  }

  console.timeEnd(updateDashboardBetsLists_.name);
}

/**
 * Clears the daily entries list on the dashboard UI.
 */
function clearDashboardBetsLists_() {
  const sheet = getDashboardSheet_();
  sheet.getRange(dashboardSheetObj.dailyMissingEntriesRange).clearContent();
  sheet.getRange(dashboardSheetObj.dailySubmittedEntriesRange).clearContent();
}

/**
 * Update the daily missing entries range with the given entries, filling in missing entries with undefined values
 * @param {any[][]} entries - The entries to update the range with
 */
function updateDasboardDailyMissingBets_(entries) {
  const sheet = getDashboardSheet_();
  const range = sheet.getRange(dashboardSheetObj.dailyMissingEntriesRange);
  updateRangeWithEntries_(entries, range);
}

/**
 * Update the daily submitted entries range with the given entries, filling in missing entries with undefined values
 * @param {any[][]} entries - The entries to update the range with
 */
function updateDasboardDailySubmittedBets_(entries) {
  const sheet = getDashboardSheet_();
  const range = sheet.getRange(dashboardSheetObj.dailySubmittedEntriesRange);
  updateRangeWithEntries_(entries, range);
}

/**
 * Updates a given range with a 2-dimensional array of data, filling in any missing rows with empty strings.
 * 
 * @param {Array<Array<string>>} entries The 2-dimensional array of data to update the range with.
 * @param {GoogleAppsScript.Spreadsheet.Range} range The range to update with the data.
 */
function updateRangeWithEntries_(entries, range) {
  const numRows = range.getNumRows();
  const numCols = range.getNumColumns();

  // Fill in any missing rows with empty strings.
  while (entries.length < numRows) {
    entries.push(new Array(numCols).fill(""));
  }
  // Set the new values in the range.
  range.setValues(entries.map(entry => [entry]));
}

/**
 * 
 */
function refreshStandingsTable_(e) {
  const sheet = getStandingsTableSheet_();
  updateRefreshCounter_(sheet, standingsTableSheetObj.randomNumCell);
}
