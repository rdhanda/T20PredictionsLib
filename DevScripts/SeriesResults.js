//Overall Results
const PROBABLES_PLAYERS_LIMIT = 10;

/**
 * Activates the Overall Results sheet and updates the overall results
 * @param {Object} e - Event object
 */
function updateSeriesResultsMenu(e) {
  updateSeriesResults_(e);
}

/**
 * Updates the overall results with the live scores
 * @param {Object} e - Event object
 */
function updateSeriesResults_(e) {
  logSTART_(arguments.callee.name);
  const sheet = getSeriesResultsSheet_();
  activateSheet_(sheet);

  const categoriesURLs = PlayerStatsUtils.getStatsCategoriesURLs();

  if (isNotEmpty_(categoriesURLs)) {
    const maxRunsResults = getSeriesMaxRunsResults_(categoriesURLs);
    const maxWicketsResults = getSeriesMaxWicketsResults_(categoriesURLs);
    const highestSRResults = getSeriesHighestBattingSRResults_(categoriesURLs);
    const bestEconomyResults = getSeriesBestEconomyResults_(categoriesURLs);

    const data = [
      [maxRunsResults.results, maxWicketsResults.results, highestSRResults.results, bestEconomyResults.results],
      [maxRunsResults.probables, maxWicketsResults.probables, highestSRResults.probables, bestEconomyResults.probables]
    ];

    // Update the sheet range with the results and probables data
    const resultRange = sheet.getRange(overallResultsSheetObj.resultsAndProbablesRange);
    resultRange.setValues(data);
    updateRefreshCounter_(sheet, overallResultsSheetObj.randomNumCell);
  }

  logEND_(arguments.callee.name);
}



/**
 * Retrieves the overall max runs results by retrieving player data from the specified URL.
 *
 * @param {string} categoriesURLs - The array of category URLs.
 * @returns {object} - An object containing the overall max runs results.
 *                     The object has `result` and `probables` attributes.
 */
function getSeriesMaxRunsResults_(categoriesURLs) {
  const stats = getSeriesMaxRunsData_(categoriesURLs);
  const data = stats.data;

  if (isEmpty_(stats) || isEmpty_(data)) {
    return {};
  }

  const playerIndex = stats.playerIndex;
  const runsIndex = stats.runsIndex;
  const maxRuns = data[0][runsIndex];

  const results = data
    .filter(row => row[runsIndex] === maxRuns)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, runsIndex))
    .join('\n');

  const probables = data
    .slice(0, PROBABLES_PLAYERS_LIMIT)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, runsIndex))
    .join('\n');

  console.info(results);

  return {
    results: results,
    probables: probables,
  };
}


function getSeriesMaxRunsData_(categoriesURLs) {
  const rawData = getTableDataForCategory_(categoriesURLs, KEY_MOST_RUNS.key);
  if (isEmpty_(rawData)) {
    return {};
  }

  const headers = rawData[0];
  const playerIndex = headers.indexOf('Player');
  const runsIndex = headers.indexOf('Runs');

  const filteredData = rawData.slice(1); // Remove the top row
  //If there no matchign data for min overs
  if (isEmpty_(filteredData)) {
    return {}
  }

  const data = filteredData.sort((a, b) => b[runsIndex] - a[runsIndex]);
  return { data, playerIndex, runsIndex };
}

/**
 * Retrieves the overall highest wicket taker results based on the provided categories URLs.
 * @param {Array<Object>} categoriesURLs - An array of objects containing category key and URL.
 * @returns {Object} - An object containing the results and probables.
 */
function getSeriesMaxWicketsResults_(categoriesURLs) {
  const data = getTableDataForCategory_(categoriesURLs, KEY_MOST_WICKETS.key);
  if (isEmpty_(data)) {
    return {};
  }

  const headers = data[0];
  const playerIndex = headers.indexOf('Player');
  const wicketsIndex = headers.indexOf('Wkts');

  const filteredData = data.slice(1); // Remove the top row
  //If there no matchign data for min overs
  if (isEmpty_(filteredData)) {
    return {}
  }

  const sortedData = filteredData.sort((a, b) => b[wicketsIndex] - a[wicketsIndex]);
  const maxWickets = sortedData[0][wicketsIndex];

  const results = sortedData
    .filter(row => row[wicketsIndex] === maxWickets)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, wicketsIndex))
    .join('\n');

  const probables = sortedData
    .slice(0, PROBABLES_PLAYERS_LIMIT)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, wicketsIndex))
    .join('\n');

  console.info(results);

  return {
    results: results,
    probables: probables,
  };
}

/**
 * Retrieves the overall highest batting strike rate results based on the provided categories URLs.
 * @param {Array<Object>} categoriesURLs - An array of objects containing category key and URL.
 * @returns {Object} - An object containing the results and probables.
 */
function getSeriesHighestBattingSRResults_(categoriesURLs) {
  const data = getTableDataForCategory_(categoriesURLs, KEY_HIGHEST_SR.key);
  if (isEmpty_(data)) {
    return {};
  }

  const headers = data[0];
  const playerIndex = headers.indexOf('Player');
  const strikeRateIndex = headers.indexOf('SR');
  const ballsFacedIndex = headers.indexOf('BF');

  const filteredData = data
    .slice(1) // Remove the top row
    .filter(row => row[ballsFacedIndex] >= getSeriesMinBallsFacedForSR_());

  //If there no data
  if (isEmpty_(filteredData)) {
    return {};
  }

  const sortedData = filteredData.sort((a, b) => b[strikeRateIndex] - a[strikeRateIndex]);
  const maxStrikeRate = sortedData[0][strikeRateIndex];

  const results = sortedData
    .filter(row => row[strikeRateIndex] === maxStrikeRate)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, strikeRateIndex))
    .join('\n');

  const probables = sortedData
    .slice(0, PROBABLES_PLAYERS_LIMIT)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, strikeRateIndex))
    .join('\n');

  console.info(results);

  return {
    results: results,
    probables: probables,
  };
}

/**
 * Retrieves the overall best economy results based on the provided categories URLs.
 * @param {Array<Object>} categoriesURLs - An array of objects containing category key and URL.
 * @returns {Object} - An object containing the results and probables.
 */
function getSeriesBestEconomyResults_(categoriesURLs) {
  const data = getTableDataForCategory_(categoriesURLs, KEY_BEST_ECONOMY.key);
  if (isEmpty_(data)) {
    return {};
  }

  const headers = data[0];
  const playerIndex = headers.indexOf('Player');
  const economyIndex = headers.indexOf('Econ');
  const oversIndex = headers.indexOf('Overs');

  const filteredData = data
    .slice(1) // Remove the top row
    .filter(row => row[oversIndex] >= getSeriesMinOversFacedForEconomy_());

  //If there no data
  if (isEmpty_(filteredData)) {
    return {};
  }

  const sortedData = filteredData.sort((a, b) => a[economyIndex] - b[economyIndex]);
  const maxStrikeRate = sortedData[0][economyIndex];

  const results = sortedData
    .filter(row => row[economyIndex] === maxStrikeRate)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, economyIndex))
    .join('\n');

  const probables = sortedData
    .slice(0, PROBABLES_PLAYERS_LIMIT)
    .map(row => formatPlayerAndStatsDisplay_(row, playerIndex, economyIndex))
    .join('\n');

  console.info(results);

  return {
    results: results,
    probables: probables,
  };
}

/**
 * Retrieves the table data for a specific category from the provided categories URLs.
 *
 * @param {Array<Object>} categoriesURLs - An array of objects containing category key and URL.
 * @param {string} categoryKey - The key of the category to retrieve the data for.
 * @returns {Array<Array<string>>} - The table data for the specified category.
 */
function getTableDataForCategory_(categoriesURLs, categoryKey) {
  const url = extractURLForKey_(categoriesURLs, categoryKey);
  if (isEmpty_(url)) { return []; }

  return getTableDataFromUrl_(url);
}

/**
 * Retrieves the URL for a specific key from an array of URL objects.
 *
 * @param {Array} urls - An array of URL objects.
 * @param {string} key - The key to search for.
 * @returns {string|undefined} - The URL corresponding to the key, or undefined if not found.
 */
function extractURLForKey_(urls, key) {
  const urlObj = urls.find(obj => obj.key === key);
  return urlObj ? urlObj.url : undefined;
}

/**
 * Retrieves table data from the specified URL.
 *
 * @param {string} url - The URL to fetch the table data from.
 * @returns {Array<Array<any>>} - The table data as a 2D array.
 */
function getTableDataFromUrl_(url) {
  const $ = getCheerioInstances_(url);
  if (isEmpty_($)) { return []; }

  return extractTableDataByIndex_($, 0);
}

/**
 * Formats the player name and stats display.
 *
 * @param {Array<any>} row - The row of data containing the player name and stats.
 * @param {number} statsIndex - The index of the stats in the row.
 * @returns {string} - The formatted player and stats display.
 */
function formatPlayerAndStatsDisplay_(row, playerIndex, statsIndex) {
  return `${row[playerIndex]} [${row[statsIndex]}]`;
}

/**
 * Retrieves the overall minimum number of balls faced for calculating strike rate.
 *
 * @returns {number} - The minimum number of balls faced for strike rate calculation.
 */
function getSeriesMinBallsFacedForSR_() {
  return ConfigManager.getConfigClass().SERIES_MIN_BALLS_FACED_FOR_SR_VALUES[getActiveSeriesMatchCategory_()];
}

/**
 * Retrieves the overall minimum number of overs faced for calculating economy.
 *
 * @returns {number} - The minimum number of overs faced for economy calculation.
 */
function getSeriesMinOversFacedForEconomy_() {
  return ConfigManager.getConfigClass().SERIES_MIN_OVERS_FOR_ECONOMY_VALUES[getActiveSeriesMatchCategory_()];
}
