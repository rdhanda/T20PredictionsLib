//Teams.gs//

/**
 * Get the team ID by team name.
 *
 * @param {string} inputTeamName - The name of the team.
 * @returns {string} The team ID.
 */
function getTeamIdByName_(inputTeamName) {
  if (MATCH_RESULT_TIED == inputTeamName) { return MATCH_RESULT_TIED; }

  const teamInfo = getTeamInfo_(inputTeamName);
  return teamInfo.teamID;
}

/**
 * Retrieves team information based on the input team name.
 * @param {string} inputTeamName - The input team name to search for.
 * @returns {Object} An object containing the found team information or just the input team name if it's empty.
 */
function getTeamInfo_(inputTeamName) {
  const sInputTeamName = sanitizeTeamName_(inputTeamName);
  if (isEmpty_(sInputTeamName)) { return { inputTeamName }; }

  const teamsData = getTeamsSheetCachedData_();
  let teamInfo = findTeamInfo_(sInputTeamName, teamsData);

  return teamInfo;
}

/**
 * Finds team information based on the input team name and team data.
 * @param {string} inputTeamName - The input team name to search for.
 * @param {Array<Array<string>>} teamsData - The array of teams data.
 * @returns {Object} An object containing the found team information or just the input team name if no match is found.
 */
function findTeamInfo_(inputTeamName, teamsData) {
  let match;

  match = findExactTeamMatch_(inputTeamName, teamsData);
  if (match) return match;

  match = findExactPlusTeamMatch_(inputTeamName, teamsData);
  if (match) return match;

  match = findPartialTeamMatch_(inputTeamName, teamsData);
  if (match) return match;

  // If no match is found, return input team name
  log(`No Matching Team found for '${inputTeamName}'.`, LogLevel.ERROR);
  return { inputTeamName };
}

/**
 * Finds an exact match of the input team name in the team data.
 * @param {string} inputTeamName - The input team name to search for.
 * @param {Array<Array<string>>} teamsData - The array of team data.
 * @returns {Object|null} An object containing the found team information or null if no exact match is found.
 */
function findExactTeamMatch_(inputTeamName, teamsData) {
  for (let i = 1; i < teamsData.length; i++) {
    const { teamID, teamName, otherTeamNames } = extractTeamInfo_(teamsData, i);

    if (isEmpty_(teamID) || isEmpty_(teamName)) { continue; }

    if (containsExactMatch_([teamID, teamName, otherTeamNames], inputTeamName)) {
      //log(`Found exact match of ${inputTeamName}`)
      return { inputTeamName, teamID, teamName, otherTeamNames };
    }
  }
  return null;
}

/**
 * Checks if the target string is an exact match for any element in the source array or the source string, considering comma-separated values.
 * @param {Array<string>|string} source - The array of strings or a single string to search for matches.
 * @param {string} target - The target string to search for.
 * @returns {boolean} True if an exact match is found, otherwise false.
 */
function containsExactMatch_(source, target) {
  // If source is a string, split it into an array containing only one element
  if (typeof source === 'string') { source = [source]; }

  // Check each element in the source array
  return source.some(str => {
    // Split the element by commas, trim and lowercase each part, and check for a match
    return str.split(',').some(part => part.trim().toLowerCase() === target.trim().toLowerCase());
  });
}

/**
 * Finds a team match based on the input team name within the provided teams data.
 * @param {string} inputTeamName - The team name to search for.
 * @param {Array<Array<any>>} teamsData - The array containing team data.
 * @returns {?{teamID: string, teamName: string, otherTeamNames: Array<string>} | null} The matched team information if found, or null if no match is found.
 * @example
 * // Example usage:
 * const teamsData = [
 *   [ 'ID', 'Team', 'Name', 'Other Names' ],
 *   [ 1, 'CSK', 'Chennai Super Kings', '' ],
 *   [ 2, 'MI', 'Mumbai Indians', '' ],
 *   // Additional team data...
 * ];
 * const teamName = 'Something Chennai Super Kings Whatever';
 * const match = findRegexTeamMatch_(teamName, teamsData);
 * console.log(match);//CSK
 */
function findExactPlusTeamMatch_(inputTeamName, teamsData) {
  for (let i = 1; i < teamsData.length; i++) {
    const { teamID, teamName, otherTeamNames } = extractTeamInfo_(teamsData, i);

    if (isEmpty_(teamID) || isEmpty_(teamName)) { continue; }

    const regexPattern = buildRegexPattern_(teamName, teamID, otherTeamNames);

    if (regexPattern.test(inputTeamName.trim())) {
      log(`Found Exact Plus match of ${inputTeamName}`, LogLevel.WARN);
      return { inputTeamName, teamID, teamName, otherTeamNames };
    }
  }
  return null;
}

/**
 * Builds a regular expression pattern for matching team names or codes, including other team names if provided.
 * 
 * @param {string} teamName - The team name.
 * @param {string} teamCode - The team code.
 * @param {string} [otherTeamNames] - Optional other team names, separated by commas.
 * @returns {RegExp} The regular expression pattern.
 */
function buildRegexPattern_(teamName, teamCode, otherTeamNames) {
  const escapedName = escapeRegExp_(teamName);
  const escapedCode = escapeRegExp_(teamCode);
  let pattern = `(?:\\b${escapedName}|${escapedCode}(?:\\s*\\(.*?\\))?\\b)`;

  if (isNotEmpty_(otherTeamNames)) {
    const otherNamesArray = otherTeamNames.split(',').map(name => name.trim());
    const escapedOtherNames = otherNamesArray.map(escapeRegExp_).join('|');
    pattern += `|(?:\\b(?:${escapedOtherNames})\\b)`;
  }
  return new RegExp(pattern, 'i');
}

/**
 * Finds a partial match of the input team name in the team data.
 * @param {string} inputTeamName - The input team name to search for.
 * @param {Array<Array<string>>} teamsData - The array of teams data.
 * @returns {Object|null} An object containing the found team information or null if no partial match is found.
 */
function findPartialTeamMatch_(inputTeamName, teamsData) {
  for (let i = 1; i < teamsData.length; i++) {
    const { teamID, teamName, otherTeamNames } = extractTeamInfo_(teamsData, i);

    if (isEmpty_(teamID) || isEmpty_(teamName)) { continue; }

    if (containsPartialMatch_([teamName, otherTeamNames, teamID], inputTeamName)) {
      log(`Found Partial match of ${inputTeamName}`, LogLevel.WARN);
      return { inputTeamName, teamID, teamName, otherTeamNames };
    }
  }
  return null;
}

/**
 * Checks if the source string or array contains any partial match for the substring.
 * @param {string|Array<string>} teamInfo - The source string or array to search for matches.
 * @param {string} inputTeamName - The substring to search for.
 * @returns {boolean} True if a partial match is found, otherwise false.
 */
function containsPartialMatch_(teamInfo, inputTeamName) {
  // If source is a string, split it into an array containing only one element
  if (typeof teamInfo === 'string') {
    teamInfo = [teamInfo];
  }

  // Convert substring to partial words array
  const partialWords = inputTeamName.split(/\s+/).filter(Boolean);

  // Check each element in the source array
  return teamInfo.some(str => {
    // Check if any partial word is found in the source element
    return partialWords.some(word => new RegExp(`\\b${escapeRegExp_(word)}\\b`, 'i').test(str));
  });
}

/**
 * Extracts team information from the team data array.
 * @param {Array<Array<string>>} teamData - The array of team data.
 * @param {number} index - The index of the team data row.
 * @returns {Object} An object containing the extracted team information.
 */
function extractTeamInfo_(teamData, index) {
  return {
    teamID: teamData[index][colNameToIndex_(teamsSheetObj.teamIDColName)],
    teamName: teamData[index][colNameToIndex_(teamsSheetObj.teamNameColName)],
    otherTeamNames: teamData[index][colNameToIndex_(teamsSheetObj.otherTeamNamesColName)]
  };
}

/**
 * Sanitizes the team name by removing specified characters.
 *
 * @param {string} inputTeamName - The team name to sanitize.
 * @returns {string} - The sanitized team name.
 */
function sanitizeTeamName_(inputTeamName) {
  if (isEmpty_(inputTeamName)) { return; }

  var outputString = inputTeamName;

  for (var i = 0; i < TEAM_NAME_CHARS_TO_REMOVE.length; i++) {
    var escapedChar = escapeRegExp_(TEAM_NAME_CHARS_TO_REMOVE[i]);
    var regex = new RegExp(escapedChar, 'gi'); // Adding 'i' flag for case insensitivity
    outputString = outputString.replace(regex, '');
  }

  return outputString.trim();
}

/**
 * Escapes special characters in a string for regex.
 *
 * @param {string} str - The input string to escape.
 * @returns {string} The input string with special characters escaped for regex.
 */
function escapeRegExp_(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
