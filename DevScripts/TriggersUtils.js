//Trigger Utlis//

const TriggerObjKeys = {
  execution: "exec",
  triggerID: "tid"
}

/**
 * Class for Trigger Utils
 */
class TriggersUtils {

  /**
   * Checks if a trigger can be executed based on a function name and lock status.
   *
   * @returns {boolean} - Returns true if the trigger can be executed, otherwise false.
   */
  static canExecuteTrigger() {
    if (EnvConfigClass.isTriggerExecutionHalted()) {
      log(`Temporary HALT in Trigger execution: no triggers are being executed.`, LogLevel.WARN);
      return false;
    }

    return true;
  }

  /**
   * Saves the trigger ID to properties and returns the new trigger ID.
   *
   * @param {GoogleAppsScript.Script.Trigger} trigger - The trigger to save.
   * @returns {string} The new trigger ID.
   */
  static saveTrigger(trigger) {
    if (trigger) {
      const fullFuncName = trigger.getHandlerFunction();
      const tid = trigger.getUniqueId();
      const obj = { [TriggerObjKeys.triggerID]: tid };
      setEnvProperty_(fullFuncName, obj);
      return tid;
    }
  }

  /**
   * Delete all existing triggers.
   */
  static deleteAllTriggers(e) {
    const triggers = ScriptApp.getProjectTriggers();
    triggers.forEach(trigger => {
      TriggersUtils.deleteTrigger(trigger);
    });
  }

  /**
   * Deletes a single trigger from the current project.
   * 
   * @param {GoogleAppsScript.Script.Trigger} trigger - The trigger to be deleted.
   */
  static deleteTrigger(trigger) {
    if (trigger) {
      ScriptApp.deleteTrigger(trigger);
      const fullFuncName = trigger.getHandlerFunction();

      if (fullFuncName) {
        deleteEnvProperty_(fullFuncName);
      }
      const triggerId = trigger.getUniqueId();
      console.info(`DELETED Trigger for "${fullFuncName}" with ID "${triggerId}"`);
    }
  }

  /**
   * Deletes all triggers associated with the specified function name.
   * 
   * @param {string} funcName - The name of the function whose triggers should be deleted.
   */
  static deleteTriggerByFuncName(funcName) {
    const trigger = TriggersUtils.getTriggerByFuncName(funcName);
    TriggersUtils.deleteTrigger(trigger);
  }

  // Trigger Execution Time

  /**
   * Updates the execution time of the specified function in the environment properties.
   * 
   * @param {string} funcName - The name of the function whose execution time to update.
   */
  static updateTriggerExecutionTime(funcName) {
    const trigger = TriggersUtils.getTriggerByFuncName(funcName);

    if (trigger) {
      const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
      const obj = getEnvProperty_(fullFuncName);
      obj[TriggerObjKeys.execution] = getCurrentLongTimesptamp_();
      setEnvProperty_(fullFuncName, obj);
    }
  }

  /**
   * Retrieves the execution time of the specified function from the environment properties.
   * 
   * @param {string} funcName - The name of the function whose execution time to retrieve.
   * @returns {number|null} The execution time as a timestamp, or null if not found.
   */
  static getFunctionExecutedTime(funcName) {
    const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
    const obj = getEnvProperty_(fullFuncName);
    return obj && obj.hasOwnProperty(TriggerObjKeys.execution) ? obj[TriggerObjKeys.execution] : null;
  }

  /**
   * Checks if the specified function has been executed by retrieving its execution time.
   * 
   * @param {string} funcName - The name of the function to check.
   * @returns {boolean} True if the function has been executed, otherwise false.
   */
  static hasFunctionExecuted(funcName) {
    const executionTime = TriggersUtils.getFunctionExecutedTime(funcName);
    return executionTime !== null;
  }

  /**
   * Checks if a trigger already exists for the specified function.
   *
   * @param {string} funcName - The full name of the function (including library ID) to check for a trigger.
   * @returns {boolean} True if a trigger exists, false otherwise.
   */
  static isTriggerExists(funcName) {
    const trigger = TriggersUtils.getTriggerByFuncName(funcName);
    if (trigger != null) {
      log(`EXISTS - Trigger for "${funcName}" already exists with ID "${trigger.getUniqueId()}"`, LogLevel.INFO);
      return true;
    }

    return false;
  }

  /**
   * Retrieves the trigger associated with the specified function name.
   * 
   * @param {string} funcName - The name of the function whose trigger should be retrieved.
   * @returns {GoogleAppsScript.Script.Trigger|null} The trigger if found, otherwise null.
   */
  static getTriggerByFuncName(funcName) {
    const fullFuncName = TriggersUtils.getFullFunctionName(funcName);
    const existingTriggers = ScriptApp.getProjectTriggers();

    for (const trigger of existingTriggers) {
      if (trigger.getHandlerFunction() === fullFuncName) {
        return trigger;
      }
    }

    return null;
  }

  /**
   * Returns the full name of the function (including library ID).
   *
   * @param {string} funcName - The name of the function.
   * @returns {string} The full name of the function.
   */
  static getFullFunctionName(funcName) {
    return `${getLibraryID_()}${funcName}`;
  }

  /**
   * Gets the create update match results trigger function name by sheet name.
   * @param {string} sheetName - The name of the sheet.
   * @returns {string|null} The function name for creating the update match results trigger, or null if not found.
   */
  static getMatchResultsStartTriggerFuncName(sheetName) {
    const config = MatchSheetConfigs.find(config => config.sheetName === sheetName);
    if (config) {
      return config.startTriggerName;
    } else {
      log(`No configuration found for sheetName '${sheetName}'.`, LogLevel.ERROR);
      return null;
    }
  }

  /**
   * Gets the create update match results trigger function name by sheet name.
   * @param {string} sheetName - The name of the sheet.
   * @returns {string|null} The function name for creating the update match results trigger, or null if not found.
   */
  static getMatchResultsExecTriggerFuncName(sheetName) {
    const config = MatchSheetConfigs.find(config => config.sheetName === sheetName);
    if (config) {
      return config.execTriggerFuncName;
    } else {
      log(`No configuration found for sheetName '${sheetName}'.`, LogLevel.ERROR);
      return null;
    }
  }

  /**
   * Deletes the triggers associated with the match results update for a given sheet name.
   * 
   * @param {string} sheetName - The name of the sheet whose triggers are to be deleted.
   */
  static deleteMatchResultsTriggers(sheetName) {
    const config = MatchSheetConfigs.find(config => config.sheetName === sheetName);
    if (config) {
      TriggersUtils.deleteTriggerByFuncName(config.startTriggerName);
      TriggersUtils.deleteTriggerByFuncName(config.execTriggerFuncName);
    }
  }
}
