//DailyResultsSheetCache.gs//

var DailyBetsResultsCache = null;

/**
 * Loads the daily results from the schedule sheet data into an object of DailyBetsResult instances.
 * Each entry is keyed by a combination of matchID and punterName.
 *
 * @returns {Object<string, DailyBetsResult>} An object where each key is a combination of matchID and punterName, and each value is a DailyBetsResult instance.
 */
function getDailyBetsResults_() {
  if (DailyBetsResultsCache != null) {
    return DailyBetsResultsCache;
  }

  const sheetData = getDailyResultsSheetCachedData_(); // Assuming the first row is the header
  const colIndices = getResultsSheetIndices_();

  const dailyResults = {};

  sheetData.forEach(row => {
    const matchID = row[colIndices.matchID];
    const entryID = row[colIndices.entryID];
    const punterName = row[colIndices.punterName];
    const tossWinTeam = row[colIndices.tossWinTeam];
    const firstInningsScore = row[colIndices.firstInningsScore];
    const matchWinTeam = row[colIndices.matchWinTeam];
    const runsMargin = row[colIndices.runsMargin];
    const wicketsMargin = row[colIndices.wicketsMargin];
    const potm = row[colIndices.potm];
    const maxRuns = row[colIndices.maxRuns];
    const highestSR = row[colIndices.highestSR];
    const maxWickets = row[colIndices.maxWickets];
    const bestEconomy = row[colIndices.bestEconomy];
    const points = row[colIndices.points];
    const rank = row[colIndices.rank];
    const tossWinPoints = row[colIndices.tossWinPoints];
    const firstInningsScorePoints = row[colIndices.firstInningsScorePoints];
    const matchWinPoints = row[colIndices.matchWinPoints];
    const runsMarginPoints = row[colIndices.runsMarginPoints];
    const wicketsMarginPoints = row[colIndices.wicketsMarginPoints];
    const potmPoints = row[colIndices.potmPoints];
    const maxRunsPoints = row[colIndices.maxRunsPoints];
    const highestSRPoints = row[colIndices.highestSRPoints];
    const maxWicketsPoints = row[colIndices.maxWicketsPoints];
    const bestEconomyPoints = row[colIndices.bestEconomyPoints];

    const result = new DailyBetsResult(
      matchID,
      entryID,
      punterName,
      tossWinTeam,
      firstInningsScore,
      matchWinTeam,
      runsMargin,
      wicketsMargin,
      potm,
      maxRuns,
      highestSR,
      maxWickets,
      bestEconomy,
      points,
      rank,
      tossWinPoints,
      firstInningsScorePoints,
      matchWinPoints,
      runsMarginPoints,
      wicketsMarginPoints,
      potmPoints,
      maxRunsPoints,
      highestSRPoints,
      maxWicketsPoints,
      bestEconomyPoints
    );

    const key = `${matchID}_${punterName}`;
    dailyResults[key] = result;
  });

  DailyBetsResultsCache = dailyResults;
  return dailyResults;
}
