//Match Title Question
const question1MatchTitle = ": Match";
const question1MatchTitleDesc = () => ConfigManager.getConfigClass().TrumpCardConfig.IS_ENABLED ? `● select ${TrumpCardKeys.INDICATOR_EMOJI.trim()} option to double match points with any remaining trump card(s) (max ${ConfigManager.getConfigClass().TrumpCardConfig.MAX_LIMIT} allowed)?` : '';

//Team questions
const question2TossTeam = ": Toss";
const question2TossTeamDesc = "● 2 points";

const question3FirstInningsScore = ": First Innings Score";
const question3FirstInningsScoreDesc = "● 5 points for exact, 3 points for +/- 5 runs";

const question4WinTeam = ": Win";
const question4WinTeamDesc = "● 4 points";

//Margin questions
const question5RunsMargin = ": Runs Margin";
const question5RunsMarginDesc = "● 2 points for exact (or closest if no exact)\n● conditions: 1) requires winning team prediction. 2) 0 margin valid for a tied match only";

const question6WktsMargin = ": Wickets Margin";
const question6WktsMarginDesc = "● 2 points for exact (or closest if no exact)\n● conditions: 1) requires winning team prediction. 2) ignored for a tied match";

//Players questions
const question7POTMPlayer = ": POTM";
const question7POTMPlayerDesc = "● 5 points for player of the match";

const question8MaxRunsPlayer = ": Max Runs";
const question8MaxRunsPlayerDesc = "● 3 points";

const question9BestSRPlayer = ": Highest Batting S/R";

//@IMPORATNT When calling a function while defining a const, always use this as a function, this will DELAY calling the function, which may not be ready at script initialization
const question9BestSRPlayerDesc = () => `● 3 points, condition: requires minimum ${getMinBallsForBatterSR_()} balls faced`;

const question10MaxWktsPlayer = ": Max Wickets";
const question10MaxWktsPlayerDesc = "● 3 points";

const question11BestEcoPlayer = ": Best Bowling Economy";

//@IMPORATNT When calling a function while defining a const, always use this as a function, this will DELAY calling the function, which may not be ready at script initialization
const question11BestEcoPlayerDesc = () => `● 3 points, condition: requires minimum ${getMinOversForBowlerEconomy_()} overs bowled`;

const playersQuestionsArr = [question7POTMPlayer, question8MaxRunsPlayer, question9BestSRPlayer, question10MaxWktsPlayer, question11BestEcoPlayer];
const sectionHeaderTitlesArr = ["FIRST MATCH", "SECOND MATCH", "THIRD MATCH"];

const ItemValueTypes = {
  MATCH_TITLE: "MATCH_TITLE",
  PUNTERS: "PUNTERS",
  TEAMS: "TEAMS",
  PLAYERS_ALL: "PLAYERS_ALL",
  PLAYERS_BATTERS: "PLAYERS_BATTERS",
  PLAYERS_BOWLERS: "PLAYERS_BOWLERS",
  NONE: "",
}

const dailyBetsFormMatchQuestions = () => [
  { title: question1MatchTitle, itemType: FormItemTypes.MULTIPLE_CHOICE, helpText: question1MatchTitleDesc(), valueType: ItemValueTypes.MATCH_TITLE },
  { title: question2TossTeam, itemType: FormItemTypes.MULTIPLE_CHOICE, helpText: question2TossTeamDesc, valueType: ItemValueTypes.TEAMS },
  { title: question3FirstInningsScore, itemType: FormItemTypes.TEXT_WHOLE_NUMBER, helpText: question3FirstInningsScoreDesc, valueType: ItemValueTypes.NONE },
  { title: question4WinTeam, itemType: FormItemTypes.MULTIPLE_CHOICE, helpText: question4WinTeamDesc, valueType: ItemValueTypes.TEAMS },
  { title: question5RunsMargin, itemType: FormItemTypes.TEXT_WHOLE_NUMBER, helpText: question5RunsMarginDesc, valueType: ItemValueTypes.NONE },
  { title: question6WktsMargin, itemType: FormItemTypes.SCALE, helpText: question6WktsMarginDesc, valueType: ItemValueTypes.NONE },
  { title: question7POTMPlayer, itemType: FormItemTypes.LIST, helpText: question7POTMPlayerDesc, valueType: ItemValueTypes.PLAYERS_ALL },
  { title: question8MaxRunsPlayer, itemType: FormItemTypes.LIST, helpText: question8MaxRunsPlayerDesc, valueType: ItemValueTypes.PLAYERS_BATTERS },
  { title: question9BestSRPlayer, itemType: FormItemTypes.LIST, helpText: () => question9BestSRPlayerDesc(), valueType: ItemValueTypes.PLAYERS_BATTERS },
  { title: question10MaxWktsPlayer, itemType: FormItemTypes.LIST, helpText: question10MaxWktsPlayerDesc, valueType: ItemValueTypes.PLAYERS_BOWLERS },
  { title: question11BestEcoPlayer, itemType: FormItemTypes.LIST, helpText: () => question11BestEcoPlayerDesc(), valueType: ItemValueTypes.PLAYERS_BOWLERS },
];
