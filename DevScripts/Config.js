//Config Classes

/**
 * Displays the configuration information menu.
 * @param {Event} e - The event object.
 */
function displayConfigInfoMenu(e) {
  displayConfigInfo_(e);
}

/**
 * Displays the configuration information.
 * @param {Event} e - The event object.
 * @private
 */
function displayConfigInfo_(e) {
  const message = getConfigInfo_(e);

  showCustomInfoAlert_("Config Info", message);
}

/**
 * Retrieves the configuration information.
 * @param {Event} e - The event object.
 * @returns {string} The configuration information.
 * @private
 */
function getConfigInfo_(e) {
  const obj = ConfigManager.getConfigClass();
  let attributes = [];

  // Sorting keys alphabetically
  const sortedKeys = Object.keys(obj).sort();

  for (const key of sortedKeys) {
    attributes.push(JSON.stringify({ [key]: obj[key] }));
  }

  return attributes.join("\n");
}

// Enum for config groups
const ConfigGroup = {
  AUChem: EnvConfigConstants.ENV_GROUP_AUCHEM,
  YEG: EnvConfigConstants.ENV_GROUP_YEG,
};

/**
 * BaseConfig class containing base and common properties.
 */
class BaseConfig {
  /**
   * Creates an instance of BaseConfig.
   */
  constructor() {
    this.TEST_ENV_SPREADSHEET_ID = null;//TEST (where to copy SHEETS from)
    this.PROD_ENV_SPREADSHEET_ID = null;//PROD (where to copy FORMS from)

    this.AUTO_SUBMIT_BETS_ENABLED = true;
    this.AUTO_SUBMIT_BETS_LIMIT_PER_PUNTER = 5;

    this.REMINDERS_ENABLED = true;
    this.EXCLUDED_MATCH_IDS_IN_SCHEDULE = [2, 3, 5, 7, 9, 10, 12, 18, 20, 23, 24, 28, 29, 31, 32, 34, 37, 39];

    this.TrumpCardConfig = {
      IS_ENABLED: true,
      MATCH_IDS_RANGE: {
        min: 1,
        max: 74,
      },
      MAX_LIMIT: 2,
      AUTO_USE_REMAINING_CARDS: true,
    };

    this.MIN_BALLS_FOR_BATTER_STRIKERATE_VALUES = { [MATCH_TYPES.T20]: 10, [MATCH_TYPES.ODI]: 20, };
    this.MIN_OVERS_FOR_BOWLER_ECONOMY_VALUES = { [MATCH_TYPES.T20]: 3, [MATCH_TYPES.ODI]: 6, };
    this.MAX_OVERS_PER_BOWLER_VALUES = { [MATCH_TYPES.T20]: 4, [MATCH_TYPES.ODI]: 10, };
    this.MAX_OVERS_PER_INNINGS_VALUES = { [MATCH_TYPES.T20]: 20, [MATCH_TYPES.ODI]: 50, };
    this.SERIES_MIN_BALLS_FACED_FOR_SR_VALUES = { [MATCH_TYPES.T20]: 50, [MATCH_TYPES.ODI]: 90, };//T20 - IPL 90, WC 50
    this.SERIES_MIN_OVERS_FOR_ECONOMY_VALUES = { [MATCH_TYPES.T20]: 15, [MATCH_TYPES.ODI]: 30, };//T20 - IPL 30, WC 15
    this.INNINGS_SCORE_PREDICTION_RANGES = { [MATCH_TYPES.T20]: [[150, 204]], [MATCH_TYPES.ODI]: [[185, 350]], };
  }

  /**
   * Gets the base sheet configurations.
   * @returns {Array} The base sheet configurations.
   */
  getSheetConfigs() {
    return BaseSheetConfigs;
  }
}

/**
 * AUChemConfig class for AUChem group specific configurations.
 */
class AUChemConfig extends BaseConfig {
  /**
   * Creates an instance of AUChemConfig.
   */
  constructor() {
    super(); // Calling constructor of parent class
    this.TEST_ENV_SPREADSHEET_ID = "1tG_u6dplBFjxyeG0PFXE1jjCIfeg06B2D3Np3Tjw0PM";//TEST (where to copy SHEETS from)
    this.PROD_ENV_SPREADSHEET_ID = "1K7OPtnZWi77m91T7jW9shI2zcRvgU3TAzv9i8fzdwfs";//PROD (where to copy FORMS from)
  }

  /**
     * Gets the combined sheet configurations for AUChem.
     * @returns {Array} The combined sheet configurations.
     */
  getSheetConfigs() {
    const baseConfigs = super.getSheetConfigs();
    return [...baseConfigs, ...AUChemSheetConfigs];
  }
}

/**
 * YEGConfig class for YEG group specific configurations.
 */
class YEGConfig extends BaseConfig {
  /**
   * Creates an instance of YEGConfig.
   */
  constructor() {
    super(); // Calling constructor of parent class

    this.AUTO_SUBMIT_BETS_ENABLED = false;
    this.TrumpCardConfig.IS_ENABLED = false;
  }

  /**
     * Gets the combined sheet configurations for YEG.
     * @returns {Array} The combined sheet configurations.
     */
  getSheetConfigs() {
    const baseConfigs = super.getSheetConfigs();
    return [...baseConfigs, ...YEGSheetConfigs];
  }
}

/**
 * Static class for configuration management.
 */
class ConfigManager {
  /**
   * Get config class by group.
   * @param {string} group - The group to get config for.
   * @returns {BaseConfig} Config class for the specified group.
   */
  static getConfigClassByGroup(group) {
    switch (group) {
      case ConfigGroup.AUChem:
        return new AUChemConfig();
      case ConfigGroup.YEG:
        return new YEGConfig();
      default:
        return new BaseConfig(); // Default to common config
    }
  }

  /**
   * Get config class based on environment variable.
   * @returns {BaseConfig} Default config class based on environment variable.
   */
  static getConfigClass() {
    const configGroup = ConfigGroup[EnvConfigClass.getEnvDefaultConfigGroup()] || ''; // Check if groupID exists in enum
    return ConfigManager.getConfigClassByGroup(configGroup);
  }
}

/**
 * Retrieves the final sheet configurations based on the active configuration.
 * @returns {Array} The final sheet configurations.
 */
function getFinalSheetConfigs() {
  const configClass = ConfigManager.getConfigClass();
  return configClass.getSheetConfigs();
};


/**
 * An array of sheet configurations.
 * @typedef {Array<Object>} SheetConfigArray
 */

/**
 * A configuration object for a Google Sheets sheet.
 * @typedef {Object} SheetConfig
 * @property {string} name - The name of the sheet.
 * @property {SheetVisibility} visibility - The visibility of the sheet.
 * @property {Protection} protection - The protection level of the sheet.
 * @property {Promotion} promotion - The promotion batch of the sheet.
 */

/**
 * An array of sheet configurations.
 * @type {SheetConfigArray} 
 */
const BaseSheetConfigs = [
  { name: homeSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: match1SheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH2 },
  { name: match2SheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH2 },
  { name: match3SheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH2 },
  { name: matchTSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH2 },
  { name: dailyTeamResultsSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: dailyBetsSheetName, visibility: SheetVisibility.SHOW, protection: Protection.ON, promotion: Promotion.EXCLUDED },
  { name: dailyBetsClosedSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: dailyBetsBackupSheetName, visibility: SheetVisibility.ALWAYS_HIDDEN, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: dailyBetsPointsSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: dailyBetsResultsSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: dailyBetsResultsJournalSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: dailyTopBetsSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: dailyAutoBetsLogSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: seriesBetsSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.EXCLUDED },
  { name: seriesBetsBackupSheetName, visibility: SheetVisibility.ALWAYS_HIDDEN, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: seriesBetsPointsSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: seriesBetsModSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: seriesBetsFormSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: predictoSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: playerStatsSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: tableSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH3 },
  { name: firstInningsScorePointsJSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: firstInningsScoreSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: playersSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: playersExcludedSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: scheduleViewSheetName, visibility: SheetVisibility.SHOW, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: scheduleTableSheetName, visibility: SheetVisibility.HIDE, protection: Protection.ON, promotion: Promotion.EXCLUDED },
  { name: systemLogsSheetName, visibility: SheetVisibility.AS_IS, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: puntersSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: teamsSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: seriesViewSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: seriesTableSheetName, visibility: SheetVisibility.HIDE, protection: Protection.ON, promotion: Promotion.EXCLUDED, replaceHeader: true },
  { name: countriesSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
  { name: winnersSheetName, visibility: SheetVisibility.HIDE, protection: Protection.OFF, promotion: Promotion.BATCH1 },
];

const AUChemSheetConfigs = [
];

const YEGSheetConfigs = [
];
