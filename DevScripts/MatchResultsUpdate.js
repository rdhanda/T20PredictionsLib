//MatchResultsUpdate.gs//

/**
 * Forces a refresh of the match results for the active sheet if it is a match sheet or a score sheet.
 * 
 * @param {Object} e - Event object passed to the trigger function.
 */
function forceRefreshActiveMatchResultsMenu(e) {
  const activeSheetName = SpreadsheetApp.getActiveSheet().getName();
  const matchSheetName = getMatchSheetNames_().find((matchSheetName) => matchSheetName === activeSheetName);
  if (!matchSheetName) { return };

  const matchSheet = getSheetByName_(matchSheetName);
  if (!matchSheet || !hasMatchID_(matchSheet)) { return };

  updateSheetWithScorecardData_(e, matchSheet);

  activateSheet_(matchSheet);
}

/**
 * updateAllMatchResultsUI - updates the UI of all match results in the sheets
 * @param {Object} e - event object containing information about the triggering event
 */
function updateAllMatchResultsMenu(e) {
  updateAllMatchResults_(e);
}

/**
 * Updates match results for all match sheets based on their current status.
 *
 * @param {Object} e - The event object that triggered this function.
 */
function updateAllMatchResults_(e) {
  getMatchSheetNames_().forEach((sheetName) => {
    updateMatchResult_(e, sheetName);
  });
}

/**
 * Updates the match result for a given sheet name.
 * 
 * @param {Event} e - The event object triggered by the script.
 * @param {string} sheetName - The name of the sheet to update.
 */
function updateMatchResult_(e, sheetName) {
  updateMatchResultCache_(sheetName);
  let updatesInProgress = false;

  const matchResult = MatchResultsCache[sheetName];

  if (matchResult) {
    const resultStatus = matchResult.getResultStatus();

    if (ActiveMatchResultStatusList.includes(resultStatus)) {
      const sheet = matchResult.getSheet();
      const matchID = matchResult.getMatchID();
      const status = doUpdateMatchResult_(e, sheet, matchID, resultStatus);

      if (!updatesInProgress && status !== MatchResultStatus.DONE) {
        updatesInProgress = true;
      }
    }
  }

  return updatesInProgress;
}

/**
   * Performs the necessary actions to update the match result for a given match sheet.
   *
   * @param {Object} sheet - The match sheet object.
   * @param {string} matchID - The unique identifier for the match.
   * @param {string} resultStatus - The current status of the match (MatchResult).
   */
function doUpdateMatchResult_(e, sheet, matchID, resultStatus) {
  const funcName = `${doUpdateMatchResult_.name}${sheet.getName()}_${matchID}`;
  console.time(funcName);

  if (resultStatus === MatchResultStatus.WAITING && isItTimeToStartScoring_(matchID)) {
    resultStatus = MatchResultStatus.LIVE;
    setMatchResultStatus_(sheet, resultStatus);
  }

  if (resultStatus === MatchResultStatus.LIVE) {
    resultStatus = updateSheetWithScorecardData_(e, sheet);
    setMatchResultStatus_(sheet, resultStatus);
  }

  if (resultStatus === MatchResultStatus.READY) {
    setMatchResultStatus_(sheet, MatchResultStatus.READY);

    if (canCopyMatchResult_(sheet)) {
      resultStatus = copyMatchResult_(e, sheet);
    } else if (isMatchResultRecorded_(matchID)) {
      setMatchResultStatus_(sheet, MatchResultStatus.DONE);
      resultStatus = MatchResultStatus.DONE;
    }

    removePlayingXICachedData_(sheet.getName());
  }

  if (resultStatus === MatchResultStatus.NO_RESULT) {
    doAfterMatchCompletion_(e, sheet, matchID);
  }

  console.timeEnd(funcName);
  return resultStatus;
}

/**
 * Updates the results for Match 1.
 *
 * @param {Object} e - The event object.
 */
function execUpdateMatch1Results(e) {
  execUpdateMatchResults(e, match1SheetName);
}

/**
 * Updates the results for Match 2.
 *
 * @param {Object} e - The event object.
 */
function execUpdateMatch2Results(e) {
  execUpdateMatchResults(e, match2SheetName);
}

/**
 * Updates the results for Match 3.
 *
 * @param {Object} e - The event object.
 */
function execUpdateMatch3Results(e) {
  execUpdateMatchResults(e, match3SheetName); // Changed to match3SheetName
}

/**
 * Main function to update match results.
 *
 * @param {Object} e - The event object.
 * @param {string} sheetName - The name of the sheet to update.
 */
function execUpdateMatchResults(e, sheetName) {
  let itr = 0;
  let updatesInProgress = false;

  const perMinute = EnvConfigClass.getUpdateMatchResultsMaxPerMinute();
  const totalMinutes = 5;
  const maxItrs = perMinute * totalMinutes;
  const maxTime = totalMinutes * 60 * 1000;
  const startTime = new Date().getTime();

  console.info(`Updating '${sheetName}' match results '${perMinute}' time(s) per minute.`);

  while (keepUpdatingMatchResults(sheetName, itr, maxItrs, startTime, maxTime)) {
    itr++;

    let itrStartTime = new Date().getTime();
    updatesInProgress = updateMatchResult_(e, sheetName);

    if (updatesInProgress && itr < maxItrs) {
      const sleepTime = calculateSleepTime(maxItrs, itrStartTime, maxTime);
      console.info(`>>>> Sleeping for '${sleepTime}'ms. Completed ${itr} of ${maxItrs} execution times. <<<<`);
      Utilities.sleep(sleepTime);
    }
  }
  console.info(`DONE. Completed ${itr} of ${maxItrs} execution times.`);

  if(!updatesInProgress){
    TriggersUtils.deleteMatchResultsTriggers(sheetName);
  }
}

/**
 * Checks if the match results should keep updating.
 *
 * @param {string} sheetName - The name of the sheet to update.
 * @param {number} itr - The current iteration count.
 * @param {number} maxItrs - The maximum number of iterations.
 * @param {number} startTime - The start time in milliseconds.
 * @param {number} maxTime - The maximum time allowed in milliseconds.
 * @returns {boolean} - True if the match results should keep updating, false otherwise.
 */
function keepUpdatingMatchResults(sheetName, itr, maxItrs, startTime, maxTime) {
  const matchInProgress = isMatchInProgress_(sheetName);
  const firstItr = itr === 0;
  const maxItrNotReached = itr < maxItrs;
  const isWithinMaxTime = isWithinMaxTime_(itr, startTime, maxTime);
  const keepUpdating = (matchInProgress || firstItr) && maxItrNotReached && isWithinMaxTime;

  return keepUpdating;
}

/**
 * Checks if the elapsed time since the start time is within the maximum time limit,
 * taking into account the average iteration time.
 *
 * @param {number} itr - The current iteration count.
 * @param {number} startTime - The start time of the process in milliseconds.
 * @param {number} maxTime - The maximum allowed time for the process in milliseconds.
 * @returns {boolean} True if the elapsed time is within the maximum time limit, otherwise false.
 */
function isWithinMaxTime_(itr, startTime, maxTime) {
  const currentTime = new Date().getTime();
  const averageIterationTime = itr > 0 ? (currentTime - startTime) / itr : 0;
  const remainingTime = maxTime - averageIterationTime;
  const isWithinMaxTime = (currentTime - startTime) < (remainingTime + (30 * 1000));//Added 30s extra for the last itr.

  if (!isWithinMaxTime) {
    console.log(`Max time reached. Exiting now.`);
  }
  return isWithinMaxTime;
}

/**
 * Calculates the remaining time until the next iteration based on the elapsed time,
 * maximum time, and maximum iterations.
 *
 * @param {number} maxItrs - The maximum number of iterations.
 * @param {number} itrStartTime - The start time of the current iteration in milliseconds.
 * @param {number} maxTime - The maximum time allowed for all iterations in milliseconds.
 * @returns {number} The remaining time until the next iteration in milliseconds.
 */
function calculateSleepTime(maxItrs, itrStartTime, maxTime) {
  const elapsedTime = new Date().getTime() - itrStartTime;
  const timePerIteration = Math.ceil(maxTime / maxItrs);
  return Math.max(timePerIteration - elapsedTime, 0);
}

/**
 * Determines if it is time to start scoring a match based on its start time.
 * 
 * @param {string} matchID - The ID of the match.
 * @return {boolean} - True if it is time to start scoring, false otherwise.
 */
function isItTimeToStartScoring_(matchID) {
  return isBeforeCurrentTime_(addMinutes_(ScheduleUtils.getMatchStartTimeMST(matchID), EnvConfigClass.getAddMinutesToCloseDailyBets()));
}

/**
 * Check if the match result can be copied from the sheet
 * 
 * @param {Google Apps Script API} sheet - The sheet to check for copy conditions
 * @return {Boolean} - True if result can be copied, false otherwise
 */
function canCopyMatchResult_(sheet) {
  let matchID = getMatchID_(sheet);
  const canCopy = (isNotEmpty_(matchID) && isMatchResultNotRecorded_(matchID) && isResultDecided_(sheet));

  return canCopy;
}

/**
 * Gets the match result status from the specified sheet.
 *
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet containing the match result status.
 * @returns {string} - The match result status or "UNKNOWN" if the status is not a valid value.
 */
function getMatchResultStatus_(sheet) {
  let status = sheet.getRange(matchSheetObj.matchResultStatusCell).getValue();

  const key = getMatchResultStatusKeyByLabel_(status);
  return key ? MatchResultStatus[key] : MatchResultStatus.NA;
}

/**
 * Retrieves the key associated with the provided label from the MatchResultStatus object.
 * @param {string} label - The label to search for.
 * @returns {string|null} The key associated with the label, or null if label is not found.
 */
function getMatchResultStatusKeyByLabel_(label) {
  for (const key in MatchResultStatus) {
    if (MatchResultStatus[key].label === label) {
      return key;
    }
  }

  log(`Status '${label}' not found.`, LogLevel.INFO);
  return null; // Return null if label is not found
}

/**
 * Sets the match result status for a given sheet.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet to update.
 * @param {string} status - The match result status.
 */
function setMatchResultStatus_(sheet, status) {
  if (status) {
    sheet.getRange(matchSheetObj.matchResultStatusCell).setValue(status.label);
    sheet.setTabColor(status.color);

    updateMatchResultSheetStatusCache_(sheet.getName(), status);
  } else {
    log(`Invalid match result status: '${status}' in sheet '${sheet.getName()}'.`, LogLevel.ERROR);
  }
}

/**
 * Performs steps required after the completion of a match, including handling cases with no result.
 *
 * @param {Object} e - The event object.
 * @param {Object} matchSheet - The sheet object where match results are stored.
 * @param {string} matchID - The ID of the completed match.
 */
function doAfterMatchCompletion_(e, matchSheet, matchID) {
  // Update the match completed time in the Schedule sheet
  updateMatchCompletedTime_(matchID);

  // Send email confirmation
  EmailResultCompletion.sendEmail(e, matchSheet, matchID);

  // Create all scheduled update triggers
  Triggers.createAllScheduledUpdateTriggers();
}

/**
 * Checks if all result elements (toss, winner, margin, points, POTM, max runs, strike rate, max wickets, economy) have been decided for the match.
 *
 * @param {Object} sheet - The match sheet object
 * @return {Boolean} - Returns true if all result elements have been decided, false otherwise.
 */
function isResultDecided_(sheet) {
  flushAndWait_();

  const isTossDecided = isTossDecided_(sheet);
  const isFirstInningsScoreDecided = isFirstInningsScoreDecided_(sheet);
  const isWinnerDecided = isWinnerDecided_(sheet);
  const isMarginDecided = isMarginDecided_(sheet);
  const isMarginPointsAllocated = isMarginPointsAllocated_(sheet);
  const isPOTMDecided = isPOTMDecided_(sheet);
  const isMaxRunsDecided = isMaxRunsDecided_(sheet);
  const isStrikeRateDecided = isStrikeRateDecided_(sheet);
  const isMaxWicketsDecided = isMaxWicketsDecided_(sheet);
  const isEconomyDecided = isEconomyDecided_(sheet);

  const resultDecided = isTossDecided && isFirstInningsScoreDecided && isWinnerDecided && isMarginDecided && isMarginPointsAllocated && isPOTMDecided && isMaxRunsDecided && isStrikeRateDecided && isMaxWicketsDecided && isEconomyDecided;

  let logMessage = '';

  if (!resultDecided) logMessage += `** Match Result not decided. **\n`;
  if (!isTossDecided) logMessage += `Toss not decided.\n`;
  if (!isFirstInningsScoreDecided) logMessage += `1st innings score not decided.\n`;
  if (!isWinnerDecided) logMessage += `Winner not decided.\n`;
  if (!isMarginDecided) logMessage += `Margin not decided.\n`;
  if (!isMarginPointsAllocated) logMessage += `Margin points not allocated.\n`;
  if (!isPOTMDecided) logMessage += `POTM not decided.\n`;
  if (!isMaxRunsDecided) logMessage += `Max runs not decided.\n`;
  if (!isStrikeRateDecided) logMessage += `Strike rate not decided.\n`;
  if (!isMaxWicketsDecided) logMessage += `Max wickets not decided.\n`;
  if (!isEconomyDecided) logMessage += `Economy not decided.\n`;

  if (logMessage) {
    log(logMessage, LogLevel.DEBUG);
  }

  return resultDecided;
}

/**
 * Checks if the toss has been decided for the match.
 *
 * @param {Object} sheet - The match sheet object
 * @return {Boolean} - Returns true if the toss has been decided, false otherwise.
 */
function isTossDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultTossCell).isBlank());
}

/**
 * Checks if the toss has been decided for the match.
 *
 * @param {Object} sheet - The match sheet object
 * @return {Boolean} - Returns true if the toss has been decided, false otherwise.
 */
function isFirstInningsScoreDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultFirstInningsScoreCell).isBlank());
}

/**
 * Checks if the winner has been decided for the match.
 *
 * @param {Object} sheet - The match sheet object
 * @return {Boolean} - Returns true if the winner has been decided, false otherwise.
 */
function isWinnerDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultWinCell).isBlank());
}

/**
 * Checks if the margin (runs or wickets) has been decided for the match.
 *
 * @param {Object} sheet - The match sheet object
 * @return {Boolean} - Returns true if the margin has been decided, false otherwise.
 */
function isMarginDecided_(sheet) {
  return ((isRunsMarginDecided_(sheet) && !isWktsMarginDecided_(sheet)) || (!isRunsMarginDecided_(sheet) && isWktsMarginDecided_(sheet)))
}

/**
 * 
 */
function isRunsMarginTied_(sheet) {
  let runsMarginResultValue = sheet.getRange(matchSheetObj.resultRunsMarginCell).getValue();
  return MATCH_RESULT_TIED.toLocaleLowerCase() === runsMarginResultValue.toLowerCase();
}

/**
 * Returns true if margin points have been allocated correctly
 * If sum of team win point is 0, then margin sum should be 0
 * If sum of team win points is > 0, then margin points range can not be empty
 * 
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The sheet object representing match data
 * @return {boolean} true if margin points have been allocated correctly, false otherwise
 */
function isMarginPointsAllocated_(sheet) {
  const marginPointsSum = sheet.getRange(matchSheetObj.marginPointsSumCell).getValue();
  const winPointsSum = sheet.getRange(matchSheetObj.winPointsSumCell).getValue();
  const marginPointsAllocated = !sheet.getRange(matchSheetObj.marginPointsRange).isBlank();

  return (winPointsSum === 0 && marginPointsSum === 0) || (winPointsSum > 0 && marginPointsAllocated);
}

/**
 * Returns a boolean indicating whether the runs margin has been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if runs margin has been decided, false otherwise
 */
function isRunsMarginDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultRunsMarginCell).isBlank());
}

/**
 * Returns a boolean indicating whether the wickets margin has been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if wickets margin has been decided, false otherwise
 */
function isWktsMarginDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultWktsMarginCell).isBlank());
}

/**
 * Returns a boolean indicating whether the Player of the Match has been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if Player of the Match has been decided, false otherwise
 */
function isPOTMDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultPOTMCell).isBlank());
}

/**
 * Returns a boolean indicating whether the max runs have been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if max runs have been decided, false otherwise
 */
function isMaxRunsDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultMaxRunsCell).isBlank());
}

/**
 * Returns a boolean indicating whether the strike rate has been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if strike rate has been decided, false otherwise
 */
function isStrikeRateDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultStrikeRateCell).isBlank());
}

/**
 * Returns a boolean indicating whether the max wickets have been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if max wickets have been decided, false otherwise
 */
function isMaxWicketsDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultMaxWicketsCell).isBlank());
}

/**
 * Returns a boolean indicating whether the economy has been decided
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The current sheet object
 * @return {Boolean} - True if economy has been decided, false otherwise
 */
function isEconomyDecided_(sheet) {
  return !(sheet.getRange(matchSheetObj.resultEconomyCell).isBlank());
}
