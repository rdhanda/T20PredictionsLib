/**
 * Number of days the form can be opened in advance
 * @type {number}
 */
const DAYS_TO_OPEN_FORM = 4;

/**
 * Class containing utility functions for scheduled matches.
 */
class ScheduleUtils {

  /**
   * Retrieves the open matches by filtering the scheduled matches.
   * @returns {ScheduleMatch[]} An array of open match objects.
   */
  static getOpenMatches() {
    const matches = ScheduleCache.getAllMatches();
    if (!matches) return [];
    return Object.values(matches).filter(match => match.isOpen());
  }

  /**
   * Retrieves the closed matches by filtering the scheduled matches.
   * @returns {ScheduleMatch[]} An array of closed match objects.
   */
  static getClosedMatches() {
    const matches = ScheduleCache.getAllMatches();
    if (!matches) return [];
    return Object.values(matches).filter(match => match.isClosed());
  }

  /**
   * Retrieves the empty matches by filtering the scheduled matches.
   * @returns {ScheduleMatch[]} An array of empty match objects.
   */
  static getEmptyMatches() {
    const matches = ScheduleCache.getAllMatches();
    if (!matches) return [];
    return Object.values(matches).filter(match => match.isEmpty() && isAfterCurrentTime_(match.getStartTimeMST()));
  }

  /**
   * Retrieves the excluded matches by filtering the scheduled matches.
   * @returns {ScheduleMatch[]} An array of excluded match objects.
   */
  static getExcludedMatches() {
    const matches = ScheduleCache.getAllMatches();
    if (!matches) return [];
    return Object.values(matches).filter(match => match.isExcluded());
  }

  /**
   * Check if it's time to open bets.
   * 
   * @param {Object} e - Event object
   * @return {Boolean} - True if it's time to open predictions, false otherwise
   */
  static isItTimeToOpen(e) {
    if (ScheduleUtils.isAnyMatchOpen()) { return false; }

    let isItTimeToOpen = false;
    let startTime = ScheduleUtils._findStartTimeOfLastClosedMatchDate(e);

    if (startTime) {
      isItTimeToOpen = isBeforeCurrentTime_(startTime);
    } else {
      // If there are no closed matches, time to open then
      isItTimeToOpen = true;
    }
    return isItTimeToOpen;
  }

  /**
   * Function to get the start time of the first closed match on the last closed match date.
   * @return {string|null} The start time in MST or null if no matches are closed.
   */
  static _findStartTimeOfLastClosedMatchDate(e) {
    const closedMatches = ScheduleUtils.getClosedMatches();
    if (closedMatches.length === 0) { return null; }

    const lastClosedShortDate = closedMatches[closedMatches.length - 1].getShortDate();

    for (const match of closedMatches) {
      if (match.getShortDate() === lastClosedShortDate) {
        return match.getStartTimeMST();
      }
    }
    return null;
  }

  /**
   * Retrieves the date of the next match to open, or logs a warning if a match with "TBA" team is found.
   * @param {Event} e - The event object.
   * @returns {[ScheduleMatch]} The date of the next match to open, or null if no match is found or if any match is open.
   */
  static getMatchesToOpen(e) {
    if (ScheduleUtils.isAnyMatchOpen()) { return null; }

    const possibleDates = ScheduleUtils._getPossibleDatesToOpen();
    const matches = ScheduleUtils._getNextMatchToOpen(possibleDates);

    if (matches.length > 0) {
      const tbaTeamMatchID = ScheduleUtils._getMatchIdWithTBATeam(matches);
      if (tbaTeamMatchID !== null) {
        log(`There is a '${TEAM_TBA}' team in the schedule for the match ID '${tbaTeamMatchID}'.`, LogLevel.WARN);
        return null;
      }
    }

    return matches;
  }

  /**
   * Checks if there is any match currently open.
   * @returns {boolean} True if any match is open, false otherwise.
   */
  static isAnyMatchOpen() {
    return ScheduleUtils.getOpenMatches().length > 0;
  }

  /**
   * Returns an array of dates that are today, today+1, today+2 and today+3.
   *
   * @param {Event} e - The event parameter from the onOpen() trigger.
   * @returns {Date[]} An array of dates in short match date format (e.g. "MM/dd").
   */
  static _getPossibleDatesToOpen() {
    let dates = [];
    let today = new Date();

    for (let i = 0; i < DAYS_TO_OPEN_FORM; i++) {
      let date = new Date(today.getTime());
      date.setDate(today.getDate() + i);
      dates.push(toShortMatchDateFormat_(date).toUpperCase());
    }

    return dates;
  }

  /**
   * Retrieves all matches with status "EMPTY" for the very first possible date in the list.
   * @param {string[]} possibleDates - An array of possible dates in YYYY-MM-DD format.
   * @returns {ScheduleMatch[]} An array of matches with status "EMPTY" for the very first possible date, or an empty array if no matches are found.
   */
  static _getNextMatchToOpen(possibleDates) {
    const emptyMatches = ScheduleUtils.getEmptyMatches(true);

    const firstPossibleDate = possibleDates.find(date => {
      return emptyMatches.some(match => match.getShortDate() === date);
    });

    if (!firstPossibleDate) { return []; }

    const matches = emptyMatches.filter(match => match.getShortDate() === firstPossibleDate);

    return matches;
  }

  /**
   * Returns the ID of the first match where "TBA" (To Be Announced) is found in either home team or away team.
   * @param {ScheduleMatch[]} matches - An array of Match objects.
   * @returns {string|null} The ID of the first match where "TBA" is found, or null if not found.
   */
  static _getMatchIdWithTBATeam(matches) {
    for (const match of matches) {
      if (match.getHomeTeam() === TEAM_TBA || match.getAwayTeam() === TEAM_TBA) {
        return match.getId();
      }
    }
    return null;
  }

  /**
   * Retrieves the number of matches closed on a given date.
   * @param {string} shortDate - The short date string (e.g., "MM/DD").
   * @returns {number} The number of closed matches on the given date.
   */
  static getNumberOfMatchesClosed(shortDate) {
    return ScheduleUtils.getClosedMatches().reduce((count, match) => {
      return count + (match.getShortDate() === shortDate ? 1 : 0);
    }, 0);
  }

  /**
   * Retrieves a match object by ID from the scheduled matches cache.
   * @param {string} matchID - The ID of the match.
   * @returns {ScheduleMatch|undefined} The match object if found, otherwise undefined.
   * @private
   */
  static _getMatchByID(matchID) {
    return ScheduleCache.getAllMatches()[matchID];
  }

  /**
   * Retrieves the title of the match corresponding to the given match ID.
   * @param {string} matchID - The ID of the match.
   * @returns {string|undefined} The title of the match, or undefined if no match is found.
   */
  static getMatchTitle(matchID) {
    const match = ScheduleUtils._getMatchByID(matchID);
    return match ? match.getTitle() : undefined;
  }

  /**
   * Retrieves the scorecard URL of the match corresponding to the given match ID.
   * @param {string} matchID - The ID of the match.
   * @returns {string|undefined} The scorecard URL of the match, or undefined if no match is found.
   */
  static getScorecardURL(matchID) {
    const match = ScheduleUtils._getMatchByID(matchID);
    return match ? match.getUrl() : undefined;
  }

  /**
   * Checks if the match corresponding to the given match ID is open.
   * @param {string} matchID - The ID of the match.
   * @returns {boolean} - Returns true if the match is open, otherwise false.
   */
  static isMatchOpen(matchID) {
    const match = ScheduleUtils._getMatchByID(matchID);
    return match && match.isOpen();
  }

  /**
   * Checks if the match corresponding to the given match ID is closed.
   * @param {string} matchID - The ID of the match.
   * @returns {boolean} - Returns true if the match is closed, otherwise false.
   */
  static isMatchClosed(matchID) {
    const match = ScheduleUtils._getMatchByID(matchID);
    return match && match.isClosed();
  }

  /**
   * Retrieves the start time in MST (Mountain Standard Time) of the match corresponding to the given match ID.
   * @param {string} matchID - The ID of the match.
   * @returns {string|undefined} The start time in MST of the match, or undefined if no match is found.
   */
  static getMatchStartTimeMST(matchID) {
    const match = ScheduleUtils._getMatchByID(matchID);
    return match ? match.getStartTimeMST() : undefined;
  }

  /**
   * Retrieves the scheduled closed time of the match corresponding to the given match ID.
   * @param {string} matchID - The ID of the match.
   * @returns {string|undefined} The scheduled closed time of the match in MST, or undefined if no match is found or start time is not available.
   */
  static getMatchScheduleClosedTime(matchID) {
    const startTime = ScheduleUtils.getMatchStartTimeMST(matchID);
    if (startTime) {
      return addMinutes_(startTime, EnvConfigClass.getAddMinutesToCloseDailyBets());
    }
    return undefined;
  }

  /**
   * Retrieves the ID of the first scheduled match on the given short date that is not excluded.
   * @param {string} shortDate - The short date string (e.g., "MM/DD").
   * @returns {string|null} The ID of the first scheduled match found, or null if no match is found.
   */
  static getFirstScheduledMatchID(shortDate) {
    const matchObjects = ScheduleCache.getAllMatches();

    for (const matchId in matchObjects) {
      const match = matchObjects[matchId];
      if (match.getShortDate() === shortDate && !match.isExcluded()) {
        return matchId;
      }
    }

    return null; // Return null if no match is found
  }

  /**
   * Retrieves the ID of the last closed match.
   * @returns {string|null} The ID of the last closed match, or null if no closed matches are found.
   */
  static getLastClosedMatchID() {
    const closedMatches = ScheduleUtils.getClosedMatches();
    if (closedMatches.length > 0) {
      const lastClosedMatch = closedMatches[closedMatches.length - 1];
      return lastClosedMatch.getId();
    }
    return null;
  }

  /**
   * Retrieves the ID of the first open match.
   * @returns {string|null} The ID of the first open match, or null if no open match is found.
   */
  static getFirstOpenMatchID() {
    const openMatches = ScheduleUtils.getOpenMatches();
    return openMatches.length > 0 ? openMatches[0].getId() : null;
  }

  /**
   * Retrieves the start time in MST of the first open match.
   * @returns {string|null} The start time in MST of the first open match, or null if no open match is found.
   */
  static getFirstOpenMatchStartTimeMST() {
    const openMatches = ScheduleUtils.getOpenMatches();
    return ScheduleUtils.getFirstMatchStartTimeMST(openMatches);
  }

  /**
   * Retrieves the start time in MST of the first match in the provided array.
   * @param {ScheduleMatch[]} matches - An array of match objects.
   * @returns {string|null} The start time in MST of the first match, or null if no match is found.
   */
  static getFirstMatchStartTimeMST(matches) {
    return matches && matches.length > 0 ? matches[0].getStartTimeMST() : null;
  }

  /**
   * Retrieves the short date of the first open match.
   * @returns {string|null} The short date of the first open match, or null if no open match is found.
   */
  static getFirstOpenMatchShortDate() {
    const openMatches = ScheduleUtils.getOpenMatches();
    return ScheduleUtils.getFirstMatchShortDate(openMatches);
  }

  /**
   * Retrieves the short date of the first match in the provided array.
   * @param {ScheduleMatch[]} matches - An array of match objects.
   * @returns {string|null} The short date of the first match, or null if no match is found.
   */
  static getFirstMatchShortDate(matches) {
    return matches && matches.length > 0 ? matches[0].getShortDate() : null;
  }

  /**
   * Retrieves all teams (home and away) for all open matches.
   * @returns {string[]} An array containing all teams (home and away) for all open matches.
   */
  static getOpenMatchesAllTeams() {
    const openMatches = ScheduleUtils.getOpenMatches();
    return ScheduleUtils.getMatchesAllTeams(openMatches);
  }

  /**
   * Generates a description list for the provided matches.
   * @param {ScheduleMatch[]} matches - An array of match objects.
   * @returns {string[]} An array containing descriptions for the matches.
   */
  static getMatchesDescList(matches) {
    let descList = matches.map(match => {
      return ScheduleUtils._formatMatchDescription(match.getHomeTeam(), match.getAwayTeam(), match.getStage(), match.getVenue(), match.getStartTimeMST());
    });

    const deadline = ScheduleUtils._getFirstMatchDeadlineMST(matches);
    if (deadline) {
      descList.push(`\n${STAR_CHAR}${STAR_CHAR} Cutoff Time for Submission: ${toScheduleDateTimeFormat_(deadline)} (MST). ${STAR_CHAR}${STAR_CHAR}`);
    }
    return descList;
  }

  /**
   * Retrieves the closing time for the first match in MST.
   * @param {ScheduleMatch[]} matches - An array of match objects.
   * @returns {string|null} The closing time in MST, or null if no match is found.
   */
  static _getFirstMatchDeadlineMST(matches) {
    const firstMatchStartTimeMST = matches && matches.length > 0 ? matches[0].getStartTimeMST() : null;
    if (firstMatchStartTimeMST) {
      return addMinutes_(firstMatchStartTimeMST, EnvConfigClass.getAddMinutesToCloseDailyBets());
    }
    return null;
  }

  /**
   * Retrieves all teams (home and away) for the provided matches.
   * @param {ScheduleMatch[]} matches - An array of match objects.
   * @returns {string[]} An array containing all teams (home and away) for the matches.
   */
  static getMatchesAllTeams(matches) {
    const allTeams = [];
    matches.forEach(match => {
      allTeams.push(match.getHomeTeam(), match.getAwayTeam());
    });
    return allTeams;
  }

  /**
   * Formats match description based on provided match data.
   * @param {string} homeTeam - The home team.
   * @param {string} awayTeam - The away team.
   * @param {string} stage - The stage of the match.
   * @param {string} venue - The venue of the match.
   * @param {string} startTimeMST - The start time in MST.
   * @returns {string} The formatted match description.
   */
  static _formatMatchDescription(homeTeam, awayTeam, stage, venue, startTimeMST) {
    const formattedDate = toScheduleDateTimeFormat_(startTimeMST);
    return `● ${stage} ${homeTeam} vs ${awayTeam}, ${formattedDate} (MST) at ${venue}`;
  }

  /**
   * Checks if a reminder has not been sent for any match on the given match date.
   * @param {string} matchDate - The short date of the match.
   * @returns {boolean} True if a reminder has not been sent for any match on the given date, otherwise false.
   */
  static isReminderNotSent(matchDate) {
    const matchObjects = ScheduleCache.getAllMatches();

    for (const matchId in matchObjects) {
      const match = matchObjects[matchId];
      if (match.getShortDate() === matchDate && !match.getRemindedTime()) {
        return true;
      }
    }

    return false;
  }

  /**
  * Hides rows in the schedule view sheet up to the first open match ID.
  * Shows the rows from the end of hidden rows to the maximum number of rows in the sheet.
  * Ensures that the operations stay within the bounds of the sheet's row count.
  */
  static hideScheduleViewRows() {
    const matchID = ScheduleUtils.getFirstOpenMatchID();
    if (matchID) {
      const endRow = Math.max(1, matchID - 1); // Ensure endRow is at least 1
      const sheet = getScheduleViewSheet_();
      const maxRows = sheet.getMaxRows(); // Total rows in the sheet

      // Ensure that endRow does not exceed maxRows
      if (endRow <= maxRows) {
        sheet.hideRows(2, endRow);

        // Ensure the range of rows to show is within the bounds
        if (endRow + 1 <= maxRows) {
          sheet.showRows(endRow + 1, maxRows - endRow);
        }
      }
    }
  }
}