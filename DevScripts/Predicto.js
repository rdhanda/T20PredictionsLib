//Predico.gs//

/**
 * Updates the Predicto sheet menu.
 * @param {Object} e - The event object.
 */
function updatePredictoSheetMenu(e) {
  updatePredictoSheet_(e);
}

/**
 * Updates the Predicto sheet and player category winners.
 * @param {Object} e - The event object.
 */
function updatePredictoSheet_(e) {
  activateSheet_(getPredictoSheet_());
  updatePredicto_(e);
  updatePlayerCategoryWinners_(e);
}

/**
 * Generates prediction suggestions for an open match, adds them to the Predicto sheet,
 * and logs the start and end times of the function execution.
 * @param {Object} e - The event object.
 */
function updatePredicto_(e) {
  console.time(updatePredicto_.name);

  const sheet = getPredictoSheet_();
  sheet.getRangeList(predictoSheetObj.clearRangesPredicto).clearContent();

  doUpdatePredicto_(e, sheet);

  console.timeEnd(updatePredicto_.name);
}

/**
 * Updates the player category winners in the Predicto sheet,
 * and logs the start and end times of the function execution.
 * @param {Object} e - The event object.
 */
function updatePlayerCategoryWinners_(e) {
  console.time(updatePlayerCategoryWinners_.name);

  const sheet = getPredictoSheet_();
  sheet.getRangeList(predictoSheetObj.clearRangesPlayerCategories).clearContent();

  doUpdatePlayerCategoryWinners_(e, sheet);

  console.timeEnd(updatePlayerCategoryWinners_.name);
}

/**
 * Updates the Predicto sheet with prediction suggestions.
 * @param {Object} e - The event object.
 * @param {Object} sheet - The Predicto sheet object.
 */
function doUpdatePredicto_(e, sheet) {
  if (sheet) {
    const allBets = getOpenMatchBetsSuggestions_(e).allPredictions;
    allBets.unshift(new Date());

    const startColNum = colNameToNum_(predictoSheetObj.predictoColName);
    sheet.getRange(1, startColNum, allBets.length, 1).setValues(allBets.map(function (name) { return [name]; }));
  }
}

/**
 * Fetches suggestions for all open matches and displays them in a custom dialog.
 * 
 * @param {Object} e - The event object.
 */
function getOpenMatchBetsSuggestions_(e) {
  const matchIDs = [];
  const allPredictions = [];
  const teamWinCounts = getTeamWinCounts_(e);
  const openMatches = ScheduleUtils.getOpenMatches();

  for (const openMatch of openMatches) {
    const id = openMatch.getId();
    const title = openMatch.getTitle();
    const homeTeam = openMatch.getHomeTeam()
    const awayTeam = openMatch.getAwayTeam();

    const allTeamPlayers = getAllProbablePlayers_(homeTeam, awayTeam);
    const predictions = getAllBets_(title, homeTeam, awayTeam, allTeamPlayers, teamWinCounts).values;
    allPredictions.push(...predictions, ``);
    matchIDs.push(id);
  }
  return { matchIDs: matchIDs, allPredictions: allPredictions };
}

/**
 * Updates the Predicto sheet with player category winners based on daily results.
 * @param {Event} e - Trigger event object.
 */
function doUpdatePlayerCategoryWinners_(e, sheet) {
  const teams = ScheduleUtils.getOpenMatchesAllTeams();

  if (isNotEmpty_(teams)) {
    const resultsData = getResultsJournalSheetCachedData_().slice(1);//getDailyResultsSheet_().getDataRange().getValues();
    const resultsStartColIdx = colNameToIndex_(resultsJournalSheetObj.potmColName); // POTM
    const resultsEndColIdx = colNameToIndex_(resultsJournalSheetObj.economyColName); // Eco

    const playerData = getPlayersCachedData_();
    const headers = getResultsDataHeaders_(resultsData, resultsStartColIdx, resultsEndColIdx);

    const allPlayerCountsArray = getResultsPlayerCountsArray_(resultsData, resultsStartColIdx, resultsEndColIdx, playerData, teams);
    fillArraysToMatchLength_(allPlayerCountsArray);

    const transposedArray = transposeArray_(allPlayerCountsArray);

    updatePredictoPlayerCategories_(sheet, transposedArray, headers);

    autoResizeColumns_(sheet);
  }
}

/**
 * Gets headers for the results data.
 * @param {Array} resultsData - The results data array.
 * @param {number} startColNum - The starting column number.
 * @param {number} endColNum - The ending column number.
 * @returns {Array} - An array of headers.
 */
function getResultsDataHeaders_(resultsData, startColNum, endColNum) {
  return Array.from({ length: endColNum - startColNum + 1 }, (_, i) => resultsData[0][startColNum + i]);
}

/**
 * Orchestrates the process of generating player counts array.
 * @param {Array<Array<string>>} resultsData - The results data.
 * @param {number} startColNum - The starting column number.
 * @param {number} endColNum - The ending column number.
 * @param {object} playerData - The player data.
 * @param {Array<string>} teams - The list of team names.
 * @returns {Array<Array<string>>} - Array of player counts.
 */
function getResultsPlayerCountsArray_(resultsData, startColNum, endColNum, playerData, teams) {
  const allPlayerCountsArray = [];
  const teamMaxPlayers = getMaxPlayersPerTeam_(resultsData, startColNum, endColNum, playerData, teams);

  for (let col = startColNum; col <= endColNum && isNotEmpty_(teams); col++) {
    const teamPlayerCounts = teams.map(team => {
      const playerCountsForTeam = getPlayerCountsForTeam_(resultsData, col, team, playerData);
      const paddedPlayerCounts = padPlayerCounts_(playerCountsForTeam, teamMaxPlayers[team]);
      return { team, players: paddedPlayerCounts };
    });

    const sortedTeamPlayerCounts = teamPlayerCounts.sort((a, b) => teams.indexOf(a.team) - teams.indexOf(b.team));
    const flattenedSortedData = sortedTeamPlayerCounts.flatMap(team => [team.team, ...team.players.map(player => player[0])]);

    allPlayerCountsArray.push(flattenedSortedData);
  }

  return allPlayerCountsArray;
}

/**
 * Calculates the maximum number of players per team across all columns.
 * @param {Array<Array<string>>} resultsData - The results data.
 * @param {number} startColNum - The starting column number.
 * @param {number} endColNum - The ending column number.
 * @param {object} playerData - The player data.
 * @param {Array<string>} teams - The list of team names.
 * @returns {object} - An object containing maximum players per team.
 */
function getMaxPlayersPerTeam_(resultsData, startColNum, endColNum, playerData, teams) {
  const teamMaxPlayers = {};
  teams.forEach(team => {
    let maxCount = 0;
    for (let col = startColNum; col <= endColNum; col++) {
      const uniquePlayers = new Set();
      resultsData.forEach(row => {
        if (typeof row[col] === 'string') {
          row[col].split("\n").forEach(playerName => {
            if (getTeamNameByPlayer_(playerData, playerName) === team) {
              uniquePlayers.add(playerName);
            }
          });
        }
      });
      maxCount = Math.max(maxCount, uniquePlayers.size);
    }
    teamMaxPlayers[team] = maxCount;
  });
  return teamMaxPlayers;
}

/**
 * Retrieves the player counts for a specific team in a given column.
 * @param {Array<Array<string>>} resultsData - The results data.
 * @param {number} col - The column index.
 * @param {string} team - The team name.
 * @param {object} playerData - The player data.
 * @returns {Array<Array<string|number>>} - Array of player counts.
 */
function getPlayerCountsForTeam_(resultsData, col, team, playerData) {
  const playerNameCounts = resultsData
    .flatMap(row => (typeof row[col] === 'string' ? row[col].split("\n") : []))
    .filter(playerName => getTeamNameByPlayer_(playerData, playerName) === team)
    .reduce((counts, playerName) => {
      counts[playerName] = (counts[playerName] || 0) + 1;
      return counts;
    }, {});
  return Object.entries(playerNameCounts)
    .sort((a, b) => (a[1] !== b[1] ? b[1] - a[1] : a[0].localeCompare(b[0])))
    .map(([playerName, count]) => [`${playerName} (${count})`, count]);
}

/**
 * Pads the player counts array to match the maximum number of players for a team.
 * @param {Array<Array<string|number>>} playerCountsArray - Array of player counts.
 * @param {number} maxPlayers - The maximum number of players for the team.
 * @returns {Array<Array<string|number>>} - Padded player counts array.
 */
function padPlayerCounts_(playerCountsArray, maxPlayers) {
  const paddedPlayerCountsArray = [...playerCountsArray];
  while (paddedPlayerCountsArray.length < maxPlayers) {
    paddedPlayerCountsArray.push(["", 0]);
  }
  return paddedPlayerCountsArray;
}

/**
 * Fills all arrays with empty values to match the maximum length.
 * @param {Array} allPlayerCountsArray - The array containing player counts arrays.
 */
function fillArraysToMatchLength_(allPlayerCountsArray) {
  const maxArrayLength = Math.max(...allPlayerCountsArray.map(arr => arr.length));

  allPlayerCountsArray.forEach((playerCountsArray) => {
    while (playerCountsArray.length < maxArrayLength) {
      playerCountsArray.push(['', '']);
    }
  });
}

/**
 * Transposes a 2D array.
 * @param {Array} arr - The array to be transposed.
 * @returns {Array} - The transposed array.
 */
function transposeArray_(arr) {
  return arr[0].map((_, i) => arr.map(row => row[i]));
}

/**
 * Updates the Predicto sheet with the transposed array and headers.
 * @param {GoogleAppsScript.Spreadsheet.Sheet} sheet - The Predicto sheet.
 * @param {Array} transposedArray - The transposed array.
 * @param {Array} headers - The array of headers.
 */
function updatePredictoPlayerCategories_(sheet, transposedArray, headers) {
  //transposedArray.unshift(headers);

  const startColNum = colNameToNum_(predictoSheetObj.potmColName);
  const numRows = transposedArray.length;
  const range = sheet.getRange(2, startColNum, numRows, transposedArray[0].length);

  range.setValues(transposedArray);
}
