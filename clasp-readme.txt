README

Topic# 1
-------------------------------------------------
How to clone the script files on a PC Folder

1. Copy the Script ID (Not Sheet ID). Open Sheet, Go to Extension --> App Script. Go to Project Setting. Under IDs, Copy the Script ID
2. Under C:\Projects\T20PredictionsLib, Go to the respective folder (e.g. DevScript, TestScript, ProdScript)
3. Delete all files under it
4. Run the following command 

DEV
clasp clone 1DV97mbjhwXtJiL19Pw3l19Cc9ZtGZo2rGD_oUHyYB10tf51oakr-yD5u
TEST
clasp clone 1uw64lzi4N3rEPuuLVdOYiObxHOyE1C2ifZE-RIpdmtv7EZqYgA3d6RkW 
PROD AUCHEM
clasp clone 1g0xDw_7SUSisGcvUmguiPAODkReVZqTICiKHfL206TNEGSpOyBoWD4UT
PROD YEG
clasp clone 1FmVfnvygECnN64oGUleCjNkYNW7_kaVpq6o-Z-OwJU1JgBMnGnynFdDZ

Now all the script files are cloned on PC folder
-------------------------------------------------

Topic# 2
-------------------------------------------------
How to Copy Script Files to a NEW Sheet (e.g. making a new DEV or TEST sheet)

1. Create a brand new empty spreadsheet
2. Go to App Script
3. Copy the Script ID
4. Create a new folder (e.g. DevScriptsNEW)
5. Run the following command

clasp clone copied_script_id

6. Now copy the source .js files from an existing folder (e.g. DevScript)
7. Paste into new folder (DevScriptsNEW)
8. Run the following command

clasp push

9. Now go to the App Script file and refresh, all new copied files will show up
10. On spreadsheet Scripts, run promote all sheet and forms functions
----------------------------------------------------
