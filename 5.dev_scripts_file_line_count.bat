@echo off
setlocal enabledelayedexpansion

set CSV_FILE=dev_scripts_file_line_count.csv

echo Scanning files...

rem initialize csv file with headers
echo "File Name","Line Count" > %CSV_FILE%

set /a total_lines=0
set /a number_of_files=0

rem loop through js files and count lines
for %%f in (DevScripts\*.js) do (
    set /a line_count=0
    for /f %%l in ('type "%%f" ^| find /v /c ""') do set /a line_count=%%l
    set /a total_lines+=line_count
    set /a number_of_files+=1
    echo %%f: !line_count! lines
    echo "%%f",!line_count! >> %CSV_FILE%
)

echo Total files: %number_of_files%
echo Total lines: !total_lines!
echo "TOTAL",!total_lines! >> %CSV_FILE%

rem pause the console so the output can be read
pause
