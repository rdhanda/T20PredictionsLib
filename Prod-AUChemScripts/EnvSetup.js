//Setup Env Properties
BatBallBetsLib.EnvConfigs.ENV_DEFAULT_CONFIG_GROUP = BatBallBetsLib.EnvConfigConstants.ENV_GROUP_AUCHEM;
BatBallBetsLib.EnvConfigs.ENV_NAME = BatBallBetsLib.EnvConfigConstants.ENV_NAME_PROD;
BatBallBetsLib.EnvConfigs.ENV_ADDITIONAL_ADMIN_EMAIL_LIST = [];

BatBallBetsLib.EnvConfigs.ENV_CURRENT_LOG_LEVEL = BatBallBetsLib.EnvConfigConstants.ENV_LOG_LEVEL_INFO;
BatBallBetsLib.EnvConfigs.ENV_TEMP_HALT_TRIGGER_EXECUTIONS = false;
BatBallBetsLib.EnvConfigs.ENV_UPDATE_MATCH_RESULTS_MAX_PER_MINUTE = 2;

function testFunction(e) {
  // Run the function
  BatBallBetsLib.sendTestEmail();
}

/**
 * OnOpen
 */
function onOpen() {
  BatBallBetsLib.onOpen();
}
